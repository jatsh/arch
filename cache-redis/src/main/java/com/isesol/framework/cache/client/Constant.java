package com.isesol.framework.cache.client;

import com.isesol.framework.cache.client.util.*;

public class Constant {

	public static final String CACHE_PACKAGE_NAME = "com.isesol.framework.cache.client";

	public static final String FAIL_FILE_PATH = ServerInfoUtils.getUserHome() + "/.cache-client-failed";

	public static final String METHOD_CACHEABLE_ADVISOR_BEAN_NAME =
			CACHE_PACKAGE_NAME + ".processor.InternalCacheablePointcutAdvisor";

	public static final String CACHE_PACKAGE_PREFIX = "com.isesol.framework.cache.client.";

	public static final String[] PACKAGE_PREFIXES = { "com.isesol."};

	public static final String PACKAGE_PREFIXES_STRING = "com.isesol.";

	public static final String  CACHE_APP_CODE="X-isesol-Cache-App-Code";

	public static final String  CACHE_MESSAGE_COUNT="X-isesol-Cache-Messages-Count";

	public static final String  	DEFAULT_CHARSET="UTF-8";

	public static final String  DEFAULT_CONTENT_TYPE="application/x-isesol-cache-messages; charset=" +DEFAULT_CHARSET;

}
