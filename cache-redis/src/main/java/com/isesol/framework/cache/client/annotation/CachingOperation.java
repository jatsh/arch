package com.isesol.framework.cache.client.annotation;

import com.isesol.framework.cache.client.util.*;

import java.io.*;
import java.lang.annotation.Annotation;
import java.text.*;
import java.util.*;

public class CachingOperation implements Serializable {

	private CachingType type;

	private String keyId;

	private String key;

	private int expireSeconds;

	private String expireAt;

	private Date expireAtDate;

	private String condition;

	CachingOperation(EnableCacheable enable) {

		Assert.notNull(enable, "enable");
		Assert.notEmpty(enable.keyId(), "keyId");
		Assert.notEmpty(enable.key(), "key");
		Assert.notLesser(enable.expireSeconds(), 0, "expireSeconds");

		this.type = CachingType.ENABLE;
		this.keyId = Tools.trim(enable.keyId());
		this.key = Tools.trim(enable.key());
		this.expireSeconds = enable.expireSeconds();
		this.expireAt = Tools.trim(enable.expireAt());
		this.condition = Tools.trim(enable.condition());

		if (!Tools.isBlank(this.expireAt)) {

			try {

				this.expireAtDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.expireAt);

			} catch (ParseException e) {

				throw new IllegalArgumentException(
						"@EnableCacheable parameter expireAt value " + "[" + this.expireAt + "] is invalid" +
						", the value format should be 'yyyy-MM-dd HH:mm:ss'", e);
			}
		}
	}

	CachingOperation(ClearingCached clearing) {

		Assert.notNull(clearing, "clearing");
		Assert.notEmpty(clearing.keyId(), "keyId");
		Assert.notEmpty(clearing.key(), "key");

		this.type = CachingType.CLEARING;
		this.key = Tools.trim(clearing.key());
		this.keyId = Tools.trim(clearing.keyId());
		this.condition = Tools.trim(clearing.condition());
	}

	public static CachingOperation create(Annotation annotation) {

		for (CachingType type : CachingType.values()) {

			CachingOperation operation = type.create(annotation);

			if (operation != null) {

				return operation;
			}

		}

		return null;
	}

	public CachingType getType() {

		return type;
	}

	public String getKeyId() {

		return keyId;
	}

	public String getKey() {

		return key;
	}

	public int getExpireSeconds() {

		return expireSeconds;
	}

	public String getExpireAt() {

		return expireAt;
	}

	public Date getExpireAtDate() {

		if (expireAtDate == null) {

			return null;
		}

		return new Date(expireAtDate.getTime());
	}

	public String getCondition() {

		return condition;
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("type", type);
		builder.append("keyId", keyId);
		builder.append("key", key);
		builder.append("expireSeconds", expireSeconds);
		builder.append("expireAt", expireAt);
		builder.append("condition", condition);
		return builder.toString();
	}
}
