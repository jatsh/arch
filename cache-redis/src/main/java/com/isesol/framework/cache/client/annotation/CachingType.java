package com.isesol.framework.cache.client.annotation;

import java.lang.annotation.*;

/**
 * 方法级缓存标注类型
 */
public enum CachingType {

	/**
	 * 启用缓存
	 */
	ENABLE(EnableCacheable.class) {
		@Override
		public CachingOperation create(Annotation annotation) {

			if (annotation instanceof EnableCacheable) {

				return new CachingOperation((EnableCacheable) annotation);

			}

			return null;

		}
	},

	/**
	 * 清除缓存
	 */
	CLEARING(ClearingCached.class) {
		@Override
		public CachingOperation create(Annotation annotation) {

			if (annotation instanceof ClearingCached) {

				return new CachingOperation((ClearingCached) annotation);

			}

			return null;

		}
	};

	private Class<? extends Annotation> type;

	private CachingType(Class<? extends Annotation> type) {

		this.type = type;
	}

	public static int count() {

		return values().length;
	}

	public Class<? extends Annotation> getType() {

		return type;
	}

	public abstract CachingOperation create(Annotation annotation);
}
