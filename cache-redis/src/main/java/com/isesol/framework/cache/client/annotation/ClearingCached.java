package com.isesol.framework.cache.client.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface ClearingCached {

	String keyId();

	String key();

	String condition() default "";
}
