package com.isesol.framework.cache.client.annotation;

import com.isesol.framework.cache.client.executor.*;

import java.lang.annotation.*;

/**
 * <p>
 * 缓存方法调用的返回结果数据。标注于方法体上，表达该方法调用的返回结果需要进行缓存。
 * </p>
 * <p>
 * 实际运行时，根据标注中 <code>{@link #condition()}</code> 表达式，以及方法运行时调用的参数计算
 * <code>{@link #condition()}</code> 表达式的计算结果，根据该计算结果确定是否需要进行缓存。
 * </p>
 * <p>
 * 实际缓存数据的 key 值由 API 自动生成，结构为：
 * &lt;appcode&gt;:&lt;keyId&gt;:&lt;eval_key&gt;<br />
 * 其中：<br />
 * appcode - 业务应用代码<br />
 * keyId - 方法标注上的 <code>{@link #keyId()}</code> 值<br />
 * eval_key - 方法标注上的 <code>{@link #key()}</code> 表达式，根据方法运行时调用参数计算出来的结果
 * </p>
 * <p>
 * 注意：方法结果缓存时，只针对于有返回结果的数据。如果，方法的返回值为 <code>void</code> 时，忽略该缓存标注的数据。
 * </p>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface EnableCacheable {

	/**
	 * <p>
	 * 缓存数据 key 中 keyid 值，该值必须在业务应用中保证唯一
	 * </p>
	 *
	 * @return 缓存数据 key 中 keyid 值
	 */
	String keyId();

	/**
	 * <p>
	 * 用于区分方法中不同方法调用参数的表达式，其在运行时根据方法调用参数进行计算，计算结果作为缓存数据 key 的 eval_key 值。
	 * </p>
	 * <p>
	 * 条件表达式目前仅支持 SpEL（Spring expression language）的表达式语法，详见 <a href=
	 * "http://static.springsource.org/spring/docs/4.1.4.RELEASE/spring-framework-reference/html/expressions.html"
	 * title
	 * ="Spring Framework Reference -- Spring Expression Language (SpEL)">SpEL
	 * 参考文档</a>。
	 * </p>
	 * <p>
	 * 表达式计算结果应为该方法处理缓存的唯一标识。如方法第二个参数 <code>User</code> 对象的
	 * <code>username</code> 作为该方法的唯一标识时，<code>key</code> 值置为“p1.username”（其中 p1
	 * 表示第 2 个参数， <code>p1.username</code> 表示第二个参数对象的 <code>username</code> 属性）。
	 * </p>
	 * <p>
	 * <b>注意：</b>若根据方法参数列表对象中的属性值进行运算时，方法列表参数中的对象数据可能为 <code>null</code> 值，因此，
	 * SpEL 表达式需要使用 <a href=
	 * "http://static.springsource.org/spring/docs/4.1.4.RELEASE/spring-framework-reference/html/expressions
	 * .html#expressions-operator-safe-navigation"
	 * title="safe navigation operator">safe navigation operator</a>， 以免产生
	 * <code>{@link NullPointerException}</code>。
	 * </p>
	 *
	 * @return 缓存方法表达式
	 *
	 * @see #condition()
	 */
	String key();

	/**
	 * <p>
	 * 方法结果缓存的相对生存时间（单位：秒），表式方法调用时缓存结果的生存时间。默认值为 1800 秒。
	 * </p>
	 *
	 * @return 方法结果缓存的生存周期
	 */
	int expireSeconds() default TransactionCacheExecutor.DEFAULT_EXPIRATION_SECONDS;

	/**
	 * <p>
	 * 方法结果缓存的绝对时间（格式：<code>yyyy-MM-dd HH:mm:ss</code>），表式方法调用时缓存结果在指定时间之前有效。
	 * </p>
	 * <p>
	 * <p>
	 * <b>注意：</b>
	 * <ul>
	 * <li>当该数据有配置值时，忽略 <code>{@link #expireSeconds()}</code> 中所配置的缓存相对生存时间</li>
	 * <li>若设置的日期时间小于存入缓存服务器的时间时，方法结果不再使用缓存</li>
	 * </ul>
	 * </p>
	 *
	 * @return 方法结果缓存的绝对时间
	 */
	String expireAt() default "";

	/**
	 * <p>
	 * 根据方法调用时的方法参数，确定方法调用结果是否需要进行缓存的条件表达式。
	 * </p>
	 * <p>
	 * 该条件表达式的计算结果为一个布尔类型，值为 <code>true</code> 时，表示方法调用结果需要进行缓存，为
	 * <code>false</code> 时，表示方法调用结果不需要进行缓存。
	 * </p>
	 * <p>
	 * 条件表达式目前仅支持 SpEL（Spring expression language）的表达式语法，详见 <a href=
	 * "http://static.springsource.org/spring/docs/4.1.4.RELEASE/spring-framework-reference/html/expressions.html"
	 * title
	 * ="Spring Framework Reference -- Spring Expression Language (SpEL)">SpEL
	 * 参考文档</a>。
	 * </p>
	 * <p>
	 * <b>注意：</b>
	 * <ul>
	 * <li>如果不设置该值，或者值为 <code>null</code>，或者全为空白字符时，则该条件计算结果作为 <code>true</code>
	 * 值，即进行缓存处理</li>
	 * <li>如果方法调用参数列表为 <code>null</code> 值，则该条件计算结果作为 <code>false</code>
	 * 值，即不进行缓存处理</li>
	 * <li>如果条件表达式的计算结果不是一个布尔类型的数据时，则该条件计算结果作为 <code>false</code> 值，即不进行缓存处理</li>
	 * </ul>
	 * </p>
	 * <p>
	 * <b>注意：</b>若根据方法参数列表对象中的属性值进行运算时，方法列表参数中的对象数据可能为 <code>null</code> 值，因此，
	 * SpEL 表达式需要使用 <a href=
	 * "http://static.springsource.org/spring/docs/4.1.4.RELEASE/spring-framework-reference/html/expressions
	 * .html#expressions-operator-safe-navigation"
	 * title="safe navigation operator">safe navigation operator</a>， 以免产生
	 * <code>{@link NullPointerException}</code>。
	 * </p>
	 *
	 * @return 方法调用是否需要进行缓存处理的条件表达式
	 *
	 * @see #key()
	 */
	String condition() default "";
}
