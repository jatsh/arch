package com.isesol.framework.cache.client.app;

import com.isesol.framework.cache.client.logging.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.commons.lang3.builder.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;

/**
 * 处理分隔标识，以及应用代码的业务应用基本数据的抽象
 */
public abstract class AppCodeApplication implements CacheClientApplication, InitializingBean {

    private static Logger LOGGER = LogManager.getLogger(AppCodeApplication.class.getName());

    /**
     * 默认的数据段分隔符
     */
    private static final String DEFAULT_SEPARATOR = ":";

    /**
     * 缓存客户端业务应用的应用代码（由配置所指定）
     */
    private final String appCode;

    /**
     * 数据段分隔符
     */
    private String separator = DEFAULT_SEPARATOR;

    /**
     * 缓存客户端业务应用的节点代码
     */
    private String nodeCode;

    public AppCodeApplication(String appCode) {

        this(appCode, DEFAULT_SEPARATOR);
    }

    public AppCodeApplication(String appCode, String separator) {

        this.appCode = appCode;
        this.separator = separator;
    }

    @Override
    public void afterPropertiesSet() {

        Assert.notEmpty(appCode, "appCode");
        Assert.notEmpty(separator, "separator");

        FormattingTuple.injectAppCode(appCode);

        this.nodeCode = createNodeCode();

        if (Tools.isBlank(this.nodeCode)) {

            throw new BeanCreationException(
                    "createNodeCode() method in subclass of " + AppCodeApplication.class.getSimpleName() +
                            ", the method return result " + "should not be null or empty");
        }

        LOGGER.info("Application: {}", this);

    }

    @Override
    public final String getAppCode() {

        return appCode;
    }

    @Override
    public final String getNodeCode() {

        return nodeCode;
    }

    /**
     * 获取数据段分隔符
     *
     * @return 数据段分隔符
     */
    public String getSeparator() {

        return separator;
    }

    /**
     * 创建缓存客户端业务应用节点代码
     *
     * @return 新创建的缓存客户端业务应用节点代码
     */
    protected abstract String createNodeCode();

    @Override
    public String toString() {

        return new ToStringBuilder(this)
                .append("appCode", appCode)
                .append("separator", separator)
                .append("nodeCode", nodeCode)
                .toString();
    }
}
