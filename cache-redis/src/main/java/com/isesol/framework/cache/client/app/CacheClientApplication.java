package com.isesol.framework.cache.client.app;

/**
 * 用于获取缓存客户端业务应用的基本信息。
 */
public interface CacheClientApplication {

	/**
	 * 获取缓存客户端业务应用的应用代码（App Code）。应用代码是唯一标识一个工程应用的代码，
	 * 默认情况下还用于区分不同的工程，同时也作为数据缓存键的前缀部分。
	 *
	 * @return 缓存客户端业务应用代码
	 */
	String getAppCode();

	/**
	 * 获取缓存客户端业务应用的节点代码（Node Code）。
	 * <p>节点代码是唯一标识集群环境中业务应用中的一个集群节点。</p>
	 * 节点代码主要用于缓存客户端产生的运行时信息或者异常时，标识业务应用节点，以确定消息的产生源。
	 *
	 * @return 标识缓存客户端业务应用一个节点的节点代码
	 */
	String getNodeCode();
}
