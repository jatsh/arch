package com.isesol.framework.cache.client.app;

public interface CacheClientApplicationAware {

	void setCacheClientApplication(CacheClientApplication application);
}
