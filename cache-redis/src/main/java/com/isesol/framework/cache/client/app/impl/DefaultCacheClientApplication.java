package com.isesol.framework.cache.client.app.impl;

import com.isesol.framework.cache.client.app.*;
import com.isesol.framework.cache.client.util.*;

/**
 * 默认缓存客户端业务应用基本信息实现。
 * <p>
 * 节点代码由以下几部分组成：应用代码、业务应用部署服务器主机名、业务应用部署目录。
 * </p>
 * 每部分使用
 * {@link #getSeparator()} 的分隔符拼接所组成。
 */
public class DefaultCacheClientApplication extends AppCodeApplication {

	public DefaultCacheClientApplication(String appCode) {

		super(appCode);
	}

	@Override
	protected String createNodeCode() {

		String code = CacheClientApplication.class.getResource("").getPath().replace(":", "");

		int length = CacheClientApplication.class.getPackage().getName().length() + 1;

		code = code.substring(0, code.length() - length);

		return getAppCode() + getSeparator() + ServerInfoUtils.getHostname() + getSeparator() + code;
	}
}
