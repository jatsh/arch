package com.isesol.framework.cache.client.config;

import com.isesol.framework.cache.client.*;
import com.isesol.framework.cache.client.listener.application.*;
import com.isesol.framework.cache.client.processor.*;
import org.apache.logging.log4j.*;
import org.springframework.aop.config.*;
import org.springframework.beans.factory.config.*;
import org.springframework.beans.factory.parsing.*;
import org.springframework.beans.factory.support.*;
import org.springframework.beans.factory.xml.*;
import org.w3c.dom.*;

public class AnnotationDrivenParser implements BeanDefinitionParser {


	static Logger log = LogManager.getLogger(AnnotationDrivenParser.class.getName());

	@Override
	public BeanDefinition parse(Element element, ParserContext parserContext) {

		ExtendedParserContext context = new ExtendedParserContext(element, parserContext);

		CacheNamespaceHandler.setInstance(AnnotationDrivenSuite.CACHE_EXECUTOR.getAttrValue(context));

		configureCacheableProxyCreator(context);

		return null;
	}

	private void configureCacheableProxyCreator(ExtendedParserContext context) {

		AopNamespaceUtils.registerAutoProxyCreatorIfNecessary(context.getParserContext(), context.getRoot());

		if (!context.containsBeanDefinition(Constant.METHOD_CACHEABLE_ADVISOR_BEAN_NAME)) {

			log.info("register cache advisor bean, bean name = [{}]", Constant.METHOD_CACHEABLE_ADVISOR_BEAN_NAME);

			// register ApplicationListener that function notify the listener
			// has method that annotated cacheable annotation
			// and check whethere enable cacheable keyid duplication
			RootBeanDefinition operationListenerDef = context.newInfrastructureBeanDefinition(
					CachingOperationListener.class);
			RuntimeBeanReference operationListenerRef = context.registerDefinition(
					operationListenerDef, ExtendedParserContext.TYPE_ANNOTATION);

			// register ApplicationListener that function check whethere
			// clearing cached reference keyid is valid
			RootBeanDefinition registerListenerDef = context.newInfrastructureBeanDefinition(
					AfterCachingRegisterListener.class);
			context.addProperty(registerListenerDef, "validateClearingKeyId", operationListenerRef);
			String registerListenerName = CacheNamespaceHandler.generateBeanName(AfterCachingRegisterListener.class);
			context.registerNamed(registerListenerDef, registerListenerName);

			// register CacheablePointcutAdvisor
			RootBeanDefinition advisorDef = context.newInfrastructureBeanDefinition(CacheablePointcutAdvisor.class);
			context.addProperty(advisorDef, "pointcut", AnnotationDrivenSuite.CACHE_POINTCUT.getReference(context));
			context.addProperty(advisorDef, "advice", AnnotationDrivenSuite.CACHE_ADVICE.getReference(context));
			context.registerNamed(advisorDef, Constant.METHOD_CACHEABLE_ADVISOR_BEAN_NAME);

			CompositeComponentDefinition compositeDef = new CompositeComponentDefinition(
					context.getRootTagName(), context.getElementSource());
			compositeDef.addNestedComponent(
					new BeanComponentDefinition(
							operationListenerDef, operationListenerRef.getBeanName()));
			compositeDef.addNestedComponent(new BeanComponentDefinition(registerListenerDef, registerListenerName));
			compositeDef.addNestedComponent(
					new BeanComponentDefinition(
							advisorDef, Constant.METHOD_CACHEABLE_ADVISOR_BEAN_NAME));
			context.getParserContext().registerComponent(compositeDef);
		}
	}
}
