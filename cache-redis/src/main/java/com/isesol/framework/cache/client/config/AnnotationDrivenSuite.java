package com.isesol.framework.cache.client.config;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.expression.*;
import com.isesol.framework.cache.client.expression.spel.*;
import com.isesol.framework.cache.client.interceptor.*;
import com.isesol.framework.cache.client.processor.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.config.*;
import org.springframework.beans.factory.support.*;
import org.springframework.util.*;

enum AnnotationDrivenSuite {

	WRAP_CACHE_EXECUTOR(null, TransactionCacheExecutor.class, MethodAnnotationCacheExecutor.class) {
		@Override
		protected void addProperties(ExtendedParserContext context, BeanDefinition definition) {

			addPropertyReference(context, definition, "cacheExecutor", CACHE_EXECUTOR);
		}
	},

	CACHE_EXECUTOR("cache-executor-ref", TransactionCacheExecutor.class, null) {
		@Override
		protected String wrapReferenceName(String ref) {

			return ExtendedParserContext.getInternalCacheExecutorBeanName(ref);
		}

		@Override
		protected String getBeanName(ExtendedParserContext context) {

			return context.getInternalCacheExecutorBeanName();
		}

		@Override
		public String getAttrValue(ExtendedParserContext context) {

			String executorRef = super.getAttrValue(context);
			if (Tools.isBlank(executorRef)) {
				executorRef = ExtendedParserContext.BEAN_NAME_CACHE_EXECUTOR;
			}
			return executorRef;
		}
	},

	EVAL_FACTORY("eval-factory-ref", ExpressionEvalFactory.class, SpelExpressionEvalFactory.class),

	CACHE_REGISTRY("registry-ref", CacheMetaRegistry.class, DefaultCacheMetaRegistry.class) {
		@Override
		protected void addProperties(ExtendedParserContext context, BeanDefinition definition) {

			addPropertyReference(context, definition, "evalFactory", EVAL_FACTORY);
		}
	},

	CACHE_ADVICE("advice-ref", CacheableAdvice.class, CacheableAdvice.class) {
		@Override
		protected void addProperties(ExtendedParserContext context, BeanDefinition definition) {

			addPropertyReference(context, definition, "registry", CACHE_REGISTRY);
			addPropertyReference(context, definition, "executor", WRAP_CACHE_EXECUTOR);
		}
	},

	CACHE_POINTCUT("pointcut-ref", CacheablePointcut.class, CacheablePointcut.class) {
		@Override
		protected void addProperties(ExtendedParserContext context, BeanDefinition definition) {

			addPropertyReference(context, definition, "registry", CACHE_REGISTRY);
		}
	};

	static Logger log = LogManager.getLogger(AnnotationDrivenSuite.class.getName());


	private String attribute;

	private Class<?> defaultClass;

	private Class<?> parentClass;

	private String beanName;

	private AnnotationDrivenSuite(String attribute, Class<?> parentClass, Class<?> defaultClass) {

		this.attribute = attribute;
		this.defaultClass = defaultClass;
		this.parentClass = parentClass;
		this.beanName = CacheNamespaceHandler.generateBeanName(
				(
						defaultClass == null ? parentClass : defaultClass), ExtendedParserContext.TYPE_ANNOTATION);
	}

	public Object getReference(ExtendedParserContext context) {

		String value = (
				attribute == null ? null : context.getRoot().getAttribute(attribute));

		log.debug("[getReference] suite = [{}], attribute = [{}], value = [{}]", name(), attribute, value);

		if (StringUtils.hasText(value)) {

			if (parentClass == String.class) {

				log.debug("[getReference] value type is String, return the value");

				return value.trim();
			}

			log.debug("[getReference] value type is bean reference, return RuntimeBeanReference. [{}]", this);

			return new RuntimeBeanReference(wrapReferenceName(value.trim()));
		}

		return createReference(context);
	}

	public String getAttrValue(ExtendedParserContext context) {

		if (attribute == null) {
			return null;
		}
		return context.getRoot().getAttribute(attribute);
	}

	private RuntimeBeanReference createReference(ExtendedParserContext context) {

		// attribute value not found and attribute type is String
		if (parentClass == String.class) {
			return null;
		}

		// attribute value not found, default class not definied
		if (defaultClass == null) {
			return null;
		}

		// register default bean definition
		if (!containsBeanDefinition(context)) {

			RootBeanDefinition definition = context.newBeanDefinition(defaultClass);
			addProperties(context, definition);

			return context.registerNamedDefinition(definition, getBeanName(context));
		}

		log.debug(
				"[getReference] suite = [{}] using default to reference, bean = [{}], type = [{}]", name(),
				getBeanName(
						context), defaultClass);

		return new RuntimeBeanReference(getBeanName(context));
	}

	protected String getBeanName(ExtendedParserContext context) {

		return beanName;
	}

	protected String wrapReferenceName(String ref) {

		return ref;
	}

	protected void addProperties(ExtendedParserContext context, BeanDefinition definition) {

	}

	protected void addPropertyReference(
			ExtendedParserContext context, BeanDefinition definition, String property,
			AnnotationDrivenSuite reference) {

		definition.getPropertyValues().add(property, reference.getReference(context));
	}

	private boolean containsBeanDefinition(ExtendedParserContext context) {

		return context.containsBeanDefinition(getBeanName(context));
	}
}
