package com.isesol.framework.cache.client.config;

import com.isesol.framework.cache.client.connector.locator.*;
import com.isesol.framework.cache.client.connector.notifier.*;
import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.serializer.*;
import com.isesol.framework.cache.client.http.jdk.*;
import com.isesol.framework.cache.client.log.impl.*;
import com.isesol.framework.cache.client.message.collector.*;
import com.isesol.framework.cache.client.message.sender.*;
import com.isesol.framework.cache.client.message.sender.strategy.*;
import com.isesol.framework.cache.client.thread.impl.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.aop.framework.*;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.config.*;
import org.springframework.beans.factory.support.*;
import org.springframework.beans.factory.xml.*;
import org.springframework.util.xml.*;
import org.w3c.dom.*;

import java.util.*;

public class CacheExecutorParser implements BeanDefinitionParser {

	private static final String APP_CODE = "appcode";

	private static final String E_COLLECTOR = "message-collector";

	private static final String E_COLLECTOR_SEND_TYPE = "send-type";

	private static final String E_COLLECTOR_SEND_TYPE_CONSOLE = "send-to-console";

	private static final String E_COLLECTOR_SEND_TYPE_LOGGING = "send-to-log";

	private static final String E_COLLECTOR_SEND_TYPE_HTTP = "send-to-http";

	private static final String E_COLLECTOR_SEND_STRATEGY = "send-strategy";

	private static final String E_COLLECTOR_SEND_STRATEGY_THRESHOLD = "strategy-threshold";

	private static final String E_COLLECTOR_SEND_STRATEGY_SCHEDULE = "strategy-schedule";

	private static final String ATTR_REFERENCE = "ref";

	private static final String PROP_QUEUE = "queue";

	static Logger log = LogManager.getLogger(CacheExecutorParser.class.getName());

	private boolean isRemote;

	@Override
	public BeanDefinition parse(Element element, ParserContext parserContext) {

		ExtendedParserContext context = new ExtendedParserContext(element, parserContext);
		context.logConfigurations();

		if (context.hasCacheExecutorBeanName()) {
			throw new BeanCreationException(
					"cacheExecutor bean id \"" + context.getCacheExecutorBeanName() + "\" duplication!");
		}

		if (!element.hasAttribute(APP_CODE)) {
			throw new BeanCreationException("attribute \"appcode\" " + " must be set one attribute value");
		}

		CacheNamespaceHandler.setInstance(context.getCacheExecutorBeanName());

		publishCacheExecutor(context);

		return null;
	}

	private void publishCacheExecutor(ExtendedParserContext context) {

		RuntimeBeanReference cacheExecutor = getCacheExecutor(context);

		RootBeanDefinition definition = context.newBeanDefinition(ClientApiCacheExecutor.class);
		context.addProperty(definition, "cacheExecutor", cacheExecutor);
		definition.setLazyInit(false);
		context.registerNamed(definition, context.getCacheExecutorBeanName());
	}

	private RuntimeBeanReference getCacheExecutor(ExtendedParserContext context) {

		RuntimeBeanReference executor = getInternalCacheExecutor(context);
		RuntimeBeanReference messageCollector = getMessageCollector(context);

		RootBeanDefinition adviceDef = context.newBeanDefinition(CacheExecutorAdvice.class);
		context.addProperty(adviceDef, "messageCollector", messageCollector);
		RuntimeBeanReference advice = context.registerDefinition(adviceDef, ExtendedParserContext.TYPE_EXECUTOR);

		RootBeanDefinition proxy = context.newBeanDefinition(ProxyFactoryBean.class);
		context.addProperty(proxy, "targetName", executor.getBeanName());
		context.addProperty(proxy, "interceptorNames", advice.getBeanName());
		context.addProxyConfigProperties(proxy);

		log.info(
				"Internal cache executor, message collector has configured" +
				", register a ProxyFactoryBean object for create cache executor proxy object" +
				", the ProxyFactoryBean object with targetName [{}], interceptorNames [{}]", executor.getBeanName(),
				advice.getBeanName());

		return context.registerNamedDefinition(proxy, context.getInternalCacheExecutorBeanName());
	}

	private RuntimeBeanReference getInternalCacheExecutor(ExtendedParserContext context) {

		Element server = context.getRootChild("server");

		if (server != null && server.hasAttribute(ATTR_REFERENCE)) {

			return new RuntimeBeanReference(server.getAttribute(ATTR_REFERENCE));
		}

		Properties properties = null;

		if (server != null) {

			Element props = DomUtils.getChildElementByTagName(server, "props");

			if (props != null) {

				properties = context.parsePropsElement(props);
			}
		}

		return getRedisCacheExecutor(context, properties);
	}

	private RuntimeBeanReference getRedisCacheExecutor(ExtendedParserContext context, Properties properties) {

		RuntimeBeanReference template = getRedisTemplate(context, properties);
		RuntimeBeanReference cacheSerializable = getCacheSerializable(context);

		RootBeanDefinition definition = context.newBeanDefinition(RedisCacheExecutor.class);
		context.addProperty(definition, "template", template);
		context.addProperty(definition, "serializable", cacheSerializable);
		context.addIntProperty(definition, "defaultExpires", context.getRoot(), "default-expires");

		return context.registerDefinition(definition, ExtendedParserContext.TYPE_EXECUTOR);
	}

	private RuntimeBeanReference getCacheSerializable(ExtendedParserContext context) {

		RuntimeBeanReference keySerializer = getKeySerializer(context);
		RuntimeBeanReference valueSerializer = getValueSerializer(context);
		RuntimeBeanReference alertCacheLog = getAlertCacheLog(context);

		RootBeanDefinition definition = context.newBeanDefinition(DefaultCacheSerializable.class);
		context.addProperty(definition, "keySerializer", keySerializer);
		context.addProperty(definition, "valueSerializer", valueSerializer);
		context.addProperty(definition, "alertCacheLog", alertCacheLog);
		context.addIntProperty(definition, "cacheAlertBytes", context.getRoot(), "alert-bytes-threshold");

		return context.registerDefinition(definition, ExtendedParserContext.TYPE_EXECUTOR);
	}

	private RuntimeBeanReference getAlertCacheLog(ExtendedParserContext context) {

		RootBeanDefinition alertLogDef = context.newBeanDefinition(LoggingCacheLog.class);
		return context.registerDefinition(alertLogDef, ExtendedParserContext.TYPE_EXECUTOR);
	}

	private RuntimeBeanReference getRedisTemplate(ExtendedParserContext context, Properties properties) {

		RuntimeBeanReference locator = getCacheServerLocator(context);

		RootBeanDefinition definition = context.newBeanDefinition(RedisTemplateFactoryBean.class);
		context.addProperty(definition, "locator", locator);
		context.addProperty(definition, "properties", properties);

		return context.registerDefinition(definition, ExtendedParserContext.TYPE_EXECUTOR);
	}

	private RuntimeBeanReference getKeySerializer(ExtendedParserContext context) {

		Element keySerializer = context.getRootChild("key-serializer");
		if (keySerializer != null && keySerializer.hasAttribute(ATTR_REFERENCE)) {
			return new RuntimeBeanReference(keySerializer.getAttribute(ATTR_REFERENCE));
		}
		RootBeanDefinition definition = context.newBeanDefinition(StringSerializer.class);
		return context.registerDefinition(definition, ExtendedParserContext.TYPE_EXECUTOR);
	}

	private RuntimeBeanReference getValueSerializer(ExtendedParserContext context) {

		Element valueSerializer = context.getRootChild("value-serializer");
		if (valueSerializer != null && valueSerializer.hasAttribute(ATTR_REFERENCE)) {
			return new RuntimeBeanReference(valueSerializer.getAttribute(ATTR_REFERENCE));
		}
		RootBeanDefinition definition = context.newBeanDefinition(FastjsonSerializer.class);
		return context.registerDefinition(definition, ExtendedParserContext.TYPE_EXECUTOR);
	}

	private RuntimeBeanReference getCacheServerLocator(ExtendedParserContext context) {

		RuntimeBeanReference runtimeBeanReference = null;
		Element serverAddress = context.getRootChild("server-address");

		if (serverAddress == null) {
			throw new BeanCreationException("\"cache-executor\" tag <server-address /> is required");
		}

		if (serverAddress.hasAttribute(ATTR_REFERENCE)) {
			return context.registerCacheServerLocator(serverAddress.getAttribute(ATTR_REFERENCE));
		}

		List<Element> list = DomUtils.getChildElementsByTagName(serverAddress, new String[]{"http", "local"});

		if (Tools.isBlank(list)) {
			throw new BeanCreationException(
					"\"cache-executor\" tag \"server-address\" must contains a \"http\" or a \"local\" child element");
		}

		Element addressElement = list.get(0);

		if ("http".equals(addressElement.getLocalName())) {

			isRemote = true;

			log.debug("Cache server address from remote server");

			RuntimeBeanReference httpRequestor = getHttpRequestor(context, addressElement, ExtendedParserContext.TYPE_LOCATOR);
			RootBeanDefinition definition = context.newBeanDefinition(HttpCacheServerLocator.class);
			context.addProperty(definition, "httpRequestor", httpRequestor);
			runtimeBeanReference = context.registerCacheServerLocator(definition);
		} else if ("local".equals(addressElement.getLocalName())) {

			RootBeanDefinition definition = context.newBeanDefinition(LocalCacheServerLocator.class);
			context.addStringProperty(definition, "address", addressElement, "address");
			runtimeBeanReference = context.registerCacheServerLocator(definition);
		}
		return runtimeBeanReference;

	}

	private RuntimeBeanReference getMessageCollector(ExtendedParserContext context) {

		Element collector = context.getRootChild(E_COLLECTOR);

		if (collector == null) {

			return getCollector(context, null);
		}

		if ("close".equals(collector.getAttribute("mode"))) {
			RootBeanDefinition empty = context.newBeanDefinition(EmptyMessageCollector.class);
			return context.registerDefinition(empty, ExtendedParserContext.TYPE_COLLECTOR);
		}

		if (collector.hasAttribute(ATTR_REFERENCE)) {
			return new RuntimeBeanReference(collector.getAttribute(ATTR_REFERENCE));
		}

		// Default message collector is 'async'
		return getCollector(context, collector);
	}

	private RuntimeBeanReference getCollector(ExtendedParserContext context, Element collector) {

		// MessageCollector asynchronous thread pool bean definition
		RuntimeBeanReference threadExecutor = getCollectorThreadExecutor(context, collector);

		// MessageCollector message queue
		RuntimeBeanReference queue = context.getMessageQueue();

		// MessageCollector message send stategy
		RuntimeBeanReference sendStrategy = getCollectorSendStrategy(context, collector);

		RootBeanDefinition definition = context.newBeanDefinition(AsyncMessageCollector.class);
		context.addProperty(definition, "threadExecutor", threadExecutor);
		context.addProperty(definition, PROP_QUEUE, queue);
		context.addProperty(definition, "sendStrategy", sendStrategy);
		return context.registerDefinition(definition, ExtendedParserContext.TYPE_COLLECTOR);
	}

	private RuntimeBeanReference getCollectorThreadExecutor(ExtendedParserContext context, Element collector) {

		int threads = AsyncMessageCollector.DEFAULT_ASYNC_THREAD_NUMBER;

		if (collector != null) {

			threads = CacheClientUtils.parseInt(
					collector.getAttribute("collector-threads-count"), AsyncMessageCollector.DEFAULT_ASYNC_THREAD_NUMBER);
		}

		RootBeanDefinition definition = context.newBeanDefinition(PooledCacheClientThreadExecutor.class);
		context.addConstructorArgument(definition, 0, AsyncMessageCollector.ASYNC_COLLECTOR_THREAD_NAME);
		context.addConstructorArgument(definition, 1, threads);

		return context.registerDefinition(definition, ExtendedParserContext.TYPE_COLLECTOR);
	}

	private RuntimeBeanReference getCollectorSendStrategy(ExtendedParserContext context, Element collector) {

		RuntimeBeanReference sendType = getCollectorSendType(context, collector);

		if (collector == null) {

			return getDefaultCollectorSendStrategy(context, sendType);
		}

		Element sendStrategy = DomUtils.getChildElementByTagName(collector, E_COLLECTOR_SEND_STRATEGY);

		if (sendStrategy == null) {
			return getDefaultCollectorSendStrategy(context, sendType);
		}

		if (sendStrategy.hasAttribute(ATTR_REFERENCE)) {
			return new RuntimeBeanReference(sendStrategy.getAttribute(ATTR_REFERENCE));
		}

		List<Element> sendStategies = DomUtils.getChildElementsByTagName(
				sendStrategy, new String[]{E_COLLECTOR_SEND_STRATEGY_SCHEDULE, E_COLLECTOR_SEND_STRATEGY_THRESHOLD});

		log.trace("SendStrategies: {}", sendStategies);

		if (Tools.isBlank(sendStategies)) {
			return getDefaultCollectorSendStrategy(context, sendType);
		}

		Element element = sendStategies.get(0);

		if (E_COLLECTOR_SEND_STRATEGY_SCHEDULE.equals(element.getLocalName())) {
			return getCollectorSendStrategySchedule(context, element, sendType);
		}

		if (E_COLLECTOR_SEND_STRATEGY_THRESHOLD.equals(element.getLocalName())) {
			return getCollectorSendStrategyThreshold(context, element, sendType);
		}

		return getDefaultCollectorSendStrategy(context, sendType);
	}

	private RuntimeBeanReference getDefaultCollectorSendStrategy(
			ExtendedParserContext context, RuntimeBeanReference sendType) {

		RootBeanDefinition definition = context.newBeanDefinition(ThresholdStrategySender.class);
		context.addProperty(definition, "sender", sendType);
		context.addProperty(definition, PROP_QUEUE, context.getMessageQueue());

		return context.registerDefinition(definition, ExtendedParserContext.TYPE_COLLECTOR);
	}

	private RuntimeBeanReference getCollectorSendStrategySchedule(
			ExtendedParserContext context, Element schedule, RuntimeBeanReference sendType) {

		RootBeanDefinition definition = context.newBeanDefinition(ScheduleStrategySender.class);

		context.addIntProperty(definition, "max", schedule, "send-max");
		context.addIntProperty(definition, "periods", schedule, "periods");
		context.addIntProperty(definition, "sendThreadsCount", schedule, "send-threads");

		context.addProperty(definition, "sender", sendType);
		context.addProperty(definition, PROP_QUEUE, context.getMessageQueue());

		return context.registerDefinition(definition, ExtendedParserContext.TYPE_COLLECTOR);
	}

	private RuntimeBeanReference getCollectorSendStrategyThreshold(
			ExtendedParserContext context, Element schedule, RuntimeBeanReference sendType) {

		RootBeanDefinition definition = context.newBeanDefinition(ThresholdStrategySender.class);

		if (log.isTraceEnabled()) {
			log.trace(
					"[ThresholdStrategySender] threshold = {}, sendThreadsCount = {}", schedule.getAttribute(
							"threshold"), schedule.getAttribute("send-threads"));
		}

		context.addIntProperty(definition, "threshold", schedule, "threshold");
		context.addIntProperty(definition, "sendThreadsCount", schedule, "send-threads");

		context.addProperty(definition, "sender", sendType);
		context.addProperty(definition, PROP_QUEUE, context.getMessageQueue());

		return context.registerDefinition(definition, ExtendedParserContext.TYPE_COLLECTOR);
	}

	private RuntimeBeanReference getCollectorSendType(ExtendedParserContext context, Element collector) {

		log.debug("Get collector send type, isRemote = {}", isRemote);

		if (collector == null) {
			return getDefaultCollectorSendType(context);
		}

		Element sendType = DomUtils.getChildElementByTagName(collector, E_COLLECTOR_SEND_TYPE);

		if (sendType == null) {
			return getDefaultCollectorSendType(context);
		}

		if (sendType.hasAttribute(ATTR_REFERENCE)) {
			return new RuntimeBeanReference(sendType.getAttribute(ATTR_REFERENCE));
		}

		List<Element> sendTypes = DomUtils.getChildElementsByTagName(
				sendType,
				new String[]{E_COLLECTOR_SEND_TYPE_CONSOLE, E_COLLECTOR_SEND_TYPE_LOGGING,
						E_COLLECTOR_SEND_TYPE_HTTP});

		if (Tools.isBlank(sendTypes)) {
			return getDefaultCollectorSendType(context);
		}

		Element element = sendTypes.get(0);

		if (E_COLLECTOR_SEND_TYPE_CONSOLE.equals(element.getLocalName())) {
			return getCollectorSendTypeConsole(context, element);
		}

		if (E_COLLECTOR_SEND_TYPE_LOGGING.equals(element.getLocalName())) {
			return getCollectorSendTypeLogging(context, element);
		}

		if (E_COLLECTOR_SEND_TYPE_HTTP.equals(element.getLocalName())) {
			return getCollectorSendTypeHttp(context, element);
		}

		return getDefaultCollectorSendType(context);
	}

	private RuntimeBeanReference getDefaultCollectorSendType(ExtendedParserContext context) {

		if (isRemote) {
			log.debug("Cache server locator is remote, client message sending via HTTP");
			return getCollectorSendTypeHttp(context, null);
		}

		log.debug("Cache server locator is local, client message sending via logging");
		RootBeanDefinition definition = context.newBeanDefinition(LoggingMessageSender.class);
		return context.registerDefinition(definition, ExtendedParserContext.TYPE_COLLECTOR);
	}

	private RuntimeBeanReference getCollectorSendTypeConsole(ExtendedParserContext context, Element console) {

		RootBeanDefinition definition = context.newBeanDefinition(ConsoleMessageSender.class);
		context.addBooleanProperty(definition, "debugSendSuccess", console, "success");
		context.addStringProperty(definition, "type", console, "type");

		return context.registerDefinition(definition, ExtendedParserContext.TYPE_COLLECTOR);
	}

	private RuntimeBeanReference getCollectorSendTypeLogging(ExtendedParserContext context, Element logging) {

		RootBeanDefinition definition = context.newBeanDefinition(LoggingMessageSender.class);
		context.addBooleanProperty(definition, "debugSendSuccess", logging, "success");
		context.addStringProperty(definition, "recorder", logging, "recorder");

		return context.registerDefinition(definition, ExtendedParserContext.TYPE_COLLECTOR);
	}

	private RuntimeBeanReference getCollectorSendTypeHttp(ExtendedParserContext context, Element http) {

		RuntimeBeanReference httpRequestor = getMessageSendHttpRequestor(context, http);

		RootBeanDefinition definition = context.newBeanDefinition(HttpMessageSender.class);
		context.addProperty(definition, "httpRequestor", httpRequestor);

		if (isRemote) {
			RuntimeBeanReference shutdownNotifier = getShutdownNotifier(context, http);
			context.addProperty(definition, "shutdownNotifier", shutdownNotifier);
		}

		return context.registerDefinition(definition, ExtendedParserContext.TYPE_COLLECTOR);
	}

	private RuntimeBeanReference getShutdownNotifier(ExtendedParserContext context, Element http) {

		RuntimeBeanReference requestor = getLocatorHttpRequestor(
				context, http, ShutdownNotifyHttpRequestorFactoryBean.class, ExtendedParserContext.TYPE_COLLECTOR);
		RootBeanDefinition definition = context.newBeanDefinition(ClientShutdownHttpNotifier.class);
		context.addProperty(definition, "requestor", requestor);
		return context.registerDefinition(definition, ExtendedParserContext.TYPE_COLLECTOR);
	}

	private RuntimeBeanReference getMessageSendHttpRequestor(ExtendedParserContext context, Element http) {

		if (isRemote) {
			return getLocatorHttpRequestor(context, http, MessageSendHttpRequestorFactoryBean.class, ExtendedParserContext.TYPE_COLLECTOR);
		}
		return getHttpRequestor(context, http, ExtendedParserContext.TYPE_COLLECTOR);
	}

	private RuntimeBeanReference getHttpRequestor(ExtendedParserContext context, Element http, String type) {

		return getHttpRequestor(context, http, JdkHttpRequestorFactoryBean.class, type);
	}

	private RuntimeBeanReference getLocatorHttpRequestor(
			ExtendedParserContext context, Element http,
			Class<? extends CacheServerHttpRequestorFactoryBean> factoryBeanClass, String type) {

		RootBeanDefinition definition = context.newBeanDefinition(factoryBeanClass);

		context.addProperty(definition, "locator", context.getCacheServerLocatorReference());

		if (http != null) {
			context.addIntProperty(definition, "connectTimeout", http, "connect-timeout");
			context.addIntProperty(definition, "readTimeout", http, "read-timeout");
		}

		return context.registerDefinition(definition, type);
	}

	private RuntimeBeanReference getHttpRequestor(
			ExtendedParserContext context, Element http, Class<? extends JdkHttpRequestorFactoryBean> factoryBeanClass,
			String type) {

		RootBeanDefinition definition = context.newBeanDefinition(factoryBeanClass);

		context.addStringProperty(definition, "endpoint", http, "endpoint");
		context.addIntProperty(definition, "connectTimeout", http, "connect-timeout");
		context.addIntProperty(definition, "readTimeout", http, "read-timeout");

		return context.registerDefinition(definition, type);
	}
}
