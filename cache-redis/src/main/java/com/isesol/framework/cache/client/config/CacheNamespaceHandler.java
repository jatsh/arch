package com.isesol.framework.cache.client.config;

import com.isesol.framework.cache.client.util.*;
import org.springframework.beans.factory.xml.*;

public class CacheNamespaceHandler extends NamespaceHandlerSupport {

	static final String NODE_ANNOTATION_DRIVEN = "annotation-driven";

	static final String NODE_CACHE_EXECUTOR = "cache-executor";

	private static final String BEAN_NAME = "cacheExecutor";

	private static final ThreadLocal<String> INSTANCE = new ThreadLocal<String>() {
		@Override
		protected String initialValue() {

			return BEAN_NAME;
		}
	};

	static String getInstance() {

		return INSTANCE.get();
	}

	static void setInstance(String name) {

		INSTANCE.set(name);
	}

	static void removeInstance() {

		INSTANCE.remove();
	}

	static String generateBeanName(final Class<?> beanClass) {

		return generateBeanName(beanClass, null);
	}

	static String generateBeanName(final Class<?> beanClass, final String type) {

		return generateBeanName(beanClass.getName(), type);
	}

	static String generateBeanName(final String beanClassName, final String type) {

		String internalType = type;

		if (Tools.isBlank(internalType)) {

			internalType = "internal";
		}

		String name = beanClassName;
		int offset = name.lastIndexOf('.');
		if (offset > -1) {
			name = name.substring(offset + 1);
		}

		return getInstance() + "~" + name + '.' + internalType + ".beanName";
	}

	@Override
	public void init() {

		registerBeanDefinitionParser(NODE_CACHE_EXECUTOR, new CacheExecutorParser());
		registerBeanDefinitionParser(NODE_ANNOTATION_DRIVEN, new AnnotationDrivenParser());
	}
}
