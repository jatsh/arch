package com.isesol.framework.cache.client.config;

import com.isesol.framework.cache.client.app.*;
import com.isesol.framework.cache.client.app.impl.*;
import com.isesol.framework.cache.client.connector.*;
import com.isesol.framework.cache.client.message.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.aop.framework.*;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.config.*;
import org.springframework.beans.factory.support.*;
import org.springframework.beans.factory.xml.*;
import org.springframework.util.xml.*;
import org.w3c.dom.*;

import java.util.*;

public class ExtendedParserContext {

	static final String TYPE_ANNOTATION = "annotation";

	static final String TYPE_EXECUTOR = "executor";

	static final String TYPE_COLLECTOR = "collector";

	static final String TYPE_LOCATOR = "locator";

	static final String BEAN_NAME_CACHE_EXECUTOR = "cacheExecutor";

	static Logger log = LogManager.getLogger(ExtendedParserContext.class.getName());

	private final Element root;

	private final ParserContext parserContext;

	private final Object source;

	ExtendedParserContext(Element root, ParserContext parserContext) {

		this.root = root;
		this.parserContext = parserContext;
		this.source = parserContext.extractSource(root);
	}

	public static String getInternalCacheExecutorBeanName(String name) {

		if (name == null) {

			return null;
		}

		return name + "~internal";
	}

	public ParserContext getParserContext() {

		return parserContext;
	}

	public String getRootTagName() {

		return getRoot().getTagName();
	}

	public Object getElementSource() {

		return source;
	}

	public boolean hasCacheExecutorBeanName() {

		return containsBeanDefinition(getCacheExecutorBeanName());
	}

	public String getCacheExecutorBeanName() {

		if (CacheNamespaceHandler.NODE_CACHE_EXECUTOR.equals(root.getLocalName())) {

			if (root.hasAttribute("id")) {

				return root.getAttribute("id");
			}

			return BEAN_NAME_CACHE_EXECUTOR;
		}

		return null;
	}

	public String getInternalCacheExecutorBeanName() {

		return getInternalCacheExecutorBeanName(getCacheExecutorBeanName());
	}

	public RuntimeBeanReference getCacheCientApplication() {

		if (!containsBeanDefinition(getCacheClientApplicationBeanName())) {
			registerCacheClientApplication();
		}
		return new RuntimeBeanReference(getCacheClientApplicationBeanName());
	}

	public RuntimeBeanReference getMessageQueue() {

		if (!containsBeanDefinition(getMessageQueueBeanName())) {
			registerMessageQueue();
		}

		return new RuntimeBeanReference(getMessageQueueBeanName());
	}

	public Element getRoot() {

		return root;
	}

	public Properties parsePropsElement(Element propsElement) {

		return parserContext.getDelegate().parsePropsElement(propsElement);
	}

	public Element getRootChild(String name) {

		return DomUtils.getChildElementByTagName(root, name);
	}

	public RootBeanDefinition newBeanDefinition(Class<?> clazz) {

		RootBeanDefinition definition = new RootBeanDefinition(clazz);
		definition.setSource(source);
		return definition;
	}

	public RootBeanDefinition newInfrastructureBeanDefinition(Class<?> clazz) {

		RootBeanDefinition definition = new RootBeanDefinition(clazz);
		definition.setSource(source);
		definition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
		return definition;
	}

	public void addConstructorArgument(RootBeanDefinition definition, int index, Object value) {

		definition.getConstructorArgumentValues().addIndexedArgumentValue(index, value);
	}

	public void addProperty(RootBeanDefinition definition, String property, Object value) {

		if (value != null) {
			definition.getPropertyValues().addPropertyValue(property, value);
			log.debug(
					"Add property, beanClass = {}, property '{}' = {}", definition.getBeanClassName(), property,
					value);

		}
	}

	public void addProxyConfigProperties(RootBeanDefinition definition) {

		if (!ProxyConfig.class.isAssignableFrom(definition.getBeanClass())) {
			log.warn(
					" BeanDefinition is '{}' type that is not a ProxyConfig that cannot add proxy configurations",
					definition.getBeanClassName());
			return;
		}

		for (ProxyConfigProperty property : ProxyConfigProperty.values()) {

			addBooleanProperty(definition, property.getProperty(), getRoot(), property.getAttribute());
		}
	}

	public void addStringProperty(RootBeanDefinition definition, String property, Element element, String name) {

		logAttribute(definition, property, element, name, "String");
		if (element.hasAttribute(name)) {
			addProperty(definition, property, element.getAttribute(name));
		}
	}

	public void addStringArrayProperty(RootBeanDefinition definition, String property, Element element, String name) {

		logAttribute(definition, property, element, name, "StringArray");
		if (element.hasAttribute(name)) {
			addProperty(definition, property, element.getAttribute(name).split("\\s*,\\s*"));
		}
	}

	public void addIntProperty(RootBeanDefinition definition, String property, Element element, String name) {

		logAttribute(definition, property, element, name, "int");

		if (element.hasAttribute(name)) {
			addProperty(definition, property, Integer.parseInt(element.getAttribute(name)));
		}
	}

	public void addBooleanProperty(RootBeanDefinition definition, String property, Element element, String name) {

		logAttribute(definition, property, element, name, "boolean");

		if (element.hasAttribute(name)) {
			addProperty(definition, property, Boolean.valueOf(element.getAttribute(name)));
		}
	}

	private void logAttribute(
			RootBeanDefinition definition, String property, Element element, String name, String type) {

		if (log.isTraceEnabled()) {

			StringBuilder builder = new StringBuilder();
			builder.append("Set attribute")
			       .append(": property = ")
			       .append(property)
			       .append(", property type = ")
			       .append(type)
			       .append(", beanClass = ")
			       .append(definition.getBeanClass().getSimpleName())
			       .append(", element = ")
			       .append(element.getLocalName())
			       .append("[@")
			       .append(name)
			       .append("=")
			       .append(element.getAttribute(name))
			       .append(']');

			log.trace(builder.toString());
		}
	}

	public void registerAlias(String beanName, String alias) {

		parserContext.getRegistry().registerAlias(beanName, alias);
	}

	public void quickRegisterBean(Class<?> clazz) {

		RootBeanDefinition definition = newBeanDefinition(clazz);
		String beanName = CacheNamespaceHandler.generateBeanName(clazz);
		registerNamed(definition, beanName);
	}

	public void registerNamed(RootBeanDefinition definition, String name) {

		if (CacheClientApplicationAware.class.isAssignableFrom(definition.getBeanClass())) {
			addProperty(definition, "cacheClientApplication", getCacheCientApplication());
		}

		parserContext.getRegistry().registerBeanDefinition(name, definition);
	}

	public RuntimeBeanReference registerNamedDefinition(RootBeanDefinition definition, String name) {

		registerNamed(definition, name);

		if (log.isDebugEnabled()) {
			log.debug("Register beanName = {}, type = {}", name, definition.getBeanClassName());
		}

		return new RuntimeBeanReference(name);
	}

	public RuntimeBeanReference registerDefinition(RootBeanDefinition definition, String type) {

		String beanName = generateBeanName(definition, type);
		return registerNamedDefinition(definition, beanName);
	}

	public BeanDefinition getBeanDefinition(String beanName) {

		try {
			return parserContext.getRegistry().getBeanDefinition(beanName);
		} catch (NoSuchBeanDefinitionException e) {
			return null;
		}
	}

	public void removeBeanDefinition(String beanName) {

		try {
			parserContext.getRegistry().removeBeanDefinition(beanName);
		} catch (NoSuchBeanDefinitionException e) {
			return;
		}
	}

	public String generateBeanName(RootBeanDefinition definition, String type) {

		return CacheNamespaceHandler.generateBeanName(definition.getBeanClass(), type);
	}

	public boolean containsBeanDefinition(String beanName) {

		return parserContext.getRegistry().containsBeanDefinition(beanName);
	}

	public RuntimeBeanReference registerCacheServerLocator(RootBeanDefinition definition) {

		return registerNamedDefinition(definition, getCacheserverLocatorBeanName());
	}

	public RuntimeBeanReference registerCacheServerLocator(String beanName) {

		BeanDefinition definition = getBeanDefinition(beanName);
		parserContext.getRegistry().registerBeanDefinition(getCacheserverLocatorBeanName(), definition);
		return new RuntimeBeanReference(beanName);
	}

	public RuntimeBeanReference getCacheServerLocatorReference() {

		return new RuntimeBeanReference(getCacheserverLocatorBeanName());
	}

	private String getCacheserverLocatorBeanName() {

		return CacheNamespaceHandler.generateBeanName(CacheServerLocator.class);
	}

	private String getCacheClientApplicationBeanName() {

		return CacheNamespaceHandler.generateBeanName(CacheClientApplication.class);
	}

	private String getMessageQueueBeanName() {

		return CacheNamespaceHandler.generateBeanName(CacheClientQueue.class);
	}

	private void registerCacheClientApplication() {

		if (!isCacheExecutorNodes()) {
			return;
		}

		if (containsBeanDefinition(getCacheClientApplicationBeanName())) {
			return;
		}

		if (root.hasAttribute("cache-app-ref")) {

			String name = root.getAttribute("cache-app-ref");

			registerAlias(name, getCacheClientApplicationBeanName());

			return;
		}

		String appCode = root.getAttribute("appcode");
		RootBeanDefinition definition = newBeanDefinition(DefaultCacheClientApplication.class);
		addConstructorArgument(definition, 0, appCode);

		registerNamed(definition, getCacheClientApplicationBeanName());
	}

	private void registerMessageQueue() {

		if (!isCacheExecutorNodes()) {
			return;
		}

		if (containsBeanDefinition(getMessageQueueBeanName())) {
			return;
		}

		RootBeanDefinition definition = newBeanDefinition(CacheClientQueue.class);
		registerNamed(definition, getMessageQueueBeanName());
	}

	public void logConfigurations() {

		StringBuilder builder = new StringBuilder();
		builder.append(root.getLocalName() + "  configurations:");
		appendElement(builder, root, 0);
		log.info(builder.toString());
	}

	private void appendElement(StringBuilder builder, Element element, int level) {

		String nodeName = element.getLocalName();

		builder.append('\n');

		CacheClientUtils.repeatSpace(level, builder).append('<').append(nodeName);

		NamedNodeMap attributes = element.getAttributes();

		for (int i = 0, k = attributes.getLength(); i < k; i++) {
			Attr attr = (Attr) attributes.item(i);
			builder.append(' ').append(attr.getLocalName()).append("=\"").append(attr.getValue()).append("\"");
		}

		List<Element> children = DomUtils.getChildElements(element);

		if (Tools.isBlank(children)) {
			builder.append(" />");
			return;
		}

		builder.append('>');

		for (Element child : children) {
			appendElement(builder, child, level + 1);
		}

		builder.append('\n');

		CacheClientUtils.repeatSpace(level, builder).append("</").append(nodeName).append('>');
	}

	private boolean isCacheExecutorNodes() {

		return "cache-executor".equals(root.getLocalName());
	}

	private static enum ProxyConfigProperty {

		PROXY_TARGET_CLASS("proxyTargetClass", "proxy-target-class", true),

		OPTIMIZE("optimize", "optimize", false),

		OPAQUE("opaque", "opaque", false),

		EXPOSE_PROXY("exposeProxy", "expose-proxy", false),

		FROZEN("frozen", "freeze-proxy", false);

		private String property;

		private String attribute;

		private ProxyConfigProperty(String property, String attribute, boolean defaultValue) {

			this.property = property;
			this.attribute = attribute;
		}

		public String getProperty() {

			return property;
		}

		public String getAttribute() {

			return attribute;
		}
	}
}
