package com.isesol.framework.cache.client.connector;

import com.isesol.framework.cache.client.app.*;
import com.isesol.framework.cache.client.util.*;
import org.springframework.beans.factory.*;

public abstract class AbstractCacheServerLocator
		implements CacheServerLocator, CacheClientApplicationAware, InitializingBean {

	private CacheClientApplication application;

	private CacheServerConnector connector;

	public AbstractCacheServerLocator() {

	}

	@Override
	public CacheServerConnector getCacheServer() {

		return connector;
	}

	@Override
	public void setCacheClientApplication(CacheClientApplication application) {

		this.application = application;
	}

	public String getAppCode() {

		return application.getAppCode();
	}

	public String getNodeCode() {

		return application.getNodeCode();
	}

	@Override
	public final void afterPropertiesSet() {

		Assert.notNull(application, "application");
		doAfterPropertiesSet();
		connector = initCacheServerConnector();
		Assert.notNull(connector, "connector");
	}

	protected void doAfterPropertiesSet() {

	}

	protected abstract CacheServerConnector initCacheServerConnector();
}
