package com.isesol.framework.cache.client.connector;

import com.isesol.framework.cache.client.util.*;

import java.io.*;
import java.util.*;

public class CacheServerAddress implements Serializable {

	public static final String MULTI_ADDRESS_FORMAT = "<host>:<port>[:<password>]{(,|;)<host>:<port>[:<password>]}";

	private static final int PORT_MAX = 65535;

	private static final int DEFAULT_ADDRESS_SIZE = 3;

	private static final int LENGTH_ADDR_PORT = 2;

	private static final int LENGTH_ADDR_PORT_PASSWORD = 3;

	private String host;

	private int port;

	private String password;

	public CacheServerAddress() {

	}

	public CacheServerAddress(String host, int port) {

		this(host, port, null);
	}

	public CacheServerAddress(String host, int port, String password) {

		this.host = Tools.trim(host);
		this.port = port;
		this.password = Tools.trim(password);
	}

	public static List<CacheServerAddress> parseAddress(String addresses) {

		if (Tools.isBlank(addresses)) {

			throw new IllegalArgumentException("configured address is null or empty");
		}

		List<CacheServerAddress> list = new ArrayList<CacheServerAddress>(DEFAULT_ADDRESS_SIZE);

		String[] address = addresses.trim().split("\\s*[,;]\\s*");

		for (String addr : address) {

			if (Tools.isBlank(addr)) {

				continue;
			}

			CacheServerAddress csa = parseSingleAddress(addresses, addr);

			if (csa != null) {

				list.add(csa);
			}

		}

		return list;
	}

	private static CacheServerAddress parseSingleAddress(String addresses, String address) {

		String[] segement = address.split("\\s*:\\s*");

		if (segement == null || segement.length < LENGTH_ADDR_PORT || segement.length > LENGTH_ADDR_PORT_PASSWORD) {

			invalidAddresses(addresses);
		}

		if (Tools.isBlank(segement[0])) {

			invalidParameter(addresses, "host", segement[0]);
		}

		int port = -1;

		try {

			port = Integer.parseInt(segement[1]);
		} catch (NumberFormatException e) {

			invalidParameter(addresses, "port", segement[1]);
		}

		if (port < 0 || port > PORT_MAX) {

			invalidParameter(addresses, "port", segement[1]);
		}

		if (segement.length == LENGTH_ADDR_PORT) {

			return new CacheServerAddress(segement[0], port);
		}

		return new CacheServerAddress(segement[0], port, segement[2]);
	}

	private static void invalidParameter(String addresses, String name, String value) {

		throw new IllegalArgumentException(
				"configured address [" + addresses + "] " + name + " [" + value + "] is invalid");
	}

	private static void invalidAddresses(String addresses) {

		throw new IllegalArgumentException(
				"configured address [" + addresses + "] is invalid" + ", correct format is " + MULTI_ADDRESS_FORMAT);
	}

	public String getHost() {

		return host;
	}

	public void setHost(String host) {

		this.host = host;
	}

	public int getPort() {

		return port;
	}

	public void setPort(int port) {

		this.port = port;
	}

	public String getPassword() {

		return password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	@Override
	public String toString() {

		if (Tools.isBlank(password)) {
			return host + ':' + port;
		}
		return host + ':' + port + ":******";
	}
}
