package com.isesol.framework.cache.client.connector;

import com.alibaba.fastjson.annotation.*;
import com.isesol.framework.cache.client.util.*;

import java.io.*;
import java.util.*;

/**
 * 缓存服务器连接器。封装一些对于缓存服务器的连接参数数据。
 */
public class CacheServerConnector implements Serializable {

	private static final String MESSAGE_SUCCESS = "SUCCESS";

	private String appCode;

	private String message;

	private String messageSendUrl;

	private String shutdownNotifyUrl;

	private List<CacheServerAddress> address;

	public CacheServerConnector() {

	}

	public CacheServerConnector(String appCode, String address) {

		Assert.notEmpty(appCode, "appCode");
		Assert.notEmpty(address, "address");
		this.appCode = appCode.trim();
		this.address = CacheServerAddress.parseAddress(address);
		this.message = MESSAGE_SUCCESS;
	}

	@JSONField(serialize = false, deserialize = false)
	public boolean isSuccess() {

		return MESSAGE_SUCCESS.equals(message);
	}

	public String getAppCode() {

		return appCode;
	}

	public void setAppCode(String appCode) {

		this.appCode = Tools.trim(appCode);
	}

	public CacheServerAddress[] getAddress() {

		if (Tools.isBlank(address)) {
			return null;
		}
		return address.toArray(new CacheServerAddress[address.size()]);
	}

	public void setAddress(List<CacheServerAddress> address) {

		this.address = address;
	}

	public String getMessage() {

		return message;
	}

	public void setMessage(String message) {

		this.message = Tools.trim(message);
	}

	public String getMessageSendUrl() {

		return messageSendUrl;
	}

	public void setMessageSendUrl(String messageSendUrl) {

		this.messageSendUrl = Tools.trim(messageSendUrl);
	}

	public String getShutdownNotifyUrl() {

		return shutdownNotifyUrl;
	}

	public void setShutdownNotifyUrl(String shutdownNotifyUrl) {

		this.shutdownNotifyUrl = Tools.trim(shutdownNotifyUrl);
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("appCode", appCode);
		builder.append("message", message);
		builder.append("messageSendUrl", messageSendUrl);
		builder.append("shutdownNotifyUrl", shutdownNotifyUrl);
		builder.append("address", address);
		return builder.toString();
	}
}
