package com.isesol.framework.cache.client.connector;

/**
 * 缓存服务器连接器定位接口。用于获取缓存服务器连接器 {@link CacheServerConnector} 缓存客户端连接服务器。
 */
public interface CacheServerLocator {

	/**
	 * <p>
	 * 获取缓存服务器连接器对象
	 * </p>
	 *
	 * @return 缓存服务器连接器对象
	 */
	CacheServerConnector getCacheServer();
}
