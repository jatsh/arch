package com.isesol.framework.cache.client.connector;

public interface ClientShutdownNotifier {

	void notifyShutdown();
}
