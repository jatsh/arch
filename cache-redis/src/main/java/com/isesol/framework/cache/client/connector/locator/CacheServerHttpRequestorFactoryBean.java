package com.isesol.framework.cache.client.connector.locator;

import com.isesol.framework.cache.client.connector.*;
import com.isesol.framework.cache.client.http.jdk.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;

public abstract class CacheServerHttpRequestorFactoryBean extends JdkHttpRequestorFactoryBean {

	static Logger log = LogManager.getLogger(CacheServerHttpRequestorFactoryBean.class.getName());

	private CacheServerLocator locator;

	private CacheServerConnector connector;

	public CacheServerHttpRequestorFactoryBean() {

	}

	public void setLocator(CacheServerLocator locator) {

		this.locator = locator;
	}

	public CacheServerConnector getConnector() {

		return connector;
	}

	@Override
	protected void doAfterPropertiesSet() {

		Assert.notNull(locator, "locator");
		this.connector = locator.getCacheServer();
	}

	protected void logEndpoint(String info, String endpoint) {

		log.debug("{} URL from cache server locator, endpoint is '{}'", info, endpoint);
	}
}
