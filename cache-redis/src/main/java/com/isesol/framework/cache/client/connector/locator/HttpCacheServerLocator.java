package com.isesol.framework.cache.client.connector.locator;

import com.alibaba.fastjson.*;
import com.alibaba.fastjson.parser.*;
import com.isesol.framework.cache.client.connector.*;
import com.isesol.framework.cache.client.http.*;
import com.isesol.framework.cache.client.logging.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;

import java.io.*;

import static com.alibaba.fastjson.parser.Feature.*;

public class HttpCacheServerLocator extends AbstractCacheServerLocator {

	private static final Feature[] DEFAULT_FEATURES = new Feature[]{AllowSingleQuotes, DisableASM};

	static Logger log = LogManager.getLogger(HttpCacheServerLocator.class.getName());

	private HttpRequestor httpRequestor;

	public HttpCacheServerLocator() {

	}

	public HttpRequestor getHttpRequestor() {

		return httpRequestor;
	}

	public void setHttpRequestor(HttpRequestor httpRequestor) {

		this.httpRequestor = httpRequestor;
	}

	@Override
	public void doAfterPropertiesSet() {

		Assert.notNull(getHttpRequestor(), "httpRequestor");
	}

	@Override
	protected CacheServerConnector initCacheServerConnector() {

		HttpResponse response = doRequest();

		if (!response.isSuccessResponse()) {

			throw new BeanCreationException(
					"Request HTTP response code is " + "'" + response.getResponseCode() + "'" + ", the code is not " +
					"OK");
		}

		String json = Tools.trim(response.getContentTypeResponse());

		log.info("Request cache server address, response: {}", json);

		return parse(json);
	}

	private CacheServerConnector parse(String json) {

		if (!isValidJSONAddresses(json)) {

			parseError(json, "format is incorrect", null);
		}

		CacheServerConnector connector = null;

		try {

			connector = JSON.parseObject(json, CacheServerConnector.class, DEFAULT_FEATURES);
		} catch (Exception e) {

			throw parseError(json, "Cache server address parse error", e);
		}

		if (connector == null) {

			throw parseError(json, "Cache server address is empty", null);
		}

		if (!connector.isSuccess()) {
			throw parseError(
					json, "Cache server address request result is failed (" + connector.getMessage() + ")", null);
		}

		if (Tools.isBlank(connector.getAddress())) {
			throw parseError(json, "Cache server address is empty", null);
		}

		return connector;
	}

	private BeanCreationException parseError(String json, String msg, Exception e) {

		String message = msg + " (details: cache server address '" + HideUtils.securityHidden(json) + "'" +
		                 ", appCode is '" + getAppCode() + "', nodeCode is '" + getNodeCode() + "', request info: " +
		                 getHttpRequestor() + ")";

		if (e == null) {
			return new BeanCreationException(message);
		}

		return new BeanCreationException(message, e);
	}

	private HttpResponse doRequest() {

		HttpRequest request = new HttpRequest();
		NamePairRequestEntity entity = new NamePairRequestEntity();
		entity.add("appCode", getAppCode());
		entity.add("nodeCode", getNodeCode());
		entity.add("timestamp", System.currentTimeMillis());
		request.setRequestEntity(entity);

		try {

			HttpResponse response = getHttpRequestor().doPost(request);

			return response;

		} catch (IOException e) {
			throw new BeanCreationException(
					"Request cause exception, appCode is" + " '" + getAppCode() + "', nodeCode is '" + getNodeCode() +
					"'" + ", request info: " + getHttpRequestor(), e);
		}
	}

	private boolean isValidJSONAddresses(String addresses) {

		if (Tools.isBlank(addresses)) {

			return false;
		}

		return true;
	}
}
