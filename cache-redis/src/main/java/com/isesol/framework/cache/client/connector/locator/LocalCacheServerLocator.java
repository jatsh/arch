package com.isesol.framework.cache.client.connector.locator;

import com.isesol.framework.cache.client.connector.*;
import org.springframework.beans.factory.*;

public class LocalCacheServerLocator extends AbstractCacheServerLocator {

	/**
	 * local locator, format see {@link CacheServerAddress#MULTI_ADDRESS_FORMAT}
	 */
	private String address;

	public LocalCacheServerLocator() {

	}

	public void setAddress(String address) {

		this.address = address;
	}

	@Override
	public void doAfterPropertiesSet() {

		try {
			CacheServerAddress.parseAddress(address);
		} catch (Exception e) {
			throw new BeanCreationException(
					"address [" + address + "] is invalid" + ", the address format should be '" +
					CacheServerAddress.MULTI_ADDRESS_FORMAT +
					"'", e);
		}
	}

	@Override
	protected CacheServerConnector initCacheServerConnector() {

		return new CacheServerConnector(getAppCode(), address);
	}
}
