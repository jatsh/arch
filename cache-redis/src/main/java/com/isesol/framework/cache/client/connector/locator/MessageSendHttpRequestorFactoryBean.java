package com.isesol.framework.cache.client.connector.locator;

public class MessageSendHttpRequestorFactoryBean extends CacheServerHttpRequestorFactoryBean {

	@Override
	protected String initEndpointUrl() {

		String endpoint = getConnector().getMessageSendUrl();
		logEndpoint("Message send", endpoint);
		return endpoint;
	}
}