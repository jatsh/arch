package com.isesol.framework.cache.client.connector.locator;

public class ShutdownNotifyHttpRequestorFactoryBean extends CacheServerHttpRequestorFactoryBean {

	@Override
	protected String initEndpointUrl() {

		String endpoint = getConnector().getShutdownNotifyUrl();
		logEndpoint("Shutdown notify", endpoint);
		return endpoint;
	}
}