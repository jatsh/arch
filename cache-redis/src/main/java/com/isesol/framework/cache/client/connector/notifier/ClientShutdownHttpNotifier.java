package com.isesol.framework.cache.client.connector.notifier;

import com.isesol.framework.cache.client.app.*;
import com.isesol.framework.cache.client.connector.*;
import com.isesol.framework.cache.client.http.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;

import java.io.*;

public class ClientShutdownHttpNotifier
		implements InitializingBean, ClientShutdownNotifier, CacheClientApplicationAware {

	static Logger log = LogManager.getLogger(ClientShutdownHttpNotifier.class.getName());

	private CacheClientApplication application;

	private HttpRequestor requestor;

	public ClientShutdownHttpNotifier() {

	}

	@Override
	public void setCacheClientApplication(CacheClientApplication application) {

		this.application = application;
	}

	public HttpRequestor getRequestor() {

		return requestor;
	}

	public void setRequestor(HttpRequestor requestor) {

		this.requestor = requestor;
	}

	@Override
	public void afterPropertiesSet() {

		Assert.notNull(application, "application");
		Assert.notNull(requestor, "requestor");
		log.debug("Startup shutdown notify connector, {}", requestor);
	}

	@Override
	public final void notifyShutdown() {

		log.info("Shutdown notify start working...");
		httpNotifyShutdown();
	}

	protected void httpNotifyShutdown() {

		HttpRequest request = new HttpRequest();

		NamePairRequestEntity entity = new NamePairRequestEntity();
		entity.add("appCode", application.getAppCode());
		entity.add("nodeCode", application.getNodeCode());

		request.setRequestEntity(entity);

		try {
			HttpResponse response = requestor.doPost(request);

			if (log.isDebugEnabled()) {
				log.debug(
						"Notify shutdown, response code is '{}', response data = {}", response.getResponseCode(),
						response.getDefaultEncodingResponse());
			}
		} catch (IOException e) {
			log.warn(
					"Notify shutdown threw exception, shutdown requestor = {}, entity = {}", requestor,
					entity.getDefaultEncodingEntity(), e);
		}
	}
}
