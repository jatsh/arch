package com.isesol.framework.cache.client.exception;

/**
 * 缓存客户端异常
 */
public class CacheClientException extends Exception {

	public CacheClientException(String message, Throwable cause) {

		super(message, cause);
	}
}
