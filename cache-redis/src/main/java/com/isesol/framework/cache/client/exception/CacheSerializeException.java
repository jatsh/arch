package com.isesol.framework.cache.client.exception;

public class CacheSerializeException extends RuntimeException {

	public CacheSerializeException(String message, Throwable cause) {

		super(message, cause);
	}
}
