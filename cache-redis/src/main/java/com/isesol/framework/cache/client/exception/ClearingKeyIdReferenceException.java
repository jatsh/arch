package com.isesol.framework.cache.client.exception;

public class ClearingKeyIdReferenceException extends IllegalArgumentException {

	private String keyId;

	private String info;

	private String message;

	public ClearingKeyIdReferenceException(String keyId, String info) {

		super();
		this.keyId = keyId;
		this.info = info;
	}

	@Override
	public String getMessage() {

		if (message == null) {

			message = "keyId \"" + keyId + "\" that annotated on \"" + info + "\"" +
			          " cannot find reference keyId, any enable cacheabe annotation has not the keyId";
		}

		return message;
	}
}
