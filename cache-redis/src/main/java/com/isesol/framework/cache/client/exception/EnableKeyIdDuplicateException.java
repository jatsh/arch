package com.isesol.framework.cache.client.exception;

public class EnableKeyIdDuplicateException extends IllegalArgumentException {

	private String keyId;

	private String info;

	private String existInfo;

	private String message;

	public EnableKeyIdDuplicateException(String keyId, String info, String existInfo) {

		super();
		this.keyId = keyId;
		this.info = info;
		this.existInfo = existInfo;
	}

	@Override
	public String getMessage() {

		if (message == null) {

			message = "keyId \"" + keyId + "\" that annotated" + " on \"" + info +
			          "\" has existed, reference keyId value that annotated" + " on \"" + existInfo + "\"";
		}

		return message;
	}
}
