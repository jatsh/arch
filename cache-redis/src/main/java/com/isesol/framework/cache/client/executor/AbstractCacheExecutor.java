package com.isesol.framework.cache.client.executor;

public abstract class AbstractCacheExecutor extends AbstractWrapKeyCacheExecutor implements CacheExecutor {

	public AbstractCacheExecutor(String cacheExecutorType) {

		super(cacheExecutorType);
	}
}
