package com.isesol.framework.cache.client.executor;

import com.isesol.framework.cache.client.util.*;

/**
 * {@link WrapKey} 的部分抽象实现，实现类需要实现其中的
 * {@link #doWrapKey(String)} 或者
 * {@link #doWrapMapKey(String)} 方法。
 * <p>
 * 包装 key 或者 mapKey 的值不能为空或者空白字符，否则将会抛出 {@link IllegalArgumentException} 异常
 */
public abstract class AbstractWrapKey implements WrapKey {

	@Override
	public final String wrapKey(String key) {

		Assert.notEmpty(key, "key");
		return doWrapKey(key);
	}

	@Override
	public final String wrapMapKey(String mapKey) {

		Assert.notEmpty(mapKey, "mapKey");
		return doWrapMapKey(mapKey);
	}

	@Override
	public final String[] wrapKeys(String... keys) {

		Assert.notEmpty(keys, "keys");

		String[] wrapKeys = new String[keys.length];

		for (int i = 0; i < keys.length; i++) {

			wrapKeys[i] = wrapKey(keys[i]);
		}

		return wrapKeys;
	}

	@Override
	public final String[] wrapMapKeys(String... mapKeys) {

		Assert.notEmpty(mapKeys, "mapKeys");

		String[] wrapMapKeys = new String[mapKeys.length];

		for (int i = 0; i < mapKeys.length; i++) {

			wrapMapKeys[i] = wrapMapKey(mapKeys[i]);
		}

		return wrapMapKeys;
	}

	protected String doWrapKey(String key) {

		throw new UnsupportedOperationException("doWrapKey method is unsupported");
	}

	protected String doWrapMapKey(String mapKey) {

		return mapKey;
	}

	/**
	 * 使用 {@link WrapKey#KEY_SEPARATOR} 作为连接符拼接多个字符串。
	 *
	 * @param strs 需要拼接的字符串
	 *
	 * @return 使用连接符拼接后的字符串
	 */
	protected String join(String... strs) {

		return Tools.join(KEY_SEPARATOR, strs);
	}
}
