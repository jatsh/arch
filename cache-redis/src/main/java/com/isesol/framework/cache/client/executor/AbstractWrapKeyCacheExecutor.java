package com.isesol.framework.cache.client.executor;

import com.isesol.framework.cache.client.app.*;
import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.operation.*;
import com.isesol.framework.cache.client.util.*;
import org.springframework.beans.factory.*;

import java.util.*;

/**
 * 包装缓存数据 key 值（mapKey 值使用原始值）的 {@link CacheExecutor} 抽象实现。
 * <p>
 * 实际存入缓存服务器的 key 结构为：appcode:type:key，
 * <p>
 * <code>key</code> 为客户端原始的 key 值；
 * <p>
 * <code>appcode</code> 为业务应用代码；
 * <p>
 * <code>type</code> 为缓存应用类型，
 * 主要有“api”和“method”两种可选值，以表示不同的应用场景。
 * </p>
 */
public abstract class AbstractWrapKeyCacheExecutor extends AbstractWrapKey
		implements InitializingBean, CacheClientApplicationAware, TransactionCacheExecutor {

	/**
	 * 缓存应用类型
	 */
	private final String cacheExecutorType;

	/**
	 * 缓存客户端业务应用参数
	 */
	private CacheClientApplication application;

	/**
	 * 内部缓存执行器对象，该对象为缓存操作的实现
	 */
	private TransactionCacheExecutor cacheExecutor;

	/**
	 * 事务操作回调方法操作
	 */
	private CacheExecutorOperation operation;

	public AbstractWrapKeyCacheExecutor(String cacheExecutorType) {

		this.cacheExecutorType = cacheExecutorType;
		this.operation = new DefaultCacheExecutorOperation(this);
	}

	public void setCacheExecutor(TransactionCacheExecutor cacheExecutor) {

		/**
		 * set from CacheExecutorParser.publishCacheExecutor
		 */
		this.cacheExecutor = cacheExecutor;
	}

	public String getCacheExecutorType() {

		return cacheExecutorType;
	}

	@Override
	public void setCacheClientApplication(CacheClientApplication application) {

		this.application = application;
	}

	protected CacheClientApplication getApplication() {

		return application;
	}

	protected CacheExecutorOperation getOperation() {

		return operation;
	}

	@Override
	protected String doWrapKey(String key) {

		return join(getApplication().getAppCode(), getCacheExecutorType(), key);
	}

	@Override
	protected String doWrapMapKey(String mapKey) {

		return mapKey;
	}

	@Override
	public void afterPropertiesSet() {

		Assert.notNull(application, "cacheClientApplication");
		Assert.notNull(cacheExecutor, "internalCacheExecutor");
	}

	@Override
	public void executeInTransaction(final TransactionAction tx) throws CacheClientException {

		cacheExecutor.executeInTransaction(
				new TransactionAction() {
					@Override
					public void doInTxAction(CacheExecutorOperation operation) throws CacheClientException {

						tx.doInTxAction(getOperation());
					}
				});
	}

	@Override
	public void del(String... keys) throws CacheClientException {

		cacheExecutor.del(wrapKeys(keys));
	}

	@Override
	public void delMapKey(String key, String mapKey) throws CacheClientException {

		cacheExecutor.delMapKey(wrapKey(key), wrapMapKey(mapKey));
	}

	@Override
	public Boolean hasKey(String key) throws CacheClientException {

		return cacheExecutor.hasKey(wrapKey(key));
	}

	@Override
	public Boolean sIsMember(String key, String value) throws CacheClientException {

		return cacheExecutor.sIsMember(wrapKey(key), value);
	}

	@Override
	public <T> T get(String key) throws CacheClientException {

		return cacheExecutor.get(wrapKey(key));
	}

	@Override
	public void set(String key, Object value) throws CacheClientException {

		cacheExecutor.set(wrapKey(key), value);
	}

	@Override
	public void expire(String key, int seconds) throws CacheClientException {

		cacheExecutor.expire(wrapKey(key), seconds);
	}

	@Override
	public <V> Map<String, V> getMap(String key) throws CacheClientException {

		return cacheExecutor.getMap(wrapKey(key));
	}

	@Override
	public void set(String key, Object value, int expireSeconds) throws CacheClientException {

		cacheExecutor.set(wrapKey(key), value, expireSeconds);
	}

	@Override
	public void expireAt(String key, Date expireAt) throws CacheClientException {

		cacheExecutor.expireAt(wrapKey(key), expireAt);
	}

	@Override
	public List<Object> mget(String... keys) throws CacheClientException {

		return cacheExecutor.mget(wrapKeys(keys));
	}

	@Override
	public <T> List<T> mgetMapEntry(String key, String... mapKeys) throws CacheClientException {

		return cacheExecutor.mgetMapEntry(wrapKey(key), wrapMapKeys(mapKeys));
	}

	@Override
	public void set(String key, Object value, Date expireAt) throws CacheClientException {

		cacheExecutor.set(wrapKey(key), value, expireAt);
	}

	@Override
	public <T> T getMapEntry(String key, String mapKey) throws CacheClientException {

		return cacheExecutor.getMapEntry(wrapKey(key), wrapMapKey(mapKey));
	}

	@Override
	public <V> void setMap(String key, Map<String, V> map) throws CacheClientException {

		cacheExecutor.setMap(wrapKey(key), map);
	}

	@Override
	public <V> void setMap(String key, Map<String, V> map, int expireSeconds) throws CacheClientException {

		cacheExecutor.setMap(wrapKey(key), map, expireSeconds);
	}

	@Override
	public <V> void setMap(String key, Map<String, V> map, Date expireAt) throws CacheClientException {

		cacheExecutor.setMap(wrapKey(key), map, expireAt);
	}

	@Override
	public void setMapEntry(String key, String mapKey, Object mapValue) throws CacheClientException {

		cacheExecutor.setMapEntry(wrapKey(key), wrapMapKey(mapKey), mapValue);
	}

	@Override
	public void setMapEntry(String key, String mapKey, Object mapValue, int expireSeconds) throws
	                                                                                       CacheClientException {

		cacheExecutor.setMapEntry(wrapKey(key), wrapMapKey(mapKey), mapValue, expireSeconds);
	}

	@Override
	public void setMapEntry(String key, String mapKey, Object mapValue, Date expireAt) throws CacheClientException {

		cacheExecutor.setMapEntry(wrapKey(key), wrapMapKey(mapKey), mapValue, expireAt);
	}

	@Override
	public <V> void setMapEntry(String key, Map<String, V> mapEntry) throws CacheClientException {

		cacheExecutor.setMapEntry(wrapKey(key), mapEntry);
	}

	@Override
	public <V> void setMapEntry(String key, Map<String, V> mapEntry, int expireSeconds) throws CacheClientException {

		cacheExecutor.setMapEntry(wrapKey(key), mapEntry, expireSeconds);
	}

	@Override
	public <V> void setMapEntry(String key, Map<String, V> mapEntry, Date expireAt) throws CacheClientException {

		cacheExecutor.setMapEntry(wrapKey(key), mapEntry, expireAt);
	}

	@Override
	public Long ttl(String key) throws CacheClientException {

		return cacheExecutor.ttl(wrapKey(key));
	}

	@Override
	public Number increment(String key) throws CacheClientException {

		return cacheExecutor.increment(wrapKey(key));
	}

	@Override
	public Number increment(String key, String mapKey) throws CacheClientException {

		return cacheExecutor.increment(wrapKey(key), wrapMapKey(mapKey));
	}

	@Override
	public Number incrementBy(String key, int delta) throws CacheClientException {

		return cacheExecutor.incrementBy(wrapKey(key), delta);
	}

	@Override
	public Number incrementBy(String key, String mapKey, int delta) throws CacheClientException {

		return cacheExecutor.incrementBy(wrapKey(key), wrapMapKey(mapKey), delta);
	}

	@Override
	public void listAppend(String key, Object element) throws CacheClientException {

		cacheExecutor.listAppend(wrapKey(key), element);
	}

	@Override
	public void listAppend(String key, Object element, int expireSeconds) throws CacheClientException {

		cacheExecutor.listAppend(wrapKey(key), element, expireSeconds);
	}

	@Override
	public void listAppend(String key, Object element, Date expireAt) throws CacheClientException {

		cacheExecutor.listAppend(wrapKey(key), element, expireAt);
	}

	@Override
	public Long listLength(String key) throws CacheClientException {

		return cacheExecutor.listLength(wrapKey(key));
	}

	@Override
	public void listRemoveFirst(String key, Object element) throws CacheClientException {

		cacheExecutor.listRemoveFirst(wrapKey(key), element);
	}

	@Override
	public <E> E listPopHead(String key) throws CacheClientException {

		return cacheExecutor.listPopHead(wrapKey(key));
	}

	@Override
	public <E> List<E> listElements(String key) throws CacheClientException {

		return cacheExecutor.listElements(wrapKey(key));
	}

	@Override
	public void treeAdd(String key, Object element, double score) throws CacheClientException {

		cacheExecutor.treeAdd(wrapKey(key), element, score);
	}

	@Override
	public void treeAdd(String key, Object element, double score, int expireSeconds) throws CacheClientException {

		cacheExecutor.treeAdd(wrapKey(key), element, score, expireSeconds);
	}

	@Override
	public void treeAdd(String key, Object element, double score, Date expireAt) throws CacheClientException {

		cacheExecutor.treeAdd(wrapKey(key), element, score, expireAt);
	}

	@Override
	public void treeRemove(String key, Object element) throws CacheClientException {

		cacheExecutor.treeRemove(wrapKey(key), element);
	}

	@Override
	public Double treeElementScore(String key, Object element) throws CacheClientException {

		return cacheExecutor.treeElementScore(wrapKey(key), element);
	}

	@Override
	public <E> SortedTree<E> treeSort(String key, SortedCond condition) throws CacheClientException {

		return cacheExecutor.treeSort(wrapKey(key), condition);
	}

	@Override
	public void setAdd(String key, Object element) throws CacheClientException {

		cacheExecutor.setAdd(wrapKey(key), element);
	}

	@Override
	public void setAdd(String key, Object element, int expireSeconds) throws CacheClientException {

		cacheExecutor.setAdd(wrapKey(key), element, expireSeconds);
	}

	@Override
	public void setAdd(String key, Object element, Date expireAt) throws CacheClientException {

		cacheExecutor.setAdd(wrapKey(key), element, expireAt);
	}

	@Override
	public void setRemove(String key, Object element) throws CacheClientException {

		cacheExecutor.setRemove(wrapKey(key), element);
	}

	@Override
	public boolean nxSet(String key, Object value) throws CacheClientException {

		return cacheExecutor.nxSet(wrapKey(key), value);
	}

	@Override
	public boolean nxSet(String key, Object value, int expireSeconds) throws CacheClientException {

		return cacheExecutor.nxSet(wrapKey(key), value, expireSeconds);
	}

	@Override
	public boolean nxSet(String key, Object value, Date expireAt) throws CacheClientException {

		return cacheExecutor.nxSet(wrapKey(key), value, expireAt);
	}

	@Override
	public boolean nxSetMapEntry(String key, String mapKey, Object value) throws CacheClientException {

		return cacheExecutor.nxSetMapEntry(wrapKey(key), wrapMapKey(mapKey), value);
	}

	@Override
	public boolean nxSetMapEntry(String key, String mapKey, Object value, int expireSeconds) throws
	                                                                                         CacheClientException {

		return cacheExecutor.nxSetMapEntry(wrapKey(key), wrapMapKey(mapKey), value, expireSeconds);
	}

	@Override
	public boolean nxSetMapEntry(String key, String mapKey, Object value, Date expireAt) throws CacheClientException {

		return cacheExecutor.nxSetMapEntry(wrapKey(key), wrapMapKey(mapKey), value, expireAt);
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("cacheExecutorType", cacheExecutorType);
		builder.append("application", application);
		builder.append("operation", operation);
		builder.append("cacheExecutor", cacheExecutor);
		return builder.toString();
	}
}
