package com.isesol.framework.cache.client.executor;

import com.isesol.framework.cache.client.executor.operation.*;

/**
 * 用于进行读取、存储、删除，以及缓存辅助功能的执行器。在使用 API 方式时，应使用该接口提供的接口对缓存数据进行操作。
 *
 * @see GetOperation
 * @see SetOperation
 * @see DeleteOperation
 * @see AtomicCounterOperation
 * @see KeyOperation
 * @see AppendOperation
 * @see TransactionCacheExecutor
 */
public interface CacheExecutor extends TransactionCacheExecutor {
}
