package com.isesol.framework.cache.client.executor;

import com.isesol.framework.cache.client.message.*;
import com.isesol.framework.cache.client.util.*;
import org.aopalliance.intercept.*;
import org.apache.logging.log4j.*;

import java.lang.reflect.*;
import java.util.*;


/**
 * <p>
 * 用于缓存操作的 Spring AOP 的 <code>Advice</code> 的实现。
 * </p>
 * <p>
 * <p>
 * 主要用于对缓存操作方法的参数有效性（非空）进行检查，以及在缓存操作异常时搜集错误信息。
 * </p>
 */
public class CacheExecutorAdvice implements MessageCollectorAware, MethodInterceptor {

	static Logger log = LogManager.getLogger(CacheExecutorAdvice.class.getName());

	/**
	 * 缓存客户端消息收集器
	 */
	private MessageCollector messageCollector;

	public CacheExecutorAdvice() {

	}

	@Override
	public void setMessageCollector(MessageCollector messageCollector) {

		this.messageCollector = messageCollector;
	}

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {

		CacheInspect inspect = new CacheInspect(invocation);

		log.trace(inspect.getInvokeDetails());

		checkArguments(invocation);

		try {

			return invocation.proceed();
		} catch (Exception e) {

			String messageId = messageCollector.collectMessage(inspect.getInvokeMethodSignature(), null, e);

			log.error("messageId = [{}], {}", messageId, inspect.getInvokeDetails(), e);

			throw e;
		}
	}

	/**
	 * <p>
	 * 检查方法调用参数值的有效性，以确保参数值不为 <code>null</code>。
	 * </p>
	 *
	 * @param invocation
	 */
	private void checkArguments(MethodInvocation invocation) {

		Object[] args = invocation.getArguments();

		if (Tools.isBlank(args)) {
			return;
		}

		Method method = invocation.getMethod();
		Object target = invocation.getThis();

		for (int i = 0; i < args.length; i++) {

			checkArgument(target, method, i, args[i]);
		}
	}

	private void checkArgument(Object target, Method method, int position, Object arg) {

		if (arg == null) {
			checkFailed(target, method, position, null, false);
		}

		if (arg instanceof Map) {
			for (Object key : ((Map<?, ?>) arg).keySet()) {
				if (key == null) {
					checkFailed(target, method, position, "map key", false);
				}
				if ((key instanceof String) && Tools.isBlank((String) key)) {
					checkFailed(target, method, position, "map key", true);
				}
			}
			return;
		}
	}

	private void checkFailed(Object target, Method method, int position, String message, boolean isIncludeEmpty) {

		StringBuilder builder = new StringBuilder();

		builder.append("argument '").append(MethodArgumentNames.getName(target, method, position)).append("'");

		if (message != null) {
			builder.append(' ').append(message);
		}

		builder.append(" is required, it value must be not null");

		if (isIncludeEmpty) {
			builder.append(" or not empty");
		}

		throw new IllegalArgumentException(builder.toString());
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("collector", messageCollector);
		return builder.toString();
	}
}
