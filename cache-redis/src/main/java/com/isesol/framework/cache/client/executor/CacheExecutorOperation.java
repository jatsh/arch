package com.isesol.framework.cache.client.executor;

import com.isesol.framework.cache.client.executor.operation.*;

/**
 * 基础接口：缓存基础 API 接口，接口定义的是涉及缓存处理基础的 API
 *
 * @see GetOperation
 * @see SetOperation
 * @see DeleteOperation
 * @see AtomicCounterOperation
 * @see KeyOperation
 * @see AppendOperation
 */

public interface CacheExecutorOperation
		extends GetOperation, SetOperation, DeleteOperation, AtomicCounterOperation, KeyOperation, AppendOperation {
}
