package com.isesol.framework.cache.client.executor;

import com.isesol.framework.cache.client.executor.serializer.*;

import java.util.*;

/**
 * 缓存数据可序列化操作。用于对象与字节数组之间的相互转换
 *
 * @see CacheSerializer
 */
public interface CacheSerializable {

	/**
	 * 将字符串的 key 转换成为缓存服务器所使用的原生数据
	 *
	 * @param key 缓存数据的 key
	 *
	 * @return 缓存所使用原生的数据
	 */
	byte[] toRawKey(String key);

	/**
	 * 将多个字符串的 key 转换成为缓存服务器所使用的原生数据
	 *
	 * @param keys 多个缓存数据的 key
	 *
	 * @return 缓存所使用原生的数据
	 */
	byte[][] toRawKeys(String... keys);

	/**
	 * 将缓存条目数据的 key 转换成为缓存服务器所使用的原生数据
	 *
	 * @param key 缓存数据的 key
	 * @param field 缓存条目数据的 key
	 *
	 * @return 缓存服务器所使用的原生字节数据
	 */
	byte[] toRawField(String key, String field);

	/**
	 * 将多个缓存条目数据的 key 转换成为缓存服务器所使用的原生数据
	 *
	 * @param key 缓存数据的 key
	 * @param fields 多个缓存条目数据的 key
	 *
	 * @return 缓存服务器所使用的原生字节数据
	 */
	byte[][] toRawFields(String key, String... fields);

	/**
	 * 将缓存数据对象值，转换成为缓存服务器所使用的原生数据
	 *
	 * @param key 缓存数据的 key
	 * @param value 缓存数据对象
	 *
	 * @return 缓存服务器缓存数据所使用的原生字节数据
	 */
	byte[] toRawValue(String key, Object value);

	/**
	 * 将缓存条目数据对象值，转换成为缓存服务器所使用的原生数据。
	 * <p>
	 * 若无特殊要求，可以直接使用 {@link #toRawValue(String, Object)} 的实现。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param field 缓存条目数据的 key
	 * @param value 缓存条目数据值
	 *
	 * @return 缓存服务器缓存条目所使用的原生字节数据
	 */
	byte[] toRawHashValue(String key, String field, Object value);

	/**
	 * <p>
	 * 将多个缓存条目 key 和条目数据，转换成为缓存服务器所使用的原生数据。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param map 多个缓存条目数据（key 和 value）
	 *
	 * @return 缓存服务器多个缓存条目所使用的原生字节数据
	 */
	Map<byte[], byte[]> toRawHash(String key, Map<String, ?> map);

	/**
	 * 反序列化从缓存服务器获取的原生字节的 key 值数据，转换成为 Java 中的字符串格式的 key。
	 *
	 * @param rawKey 缓存服务器使用的原生字节的 key 数据
	 *
	 * @return 字符串格式的 key 数据
	 */
	String deserializeKey(byte[] rawKey);

	/**
	 * 反序列化从缓存服务器获取的原生字节的缓存条目 key 值数据，转换成为 Java 中的字符串格式的缓存条目 key。
	 *
	 * @param rawField 缓存服务器使用的原生字节的缓存条目 key 数据
	 *
	 * @return 字符串格式的缓存条目 key 数据
	 */
	String deserializeField(byte[] rawField);

	/**
	 * 反序列化从缓存服务器获取的原生字节的缓存数据值，转换成为 Java 中的对象格式的缓存数据。
	 *
	 * @param rawValue 缓存服务器使用的原生字节的缓存数据
	 *
	 * @return 对象格式的缓存数据
	 */
	<V> V deserializeValue(byte[] rawValue);
}
