package com.isesol.framework.cache.client.executor;

/**
 * 缓存客户端 API 缓存操作执行器。用于缓存客户端 API 操作的缓存 key 包装 API 缓存的标识数据。
 */
public class ClientApiCacheExecutor extends AbstractCacheExecutor {

	/**
	 * 缓存客户端 API 所产生的缓存 key 的标识
	 */
	private static final String CACHE_EXECUTOR_TYPE = "api";

	public ClientApiCacheExecutor() {

		super(CACHE_EXECUTOR_TYPE);
	}
}
