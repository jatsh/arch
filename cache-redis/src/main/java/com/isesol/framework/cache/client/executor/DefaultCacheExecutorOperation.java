package com.isesol.framework.cache.client.executor;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.operation.*;

import java.util.*;

/**
 * 将操作纳入在一个事务体内回调方法参数
 * {@link CacheExecutorOperation} 的默认实现。
 */
public class DefaultCacheExecutorOperation implements CacheExecutorOperation {

	/**
	 * 委托使用的操作对象
	 */
	private CacheExecutorOperation operation;

	public DefaultCacheExecutorOperation(CacheExecutorOperation operation) {

		this.operation = operation;
	}

	@Override
	public void set(String key, Object value) throws CacheClientException {

		operation.set(key, value);
	}

	@Override
	public void set(String key, Object value, int expireSeconds) throws CacheClientException {

		operation.set(key, value, expireSeconds);
	}

	@Override
	public void set(String key, Object value, Date expireAt) throws CacheClientException {

		operation.set(key, value, expireAt);
	}

	@Override
	public boolean nxSet(String key, Object value) throws CacheClientException {

		return operation.nxSet(key, value);
	}

	@Override
	public boolean nxSet(String key, Object value, int expireSeconds) throws CacheClientException {

		return operation.nxSet(key, value, expireSeconds);
	}

	@Override
	public boolean nxSet(String key, Object value, Date expireAt) throws CacheClientException {

		return operation.nxSet(key, value, expireAt);
	}

	@Override
	public <V> void setMap(String key, Map<String, V> map) throws CacheClientException {

		operation.setMap(key, map);
	}

	@Override
	public <V> void setMap(String key, Map<String, V> map, int expireSeconds) throws CacheClientException {

		operation.setMap(key, map, expireSeconds);
	}

	@Override
	public <V> void setMap(String key, Map<String, V> map, Date expireAt) throws CacheClientException {

		operation.setMap(key, map, expireAt);
	}

	@Override
	public void setMapEntry(String key, String mapKey, Object mapValue) throws CacheClientException {

		operation.setMapEntry(key, mapKey, mapValue);
	}

	@Override
	public void setMapEntry(String key, String mapKey, Object mapValue, int expireSeconds) throws
	                                                                                       CacheClientException {

		operation.setMapEntry(key, mapKey, mapValue, expireSeconds);
	}

	@Override
	public void setMapEntry(String key, String mapKey, Object mapValue, Date expireAt) throws CacheClientException {

		operation.setMapEntry(key, mapKey, mapValue, expireAt);
	}

	@Override
	public boolean nxSetMapEntry(String key, String mapKey, Object value) throws CacheClientException {

		return operation.nxSetMapEntry(key, mapKey, value);
	}

	@Override
	public boolean nxSetMapEntry(String key, String mapKey, Object value, int expireSeconds) throws
	                                                                                         CacheClientException {

		return operation.nxSetMapEntry(key, mapKey, value, expireSeconds);
	}

	@Override
	public boolean nxSetMapEntry(String key, String mapKey, Object value, Date expireAt) throws CacheClientException {

		return operation.nxSetMapEntry(key, mapKey, value, expireAt);
	}

	@Override
	public <V> void setMapEntry(String key, Map<String, V> mapEntry) throws CacheClientException {

		operation.setMapEntry(key, mapEntry);
	}

	@Override
	public <V> void setMapEntry(String key, Map<String, V> mapEntry, int expireSeconds) throws CacheClientException {

		operation.setMapEntry(key, mapEntry, expireSeconds);
	}

	@Override
	public <V> void setMapEntry(String key, Map<String, V> mapEntry, Date expireAt) throws CacheClientException {

		operation.setMapEntry(key, mapEntry, expireAt);
	}

	@Override
	public <T> T get(String key) throws CacheClientException {

		return operation.get(key);
	}

	@Override
	public List<Object> mget(String... keys) throws CacheClientException {

		return operation.mget(keys);
	}

	@Override
	public <V> Map<String, V> getMap(String key) throws CacheClientException {

		return operation.getMap(key);
	}

	@Override
	public <T> T getMapEntry(String key, String mapKey) throws CacheClientException {

		return operation.getMapEntry(key, mapKey);
	}

	@Override
	public <T> List<T> mgetMapEntry(String key, String... mapKeys) throws CacheClientException {

		return operation.mgetMapEntry(key, mapKeys);
	}

	@Override
	public Number increment(String key) throws CacheClientException {

		return operation.increment(key);
	}

	@Override
	public Number increment(String key, String mapKey) throws CacheClientException {

		return operation.increment(key, mapKey);
	}

	@Override
	public Number incrementBy(String key, int delta) throws CacheClientException {

		return operation.incrementBy(key, delta);
	}

	@Override
	public Number incrementBy(String key, String mapKey, int delta) throws CacheClientException {

		return operation.incrementBy(key, mapKey, delta);
	}

	@Override
	public void del(String... keys) throws CacheClientException {

		operation.del(keys);
	}

	@Override
	public void delMapKey(String key, String mapKey) throws CacheClientException {

		operation.delMapKey(key, mapKey);
	}

	@Override
	public Boolean hasKey(String key) throws CacheClientException {

		return operation.hasKey(key);
	}

	@Override
	public Boolean sIsMember(String key, String value) throws CacheClientException {

		return operation.sIsMember(key, value);
	}

	@Override
	public void expire(String key, int seconds) throws CacheClientException {

		operation.expire(key, seconds);
	}

	@Override
	public void expireAt(String key, Date expireAt) throws CacheClientException {

		operation.expireAt(key, expireAt);
	}

	@Override
	public Long ttl(String key) throws CacheClientException {

		return operation.ttl(key);
	}

	@Override
	public void listAppend(String key, Object element) throws CacheClientException {

		operation.listAppend(key, element);
	}

	@Override
	public void listAppend(String key, Object element, int expireSeconds) throws CacheClientException {

		operation.listAppend(key, element, expireSeconds);
	}

	@Override
	public void listAppend(String key, Object element, Date expireAt) throws CacheClientException {

		operation.listAppend(key, element, expireAt);
	}

	@Override
	public Long listLength(String key) throws CacheClientException {

		return operation.listLength(key);
	}

	@Override
	public void listRemoveFirst(String key, Object element) throws CacheClientException {

		operation.listRemoveFirst(key, element);
	}

	@Override
	public <E> E listPopHead(String key) throws CacheClientException {

		return operation.listPopHead(key);
	}

	@Override
	public <E> List<E> listElements(String key) throws CacheClientException {

		return operation.listElements(key);
	}

	@Override
	public void treeAdd(String key, Object element, double score) throws CacheClientException {

		operation.treeAdd(key, element, score);
	}

	@Override
	public void treeAdd(String key, Object element, double score, int expireSeconds) throws CacheClientException {

		operation.treeAdd(key, element, score, expireSeconds);
	}

	@Override
	public void treeAdd(String key, Object element, double score, Date expireAt) throws CacheClientException {

		operation.treeAdd(key, element, score, expireAt);
	}

	@Override
	public void treeRemove(String key, Object element) throws CacheClientException {

		operation.treeRemove(key, element);
	}

	@Override
	public void setRemove(String key, Object element) throws CacheClientException {

		operation.setRemove(key, element);
	}

	@Override
	public Double treeElementScore(String key, Object element) throws CacheClientException {

		return operation.treeElementScore(key, element);
	}

	@Override
	public <E> SortedTree<E> treeSort(String key, SortedCond condition) throws CacheClientException {

		return operation.treeSort(key, condition);
	}

	@Override
	public void setAdd(String key, Object element) throws CacheClientException {

		operation.setAdd(key, element);
	}

	@Override
	public void setAdd(String key, Object element, int expireSeconds) throws CacheClientException {

		operation.setAdd(key, element, expireSeconds);
	}

	@Override
	public void setAdd(String key, Object element, Date expireAt) throws CacheClientException {

		operation.setAdd(key, element, expireAt);
	}
}
