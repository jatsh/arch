package com.isesol.framework.cache.client.executor;

import com.isesol.framework.cache.client.executor.serializer.*;
import com.isesol.framework.cache.client.log.*;
import com.isesol.framework.cache.client.util.*;
import org.springframework.beans.factory.*;

import java.util.*;

public class DefaultCacheSerializable implements CacheSerializable, InitializingBean {

	private static final int DEFAULT_CACHE_ALERT_BYTES = 1 * 1024 * 1024;

	private CacheSerializer<String> keySerializer;

	private CacheSerializer<Object> valueSerializer;

	private int cacheAlertBytes = DEFAULT_CACHE_ALERT_BYTES;

	private CacheLogService alertCacheLog;

	public DefaultCacheSerializable() {

	}

	public CacheSerializer<String> getKeySerializer() {

		return keySerializer;
	}

	public void setKeySerializer(CacheSerializer<String> keySerializer) {

		Assert.notNull(keySerializer, "keySerializer");
		this.keySerializer = keySerializer;
	}

	public CacheSerializer<Object> getValueSerializer() {

		return valueSerializer;
	}

	public void setValueSerializer(CacheSerializer<Object> valueSerializer) {

		Assert.notNull(valueSerializer, "valueSerializer");
		this.valueSerializer = valueSerializer;
	}

	public int getCacheAlertBytes() {

		return cacheAlertBytes;
	}

	public void setCacheAlertBytes(int cacheAlertBytes) {

		this.cacheAlertBytes = cacheAlertBytes;
	}

	public CacheLogService getAlertCacheLog() {

		return alertCacheLog;
	}

	public void setAlertCacheLog(CacheLogService alertCacheLog) {

		Assert.notNull(alertCacheLog, "alertCacheLog");
		this.alertCacheLog = alertCacheLog;
	}

	@Override
	public void afterPropertiesSet() {

		Assert.notNull(alertCacheLog, "cacheLog");
		Assert.notLesser(cacheAlertBytes, 1, "cacheAlertBytes");

		if (getKeySerializer() == null) {
			setKeySerializer(new StringSerializer());
		}

		if (getValueSerializer() == null) {
			setValueSerializer(new FastjsonSerializer());
		}
	}

	@Override
	public Map<byte[], byte[]> toRawHash(String key, Map<String, ?> map) {

		Map<byte[], byte[]> hash = new HashMap<byte[], byte[]>();

		for (Map.Entry<String, ?> entry : map.entrySet()) {

			String field = entry.getKey();

			byte[] rawField = toRawField(key, field);
			byte[] rawValue = toRawHashValue(key, field, entry.getValue());

			hash.put(rawField, rawValue);
		}

		return hash;
	}

	@Override
	public byte[] toRawKey(String key) {

		return getKeySerializer().serialize(key);
	}

	@Override
	public byte[][] toRawKeys(String... keys) {

		if (keys == null) {
			return null;
		}
		byte[][] rawKeys = new byte[keys.length][];
		for (int i = 0; i < keys.length; i++) {
			rawKeys[i] = toRawKey(keys[i]);
		}
		return rawKeys;
	}

	@Override
	public byte[] toRawField(String key, String field) {

		return getKeySerializer().serialize(field);
	}

	@Override
	public byte[][] toRawFields(String key, String... fields) {

		if (fields == null) {
			return null;
		}
		byte[][] rawFields = new byte[fields.length][];
		for (int i = 0; i < fields.length; i++) {
			rawFields[i] = toRawField(key, fields[i]);
		}
		return rawFields;
	}

	@Override
	public byte[] toRawValue(String key, Object value) {

		byte[] bytes = getValueSerializer().serialize(value);

		afterRawValue(key, bytes);

		return bytes;
	}

	@Override
	public byte[] toRawHashValue(String key, String field, Object value) {

		byte[] bytes = getValueSerializer().serialize(value);

		afterRawHashValue(key, field, bytes);

		return bytes;
	}

	@Override
	public String deserializeKey(byte[] bytes) {

		return getKeySerializer().deserialize(bytes);
	}

	@Override
	public String deserializeField(byte[] bytes) {

		return getKeySerializer().deserialize(bytes);
	}

	@SuppressWarnings(Tools.SUPPRESS_WARNINGS_UNCHECKED)
	@Override
	public <V> V deserializeValue(byte[] bytes) {

		return (V) (getValueSerializer().deserialize(bytes));
	}

	private void afterRawValue(String key, byte[] bytes) {

		if (isRecord(bytes)) {
			recordInSet(key, null, bytes);
		}
	}

	private void afterRawHashValue(String key, String field, byte[] bytes) {

		if (isRecord(bytes)) {
			recordInSet(key, field, bytes);
		}
	}

	protected boolean isRecord(byte[] bytes) {

		return (bytes.length >= getCacheAlertBytes());
	}

	protected void recordInSet(String key, String field, byte[] bytes) {

		StringBuilder builder = new StringBuilder();
		builder.append("key = [").append(key).append("]");

		if (field != null) {

			builder.append(", field = [").append(field).append("]");
		}

		builder.append(" value bytes length is [").append(bytes.length).append("]");

		getAlertCacheLog().log(CacheLogService.LogType.LARGE_BYTES, builder.toString());
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("keySerializer", keySerializer);
		builder.append("valueSerializer", valueSerializer);
		builder.append("cacheAlertBytes", cacheAlertBytes);
		builder.append("cacheLog", alertCacheLog);
		return builder.toString();
	}
}
