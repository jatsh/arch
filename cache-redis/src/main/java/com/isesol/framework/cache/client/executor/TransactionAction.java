package com.isesol.framework.cache.client.executor;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.exception.*;

/**
 * 带有事务的回调接口。无论执行的操作是否是带有事务性的,该接口中执行的所有操作都被纳入在一个事务体之中。
 */
public interface TransactionAction {

	/**
	 * 执行被纳入在一个事务体内的操作。无需关心事务处理的逻辑。
	 *
	 * @param operation 缓存操作接口。
	 *
	 * <p>
	 * {@link TransactionAction} 接口的实现中进行的操作都应源自于该接口。
	 * </p>
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他未分类的异常
	 */
	void doInTxAction(CacheExecutorOperation operation) throws CacheClientException;
}
