package com.isesol.framework.cache.client.executor;

import com.isesol.framework.cache.client.exception.*;

/**
 * <p>
 * 带有事务处理回调的缓存处理
 * </p>
 */
public interface TransactionCacheExecutor extends CacheExecutorOperation {

	/**
	 * Default cache data life time in seconds when not specified life time. the
	 * default value is 1800 (30mins)
	 */
	int DEFAULT_EXPIRATION_SECONDS = 1800;

	/**
	 * <p>
	 * 将所有的缓存操作在一个事务体中作为一个整体进行处理
	 * </p>
	 * <p>
	 * <p>
	 * 若单一的缓存操作已开启事务环境时，在方法回调中执行时，该操作的事务会合并至该事务上下文中，作为一个整体执行。
	 * </p>
	 * <p>
	 * <p>
	 * 将需要纳入一个事务体内的缓存操作放置于 {@link TransactionAction} 接口的匿名实现的方法中，示例：
	 * </p>
	 * <p>
	 * <pre>
	 * &#x2f;&#x2f; 将两个 {@link #set(String, Object)} 操作放在一个事务体内执行
	 *
	 * executeInTransaction( <b>new</b> TransactionAction() {
	 *     &#x40;Override
	 *     <b>public</b> <b>void</b> doInTxAction(CacheExecutorOperation operation)
	 *             <b>throws</b> CacheClientException {
	 *         operation.set( "k01", "value01" );
	 *         operation.set( "k02", "value02" );
	 *     }
	 * });
	 * </pre>
	 *
	 * @param tx 需要纳入一个事务体内进行处理的缓存操作回调对象
	 *
	 * @throws CacheClientException
	 */
	void executeInTransaction(TransactionAction tx) throws CacheClientException;

}
