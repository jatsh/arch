package com.isesol.framework.cache.client.executor;

/**
 * 包装客户端缓存数据的 key，用于在客户端 key 的基础上添加一些额外的数据组成实际写入缓存服务器中新的 key。
 */
public interface WrapKey {

	/**
	 * 包装 key 数据间的分隔符
	 */
	String KEY_SEPARATOR = ":";

	/**
	 * 包装处理缓存数据的 key
	 *
	 * @param key 业务应用生成缓存数据原始的 key
	 *
	 * @return 通过包装后生成新的 key，该 key 为缓存服务器实际写入的 key
	 */
	String wrapKey(String key);

	/**
	 * 包装处理 Map 的 key
	 *
	 * @param mapKey 业务应用生成 Map 数据结构 Map 的 key
	 *
	 * @return 通过包装后生成新的 Map key，该 Map key 为缓存服务器实际写入的 Map key
	 */
	String wrapMapKey(String mapKey);

	/**
	 * 包装处理多个缓存数据的 key
	 *
	 * @param keys 多个由业务应用生成缓存数据原始的 key
	 *
	 * @return 通过包装后生成新的 keys，该 keys 为缓存服务器实际写入的 key
	 */
	String[] wrapKeys(String... keys);

	/**
	 * 包装处理多个 Map 的 key
	 *
	 * @param mapKeys 业务应用生成 Map 数据结构多个 Map 的 key
	 *
	 * @return 通过包装后生成新的多个 Map key，该 Map key 为缓存服务器实际写入的 Map key
	 */
	String[] wrapMapKeys(String... mapKeys);
}
