package com.isesol.framework.cache.client.executor.adaptor;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.util.*;

public abstract class AbstractDataTypeAdaptor implements DataTypeAdaptor {

	private final String server;

	private final TemplateOperator template;

	private final Object dataType;

	private String type;

	public AbstractDataTypeAdaptor(String server, Object dataType, TemplateOperator template) {

		Assert.notEmpty(server, "server");
		Assert.notNull(dataType, "dataType");
		Assert.notNull(template, "template");

		this.server = server.toLowerCase();
		this.dataType = dataType;
		this.template = template;
	}

	private String createType() {

		return (server + '-' + toAdaptorType(dataType)).toLowerCase();
	}

	protected final TemplateOperator getTemplate() {

		return template;
	}

	protected final String getType() {

		if (type == null) {
			type = createType();
		}
		return type;
	}

	protected final String getServer() {

		return server;
	}

	protected Object getDataType() {

		return dataType;
	}

	protected AdaptorDataAccessException convertException(byte[] key, Exception e) {

		return new AdaptorDataAccessException(
				"adaptor invoke error, key '" + Tools.toString(key) + "'" + ", data type is '" + getDataType() + "'",
				e);
	}

	protected String toAdaptorType(Object dataType) {

		return dataType.toString();
	}
}
