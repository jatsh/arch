package com.isesol.framework.cache.client.executor.adaptor;

import org.springframework.dao.*;

public class AdaptorDataAccessException extends DataAccessException {

	public AdaptorDataAccessException(String msg, Throwable cause) {

		super(msg, cause);
	}
}
