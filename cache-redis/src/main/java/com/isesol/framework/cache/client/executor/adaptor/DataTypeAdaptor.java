package com.isesol.framework.cache.client.executor.adaptor;

import java.util.*;

/**
 * 数据类型适配器。用于适合用不同缓存数据类型之间的相互转换。
 */
public interface DataTypeAdaptor {

	/**
	 * 适配 GET 操作
	 *
	 * @param key
	 *
	 * @return GET 适配操作返回的对象
	 *
	 * @throws AdaptorDataAccessException
	 */
	Object adaptGet(byte[] key);

	/**
	 * 适配 getElements 操作
	 *
	 * @param key
	 *
	 * @return getElements 适配操作返回的数据集
	 */
	Collection<byte[]> adaptGetElements(byte[] key);
}
