package com.isesol.framework.cache.client.executor.adaptor;

import org.springframework.dao.*;

/**
 * 缓存客户端基础异常：数据类型的适配器不存在
 */
public class DataTypeAdaptorNotFoundException extends DataAccessException {

	public DataTypeAdaptorNotFoundException(String msg) {

		super(msg);
	}
}
