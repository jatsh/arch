package com.isesol.framework.cache.client.executor.adaptor;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.core.io.*;

import java.io.*;
import java.lang.reflect.*;
import java.util.*;

public abstract class DataTypeAdaptors {

	private static final String EXTENDED_ADAPTOR_RESOURCE_NAME = "classpath:/cache-client-extended-adaptor";

	static Logger log = LogManager.getLogger(DataTypeAdaptors.class.getName());

	private final Map<String, DataTypeAdaptor> adaptors;

	public DataTypeAdaptors(TemplateOperator template) {

		this.adaptors = new LinkedHashMap<String, DataTypeAdaptor>();
		log.debug("loadding pre-definie DataTypeAdaptor");

		loadDefaultAdaptor(template);
		log.debug("loadding extended definie DataTypeAdaptor");

		loadExtendedAdaptor(template);
		log.debug("DataTypeAdaptor detail: {}", adaptors);
	}

	public DataTypeAdaptor getAdaptor(Object dataType) {

		String adaptorType = convertDataType(dataType);

		DataTypeAdaptor adaptor = adaptors.get(adaptorType);

		if (adaptor == null) {

			throw new DataTypeAdaptorNotFoundException(
					"data type '" + dataType + "' is unsupported by DataTypeAdaptor");
		}

		return adaptor;
	}

	private String convertDataType(Object dataType) {

		Assert.notNull(dataType, "dataType");

		String adaptorType = doConvertDataType(dataType);

		if (adaptorType != null) {

			return adaptorType;
		}

		return dataType.toString().toLowerCase();
	}

	protected final void register(AbstractDataTypeAdaptor adaptor) {

		Assert.notNull(adaptor, "adaptor");

		String type = adaptor.getType();

		if (adaptors.containsKey(type)) {
			throw new IllegalArgumentException(
					"register type '" + adaptor.getType() + "' DataTypeAdaptor" + ", the type exists, class = " +
					adaptors.get(type).getClass().getName());
		}

		adaptors.put(type, adaptor);
		log.debug("register DataTypeAdaptor, type = '{}', adaptor = '{}'", type, adaptor);
	}

	private void loadExtendedAdaptor(TemplateOperator template) {

		Resource resource = new ClassPathResource(EXTENDED_ADAPTOR_RESOURCE_NAME);

		if (!resource.exists()) {

			return;
		}

		log.debug("load extended adaptor configuration resource");

		BufferedReader reader = null;

		try {

			try {

				reader = new BufferedReader(new InputStreamReader(resource.getInputStream(), Tools.DEFAULT_CHARSET));

				for (String line = null; (line = reader.readLine()) != null; ) {

					registerOneExtendedAdaptor(template, line);
				}

			} finally {

				if (reader != null) {

					reader.close();
				}
			}

		} catch (IOException e) {

			throw new IllegalArgumentException(
					"read extended DataTypeAdaptor resource failed" + ", cause '" + e.getMessage() + "'", e);
		}
	}

	private void registerOneExtendedAdaptor(TemplateOperator template, String className) {

		log.info("register extended adaptor, class name = '{}'", className);

		try {

			Class<?> clazz = Class.forName(className);

			if (!AbstractDataTypeAdaptor.class.isAssignableFrom(clazz)) {

				fail(className, "the class is not a DataTypeAdaptor type");
			}

			Constructor<?> constructor = clazz.getConstructor(TemplateOperator.class);

			register((AbstractDataTypeAdaptor) (constructor.newInstance(template)));

		} catch (ClassNotFoundException e) {

			fail(className, "the class not found");
		} catch (SecurityException e) {

			fail(className, "the class constructor method cannot be accessed");
		} catch (NoSuchMethodException e) {

			fail(
					className,
					"the class has not constructor method that the only argument is not TransactionCacheExecutor " +
					"type");
		} catch (IllegalArgumentException e) {

			fail(
					className,
					"the class has not constructor method cannot accept object that type is TransactionCacheExecutor");
		} catch (Exception e) {

			fail(className, "the class instantiation failed");
		}
	}

	private void fail(String clazz, String message) {

		fail(clazz, message, null);
	}

	private void fail(String clazz, String message, Exception e) {

		String msg = "register extended DataTypeAdaptor class '" + clazz + "'" + ", " + message +
		             ", cannot register the adaptor";

		if (e == null) {
			throw new IllegalArgumentException(msg);
		}

		throw new IllegalArgumentException(msg, e);
	}

	protected abstract String doConvertDataType(Object dataType);

	protected abstract void loadDefaultAdaptor(TemplateOperator template);

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("adaptors", adaptors);
		return builder.toString();
	}
}
