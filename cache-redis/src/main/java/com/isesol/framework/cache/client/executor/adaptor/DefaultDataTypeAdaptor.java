package com.isesol.framework.cache.client.executor.adaptor;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;

import java.util.*;

public class DefaultDataTypeAdaptor extends AbstractDataTypeAdaptor {

	static Logger log = LogManager.getLogger(DefaultDataTypeAdaptor.class.getName());

	public DefaultDataTypeAdaptor(String server, Object dataType, TemplateOperator template) {

		super(server, dataType, template);
	}

	@Override
	public final Object adaptGet(final byte[] key) {

		log.debug("[AdaptGet] key = '{}'", Tools.toString(key));
		try {
			return get(key);
		} catch (CacheClientException e) {
			throw convertException(key, e);
		}
	}

	@Override
	public final Collection<byte[]> adaptGetElements(byte[] key) {

		if (log.isDebugEnabled()) {
			log.debug("[adaptGetElements] key = '{}'", Tools.toString(key));
		}
		try {
			return getElements(key);
		} catch (CacheClientException e) {
			throw convertException(key, e);
		}
	}

	protected Object get(byte[] key) throws CacheClientException {

		throw new UnsupportedOperationException();
	}

	protected Collection<byte[]> getElements(byte[] key) throws CacheClientException {

		throw new UnsupportedOperationException();
	}
}
