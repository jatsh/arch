package com.isesol.framework.cache.client.executor.exception;

import com.isesol.framework.cache.client.exception.*;

/**
 * 缓存服务器连接异常，无法获取缓存服务器连接，或者连接超时，读取数据响应超时等之类的异常。
 */
public class CacheConnectionException extends CacheClientException {

	public CacheConnectionException(String message, Throwable cause) {

		super(message, cause);
	}
}
