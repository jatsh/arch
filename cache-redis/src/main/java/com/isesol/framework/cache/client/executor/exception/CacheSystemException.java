package com.isesol.framework.cache.client.executor.exception;

import com.isesol.framework.cache.client.exception.*;

/**
 * 缓存客户端系统异常，主要用于未分类的 {@link CacheClientException} 异常
 */
public class CacheSystemException extends CacheClientException {

	public CacheSystemException(String message, Throwable cause) {

		super(message, cause);
	}
}
