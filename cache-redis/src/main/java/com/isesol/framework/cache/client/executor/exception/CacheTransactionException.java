package com.isesol.framework.cache.client.executor.exception;

import com.isesol.framework.cache.client.exception.*;

/**
 * 带有事务功能事务处理异常
 */
public class CacheTransactionException extends CacheClientException {

	public CacheTransactionException(String message, Throwable cause) {

		super(message, cause);
	}
}
