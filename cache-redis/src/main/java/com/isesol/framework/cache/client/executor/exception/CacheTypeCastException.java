package com.isesol.framework.cache.client.executor.exception;

import com.isesol.framework.cache.client.exception.*;

/**
 * 缓存操作时，缓存中的数据类型与期望进行操作的数据类型不匹配。
 */
public class CacheTypeCastException extends CacheClientException {

	public CacheTypeCastException(String message, Throwable cause) {

		super(message, cause);
	}
}
