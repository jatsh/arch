package com.isesol.framework.cache.client.executor.operation;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.exception.*;

import java.util.*;

/**
 * 基础接口：处理可追加数据元素的各种数据结构操作，主要针对于链表、有序集等数据结构的操作。
 * <p>
 * 该接口中的方法参数若含有 <code>null</code> 值时，将抛出 {@link IllegalArgumentException}。
 * </p>
 */
public interface AppendOperation {

	/**
	 * <p>
	 * 在缓存链表尾部追加缓存数据，以默认的相对时间重置缓存 key 的生存时间
	 * </p>
	 * <p>
	 * 如果当前缓存链表不存在时，创建一个新的链表并添加缓存数据。在追加数据的同时，将该 key 链表的生存时间重置为缓存的默认时间。
	 * </p>
	 * <p>
	 * 数据当前 key 的数据类型不是链表时，将会抛出 {@link CacheClientException} 异常。
	 * </p>
	 *
	 * @param key 缓存链表的 key
	 * @param element 在链表后追加的缓存数据
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的链表数据添加错误
	 * @see #listAppend(String, Object, int)
	 * @see #listAppend(String, Object, Date)
	 */
	void listAppend(String key, Object element) throws CacheClientException;

	/**
	 * <p>
	 * 在缓存链表尾部追加缓存数据，以相对时间重置缓存 key 的生存时间
	 * </p>
	 * <p>
	 * 如果当前缓存链表不存在时，创建一个新的链表并添加缓存数据。
	 * <p>
	 * 如果 <code>expireSeconds</code> 值大于 0 时会将该 key 链表的生存时间重置为 <code>expireSeconds</code> 所设定的的过期时间
	 * </p>
	 * <b><i>如果该值小于等于 0 时，忽略该方法的调用， 即不会将数据添加至链接的尾部</i></b>。
	 * </p>
	 * <p>
	 * 数据当前 key 的数据类型不是链表时，将会抛出 {@link CacheClientException} 异常。
	 * </p>
	 *
	 * @param key 缓存链表的 key
	 * @param element 在链表后追加的缓存数据
	 * @param expireSeconds 以秒为单位的过期时间。该值应大于 0，若小于或等于 0 时忽略该方法的调用
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的链表数据添加错误
	 * @see #listAppend(String, Object)
	 * @see #listAppend(String, Object, Date)
	 */
	void listAppend(String key, Object element, int expireSeconds) throws CacheClientException;

	/**
	 * <p>
	 * 在缓存链表尾部追加缓存数据，以绝对日期时间重置缓存 key 的生存时间
	 * </p>
	 * <p>
	 * 如果当前缓存链表不存在时，创建一个新的链表并添加缓存数据。
	 * <p>如果 <code>expireAt</code> 所表示的日期时间在当前日期时间之后时，将该 key 链表的生存时间重置为 <code>expireAt</code>所设定的的日期时间</p>
	 * <b><i>如果该值小于等于当前日期时间时，忽略该方法的调用， 即不会将数据添加至链接的尾部</i></b>。
	 * </p>
	 * <p>
	 * 数据当前 key 的数据类型不是链表时，将会抛出 {@link CacheClientException} 异常。
	 * </p>
	 *
	 * @param key 缓存链表的 key
	 * @param element 在链表后追加的缓存数据
	 * @param expireAt 重置缓存数据所在链表的过期时间。过期时间应大于当前日期时间，若小于或等于当前日期时间时忽略该方法的调用
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的链表数据添加错误
	 * @see #listAppend(String, Object)
	 * @see #listAppend(String, Object, int)
	 */
	void listAppend(String key, Object element, Date expireAt) throws CacheClientException;

	/**
	 * <p>
	 * 获取缓存链表中元素的数量
	 * </p>
	 *
	 * @param key 缓存链表的 key
	 *
	 * @return 缓存链表中元素的数量，如果 key 不存在时将返回“-1”。
	 * <p>方法若在事务环境中执行时，一些缓存服务器由于采用命令批量提交的方法进行处理，
	 * 需要在事务提交后才能获取执行的操作结果，因此该方法执行完后可能返回 <code>null</code> 值
	 * </p>
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取链表中元素的数量错误
	 * @see #listElements(String)
	 */
	Long listLength(String key) throws CacheClientException;

	/**
	 * <p>
	 * 移除缓存链表与指定数据匹配的第一个元素
	 * </p>
	 * <p>
	 * 从链表的头开始寻找与指定数据值一致的第一个元素，并将其从缓存链表中移除。
	 * </p>
	 *
	 * @param key 缓存链表的 key
	 * @param element 需要移除的元素
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的移除链表元素错误
	 */
	void listRemoveFirst(String key, Object element) throws CacheClientException;

	/**
	 * <p>
	 * 从链表头部弹出一个元素
	 * </p>
	 * <p>
	 * 若链表中仅有一个元素时，执行该方法后将弹出该元素，并将该链表删除。
	 * </p>
	 *
	 * @param key 缓存链表的 key
	 *
	 * @return 链表头部的第一个元素。若链表不存在，或者链表中无元素时，将返回 <code>null</code> 值。
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的移除链表元素错误
	 */
	<E> E listPopHead(String key) throws CacheClientException;

	/**
	 * <p>
	 * 获取缓存链表中所有的元素
	 * </p>
	 * <p>
	 * 获取到链表元素数据，是以 FIFO 为顺序的。
	 * </p>
	 *
	 * @param key 缓存链表的 key
	 *
	 * @return 缓存链表 key 中以 FIFO 为序所有的元素。如果 key 不存在时，将返回 <code>null</code> 值；
	 * 在事务环境中进行操作时，将返回 <code>null</code> 值
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取链表元数数据错误
	 * @see #listLength(String)
	 */
	<E> List<E> listElements(String key) throws CacheClientException;

	/**
	 * <p>
	 * 指定序值在缓存有序集中添加缓存数据，以默认的相对时间重置缓存 key 的生存时间
	 * <p>
	 * <p>
	 * 数据的序值为 <code>double</code> 类型，由于双精度浮点数据的有效数字关系，因此数据的序值应保证在 15
	 * 个或以下的有效数字范围内，否则可能会导致数值精度丢失，不同的值造成相同的序值结果。
	 * </p>
	 * <p>
	 * 有序值中存放的数据是不重复的，因此添加至缓存有序集中的数据，在当前 key 的缓存有序值中已经存在时，则相当于更新该数据的序值数据，
	 * 操作完成之后，该 key 缓存有序值的数量并不会改变。
	 * </p>
	 * <p>
	 * 如果当前缓存有序集不存在时，创建一个新的缓存有序集并添加缓存数据。在添加数据的同时，将该 key 链表的生存时间重置为缓存的默认时间。
	 * </p>
	 * <p>
	 * 数据当前 key 的数据类型不是有序集时，将会抛出 {@link CacheClientException} 异常。
	 * </p>
	 *
	 * @param key 缓存有序集的 key
	 * @param element 需要添加至缓存有序集的数据
	 * @param score 添加至有序集数据的排序序值。序值越小时，则排序结果越靠前。若序值相同时，则有序集中的元数以字典为序。 由于
	 * <code>double</code> 的精度范围为 15 个或以下数字，若大于 15 个数字时，将会造成序值的精度丢失，
	 * 从而导致实际的序值相同
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的有序集数据添加错误
	 * @see #treeAdd(String, Object, double, int)
	 * @see #treeAdd(String, Object, double, Date)
	 */
	void treeAdd(String key, Object element, double score) throws CacheClientException;

	void setAdd(String key, Object element) throws CacheClientException;

	/**
	 * <p>
	 * 指定序值在缓存有序集中添加缓存数据，以相对时间重置缓存 key 的生存时间
	 * <p>
	 * <p>
	 * 数据的序值为 <code>double</code> 类型，由于双精度浮点数据的有效数字关系，因此数据的序值应保证在 15
	 * 个或以下的有效数字范围内，否则可能会导致数值精度丢失，不同的值造成相同的序值结果。
	 * </p>
	 * <p>
	 * 有序值中存放的数据是不重复的，因此添加至缓存有序集中的数据，在当前 key 的缓存有序值中已经存在时，则相当于更新该数据的序值数据，
	 * 操作完成之后，该 key 缓存有序值的数量并不会改变。
	 * </p>
	 * <p>
	 * 如果当前缓存有序集不存在时，创建一个新的缓存有序集并添加缓存数据。如果 <code>expireSeconds</code> 值大于 0 时会将该
	 * key 有序集的生存时间重置为 <code>expireSeconds</code> 所设定的的过期时间；<b><i>如果该值小于等于 0
	 * 时，忽略该方法的调用，即不会将数据添加至有序集中。</i></b>。
	 * </p>
	 * <p>
	 * 数据当前 key 的数据类型不是有序集时，将会抛出 {@link CacheClientException} 异常。
	 * </p>
	 *
	 * @param key 缓存有序集的 key
	 * @param element 需要添加至缓存有序集的数据
	 * @param score 添加至有序集数据的排序序值。序值越小时，则排序结果越靠前。若序值相同时，则有序集中的元数以字典为序。 由于
	 * <code>double</code> 的精度范围为 15 个或以下数字，若大于 15 个数字时，将会造成序值的精度丢失，
	 * 从而导致实际的序值相同
	 * @param expireSeconds 以秒为单位的过期时间。该值应大于 0，若小于或等于 0 时忽略该方法的调用
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的有序集数据添加错误
	 * @see #treeAdd(String, Object, double)
	 * @see #treeAdd(String, Object, double, Date)
	 */
	void treeAdd(String key, Object element, double score, int expireSeconds) throws CacheClientException;

	void setAdd(String key, Object element, int expireSeconds) throws CacheClientException;

	/**
	 * <p>
	 * 指定序值在缓存有序集中添加缓存数据，以绝对日期时间重置缓存 key 的生存时间。
	 * <p>
	 * <p>
	 * 数据的序值为 <code>double</code> 类型，由于双精度浮点数据的有效数字关系，因此数据的序值应保证在 15
	 * 个或以下的有效数字范围内，否则可能会导致数值精度丢失，不同的值造成相同的序值结果。
	 * </p>
	 * <p>
	 * 有序值中存放的数据是不重复的，因此添加至缓存有序集中的数据，在当前 key 的缓存有序值中已经存在时，则相当于更新该数据的序值数据，
	 * 操作完成之后，该 key 缓存有序值的数量并不会改变。
	 * </p>
	 * <p>
	 * 如果当前缓存有序集不存在时，创建一个新的缓存有序集并添加缓存数据。如果 <code>expireAt</code>
	 * 所表示的日期时间在当前日期时间之后时， 将该 key 有序集的生存时间重置为 <code>expireAt</code>
	 * 所设定的的日期时间；<b><i>如果该值小于等于当前日期时间时， 忽略该方法的调用，即不会将数据添加至有序集中</i></b>。
	 * </p>
	 * <p>
	 * 数据当前 key 的数据类型不是有序集时，将会抛出 {@link CacheClientException} 异常。
	 * </p>
	 *
	 * @param key 缓存有序集的 key
	 * @param element 需要添加至缓存有序集的数据
	 * @param score 添加至有序集数据的排序序值。序值越小时，则排序结果越靠前。若序值相同时，则有序集中的元数以字典为序。 由于
	 * <code>double</code> 的精度范围为 15 个或以下数字，若大于 15 个数字时，将会造成序值的精度丢失，
	 * 从而导致实际的序值相同
	 * @param expireAt 重置缓存数据所在链表的过期时间。过期时间应大于当前日期时间，若小于或等于当前日期时间时忽略该方法的调用
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的有序集数据添加错误
	 * @see #treeAdd(String, Object, double)
	 * @see #treeAdd(String, Object, double, int)
	 */
	void treeAdd(String key, Object element, double score, Date expireAt) throws CacheClientException;

	void setAdd(String key, Object element, Date expireAt) throws CacheClientException;

	/**
	 * <p>
	 * 移除缓存有序集中与指定数据匹配的元素。
	 * </p>
	 *
	 * @param key 缓存有序集的 key
	 * @param element 需要从缓存有序集中移除的数据元素
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的在有序集中移动元素错误
	 */
	void treeRemove(String key, Object element) throws CacheClientException;

	void setRemove(String key, Object element) throws CacheClientException;

	/**
	 * <p>
	 * 获取缓存有序集中指定元素的序值。
	 * </p>
	 *
	 * @param key 缓存有序集的 key
	 * @param element 需要从缓存有序集获取序值的数据元素
	 *
	 * @return 指定数据元素在当前缓存有序集中的序值。若缓存 key 不存在，或者缓存有序集中不存在指定的数据元素时将返回
	 * <code>null</code> 值；如果在事务环境中进行操作时，也将返回 <code>null</code> 值
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取有序集中数据元素的序值
	 */
	Double treeElementScore(String key, Object element) throws CacheClientException;

	/**
	 * <p>
	 * 根据排序条件对有序集中符合条件的数据进行排序。
	 * </p>
	 *
	 * @param key 缓存有序集的 key
	 * @param condition 缓存有序集的排序条件
	 *
	 * @return 缓存有序集排序后的数据集；如果 key 不存在时，将返回空数据量的 {@link SortedTree} 对象。
	 * 在事务环境中进行操作时，将返回 <code>null</code> 值，除此之外均不会返回 <code>null</code> 值
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的有序集排序错误
	 * @see SortedCond
	 * @see SortedTree
	 */
	<E> SortedTree<E> treeSort(String key, SortedCond condition) throws CacheClientException;
}
