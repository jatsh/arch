package com.isesol.framework.cache.client.executor.operation;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.exception.*;

/**
 * 基础接口：处理原子化计数操作
 * <p>
 * 该接口中的方法参数若含有 <code>null</code> 值时，将抛出 {@link IllegalArgumentException}。
 * </p>
 */
public interface AtomicCounterOperation {

	/**
	 * 将 key 的值原子化地增加 1，若 key 不存在时，初始化为 0 值后再进行自增。
	 *
	 * @param key 需要自增数据的 key
	 *
	 * @return 缓存 key 自增后的值
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取缓存对象错误造成的计数失败
	 */
	Number increment(String key) throws CacheClientException;

	/**
	 * 将缓存 key 和缓存条目 key 的值原子化地增加 1，若缓存 key 或者条目 key 不存在时， 初始化为 0 值后再进行自增。
	 *
	 * @param key 需要自增数据的 key
	 * @param mapKey 自增的缓存条目 key
	 *
	 * @return 缓存 key 和缓存条目 key 自增后的值
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取缓存对象错误造成的计数失败
	 */
	Number increment(String key, String mapKey) throws CacheClientException;

	/**
	 * 将 key 的值原子化地增加指定的变化值，若 key 不存在时，初始化为 0 值后再进行自增。 如果变化值小于 0，进行原子化地递减操作。
	 *
	 * @param key 需要自增数据的 key
	 * @param delta 需要原子化处理的变化量，大于 0 时进行原子化地自增操作， 小于 0 时进行原子化地递减操作
	 *
	 * @return 缓存 key 增加变化量后的值
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取缓存对象错误造成的计数失败
	 */
	Number incrementBy(String key, int delta) throws CacheClientException;

	/**
	 * 将缓存 key 和缓存条目 key 的值原子化地增加指定的变化量，若缓存 key 或者条目 key 不存在时， 初始化为 0
	 * 值后再进行自增。如果变化值小于 0，进行原子化地递减操作。
	 *
	 * @param key 需要自增数据的 key
	 * @param mapKey 自增的缓存条目 key
	 * @param delta 需要原子化处理的变化量，大于 0 时进行原子化地自增操作， 小于 0 时进行原子化地递减操作
	 *
	 * @return 缓存 key 和缓存条目 key 增加变化量后的值
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取缓存对象错误造成的计数失败
	 */
	Number incrementBy(String key, String mapKey, int delta) throws CacheClientException;
}
