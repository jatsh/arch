package com.isesol.framework.cache.client.executor.operation;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.exception.*;

/**
 * 基础接口：处理各种删除缓存数据的操作
 * <p>
 * 该接口中的方法参数若含有 <code>null</code> 值时，将抛出 {@link IllegalArgumentException}。
 * </p>
 */
public interface DeleteOperation {

	/**
	 * 指定需要删除缓存数据的 key 值，删除一个或者多个缓存数据。
	 *
	 * @param keys 定位一个或者多个缓存数据的 key 值
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常造成的删除缓存数据错误
	 */
	void del(String... keys) throws CacheClientException;

	/**
	 * 指定需要删除缓存条目数据的 mapKey 值。如果当前缓存 key 仅有一个 entry 数据时， 执行删除这个 entry 的 mapKey
	 * 后，同时会清除这个 key。
	 *
	 * @param key 缓存条目的 key
	 * @param mapKey 缓存条目数据的 mapKey
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常造成的删除缓存数据错误
	 */
	void delMapKey(String key, String mapKey) throws CacheClientException;
}
