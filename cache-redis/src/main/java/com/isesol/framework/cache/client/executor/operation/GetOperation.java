package com.isesol.framework.cache.client.executor.operation;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.exception.*;

import java.util.*;

/**
 * 基础接口：处理各种获取数据缓存的操作
 * <p>
 * 该接口中的方法参数若含有 <code>null</code> 值时，将抛出 {@link IllegalArgumentException}。
 * </p>
 */
public interface GetOperation {

	/**
	 * <p>
	 * 通过缓存的 key 获取缓存服务器中还在有效期内的缓存数据。
	 * </p>
	 * <p>
	 * 该方法将返回一个对象，若缓存中的数据对象与接收该方法返回值的对象不是同一类型时将会抛出 {@link ClassCastException}。
	 * </p>
	 * 在事务环境中进行操作时，将返回 <code>null</code> 值。
	 * </p>
	 *
	 * @param key 需要查询缓存数据的 key
	 *
	 * @return 缓存服务中对应 key 的缓存数据所转换后的对象。该方法在缓存服务中不存在 key 为指定 key 值的缓存数据时，将返回 <code>null</code> 值
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取缓存对象错误
	 */
	<T> T get(String key) throws CacheClientException;

	/**
	 * <p>
	 * 通过缓存的 key 获取缓存服务器还在有效期内的 <code>Map</code> 格式的缓存数据。
	 * </p>
	 * <p>
	 * 该方法将返回一个 <code>Map</code> 对象
	 * 若缓存中这个 key 所表示的数据对象无法转换成为 <code>Map</code>对象，
	 * 或者与 <code>Map</code> 中 value 值类型不一致时时将会抛出 {@link ClassCastException}。
	 * </p>
	 *
	 * @param key 需要查询 <code>Map</code> 格式缓存数据的 key
	 *
	 * @return 缓存服务中对应 key 的缓存数据所转换后的 <code>Map</code> 对象。
	 * <p>
	 * 该方法在缓存服务中不存在 key 为指定
	 * key 值的缓存数据时，将返回 <code>null</code> 值
	 * </p>
	 * <p>
	 * 在事务环境中进行操作时，也将返回 <code>null</code> 值
	 * </p>
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取缓存对象错误
	 */
	<V> Map<String, V> getMap(String key) throws CacheClientException;

	/**
	 * <p>
	 * 通过缓存数据的 key 获取缓存服务器还在有效期内缓存数据，该方法可以一次性取出多个缓存数据。
	 * </p>
	 * <p>
	 * 该方法将返回一个 <code>List&lt;Object&gt;</code> 对象，<code>List</code> 的大小与参数数组
	 * keys 中 <code>length</code> 一致，keys 中 key 所获取的缓存数据位于 <code>List</code>
	 * 返回值相应的索引点上。
	 * </p>
	 * <p>
	 * 若参数 keys 为空或者为 <code>null</code> 值时将返回空的 <code>List</code> 的对象， 但不会返回
	 * <code>null</code> 值
	 * <p>若参数 keys 索引点上的 key 无法获缓存数据时，或者该 key 的类型是一个
	 * <code>Map</code> 类型，则返回值 <code>List</code> 对象中相对应索引中的值为 <code>null</code>
	 * 。
	 * </p>
	 *
	 * @param keys 需要查询各缓存数据 keys 的数组
	 *
	 * @return 缓存服务中对应 key 的缓存数据所组成的 <code>List</code> 对象
	 * <p>其中若有 key 无法获取缓存数据时，则相对应的 <code>List</code> 索引上的数据值为 <code>null</code> 值
	 * </p>
	 * <p>
	 * 在事务环境中进行操作时，将返回 <code>null</code> 值
	 * </p>
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取缓存对象错误
	 */
	List<Object> mget(String... keys) throws CacheClientException;

	/**
	 * <p>
	 * 通过 <code>Map</code> 格式缓存数据的 key 和 <code>Map</code> 的 key 获取
	 * <code>Map</code> 格式缓存数据中 map entry 的所表示的对象。
	 * </p>
	 * <p>
	 * 该方法将返回一个对象，若缓存中的数据对象与接收该方法返回值的对象不是同一类型时将会抛出 {@link ClassCastException}。
	 * </p>
	 *
	 * @param key 需要查询 <code>Map</code> 格式缓存数据的 key
	 * @param mapKey <code>Map</code> 格式缓存数据条目的 key
	 *
	 * @return 指定 Map 缓存数据的条目值。若 key 不存在，或者 mapKey 不存在时都将返回 <code>null</code> 值
	 * <p>
	 * 在事务环境中进行操作时，将返回 <code>null</code> 值
	 * </p>
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取缓存对象错误
	 */
	<T> T getMapEntry(String key, String mapKey) throws CacheClientException;

	/**
	 * <p>
	 * 通过 <code>Map</code> 格式缓存数据的 key 和多个 <code>Map</code> 的 key 获取
	 * <code>Map</code> 格式缓存数据中多个 map entry 所表示的对象，该方法可以一次性取出多个缓存数据。
	 * </p>
	 * <p>
	 * 该方法将返回一个 <code>List&lt;T&gt;</code> 对象，<code>List</code> 的大小与参数数组 mapKeys
	 * 中 <code>length</code> 一致，mapKeys 中 mapKey 所获取的缓存数据位于 <code>List</code>
	 * 返回值相应的索引点上。
	 * </p>
	 * <p>
	 * 若参数 mapKeys 为空或者为 <code>null</code> 值时将返回空的 <code>List</code> 的对象， 但不会返回
	 * <code>null</code> 值。
	 * </p>
	 * <p>若参数 mapKeys 索引点上的 mapKey 无法获缓存数据时，则返回值
	 * <code>List</code> 对象中相对应索引中的值为 <code>null</code>。
	 * </p>
	 *
	 * @param key 需要查询各缓存数据 key 的数组
	 *
	 * @return 缓存服务中对应 key 的缓存数据所组成的 <code>List</code> 对象。
	 * <p>其中若有 key 无法获取缓存数据时， 则相对应的 <code>List</code> 索引上的数据值为 <code>null</code> 值
	 * </p>
	 * <p>
	 * 在事务环境中进行操作时，将返回 <code>null</code> 值
	 * </p>
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的获取缓存对象错误
	 */
	<T> List<T> mgetMapEntry(String key, String... mapKeys) throws CacheClientException;
}
