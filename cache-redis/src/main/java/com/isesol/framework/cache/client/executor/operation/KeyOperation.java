package com.isesol.framework.cache.client.executor.operation;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.exception.*;

import java.util.*;

/**
 * 基础接口：处理各种缓存数据 key 的操作
 * <p>
 * 该接口中的方法参数若含有 <code>null</code> 值时，将抛出 {@link IllegalArgumentException}。
 * </p>
 */
public interface KeyOperation {

	/**
	 * 检查缓存服务中是否含有指定 key 有效的缓存数据。
	 *
	 * @param key 缓存数据的 key
	 *
	 * @return 是否指定 key 的缓存数据。若当前环境不允许支持该操作时将返回 <code>null</code> 值
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheSystemException 或者其他异常造成的缓存操作错误
	 */
	Boolean hasKey(String key) throws CacheClientException;

	Boolean sIsMember(String key, String value) throws CacheClientException;

	/**
	 * <p>
	 * 重置尚在有效时间范围内缓存数据的生存时间。
	 * </p>
	 * <p>
	 * <b>注意：</b>数据缓存的时间应大于 0，若小于或者等于 0 时，则忽略对缓存数据的过期时间进行重置操作。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param seconds 重置缓存数据的生存时间（单位：秒）
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheSystemException 或者其他异常造成的缓存操作错误
	 */
	void expire(String key, int seconds) throws CacheClientException;

	/**
	 * <p>
	 * 重置尚在有效时间范围内缓存数据的截止有效时间。
	 * </p>
	 * <p>
	 * <b>注意：</b>缓存数据的有效截止时间应在当前时间之后，若在当前时间之前时，则忽略对缓存数据的截止有效时间进行重置操作。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param expireAt 重置缓存数据的截止有效时间
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheSystemException 或者其他异常造成的缓存操作错误
	 */
	void expireAt(String key, Date expireAt) throws CacheClientException;

	/**
	 * 获取指定的 key 剩余的生存时间（秒）。
	 *
	 * @param key 缓存数据的 key
	 *
	 * @return 指定 key 剩余的生存时间（秒）
	 * <p>
	 * 如果 key 不存在时，将返回“-1”
	 * </p>
	 * <p>若在事务内时，该操作将会返回 <code>null</code> 值
	 * </p>
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheSystemException 或者其他异常造成的缓存操作错误
	 * @see #expire(String, int)
	 * @see #expireAt(String, Date)
	 * @see SetOperation#set(String, Object, int)
	 * @see SetOperation#set(String, Object, Date)
	 * @see SetOperation#setMap(String, Map, int)
	 * @see SetOperation#setMap(String, Map, Date)
	 */
	Long ttl(String key) throws CacheClientException;
}
