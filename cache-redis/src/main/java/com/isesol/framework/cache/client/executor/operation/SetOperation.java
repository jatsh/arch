package com.isesol.framework.cache.client.executor.operation;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.exception.*;

import java.util.*;

/**
 * 基础接口：处理各种缓存数据存储的操作
 * <p>
 * 该接口中的方法参数若含有 <code>null</code> 值时，将抛出 {@link IllegalArgumentException}。
 * </p>
 */
public interface SetOperation {

	/**
	 * <p>
	 * 整存整取的方式存储缓存数据
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key 作为该缓存的键，使用默认缓存数据的有效生存时间，整存的方式保存一个缓存对象。使用该方法整存的数据，只能通过整取的方式获取。
	 * </p>
	 * <p>
	 * 如果所对应的 key 已经存在时，该方法会覆写 key 之前的缓存数据。
	 * </p>
	 * <p>
	 * 默认的过期时间可以在 <code>&lt;cache-executor&gt;</code> 的 <code>default-expires</code> 属性中设置。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param value 缓存的数据对象
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #setMap(String, Map)
	 * @see GetOperation#get(String)
	 */
	void set(String key, Object value) throws CacheClientException;

	/**
	 * <p>
	 * 整存整取的方式存储缓存数据
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key
	 * 作为该缓存的键，指定缓存数据的有效生存时间，整存的方式保存一个缓存对象。使用该方法整存的数据，只能通过整取的方式获取。
	 * </p>
	 * <p>
	 * 如果所对应的 key 已经存在时，该方法会覆写 key 之前的缓存数据。
	 * </p>
	 * <p>
	 * <b>注意：</b>数据缓存的时间应大于 0，若小于或者等于 0 时，则忽略该缓存数据的存储操作。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param value 缓存的数据对象
	 * @param expireSeconds 数据缓存的时间（单位：秒），该值应大于 0
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #setMap(String, Map, int)
	 * @see GetOperation#get(String)
	 */
	void set(String key, Object value, int expireSeconds) throws CacheClientException;

	/**
	 * <p>
	 * 整存整取的方式存储缓存数据
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key
	 * 作为该缓存的键，指定缓存数据的有效截止时间，整存的方式保存一个缓存对象。使用该方法整存的数据，只能通过整取的方式获取。
	 * </p>
	 * <p>
	 * 如果所对应的 key 已经存在时，该方法会覆写 key 之前的缓存数据。
	 * </p>
	 * <p>
	 * <b>注意：</b>缓存数据的有效截止时间应在当前时间之后，若在当前时间之前时，则忽略该缓存数据的存储操作。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param value 缓存的数据对象
	 * @param expireAt 缓存数据的有效截止时间，该参数所表示的时间应在当前时间之后
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #setMap(String, Map, Date)
	 * @see GetOperation#get(String)
	 */
	void set(String key, Object value, Date expireAt) throws CacheClientException;

	/**
	 * <p>
	 * 整存整取、整存零取的方式存储缓存数据
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key 作为该 <code>Map</code> 缓存的键，使用默认缓存数据的有效生存时间，
	 * 整存整取或零取的方式保存一个缓存对象。使用该方法整存的数据可以整取所有数据，也可只取其中的一个条目数据。
	 * </p>
	 * <p>
	 * 如果所对应的 key 已经存在时，该方法会覆写 key 之前的缓存数据。
	 * </p>
	 * <p>
	 * 默认的过期时间可以在 <code>&lt;cache-executor&gt;</code> 的
	 * <code>default-expires</code> 属性中设置。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param map <code>Map</code> 结构的数据缓存对象
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object)
	 * @see GetOperation#getMap(String)
	 * @see GetOperation#getMapEntry(String, String)
	 */
	<V> void setMap(String key, Map<String, V> map) throws CacheClientException;

	/**
	 * <p>
	 * 整存整取、整存零取的方式存储缓存数据
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key 作为该 <code>Map</code> 缓存的键，指定缓存数据的有效生存时间，
	 * 整存整取或零取的方式保存一个缓存对象。使用该方法整存的数据可以整取所有数据，也可只取其中的一个条目数据。
	 * </p>
	 * <p>
	 * 如果所对应的 key 已经存在时，该方法会覆写 key 之前的缓存数据。
	 * </p>
	 * <p>
	 * <b>注意：</b>数据缓存的时间应大于 0，若小于或者等于 0 时，则忽略该缓存数据的存储操作。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param map <code>Map</code> 结构的数据缓存对象
	 * @param expireSeconds 数据缓存的时间（单位：秒），该值应大于 0
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object, int)
	 * @see GetOperation#get(String)
	 * @see GetOperation#getMapEntry(String, String)
	 */
	<V> void setMap(String key, Map<String, V> map, int expireSeconds) throws CacheClientException;

	/**
	 * <p>
	 * 整存整取、整存零取的方式存储缓存数据
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key 作为该 <code>Map</code> 缓存的键，指定缓存数据的有效截止时间，
	 * 整存整取或零取的方式保存一个缓存对象。使用该方法整存的数据可以整取所有数据，也可只取其中的一个条目数据。
	 * </p>
	 * <p>
	 * 如果所对应的 key 已经存在时，该方法会覆写 key 之前的缓存数据。
	 * </p>
	 * <p>
	 * <b>注意：</b>缓存数据的有效截止时间应在当前时间之后，若在当前时间之前时，则忽略该缓存数据的存储操作。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param map <code>Map</code> 结构的数据缓存对象
	 * @param expireAt 缓存数据的有效截止时间，该参数所表示的时间应在当前时间之后
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object, Date)
	 * @see GetOperation#get(String)
	 * @see GetOperation#getMapEntry(String, String)
	 */
	<V> void setMap(String key, Map<String, V> map, Date expireAt) throws CacheClientException;

	/**
	 * <p>
	 * 更新或者存储缓存数据的一个条目数据
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key 作为该 <code>Map</code> 缓存的键，以及期望缓存数据条目的 key 保存数据对象。
	 * </p>
	 * <p>
	 * 如果所对应的 key 不存在时，则创建一个新的数据缓存 key，并将该 key 的生存时间重置为缓存的默认时间。
	 * </p>
	 * <p>
	 * 若缓存 key 已经存在时，该方法会覆写 mapKey 之前的条目缓存数据，否则则在数据缓存 key 基础之上新增一个条目数据， 并不重置该
	 * key 的生存时间。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param mapKey 缓存数据中条目数据的 key
	 * @param mapValue 需要缓存的条目数据
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object)
	 * @see #setMapEntry(String, String, Object, int)
	 * @see #setMapEntry(String, String, Object, Date)
	 * @see GetOperation#getMapEntry(String, String)
	 */
	void setMapEntry(String key, String mapKey, Object mapValue) throws CacheClientException;

	/**
	 * <p>
	 * 更新或者存储缓存数据的一个条目数据，以相对时间设置/重置缓存 key 的生存时间
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key 作为该 <code>Map</code> 缓存的键，以及期望缓存数据条目的 key 保存数据对象。
	 * </p>
	 * <p>
	 * 如果所对应的 key 不存在时，则创建一个新的数据缓存 key。若条目的 key 已经存在时，该方法会覆写 mapKey
	 * 之前的条目缓存数据，否则则在数据缓存 key 基础之上新增一个条目数据。
	 * </p>
	 * <p>
	 * 如果当前缓存 key 数据不存在时，会新增一个条目类型的 key，并将该 key 的生存时间设为
	 * <code>expireSeconds</code> 所设定的过期时间。如果当前缓存 key 已经存在时，则将该 key 的生存时间重置为
	 * <code>expireSeconds</code> 所设定的过期时间。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param mapKey 缓存数据中条目数据的 key
	 * @param mapValue 需要缓存的条目数据
	 * @param expireSeconds 以秒为单位的过期时间。该值应大于 0 时，设置/重置缓存 key 的生存时间。该值若小于或等于 0 时该方法将退化成为
	 * {@link #setMapEntry(String, String, Object)} 的调用
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object)
	 * @see #setMapEntry(String, String, Object)
	 * @see #setMapEntry(String, String, Object, Date)
	 * @see GetOperation#getMapEntry(String, String)
	 */
	void setMapEntry(String key, String mapKey, Object mapValue, int expireSeconds) throws CacheClientException;

	/**
	 * <p>
	 * 更新或者存储缓存数据的一个条目数据，以绝对日期时间重置缓存 key 的生存时间
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key 作为该 <code>Map</code> 缓存的键，以及期望缓存数据条目的 key 保存数据对象。
	 * </p>
	 * <p>
	 * 如果所对应的 key 不存在时，则创建一个新的数据缓存 key。若条目的 key 已经存在时，该方法会覆写 mapKey
	 * 之前的条目缓存数据，否则则在数据缓存 key 基础之上新增一个条目数据。
	 * </p>
	 * <p>
	 * 如果当前缓存 key 数据不存在时，会新增一个条目类型的 key，并将该 key 的生存时间设为 <code>expireAt</code>
	 * 所设定的的日期时间。 如果当前缓存 key 已经存在时，则将该 key 的生存时间重置为 <code>expireAt</code>
	 * 所设定的的日期时间。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param mapKey 缓存数据中条目数据的 key
	 * @param mapValue 需要缓存的条目数据
	 * @param expireAt 重置缓存数据所在链表的过期时间。过期时间应大于当前日期时间，设置/重置缓存 key 的生存时间。若小于或等于当前日期时间时
	 * 时该方法将退化成为 {@link #setMapEntry(String, String, Object)} 的调用
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object)
	 * @see #setMapEntry(String, String, Object)
	 * @see #setMapEntry(String, String, Object, int)
	 * @see GetOperation#getMapEntry(String, String)
	 */
	void setMapEntry(String key, String mapKey, Object mapValue, Date expireAt) throws CacheClientException;

	/**
	 * <p>
	 * 更新或者存储缓存数据的多个条目数据
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key 作为该 <code>Map</code> 缓存的键，以及期望缓存多个数据条目的 key 保存数据对象。
	 * </p>
	 * <p>
	 * 如果所对应的 key 不存在时，则创建一个新的数据缓存 key，并将该 key 的生存时间重置为缓存的默认时间。
	 * </p>
	 * <p>
	 * 若缓存 key 已经存在时，该方法会覆写 mapKey 之前的条目缓存数据，否则则在数据缓存 key 基础之上新增一个条目数据， 并不重置该
	 * key 的生存时间。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param mapEntry 多个条目缓存的 key 和条目数据
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object)
	 * @see #setMapEntry(String, Map, int)
	 * @see #setMapEntry(String, Map, Date)
	 * @see GetOperation#getMapEntry(String, String)
	 */
	<V> void setMapEntry(String key, Map<String, V> mapEntry) throws CacheClientException;

	/**
	 * <p>
	 * 更新或者存储缓存数据的多个条目数据，以相对时间设置/重置缓存 key 的生存时间
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key 作为该 <code>Map</code> 缓存的键，以及期望缓存数据条目的 key 保存数据对象。
	 * </p>
	 * <p>
	 * 如果所对应的 key 不存在时，则创建一个新的数据缓存 key。若条目的 key 已经存在时，该方法会覆写 mapKey
	 * 之前的条目缓存数据，否则则在数据缓存 key 基础之上新增条目数据。
	 * </p>
	 * <p>
	 * 如果当前缓存 key 数据不存在时，会新增一个条目类型的 key，并将该 key 的生存时间设为
	 * <code>expireSeconds</code> 所设定的过期时间。如果当前缓存 key 已经存在时，则将该 key 的生存时间重置为
	 * <code>expireSeconds</code> 所设定的过期时间。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param mapEntry 多个条目缓存的 key 和条目数据
	 * @param expireSeconds 以秒为单位的过期时间。该值应大于 0 时，设置/重置缓存 key 的生存时间。该值若小于或等于 0 时该方法将退化成为
	 * {@link #setMap(String, Map)} 的调用
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object)
	 * @see #setMapEntry(String, Map)
	 * @see #setMapEntry(String, Map, Date)
	 * @see GetOperation#getMapEntry(String, String)
	 */
	<V> void setMapEntry(String key, Map<String, V> mapEntry, int expireSeconds) throws CacheClientException;

	/**
	 * <p>
	 * 更新或者存储缓存数据的多个条目数据，以绝对日期时间重置缓存 key 的生存时间
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key 作为该 <code>Map</code> 缓存的键，以及期望缓存数据条目的 key 保存数据对象。
	 * </p>
	 * <p>
	 * 如果所对应的 key 不存在时，则创建一个新的数据缓存 key。若条目的 key 已经存在时，该方法会覆写 mapKey
	 * 之前的条目缓存数据，否则则在数据缓存 key 基础之上新增条目数据。
	 * </p>
	 * <p>
	 * 如果当前缓存 key 数据不存在时，会新增一个条目类型的 key，并将该 key 的生存时间设为 <code>expireAt</code>
	 * 所设定的的日期时间。 如果当前缓存 key 已经存在时，则将该 key 的生存时间重置为 <code>expireAt</code>
	 * 所设定的的日期时间。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param mapEntry 多个条目缓存的 key 和条目数据
	 * @param expireAt 重置缓存数据所在链表的过期时间。过期时间应大于当前日期时间，设置/重置缓存 key 的生存时间。若小于或等于当前日期时间时
	 * 时该方法将退化成为 {@link #setMap(String, Map)} 的调用
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object)
	 * @see #setMapEntry(String, Map)
	 * @see #setMapEntry(String, Map, int)
	 * @see GetOperation#getMapEntry(String, String)
	 */
	<V> void setMapEntry(String key, Map<String, V> mapEntry, Date expireAt) throws CacheClientException;

	/**
	 * <p>
	 * 整存整取的方式存储缓存数据，当且仅当对应的 key 不存在时进行存储。
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key
	 * 作为该缓存的键，使用默认缓存数据的有效生存时间，整存的方式保存一个缓存对象。使用该方法整存的数据，只能通过整取的方式获取。
	 * </p>
	 * <p>
	 * 如果所对应的 key 已经存在时，该方法会将不会覆盖已有的数据。
	 * </p>
	 * <p>
	 * 默认的过期时间可以在 <code>&lt;cache-executor&gt;</code> 的
	 * <code>default-expires</code> 属性中设置。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param value 缓存的数据对象
	 *
	 * @return 数据是是否被保存。true: 当前缓存中没有相应的 key 数据，且数据已经保存成功；false: 当前缓存中已经存在相应的
	 * key 数据，数据未能保存成功。
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object)
	 * @see GetOperation#get(String)
	 */
	boolean nxSet(String key, Object value) throws CacheClientException;

	/**
	 * <p>
	 * 整存整取的方式存储缓存数据，当且仅当对应的 key 不存在时进行存储。
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key
	 * 作为该缓存的键，指定缓存数据的有效生存时间，整存的方式保存一个缓存对象。使用该方法整存的数据，只能通过整取的方式获取。
	 * </p>
	 * <p>
	 * 如果所对应的 key 已经存在时，该方法会将不会覆盖已有的数据。
	 * </p>
	 * <p>
	 * <b>注意：</b>数据缓存的时间应大于 0，若小于或者等于 0 时，则忽略该缓存数据的存储操作。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param value 缓存的数据对象
	 * @param expireSeconds 数据缓存的时间（单位：秒），该值应大于 0
	 *
	 * @return 数据是是否被保存。true: 当前缓存中没有相应的 key 数据，且数据已经保存成功；false: 当前缓存中已经存在相应的
	 * key 数据，数据未能保存成功。
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object, int)
	 * @see GetOperation#get(String)
	 */
	boolean nxSet(String key, Object value, int expireSeconds) throws CacheClientException;

	/**
	 * <p>
	 * 整存整取的方式存储缓存数据，当且仅当对应的 key 不存在时进行存储。
	 * </p>
	 * <p>
	 * 使用方法参数中指定的 key
	 * 作为该缓存的键，指定缓存数据的有效截止时间，整存的方式保存一个缓存对象。使用该方法整存的数据，只能通过整取的方式获取。
	 * </p>
	 * <p>
	 * 如果所对应的 key 已经存在时，该方法会将不会覆盖已有的数据。
	 * </p>
	 * <p>
	 * <b>注意：</b>缓存数据的有效截止时间应在当前时间之后，若在当前时间之前时，则忽略该缓存数据的存储操作。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param value 缓存的数据对象
	 * @param expireAt 缓存数据的有效截止时间，该参数所表示的时间应在当前时间之后
	 *
	 * @return 数据是是否被保存。true: 当前缓存中没有相应的 key 数据，且数据已经保存成功；false: 当前缓存中已经存在相应的
	 * key 数据，数据未能保存成功。
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #set(String, Object, Date)
	 * @see GetOperation#get(String)
	 */
	boolean nxSet(String key, Object value, Date expireAt) throws CacheClientException;

	/**
	 * <p>
	 * 设置指定 key 与 mapKey 的缓存数据，当且仅当 mapKey 不存在。当 mapKey 的值设置成功后，以默认的相对时间设置/重置缓存
	 * key 的生存时间。
	 * </p>
	 * <p>
	 * 如果当前缓存 key 数据不存在时，会新增一个条目类型的 key，同时设置 mapKey 所对应的值，并将该 key 的生存时间设为
	 * 默认的相对过期时间。 如果当前缓存 key 已经存在时，且 mapKey 不存在时，设置 mapKey 所对应的值，同时将该 key
	 * 的生存时间重置为 默认的相对过期时间。 如果 mapKey 已经存在时，则不作任何处理。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param mapKey 缓存数据中条目数据的 key
	 * @param value 需要缓存的条目数据
	 *
	 * @return 数据是是否被保存。true: 当前缓存中没有相应的 key 数据，且数据已经保存成功；false: 当前缓存中已经存在相应的
	 * key 数据，数据未能保存成功。
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #nxSet(String, Object)
	 * @see #nxSetMapEntry(String, String, Object, int)
	 * @see #nxSetMapEntry(String, String, Object, Date)
	 * @see GetOperation#getMapEntry(String, String)
	 */
	boolean nxSetMapEntry(String key, String mapKey, Object value) throws CacheClientException;

	/**
	 * <p>
	 * 设置指定 key 与 mapKey 的缓存数据，当且仅当 mapKey 不存在。当 mapKey 的值设置成功后，以指定的相对时间设置/重置缓存
	 * key 的生存时间。
	 * </p>
	 * <p>
	 * 如果当前缓存 key 数据不存在时，会新增一个条目类型的 key，同时设置 mapKey 所对应的值，并将该 key 的生存时间设为
	 * 指定的相对过期时间。 如果当前缓存 key 已经存在时，且 mapKey 不存在时，设置 mapKey 所对应的值，同时将该 key
	 * 的生存时间重置为 指定的相对过期时间。 如果 mapKey 已经存在时，则不作任何处理。
	 * </p>
	 * <p>
	 * <b>注意：</b>数据缓存的时间应大于 0，若小于或者等于 0 时，则忽略该缓存数据的存储操作。
	 * </p>
	 *
	 * @param key 缓存数据的 key
	 * @param mapKey 缓存数据中条目数据的 key
	 * @param value 需要缓存的条目数据
	 * @param expireSeconds 数据缓存的时间（单位：秒），该值应大于 0
	 *
	 * @return 数据是是否被保存。true: 当前缓存中没有相应的 key 数据，且数据已经保存成功；false: 当前缓存中已经存在相应的
	 * key 数据，数据未能保存成功。
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常造成的存储缓存对象错误
	 * @see #nxSet(String, Object)
	 * @see #nxSetMapEntry(String, String, Object)
	 * @see #nxSetMapEntry(String, String, Object, Date)
	 * @see GetOperation#getMapEntry(String, String)
	 */
	boolean nxSetMapEntry(String key, String mapKey, Object value, int expireSeconds) throws CacheClientException;

	boolean nxSetMapEntry(String key, String mapKey, Object value, Date expireAt) throws CacheClientException;
}
