package com.isesol.framework.cache.client.executor.operation;

import com.isesol.framework.cache.client.util.*;

/**
 * <p>
 * 缓存有序集数据排序的条件
 * </p>
 * <p>
 * 指定缓存有序集数据排序的相关条件,主要可以设定排序的序值的范围、升序或降序，以及数据选取的范围等条件。
 * </p>
 *
 * @see AppendOperation#treeSort(String, SortedCond)
 */
public final class SortedCond {

	/**
	 * 排序后的有序集第一条数据的标记值
	 */
	public static final int DEFAULT_OFFSET = 0;

	/**
	 * 排序后的有序集最后一条数据的标记值
	 */
	public static final int DEFAULT_COUNT = -1;

	/**
	 * 选取有序集排序后数据的偏移量，第一条数据的偏移量为“0”，依次类推
	 */
	private int offset = DEFAULT_OFFSET;

	/**
	 * 选取有序集排序后数据的数量，默认值为“-1”
	 */
	private int count = DEFAULT_COUNT;

	/**
	 * 有序集排序范围的序值最小值
	 */
	private Double min;

	/**
	 * 有序集排序范围的序值最大值
	 */
	private Double max;

	/**
	 * 是否使用序值的升序进行排序
	 */
	private boolean isAsc;

	/**
	 * 以是否使用序值的升序构建有序集排序条件对象。
	 *
	 * @param isAsc 是否使用序值的升序进行排序
	 */
	public SortedCond(boolean isAsc) {

		this.isAsc = isAsc;
	}

	public SortedCond(boolean isAsc, int offset, int count) {

		this(isAsc);
		this.offset = offset;
		this.count = count;
	}

	public SortedCond(boolean isAsc, Double min, Double max, int offset, int count) {

		this(isAsc, offset, count);
		this.min = min;
		this.max = max;
	}

	/**
	 * 获取选取有序集排序后数据的偏移量。
	 *
	 * @return 选取有序集排序后数据的偏移量
	 */
	public int getOffset() {

		return offset;
	}

	/**
	 * 设置选取有序集排序后数据的偏移量，第一条数据的偏移量为“0”，依次类推，默认值为“0”。
	 *
	 * @param offset 选取有序集排序后数据的偏移量，值不能小于 0
	 *
	 * @throws IllegalArgumentException <code>offset</code> 的值小于 0
	 */
	public void setOffset(int offset) {

		Assert.notLesser(offset, 0, "offset");
		this.offset = offset;
	}

	/**
	 * 获取选取有序集排序后数据的数量。
	 *
	 * @return 选取有序集排序后数据的数量
	 */
	public int getCount() {

		return count;
	}

	/**
	 * 设置选取有序集排序后数据的数量，表示从 {@link #offset} 值开始选取的数据结果数量；若值为 0 时， 不选取任何数据记录；若小于 0
	 * 时，表示选取从 {@link #offset} 开始至最后一条的数据，默认值为“-1”。
	 *
	 * @param count 选取有序集排序后数据的数量
	 */
	public void setCount(int count) {

		this.count = count;
	}

	/**
	 * 获取有序集排序范围的序值最小值。
	 *
	 * @return 有序集排序范围的序值最小值
	 */
	public Double getMin() {

		return min;
	}

	/**
	 * 设置有序集排序范围的序值最小值，表示选取序值大于等于该值的数据进行排序。 默认值为 <code>null</code>
	 * 表示从最小的序值开始进行排序。
	 *
	 * @param min 有序集排序范围的序值最小值，<code>null</code> 表示最小的序值
	 */
	public void setMin(Double min) {

		this.min = min;
	}

	/**
	 * 获取有序集排序范围的序值最大值。
	 *
	 * @return 有序集排序范围的序值最大值
	 */
	public Double getMax() {

		return max;
	}

	/**
	 * 设置有序集排序范围的序值最大值，表示选取序值小于等于该值的数据进行排序。 默认值为 <code>null</code> 表示排序范围至最大的序值。
	 *
	 * @param max 有序集排序范围的序值最大值，<code>null</code> 表示排序范围至最大的序值
	 */
	public void setMax(Double max) {

		this.max = max;
	}

	/**
	 * 是否以有序集中数据元素序值升序排序。
	 *
	 * @return 是否按序值升序排序。true：按序值升序排序；false：按序值降序排序
	 */
	public boolean isAsc() {

		return isAsc;
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString("SortedCond: ");
		builder.append("isAsc", isAsc);
		builder.append("offset", offset);
		builder.append("count", count);
		builder.append("min", min);
		builder.append("max", max);
		return builder.toString();
	}
}
