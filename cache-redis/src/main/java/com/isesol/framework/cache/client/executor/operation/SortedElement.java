package com.isesol.framework.cache.client.executor.operation;

import com.isesol.framework.cache.client.util.EclipseTools.*;

import java.io.*;

/**
 * <p>
 * 有序集排序后所选取单条数据元素
 * </p>
 * <p>
 * 对象中含有有序集排序的经反序列化后数据元素对象，以及该数据元素的排序序值。
 * </p>
 *
 * @see AppendOperation#treeSort(String, SortedCond)
 * @see SortedTree
 */
public final class SortedElement<E> implements Serializable {

	/**
	 * 数据元素的序值
	 */
	private double score;

	/**
	 * 反序列化后的数据元素对象
	 */
	private E element;

	/**
	 * 使用数据元素的序值和反序列化后的数据元素对象构建排序后的数据元素对象。
	 *
	 * @param score 数据元素的序值
	 * @param element 反序列化后的数据元素对象
	 */
	public SortedElement(double score, E element) {

		this.score = score;
		this.element = element;
	}

	public SortedElement(E element, double score) {

		this(score, element);
	}

	/**
	 * 获取数据元素的序值
	 *
	 * @return 数据元素的序值
	 */
	public double getScore() {

		return score;
	}

	/**
	 * 获取反序列化后的数据元素对象
	 *
	 * @return 反序列化后的数据元素对象
	 */
	public E getElement() {

		return element;
	}

	@Override
	public int hashCode() {

		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(score);
		builder.append(element);
		return builder.toHashCode();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}
		if (!(obj instanceof SortedElement)) {
			return false;
		}
		SortedElement other = (SortedElement) obj;
		EqualsBuilder builder = new EqualsBuilder();
		builder.append(element, other.element);
		builder.append(score, other.score);
		return builder.isEquals();
	}

	@Override
	public String toString() {

		return "score: " + score + ", element: " + element;
	}
}