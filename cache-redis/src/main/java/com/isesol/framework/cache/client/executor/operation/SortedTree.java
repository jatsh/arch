package com.isesol.framework.cache.client.executor.operation;

import com.isesol.framework.cache.client.util.*;
import com.isesol.framework.cache.client.util.EclipseTools.*;

import java.io.*;
import java.util.*;

/**
 * <p>
 * 有序集排序后的结果
 * </p>
 * <p>
 * 有序集排序后的结果中含有通过 {@link SortedCond} 条件进行排序后的数据，以及总的数据量。
 * </p>
 *
 * @see AppendOperation#treeSort(String, SortedCond)
 * @see SortedCond
 */
public final class SortedTree<E> implements Serializable, Iterable<SortedElement<E>> {

	/**
	 * 无数据元素的有序集
	 */
	private static final SortedTree<?> EMPTY_SORTED_TREE = new SortedTree<Object>(0);

	/**
	 * 满足序值在 {@link SortedCond#getMin()} 和 {@link SortedCond#getMax()}
	 * 范围内中数据元素的数量
	 */
	private final int total;

	/**
	 * 满足 {@link SortedCond} 排序条件的数据元素
	 */
	private List<SortedElement<E>> elements;

	/**
	 * 使用满足序值在 {@link SortedCond#getMin()} 和 {@link SortedCond#getMax()}
	 * 范围内中数据元素的数量构建有序集排序结果对象。
	 *
	 * @param total 序值在排序条件范围内的数据量
	 */
	public SortedTree(Number total) {

		this.total = (
				total == null ? 0 : total.intValue());
	}

	@SuppressWarnings("unchecked")
	public static <E> SortedTree<E> emptySortedTree() {

		return (SortedTree<E>) EMPTY_SORTED_TREE;
	}

	public void addElement(Double score, E element) {

		if (elements == null) {
			elements = new LinkedList<SortedElement<E>>();
		}
		elements.add(new SortedElement<E>(score, element));
	}

	/**
	 * 获取满足序值在 {@link SortedCond#getMin()} 和 {@link SortedCond#getMax()}
	 * 范围内中数据元素的数量
	 *
	 * @return 序值在排序条件范围内的数据量
	 */
	public int getTotal() {

		return total;
	}

	/**
	 * 获取按排序条件所选取的数据元素的数量
	 *
	 * @return 按排序条件所选取的数据元素的数量
	 */
	public int getElementsSize() {

		if (elements == null) {
			return 0;
		}
		return elements.size();
	}

	@Override
	public Iterator<SortedElement<E>> iterator() {

		return NullSafeReadOnlyIterable.wrapIterator(elements);
	}

	@Override
	public String toString() {

		ToString builder = new ToString(this);
		builder.append("total", total);
		builder.append(
				"elements size", elements == null ? 0 : elements.size());
		return builder.toString();
	}
}
