package com.isesol.framework.cache.client.executor.redis;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.exception.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import com.isesol.framework.cache.client.executor.redis.conn.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;
import org.springframework.dao.*;
import org.springframework.data.redis.connection.*;
import org.springframework.data.redis.connection.jedis.*;
import org.springframework.data.redis.core.*;
import org.springframework.transaction.annotation.*;
import redis.clients.jedis.*;

import static org.springframework.transaction.annotation.Propagation.*;

public class CacheRedisTemplate implements RedisTemplateOperator, InitializingBean {

	static Logger log = LogManager.getLogger(CacheRedisTemplate.class.getName());

	private RedisTemplate<String, Object> template;

	public CacheRedisTemplate() {

	}

	public void setTemplate(RedisTemplate<String, Object> template) {

		this.template = template;
	}

	@Override
	public final void afterPropertiesSet() {

		Assert.notNull(template, "template");
		doAfterPropertiesSet();
		log.info("Redis template connect information: {}", this);
	}

	@Override
	public <T> T doInTemplate(RedisAction<T> action) throws CacheClientException {

		return doInTemplateWithTx(action, REQUIRED);
	}

	public <T> T doInTemplateRequiresNew(RedisAction<T> action) throws CacheClientException {

		return doInTemplateWithTx(action, REQUIRES_NEW);
	}

	private <T> T doInTemplateWithTx(RedisAction<T> action, Propagation propagation) throws CacheClientException {

		try {
			if (REQUIRES_NEW.equals(propagation)) {
				log.debug(
						"[doInTemplateWithTx] RedisAction callback transaction propagation type is REQUIRES_NEW" +
						", current thread connection '{}' will be suspended", getRedisConnection());
				return template.execute(action);
			}

			RedisConnection connection = getRedisConnection();

			if (connection == null) {
				log.debug(
						"[doInTemplateWithTx] RedisAction callback transaction propagation type is REQUIRED" +
						", current thread cannot find any bound RedisConnection, create a new RedisConnection");
				return template.execute(action);
			}

			log.debug(
					"[doInTemplateWithTx] RedisAction callback transaction propagation type is REQUIRED" +
					", current thread connection is '{}'", connection);
			return action.doInRedis(connection);
		} catch (IllegalArgumentException e) {
			throw e;
		} catch (DataAccessResourceFailureException e) {
			throw convertToCacheConnectionException(e);
		} catch (UnsupportedOperationException e) {
			throw convertToCacheTypeCastException(e);
		} catch (InvalidDataAccessApiUsageException e) {
			throw convertToCacheTypeCastException(e);
		} catch (RedisTransactionException e) {
			throw convertToTransactionException(e);
		} catch (Exception e) {

			// DataAccessResourceFailureException

			// RedisConnectionFailureException
			// (DataAccessResourceFailureException)
			// +- UnknownHostException (IOException)
			// +- IOException
			// +- TimeoutException
			// +- JedisConnectionException (JedisException)

			// InvalidDataAccessApiUsageException (DataAccessException)
			// +- JedisDataException (JedisException)

			// RedisTransactionException

			// RedisSystemException

			throw convertToCacheSystemException(e);
		}
	}

	private CacheConnectionException convertToCacheConnectionException(Exception e) {

		return new CacheConnectionException("connection exception \"" + e.getMessage() + "\"", e);
	}

	private CacheTypeCastException convertToCacheTypeCastException(Exception e) {

		return new CacheTypeCastException("cache type cast exception \"" + e.getMessage() + "\"", e);
	}

	private CacheTransactionException convertToTransactionException(Exception e) {

		return new CacheTransactionException("transaction exception \"" + e.getMessage() + "\"", e);
	}

	private CacheSystemException convertToCacheSystemException(Exception e) {

		return new CacheSystemException("invoke doInTemplate cause error \"" + e.getMessage() + "\"", e);
	}

	protected RedisConnection getRedisConnection() {

		return RedisConnectionSynchronizer.get();
	}

	protected void doAfterPropertiesSet() {

	}

	public EclipseTools.ToLineString toString(final EclipseTools.ToLineString build, final int lev) {

		EclipseTools.ToLineString builder = build;
		if (builder == null) {
			builder = new EclipseTools.ToLineString();
		}
		builder.appendln(this);

		int level = lev + 1;
		builder.appendln(level, "connectionFactory", template.getConnectionFactory());

		if (template.getConnectionFactory() instanceof JedisConnectionFactory) {
			JedisConnectionFactory factory = (JedisConnectionFactory) template.getConnectionFactory();
			level++;
			builder.appendln(level, "hostName", factory.getHostName());
			builder.appendln(level, "port", factory.getPort());
			builder.appendln(
					level, "password", factory.getPassword() == null ? "null" : "******");
			builder.appendln(level, "timeout", factory.getTimeout());
			builder.appendln(level, "database", factory.getDatabase());
			builder.appendln(level, "usePool", factory.getUsePool());
			builder.appendln(level, "poolConfig", factory.getPoolConfig(), true);
			JedisShardInfo jedisShardInfo = factory.getShardInfo();
			jedisShardInfo.setPassword(
					jedisShardInfo.getPassword() == null ? "null" : "******");
			builder.appendln(level, "shardInfo", jedisShardInfo, true);
		}

		return builder;
	}

	@Override
	public String toString() {

		return toString(null, 0).toString();
	}
}
