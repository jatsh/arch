package com.isesol.framework.cache.client.executor.redis;

import com.isesol.framework.cache.client.util.*;
import com.isesol.framework.cache.client.util.property.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;
import org.springframework.data.redis.connection.jedis.*;
import redis.clients.jedis.*;

import java.util.*;

enum ConnectionFactoryConfiguration {

	JEDIS_CONFIG("jedis.config.", JedisConnectionFactory.class),

	JEDIS_POOL_CONFIG("jedis.pool.", JedisPoolConfig.class) {
		@Override
		protected void doInjectPropertyValue(RedisConnectionFactory connectionFactory, String property, String value) {

			JedisConnectionFactory jedisConnectionFactory = (JedisConnectionFactory) connectionFactory;

			getPropertyManager().setPropertyValue(jedisConnectionFactory.getPoolConfig(), property, value);
		}
	};

	static Logger LOG = LogManager.getLogger(ConnectionFactoryConfiguration.class.getName());

	private final String configurationPrefix;

	private final transient PropertyManager propertyManager;

	private ConnectionFactoryConfiguration(String configurationPrefix, Class<?> clazz) {

		this.configurationPrefix = configurationPrefix;
		this.propertyManager = new PropertyManager(clazz);
	}

	boolean matches(String name) {

		if (Tools.isBlank(name)) {
			return false;
		}
		return name.startsWith(getConfigurationPrefix());
	}

	void injectPropertyValue(RedisConnectionFactory connectionFactory, Map<String, String> configurations) {

		if (connectionFactory == null || Tools.isBlank(configurations)) {
			LOG.warn(
					"connectionFactory is null, or configurations is null or empty," +
					" ignore inject property value. connectionFactory: {}, configurations: {}", connectionFactory,
					configurations);
			return;
		}

		if (!isEglibleConnectionFactory(connectionFactory)) {
			LOG.warn("connectionFactory is not eglible, ignore inject property value, {}", connectionFactory);
			return;
		}

		for (Map.Entry<String, String> entry : configurations.entrySet()) {
			if (!matches(entry.getKey())) {
				LOG.trace(
						"entry name '{}' not matches prefix '{}', ignore it", entry.getKey(), getConfigurationPrefix
								());
				continue;
			}
			String property = entry.getKey().substring(configurationPrefix.length());
			LOG.trace("inject value '{}' to connectionFactory property '{}'", property, entry.getValue());
			doInjectPropertyValue(connectionFactory, property, entry.getValue());
		}
	}

	void doInjectPropertyValue(RedisConnectionFactory connectionFactory, String property, String value) {

		getPropertyManager().setPropertyValue(connectionFactory, property, value);
	}

	boolean isEglibleConnectionFactory(RedisConnectionFactory connectionFactory) {

		return (connectionFactory instanceof JedisConnectionFactory);
	}

	String getConfigurationPrefix() {

		return configurationPrefix;
	}

	PropertyManager getPropertyManager() {

		return propertyManager;
	}
}
