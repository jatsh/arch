package com.isesol.framework.cache.client.executor.redis;

import com.isesol.framework.cache.client.executor.*;

public class DefaultRedisWrapKey extends AbstractWrapKey {

	private static final WrapKey INSTANCE = new DefaultRedisWrapKey();

	public static WrapKey getInstance() {

		return INSTANCE;
	}

	@Override
	protected String doWrapKey(String key) {

		return key;
	}

	@Override
	protected String doWrapMapKey(String mapKey) {

		return mapKey;
	}
}