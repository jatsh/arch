package com.isesol.framework.cache.client.executor.redis;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.adaptor.*;
import com.isesol.framework.cache.client.executor.operation.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import com.isesol.framework.cache.client.executor.redis.adaptor.*;
import com.isesol.framework.cache.client.executor.redis.callback.*;
import com.isesol.framework.cache.client.executor.redis.conn.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * Redis 的 {@link TransactionCacheExecutor} 实现。用于基于 Redis 服务器的缓存数据操作。
 */
public class RedisCacheExecutor extends CacheRedisTemplate implements TransactionCacheExecutor {

	static Logger log = LogManager.getLogger(RedisCacheExecutor.class.getName());

	/**
	 * 默认的过期时间（单位：秒）
	 */
	private int defaultExpires = DEFAULT_EXPIRATION_SECONDS;

	/**
	 * key, field, value 等序列化及反序列化处理器
	 */
	private CacheSerializable serializable;

	/**
	 * 数据类型适配器
	 */
	private DataTypeAdaptors adaptors;

	/**
	 * 事务操作回调方法操作
	 */
	private CacheExecutorOperation operation;

	public RedisCacheExecutor() {

		this.operation = new DefaultCacheExecutorOperation(this);
	}

	public int getDefaultExpires() {

		return defaultExpires;
	}

	public void setDefaultExpires(int defaultExpires) {

		this.defaultExpires = defaultExpires;
	}

	public CacheSerializable getSerializable() {

		return serializable;
	}

	public void setSerializable(CacheSerializable serializable) {

		this.serializable = serializable;
	}

	protected CacheExecutorOperation getOperation() {

		return operation;
	}

	@Override
	protected void doAfterPropertiesSet() {

		super.doAfterPropertiesSet();
		Assert.notNull(getSerializable(), "serializable");

		this.adaptors = new RedisDataTypeAdaptors(this);
	}

	/**
	 * <p>
	 * 检查进行操作 <code>Map</code> 对象的有效性。
	 * </p>
	 *
	 * @param key 进行操作缓存的 key
	 * @param map 进行操作的 <code>Map</code> 对象
	 *
	 * @return <code>Map</code> 对象是否有效。仅在返回 <code>true</code> 时继续处理需要进行的操作； 如果返回
	 * <code>false</code> 时将忽略需要进行的操作
	 */
	protected <V> boolean isValidMap(String key, Map<String, V> map) {

		if (Tools.isBlank(map)) {
			log.warn("Map is null or empty, ignore the operation. key '{}'", key);
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * 检查设置 key 的绝对生存日期时间的有效性。
	 * </p>
	 *
	 * @param key 进行操作缓存的 key
	 * @param expireAt 缓存的绝对生存日期时间
	 *
	 * @return 绝对生存日期时间是否有效。仅在返回 <code>true</code> 时继续处理需要进行的操作； 如果返回
	 * <code>false</code> 时将忽略需要进行的操作
	 */
	protected boolean isValidExpireAt(String key, Date expireAt) {

		if (expireAt.getTime() <= System.currentTimeMillis()) {

			log.warn("expireAt time before the current time, ignore it. key '{}', expireAt '{}'", key, expireAt);

			return false;
		}

		return true;
	}

	@Override
	public <T> T get(String key) throws CacheClientException {

		return doInTemplate(new StringGet<T>(getSerializable(), adaptors, key));
	}

	@Override
	public <V> Map<String, V> getMap(String key) throws CacheClientException {

		return doInTemplate(new HashGetAll<V>(getSerializable(), key));
	}

	@Override
	public List<Object> mget(final String... keys) throws CacheClientException {

		return doInTemplate(new StringMGet(getSerializable(), keys));
	}

	@SuppressWarnings(Tools.SUPPRESS_WARNINGS_UNCHECKED)
	@Override
	public <T> T getMapEntry(String key, String mapKey) throws CacheClientException {

		return (T) doInTemplate(new HashGet(getSerializable(), key, mapKey));
	}

	@Override
	public <T> List<T> mgetMapEntry(final String key, String... mapKeys) throws CacheClientException {

		return doInTemplate(new HashMGet<T>(getSerializable(), key, mapKeys));
	}

	@Override
	public void set(String key, Object value) throws CacheClientException {

		set(key, value, getDefaultExpires());
	}

	@Override
	public void set(String key, Object value, int expireSeconds) throws CacheClientException {

		doInTemplate(new StringSetExpires(getSerializable(), key, value, expireSeconds));
	}

	@Override
	public void set(String key, Object value, Date expireAt) throws CacheClientException {

		if (!isValidExpireAt(key, expireAt)) {

			return;
		}

		doInTemplate(new StringSetExpireAt(getSerializable(), key, value, expireAt));
	}

	@Override
	public boolean nxSet(String key, Object value) throws CacheClientException {

		return nxSet(key, value, getDefaultExpires());
	}

	@Override
	public boolean nxSet(String key, Object value, int expireSeconds) throws CacheClientException {

		return doInTemplate(new NxStringSet(getSerializable(), key, value, expireSeconds, null));
	}

	@Override
	public boolean nxSet(String key, Object value, Date expireAt) throws CacheClientException {

		if (!isValidExpireAt(key, expireAt)) {
			return false;
		}

		return doInTemplate(new NxStringSet(getSerializable(), key, value, null, expireAt));
	}

	@Override
	public <V> void setMap(String key, Map<String, V> map) throws CacheClientException {

		setMap(key, map, getDefaultExpires());
	}

	@Override
	public <V> void setMap(String key, Map<String, V> map, int expireSeconds) throws CacheClientException {

		if (!isValidMap(key, map)) {
			return;
		}

		doInTemplate(new SetMap<V>(getSerializable(), key, map, expireSeconds, null));
	}

	@Override
	public <V> void setMap(String key, Map<String, V> map, Date expireAt) throws CacheClientException {

		if (!isValidExpireAt(key, expireAt)) {
			return;
		}

		if (!isValidMap(key, map)) {
			return;
		}

		doInTemplate(new SetMap<V>(getSerializable(), key, map, null, expireAt));
	}

	@Override
	public void setMapEntry(String key, String mapKey, Object mapValue) throws CacheClientException {

		setMapEntry(key, convertKV2Map(mapKey, mapValue));
	}

	@Override
	public void setMapEntry(String key, String mapKey, Object mapValue, int expireSeconds) throws
	                                                                                       CacheClientException {

		setMapEntry(key, convertKV2Map(mapKey, mapValue), expireSeconds);
	}

	@Override
	public void setMapEntry(String key, String mapKey, Object mapValue, Date expireAt) throws CacheClientException {

		setMapEntry(key, convertKV2Map(mapKey, mapValue), expireAt);
	}

	@Override
	public boolean nxSetMapEntry(String key, String mapKey, Object value) throws CacheClientException {

		return nxSetMapEntry(key, mapKey, value, getDefaultExpires());
	}

	@Override
	public boolean nxSetMapEntry(String key, String mapKey, Object value, int expireSeconds) throws
	                                                                                         CacheClientException {

		return doInTemplate(new NxSetMapEntry(getSerializable(), key, mapKey, value, expireSeconds, null));
	}

	@Override
	public boolean nxSetMapEntry(String key, String mapKey, Object value, Date expireAt) throws CacheClientException {

		if (!isValidExpireAt(key, expireAt)) {
			return false;
		}
		return doInTemplate(new NxSetMapEntry(getSerializable(), key, mapKey, value, null, expireAt));
	}

	@Override
	public <V> void setMapEntry(final String key, final Map<String, V> mapEntry) throws CacheClientException {

		if (!isValidMap(key, mapEntry)) {

			log.warn("[SetMapEntries] mapEntry object is null or empty, ignore the operation. key '{}'", key);

			return;
		}

		// 获取 key 的数据类型
		DataType type = doInTemplateRequiresNew(new KeyType(getSerializable(), key));

		doInTemplate(new SetMapEntry<V>(getSerializable(), key, mapEntry, type, getDefaultExpires()));
	}

	@Override
	public <V> void setMapEntry(String key, Map<String, V> mapEntry, int expireSeconds) throws CacheClientException {

		internalSetMapEntryExpires(key, mapEntry, expireSeconds, null);
	}

	@Override
	public <V> void setMapEntry(String key, Map<String, V> mapEntry, Date expireAt) throws CacheClientException {

		internalSetMapEntryExpires(key, mapEntry, null, expireAt);
	}

	@Override
	public void del(final String... keys) throws CacheClientException {

		doInTemplate(new DelKeys(getSerializable(), keys));
	}

	@Override
	public void delMapKey(String key, String mapKey) throws CacheClientException {

		doInTemplate(new DelMapKey(getSerializable(), key, mapKey));
	}

	@Override
	public Number increment(String key) throws CacheClientException {

		return incrementBy(key, 1);
	}

	@Override
	public Number increment(String key, String mapKey) throws CacheClientException {

		return incrementBy(key, mapKey, 1);
	}

	@Override
	public Number incrementBy(String key, int delta) throws CacheClientException {

		return doInTemplate(new IncrementBy(getSerializable(), key, null, delta));
	}

	@Override
	public Number incrementBy(String key, String mapKey, int delta) throws CacheClientException {

		return doInTemplate(new IncrementBy(getSerializable(), key, mapKey, delta));
	}

	@Override
	public Boolean hasKey(String key) throws CacheClientException {

		return doInTemplate(new HasKey(getSerializable(), key));
	}

	@Override
	public Boolean sIsMember(String key, String value) throws CacheClientException {

		return doInTemplate(new IsMember(getSerializable(), key, value));
	}

	@Override
	public void expire(String key, int seconds) throws CacheClientException {

		doInTemplate(new Expire(getSerializable(), key, seconds, null));
	}

	@Override
	public void expireAt(String key, Date expireAt) throws CacheClientException {

		if (!isValidExpireAt(key, expireAt)) {
			return;
		}

		doInTemplate(new Expire(getSerializable(), key, null, expireAt));
	}

	@Override
	public Long ttl(String key) throws CacheClientException {

		return doInTemplate(new TimeToLive(getSerializable(), key));
	}

	@Override
	public void listAppend(String key, Object element) throws CacheClientException {

		listAppend(key, element, getDefaultExpires());
	}

	@Override
	public void listAppend(final String key, Object element, final int expireSeconds) throws CacheClientException {

		doInTemplate(new ListAppend(getSerializable(), key, element, expireSeconds, null));
	}

	@Override
	public void listAppend(final String key, Object element, final Date expireAt) throws CacheClientException {

		if (!isValidExpireAt(key, expireAt)) {
			return;
		}

		doInTemplate(new ListAppend(getSerializable(), key, element, null, expireAt));
	}

	@Override
	public Long listLength(String key) throws CacheClientException {

		return doInTemplate(new ListLength(getSerializable(), key));
	}

	@Override
	public void listRemoveFirst(String key, Object element) throws CacheClientException {

		doInTemplate(new ListRemoveFirst(getSerializable(), key, element));
	}

	@Override
	public <E> E listPopHead(String key) throws CacheClientException {

		return doInTemplate(new ListPop<E>(getSerializable(), key));
	}

	@Override
	public <E> List<E> listElements(String key) throws CacheClientException {

		return doInTemplate(new ListElements<E>(getSerializable(), adaptors, key));
	}

	@Override
	public void treeAdd(String key, Object element, double score) throws CacheClientException {

		treeAdd(key, element, score, getDefaultExpires());
	}

	@Override
	public void treeAdd(final String key, Object element, double score, int expireSeconds) throws
	                                                                                       CacheClientException {

		doInTemplate(new TreeAdd(getSerializable(), key, element, score, expireSeconds, null));
	}

	@Override
	public void treeAdd(final String key, Object element, double score, Date expireAt) throws CacheClientException {

		if (!isValidExpireAt(key, expireAt)) {
			return;
		}

		doInTemplate(new TreeAdd(getSerializable(), key, element, score, null, expireAt));
	}

	@Override
	public void treeRemove(final String key, final Object element) throws CacheClientException {

		doInTemplate(new TreeRemove(getSerializable(), key, element));
	}

	@Override
	public void setRemove(final String key, final Object element) throws CacheClientException {

		doInTemplate(new SetRemove(getSerializable(), key, element));
	}

	@Override
	public Double treeElementScore(String key, Object element) throws CacheClientException {

		return doInTemplate(new TreeElementScore(getSerializable(), key, element));
	}

	@Override
	public <E> SortedTree<E> treeSort(final String key, final SortedCond condition) throws CacheClientException {

		return doInTemplate(new TreeSort<E>(getSerializable(), key, condition));
	}

	@Override
	public void setAdd(String key, Object element) throws CacheClientException {

		setAdd(key, element, getDefaultExpires());
	}

	@Override
	public void setAdd(final String key, Object element, int expireSeconds) throws CacheClientException {

		doInTemplate(new SetAdd(getSerializable(), key, element, expireSeconds, null));
	}

	@Override
	public void setAdd(final String key, Object element, Date expireAt) throws CacheClientException {

		if (!isValidExpireAt(key, expireAt)) {
			return;
		}

		doInTemplate(new SetAdd(getSerializable(), key, element, null, expireAt));
	}

	@Override
	public void executeInTransaction(final TransactionAction tx) throws CacheClientException {

		doInTemplate(
				new VoidTxRedisAction() {
					@Override
					public void doInAction(RedisConnection connection) {

						RedisConnectionSynchronizer.bind(connection);
						try {
							tx.doInTxAction(getOperation());
						} catch (CacheClientException e) {
							throw new RedisTransactionException("Redis transaction cause exception", e);
						} finally {
							RedisConnectionSynchronizer.remove();
						}
					}
				});
	}

	@Override
	public EclipseTools.ToLineString toString(final EclipseTools.ToLineString build, final int lev) {

		EclipseTools.ToLineString builder = build;

		if (builder == null) {
			builder = new EclipseTools.ToLineString();
		}
		builder.appendln(this);
		int level = lev + 1;
		builder.appendln(level, "defaultExpires", defaultExpires);
		builder.appendln(level, "serializable", serializable);
		builder.appendln(level, "operation", operation);
		builder.appendln(level, "adaptors", adaptors);
		builder.append(level, "template");
		super.toString(builder, level);
		return builder;
	}

	private <V> Map<String, V> convertKV2Map(String mapKey, V mapValue) {

		Map<String, V> map = new HashMap<String, V>();
		map.put(mapKey, mapValue);
		return map;
	}

	private <V> void internalSetMapEntryExpires(
			String key, Map<String, V> mapEntry, Integer expireSeconds, Date expireAt) throws CacheClientException {

		if (!isValidMap(key, mapEntry)) {
			log.warn("[SetMapEntries] mapEntry object is null or empty, ignore the operation. key '{}'", key);
			return;
		}

		setMapEntry(key, mapEntry);
		return;

		// doInTemplate( new SetMapEntryExpires<V>( getSerializable() , key ,
		// mapEntry , expireSeconds , expireAt ) );
	}

	@Override
	public String toString() {

		return toString(null, 0).toString();
	}
}
