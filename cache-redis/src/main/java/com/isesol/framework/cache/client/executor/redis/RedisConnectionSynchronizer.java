package com.isesol.framework.cache.client.executor.redis;

import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

final class RedisConnectionSynchronizer {

	private static final ThreadLocal<RedisConnection> RESOURCES = new ThreadLocal<RedisConnection>();

	static Logger LOG = LogManager.getLogger(RedisConnectionSynchronizer.class.getName());

	private RedisConnectionSynchronizer() {

	}

	static void bind(RedisConnection connection) {

		RESOURCES.set(connection);

		LOG.trace("bind RedisConnection '{}' to current thread '{}'", connection, Thread.currentThread());

	}

	static RedisConnection get() {

		RedisConnection connection = RESOURCES.get();


		if (connection == null) {
			LOG.trace(
					"get RedisConnection, current thread '{}' does not include RedisConnection",
					Thread.currentThread());
		} else {
			LOG.trace("get RedisConnection '{}' from current thread '{}'", connection, Thread.currentThread());
		}


		return connection;
	}

	static void remove() {

		RESOURCES.remove();

		LOG.trace("remove RedisConnection from current thread '{}'", Thread.currentThread());

	}
}
