package com.isesol.framework.cache.client.executor.redis;

import com.isesol.framework.cache.client.connector.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;
import org.springframework.core.io.*;
import org.springframework.core.io.support.*;
import org.springframework.data.redis.connection.*;
import org.springframework.data.redis.connection.jedis.*;
import org.springframework.data.redis.core.*;

import java.io.*;
import java.util.*;

public class RedisTemplateFactoryBean
		implements FactoryBean<RedisTemplate<String, Object>>, InitializingBean, DisposableBean {

	private static final String DEFAULT_CONFIGURATIONS_NAME = "redis-template-factory-bean.properties";

	static Logger log = LogManager.getLogger(RedisTemplateFactoryBean.class.getName());

	private JedisConnectionFactory connectionFactory;

	private RedisTemplate<String, Object> template;

	private Properties defaultConfigurations;

	private CacheServerLocator locator;

	private Properties properties;

	public RedisTemplateFactoryBean() {

	}

	public void setLocator(CacheServerLocator locator) {

		this.locator = locator;
	}

	public void setProperties(Properties properties) {

		this.properties = properties;
	}

	@Override
	public RedisTemplate<String, Object> getObject() {

		return template;
	}

	@Override
	public Class<?> getObjectType() {

		return RedisTemplate.class;
	}

	@Override
	public boolean isSingleton() {

		return true;
	}

	@Override
	public void afterPropertiesSet() {

		Assert.notNull(locator, "locator");

		CacheServerAddress address = getCacheServerAddress();

		log.info("CacheServerAddress is '{}' from locator '{}'", address, locator);

		connectionFactory = new JedisConnectionFactory();
		connectionFactory.setHostName(address.getHost());
		connectionFactory.setPort(address.getPort());
		connectionFactory.setPassword(address.getPassword());

		configureConnectionFactory(connectionFactory);

		connectionFactory.afterPropertiesSet();

		log.info("JedisConnectionFactory object creating success, {}", connectionFactory);

		template = new RedisTemplate<String, Object>();
		template.setConnectionFactory(connectionFactory);
		template.afterPropertiesSet();

		log.info("RedisTemplate object creating success, " + template);
	}

	private void loadDefaultConfigurations() {

		if (defaultConfigurations != null) {

			return;
		}

		try {

			defaultConfigurations = PropertiesLoaderUtils.loadProperties(
					new ClassPathResource(
							DEFAULT_CONFIGURATIONS_NAME, getClass()));
		} catch (IOException e) {

			throw new BeanCreationException(
					"load Redis default configurations file " + DEFAULT_CONFIGURATIONS_NAME + " resource failed", e);
		}
	}

	private CacheServerAddress getCacheServerAddress() {

		CacheServerAddress[] addresses = locator.getCacheServer().getAddress();

		if (Tools.isBlank(addresses)) {

			throw new BeanCreationException(
					"Cannot redis server connect address " + "and other paramters, locator = " + locator);
		}

		return addresses[0];
	}

	protected void configureConnectionFactory(RedisConnectionFactory connectionFactory) {

		Map<String, String> configurations = mergeConfigurations();

		if (Tools.isBlank(configurations)) {

			return;
		}

		log.info("Redis connection factory configurations: {}", configurations);

		for (ConnectionFactoryConfiguration config : ConnectionFactoryConfiguration.values()) {

			config.injectPropertyValue(connectionFactory, configurations);
		}
	}

	private Map<String, String> mergeConfigurations() {

		Map<String, String> configurations = new HashMap<String, String>();

		loadDefaultConfigurations();

		for (Enumeration<Object> key = defaultConfigurations.keys(); key.hasMoreElements(); ) {

			String name = String.valueOf(key.nextElement());

			configurations.put(name, defaultConfigurations.getProperty(name));
		}

		if (Tools.isBlank(properties)) {

			log.info("Redis connection factory customer configurations: {}", properties);

			return configurations;
		}

		for (Enumeration<Object> key = properties.keys(); key.hasMoreElements(); ) {

			String name = String.valueOf(key.nextElement());

			if (defaultConfigurations.containsKey(name)) {

				configurations.put(name, properties.getProperty(name));
			}
		}

		return configurations;
	}

	@Override
	public void destroy() {

		if (connectionFactory == null) {
			log.warn("connectionFactory is null that cannot execute destory() method");
			return;
		}

		connectionFactory.destroy();
		log.info("destory connectoryFactory finished, {}", connectionFactory);
	}
}
