package com.isesol.framework.cache.client.executor.redis.action;

import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

public abstract class AbstractRedisAction<T> implements RedisAction<T> {

	static Logger LOG = LogManager.getLogger(AbstractRedisAction.class.getName());

	@Override
	public final T doInRedis(RedisConnection connection) {

		LOG.trace("Current RedisConnection type is '{}'", connection);
		return doInRedisAction(connection);
	}

	protected abstract T doInRedisAction(RedisConnection connection);
}
