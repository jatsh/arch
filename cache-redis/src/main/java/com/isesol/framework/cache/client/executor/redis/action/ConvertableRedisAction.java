package com.isesol.framework.cache.client.executor.redis.action;

import org.springframework.data.redis.connection.*;

/**
 * 用于 Redis 原生存储数据与业务对象数据转化的抽象。
 */
public abstract class ConvertableRedisAction<T, R> extends AbstractRedisAction<T> {

	@Override
	protected final T doInRedisAction(RedisConnection connection) {

		R rawValue = doInAction(connection);

		if (rawValue == null) {
			return null;
		}

		return convert(rawValue, connection);
	}

	/**
	 * 将 Redis 原始数据对象转换为指定类型的数据对象。
	 *
	 * @param rawValue Redis 原始数据对象
	 * @param connection Redis 连接对象
	 *
	 * @return 需要转换后的数据对象
	 */
	protected abstract T convert(R rawValue, RedisConnection connection);

	/**
	 * 回调方法，需要由回调方法的使用者实现，用以操作 Redis 的数据。
	 *
	 * @param connection Redis 连接对象
	 *
	 * @return Redis 操作获取的原始数据对象
	 */
	public abstract R doInAction(RedisConnection connection);
}
