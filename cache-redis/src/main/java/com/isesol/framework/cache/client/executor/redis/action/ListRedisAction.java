package com.isesol.framework.cache.client.executor.redis.action;

import com.isesol.framework.cache.client.executor.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

public abstract class ListRedisAction<T> extends SerializableRedisAction<List<T>, Collection<byte[]>> {

	public ListRedisAction(CacheSerializable serializable) {

		super(serializable);
	}

	@Override
	protected List<T> convert(Collection<byte[]> rawValue, RedisConnection connection) {

		return valuesToList(rawValue);
	}
}
