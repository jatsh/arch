package com.isesol.framework.cache.client.executor.redis.action;

import com.isesol.framework.cache.client.executor.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

public abstract class MapRedisAction<V> extends SerializableRedisAction<Map<String, V>, Map<byte[], byte[]>> {

	public MapRedisAction(CacheSerializable serializable) {

		super(serializable);
	}

	@Override
	protected Map<String, V> convert(Map<byte[], byte[]> rawValue, RedisConnection connection) {

		return valuesToMap(rawValue);
	}
}
