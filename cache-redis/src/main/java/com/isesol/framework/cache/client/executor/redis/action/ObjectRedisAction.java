package com.isesol.framework.cache.client.executor.redis.action;

import com.isesol.framework.cache.client.executor.*;
import org.springframework.data.redis.connection.*;

public abstract class ObjectRedisAction<V> extends SerializableRedisAction<V, Object> {

	public ObjectRedisAction(CacheSerializable serializable) {

		super(serializable);
	}

	@Override
	@SuppressWarnings("unchecked")
	protected V convert(Object rawValue, RedisConnection connection) {

		return (V) guessValue(rawValue);
	}
}
