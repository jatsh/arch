package com.isesol.framework.cache.client.executor.redis.action;

import org.springframework.data.redis.connection.*;

/**
 * Redis 操作回调抽象：用于回调方法返回结果直接使用 Redis 原始数据的场景。
 */
public abstract class OriginalRedisAction<T> extends ConvertableRedisAction<T, T> {

	@Override
	protected T convert(T rawValue, RedisConnection connection) {

		return rawValue;
	}
}
