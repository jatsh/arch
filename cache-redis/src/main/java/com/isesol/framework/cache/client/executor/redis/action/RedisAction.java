package com.isesol.framework.cache.client.executor.redis.action;

import org.springframework.data.redis.core.*;

/**
 * 回调 Redis 底层 API 代码的接口。一般使用匿名内部类的形式调用
 */
public interface RedisAction<T> extends RedisCallback<T> {
}
