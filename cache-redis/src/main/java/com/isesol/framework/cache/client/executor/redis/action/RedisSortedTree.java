package com.isesol.framework.cache.client.executor.redis.action;

import org.springframework.data.redis.connection.RedisZSetCommands.*;

import java.util.*;

public class RedisSortedTree {

	private Long count;

	private Set<Tuple> tuples;

	public RedisSortedTree(Long count, Set<Tuple> tuples) {

		super();
		this.count = count;
		this.tuples = tuples;
	}

	public Long getCount() {

		return count;
	}

	public Set<Tuple> getTuples() {

		return tuples;
	}

	public boolean isCountEmpty() {

		if (count == null) {
			return true;
		}
		return (count == 0L);
	}
}
