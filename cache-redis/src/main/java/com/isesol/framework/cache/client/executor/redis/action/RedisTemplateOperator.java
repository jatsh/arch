package com.isesol.framework.cache.client.executor.redis.action;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.exception.*;

/**
 * Redis 模板方法操作，使用一个 {@link RedisAction} 作为回调接口进行调用。
 */
public interface RedisTemplateOperator extends TemplateOperator {

	/**
	 * 执行模板操作。调用者无需关心与 Redis 服务器的连接、关闭、异常，以及事务处理相关的逻辑，在回调接口实现业务逻辑操作。
	 *
	 * @param action 模板方法回调接口
	 *
	 * @return 模板方法执行后的返回结果。返回的数据类型由参数 <code>action</code> 指定
	 *
	 * @throws CacheClientException 缓存客户端异常
	 * @throws CacheConnectionException 缓存底层通信异常
	 * @throws CacheTypeCastException 数据类型不匹配或者 key 的数据值不是一个数值类型时
	 * @throws CacheTransactionException 带有事务处理的逻辑，由于异常的原因造成的事务处理失败，或者事务处理失败进行回滚时产生的异常
	 * @throws CacheSystemException 或者其他异常
	 * @see RedisAction
	 */
	<T> T doInTemplate(RedisAction<T> action) throws CacheClientException;
}
