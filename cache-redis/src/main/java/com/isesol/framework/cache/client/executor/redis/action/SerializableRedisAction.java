package com.isesol.framework.cache.client.executor.redis.action;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.util.*;

import java.util.*;

/**
 * Redis 回调抽象：用于 Redis 原始数据对象可以通过序列化的方式转换成为业务的数据对象。
 */
@SuppressWarnings(Tools.SUPPRESS_WARNINGS_UNCHECKED)
public abstract class SerializableRedisAction<T, R> extends ConvertableRedisAction<T, R> {

	/**
	 * 序列化操作 API
	 */
	private CacheSerializable serializable;

	public SerializableRedisAction(CacheSerializable serializable) {

		this.serializable = serializable;
	}

	protected CacheSerializable getSerializable() {

		return serializable;
	}

	/**
	 * <p>
	 * 将 Redis API 操作结果值类型为任意的数据值对象转换成为 Java 中的对象，转换根据以下情况依次进行操作：
	 * <ul>
	 * <li>数据值为 <code>null</code> 时，不转换直接返回</li>
	 * <li>数据值类型为 <code>byte[]</code> 时，使用 {@link #value(byte[])} 进行转换</li>
	 * <li>数据值类型为 <code>Map</code> 时，使用 {@link #valuesToMap(Map)} 进行转换</li>
	 * <li>数据值类型为 <code>List</code> 时，使用 {@link #valuesToList(Collection)} 进行转换</li>
	 * <li>数据值类型为 <code>Set</code> 时，使用 {@link #valuesToSet(Collection)} 进行转换</li>
	 * <li>若以上均不满足时，则抛出 {@link UnsupportedOperationException} 异常</li>
	 * </ul>
	 * </p>
	 *
	 * @param rawValue Redis API 操作结果的数据值对象
	 *
	 * @return 数据值反序列化后的 <code>Set&lt;V&gt;</code> 对象
	 *
	 * @throws UnsupportedOperationException Redis 数据值对象类型不被支持自动转换
	 */
	protected <V> V guessValue(Object rawValue) {

		if (rawValue == null) {
			return null;
		}

		if (rawValue instanceof byte[]) {
			return (V) value((byte[]) rawValue);
		}

		if (rawValue instanceof Map) {
			return (V) valuesToMap((Map<byte[], byte[]>) rawValue);
		}

		if (rawValue instanceof List) {
			return (V) valuesToList((List<byte[]>) rawValue);
		}

		if (rawValue instanceof Set) {
			return (V) valuesToSet((Set<byte[]>) rawValue);
		}

		throw new UnsupportedOperationException(
				"Redis raw value type '" + rawValue + "' " +
				"cannot convert to excepted return type according to guess value type operation");
	}

	/**
	 * <p>
	 * 将 Redis API 操作结果值类型为 <code>Set&lt;byte[]&gt;</code> 的数据值对象反序列化成为 Java 中的
	 * <code>Set&lt;V&gt;</code> 对象。
	 * </p>
	 *
	 * @param rawValues Redis API 操作结果值类型为 <code>Collection&lt;byte[]&gt;</code>
	 * 的数据值对象
	 *
	 * @return 数据值反序列化后的 <code>Set&lt;V&gt;</code> 对象
	 */
	protected <V> Set<V> valuesToSet(Collection<byte[]> rawValues) {

		if (rawValues == null) {
			return null;
		}

		Set<V> values = new LinkedHashSet<V>();
		for (byte[] rawValue : rawValues) {
			values.add((V) value(rawValue));
		}

		return values;
	}

	/**
	 * <p>
	 * 将 Redis API 操作结果值类型为 <code>List&lt;byte[]&gt;</code> 的数据值对象反序列化成为 Java 中的
	 * <code>List&lt;V&gt;</code> 对象。
	 * </p>
	 *
	 * @param rawValues Redis API 操作结果值类型为 <code>Collection&lt;byte[]&gt;</code>
	 * 的数据值对象
	 *
	 * @return 数据值反序列化后的 <code>List&lt;V&gt;</code> 对象
	 */
	protected <V> List<V> valuesToList(Collection<byte[]> rawValues) {

		if (rawValues == null) {
			return null;
		}

		List<V> values = new ArrayList<V>(rawValues.size());
		for (byte[] rawValue : rawValues) {
			values.add((V) value(rawValue));
		}
		return values;
	}

	/**
	 * <p>
	 * 将 Redis API 操作结果值类型为 <code>Map&lt;byte[], byte[]&gt;</code> 的数据值对象反序列化成为
	 * Java 中的 <code>Map&lt;String, V&gt;</code> 对象。
	 * </p>
	 *
	 * @param rawMap Redis API 操作结果值类型为 <code>Map&lt;byte[], byte[]&gt;</code>
	 * 的数据值对象
	 *
	 * @return 数据值反序列化后的 <code>Map&lt;String, V&gt;</code> 对象
	 */
	protected <V> Map<String, V> valuesToMap(Map<byte[], byte[]> rawMap) {

		if (rawMap == null) {
			return null;
		}

		Map<String, V> map = new HashMap<String, V>();
		for (Map.Entry<byte[], byte[]> entry : rawMap.entrySet()) {
			map.put(field(entry.getKey()), (V) value(entry.getValue()));
		}

		return map;
	}

	/**
	 * <p>
	 * 将 Redis API 操作结果值类型为 <code>byte[]</code> 的数据值对象反序列化成为 Java 对象。
	 * </p>
	 *
	 * @param rawValue Redis API 操作结果类型为 <code>byte[]</code> 的数据值对象
	 *
	 * @return 数据值反序列化后的 Java 对象
	 */
	protected <V> V value(byte[] rawValue) {

		return (V) getSerializable().deserializeValue(rawValue);
	}

	/**
	 * <p>
	 * 将 Redis API 操作结果类型为 <code>byte[]</code> 的 key 对象转成为 Java 中的 String 表示。
	 * </p>
	 *
	 * @param rawKey Redis API 操作结果类型为<code>byte[]</code> 的 key 对象
	 *
	 * @return 转化为 Java 中以 String 表示的 key 对象
	 */
	protected String key(byte[] rawKey) {

		return getSerializable().deserializeKey(rawKey);
	}

	/**
	 * <p>
	 * 将 Redis API 操作结果类型为 <code>byte[]</code> 的 HASH field 对象转成为 Java 中的 String
	 * 表示。
	 * </p>
	 *
	 * @param rawField Redis API 操作结果类型为 <code>byte[]</code> 的 HASH field 对象
	 *
	 * @return 转化为 Java 中以 String 表示的 HASH field 对象
	 */
	protected String field(byte[] rawField) {

		return getSerializable().deserializeField(rawField);
	}
}
