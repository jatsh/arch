package com.isesol.framework.cache.client.executor.redis.action;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.operation.*;
import com.isesol.framework.cache.client.executor.redis.conn.*;
import org.springframework.data.redis.connection.*;
import org.springframework.data.redis.connection.RedisZSetCommands.*;

import java.util.*;

/**
 * 有序集排序回调抽象
 */
public abstract class SortedTreeRedisAction<E> extends SerializableRedisAction<SortedTree<E>, RedisSortedTree> {

	public SortedTreeRedisAction(CacheSerializable serializable) {

		super(serializable);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected SortedTree<E> convert(RedisSortedTree rawValue, RedisConnection connection) {

		if (RedisTransaction.isInTransactionContext(connection)) {
			return null;
		}

		if (isSortedTreeEmpty(rawValue)) {
			return SortedTree.emptySortedTree();
		}

		SortedTree<E> sorted = new SortedTree<E>(rawValue.getCount());

		Set<Tuple> tuples = rawValue.getTuples();

		if (tuples == null) {
			return sorted;
		}

		for (Tuple tuple : tuples) {
			sorted.addElement(tuple.getScore(), (E) value(tuple.getValue()));
		}

		return sorted;
	}

	private boolean isSortedTreeEmpty(RedisSortedTree rawValue) {

		if (rawValue == null) {
			return true;
		}
		return rawValue.isCountEmpty();
	}
}
