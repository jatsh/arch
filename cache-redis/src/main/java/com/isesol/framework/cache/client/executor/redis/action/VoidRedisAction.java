package com.isesol.framework.cache.client.executor.redis.action;

import org.springframework.data.redis.connection.*;

/**
 * Redis 操作回调抽象：用于 Redis 操作不需要获取结果的场景。
 */
public abstract class VoidRedisAction extends AbstractRedisAction<Object> {

	public VoidRedisAction() {

	}

	@Override
	protected Object doInRedisAction(RedisConnection connection) {

		doInAction(connection);
		return null;
	}

	/**
	 * 无返回值的回调方法，需要由回调方法的使用者实现，用以操作 Redis 的数据。
	 *
	 * @param connection Redis 连接对象
	 */
	public abstract void doInAction(RedisConnection connection);
}
