package com.isesol.framework.cache.client.executor.redis.action;

import com.isesol.framework.cache.client.executor.redis.conn.*;
import org.springframework.data.redis.connection.*;

/**
 * Redis 操作回调抽象：用于 Redis 操作在一个事务体内进行的无需返回结果数据的场景。
 */
public abstract class VoidTxRedisAction extends VoidRedisAction {

	@Override
	protected final Object doInRedisAction(RedisConnection connection) {

		RedisTransaction.doInTransaction(
				connection, new TxTemplate() {
					@Override
					public void doInTransaction(RedisConnection txconnection) {

						doInAction(txconnection);
					}
				});

		return null;
	}
}
