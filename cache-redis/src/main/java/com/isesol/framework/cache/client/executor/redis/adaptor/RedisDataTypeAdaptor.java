package com.isesol.framework.cache.client.executor.redis.adaptor;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.adaptor.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.springframework.data.redis.connection.*;

public abstract class RedisDataTypeAdaptor extends DefaultDataTypeAdaptor {

	public RedisDataTypeAdaptor(DataType dataType, RedisTemplateOperator template) {

		super("redis", dataType, template);
	}

	protected final <T> T doInTemplate(RedisAction<T> action) throws CacheClientException {

		return ((RedisTemplateOperator) getTemplate()).doInTemplate(action);
	}

	@Override
	protected String toAdaptorType(Object dataType) {

		if (dataType instanceof DataType) {

			return ((DataType) dataType).code();
		}

		return super.toAdaptorType(dataType);
	}
}
