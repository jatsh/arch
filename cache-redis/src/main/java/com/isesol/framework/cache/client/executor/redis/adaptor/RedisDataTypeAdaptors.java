package com.isesol.framework.cache.client.executor.redis.adaptor;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.adaptor.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.springframework.data.redis.connection.*;

public class RedisDataTypeAdaptors extends DataTypeAdaptors {

	public RedisDataTypeAdaptors(RedisTemplateOperator template) {

		super(template);
	}

	static String toAdaptorType(DataType dataType) {

		return "redis-" + dataType.code().toLowerCase();
	}

	@Override
	protected String doConvertDataType(Object dataType) {

		if (dataType instanceof DataType) {

			return toAdaptorType((DataType) dataType);
		}

		return null;
	}

	@Override
	protected void loadDefaultAdaptor(TemplateOperator template) {

		if (template instanceof RedisTemplateOperator) {

			RedisTemplateOperator redisTemplate = (RedisTemplateOperator) template;

			register(new RedisStringAdaptor(redisTemplate));
			register(new RedisHashAdaptor(redisTemplate));
			register(new RedisZSetAdaptor(redisTemplate));
			register(new RedisSetAdaptor(redisTemplate));
		}
	}

}
