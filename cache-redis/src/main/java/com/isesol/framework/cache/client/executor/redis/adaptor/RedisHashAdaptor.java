package com.isesol.framework.cache.client.executor.redis.adaptor;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

public class RedisHashAdaptor extends RedisDataTypeAdaptor {

	public RedisHashAdaptor(RedisTemplateOperator template) {

		super(DataType.HASH, template);
	}

	@Override
	protected Object get(final byte[] key) throws CacheClientException {

		return doInTemplate(
				new OriginalRedisAction<Map<byte[], byte[]>>() {
					@Override
					public Map<byte[], byte[]> doInAction(RedisConnection connection) {

						return connection.hGetAll(key);
					}
				});
	}
}
