package com.isesol.framework.cache.client.executor.redis.adaptor;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

public class RedisSetAdaptor extends RedisDataTypeAdaptor {

	public RedisSetAdaptor(RedisTemplateOperator template) {

		super(DataType.SET, template);
	}

	@Override
	protected Collection<byte[]> getElements(final byte[] key) throws CacheClientException {

		return doInTemplate(
				new OriginalRedisAction<Set<byte[]>>() {
					@Override
					public Set<byte[]> doInAction(RedisConnection connection) {

						return connection.sMembers(key);
					}
				});
	}
}
