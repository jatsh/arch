package com.isesol.framework.cache.client.executor.redis.adaptor;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.springframework.data.redis.connection.*;

public class RedisStringAdaptor extends RedisDataTypeAdaptor {

	public RedisStringAdaptor(RedisTemplateOperator template) {

		super(DataType.STRING, template);
	}

	@Override
	protected Object get(final byte[] key) throws CacheClientException {

		return doInTemplate(
				new OriginalRedisAction<byte[]>() {
					@Override
					public byte[] doInAction(RedisConnection connection) {

						return connection.get(key);
					}
				});
	}
}
