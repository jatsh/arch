package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * <p>
 * {@link RedisCacheExecutor
 * RedisCacheExecutor#del(String...) del} <code>doInTemplate</code> 方法的回调
 * </p>
 */
public class DelKeys extends VoidRedisAction {

	static Logger LOG = LogManager.getLogger(DelKeys.class.getName());

	private final String keys;

	private final byte[][] rawKeys;

	public DelKeys(CacheSerializable serializable, String... keys) {

		this.keys = Arrays.toString(keys);
		this.rawKeys = serializable.toRawKeys(keys);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		Long count = connection.del(rawKeys);
		LOG.debug("keys: {}, delete count: {}", count, keys);
	}
}
