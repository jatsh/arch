package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

/**
 * <p>
 * {@link RedisCacheExecutor#delMapKey(String, String) delMapKey} <code>doInTemplate</code> 方法的回调
 * </p>
 */
public class DelMapKey extends VoidRedisAction {

	static Logger LOG = LogManager.getLogger(DelMapKey.class.getName());

	private final String key;

	private final String mapKey;

	private final byte[] rawKey;

	private final byte[] rawMapKey;

	public DelMapKey(CacheSerializable serializable, String key, String mapKey) {

		this.key = key;
		this.mapKey = mapKey;
		this.rawKey = serializable.toRawKey(key);
		this.rawMapKey = serializable.toRawField(key, mapKey);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		Long result = connection.hDel(rawKey, rawMapKey);
		LOG.debug("  key = {}, field = {}, del result = {}", key, mapKey, result);
	}
}
