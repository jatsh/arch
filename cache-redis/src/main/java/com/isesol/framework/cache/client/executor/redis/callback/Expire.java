package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * <p>
 * {@link RedisCacheExecutor RedisCacheExecutor#expire(String, int) expire(expireSeconds)} 和<br/>
 * {@link RedisCacheExecutor RedisCacheExecutor#expireAt(String, Date) expireAt(expireAt)} <code>doInTemplate</code>
 * 方法的回调
 * </p>
 */
public class Expire extends VoidRedisAction {

	private final Expires expires;

	public Expire(CacheSerializable serializable, String key, Integer expireSeconds, Date expireAt) {

		this.expires = new Expires(key, serializable.toRawKey(key), expireSeconds, expireAt);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		expires.expire(connection);
	}
}
