package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * 处理 key 的过期时间
 */
class Expires {

	static Logger LOG = LogManager.getLogger(Expires.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final Integer expireSeconds;

	private final Date expireAt;

	Expires(String key, byte[] rawKey, Integer expireSeconds, Date expireAt) {

		this.key = key;
		this.rawKey = Tools.cloneArray(rawKey);
		this.expireSeconds = expireSeconds;
		this.expireAt = expireAt;
	}

	void expire(RedisConnection connection) {

		if (expireSeconds != null && expireSeconds > 0) {
			Boolean result = connection.expire(rawKey, expireSeconds);
			LOG.debug("ExpireSeconds: key = {}, expire = {}, expireResult = {}", key, expireSeconds, result);
			return;
		}
		if (expireAt != null) {
			Boolean result = connection.expireAt(rawKey, Tools.toSeconds(expireAt.getTime()));
			LOG.debug("ExpireAt: key = {}, expireAt = {}, expireAtResult = {}", key, expireAt, result);
		}
	}

	Date getExpireAt() {

		return expireAt;
	}
}
