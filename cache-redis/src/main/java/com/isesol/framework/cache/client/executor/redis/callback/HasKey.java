package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#hasKey(String) hasKey} <code>doInTemplate</code> 方法的回调
 */
public class HasKey extends OriginalRedisAction<Boolean> {

	static Logger LOG = LogManager.getLogger(HasKey.class.getName());

	private final String key;

	private final byte[] rawKey;

	public HasKey(CacheSerializable serializable, String key) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
	}

	@Override
	public Boolean doInAction(RedisConnection connection) {

		Boolean exists = connection.exists(rawKey);
		LOG.debug("key '{}' exists? {}", key, exists);
		return exists;
	}
}
