package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.springframework.dao.*;
import org.springframework.data.redis.connection.*;

/**
 * <p>
 * {@link RedisCacheExecutor RedisCacheExecutor#getMapEntry(String, String) getMapEntry}
 * <code>doInTemplate</code> 方法的回调
 * </p>
 */
public class HashGet extends ObjectRedisAction<Object> {

	private final String key;

	private final byte[] rawKey;

	private final byte[] rawField;

	public HashGet(CacheSerializable serializable, String key, String field) {

		super(serializable);
		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		this.rawField = serializable.toRawField(key, field);
	}

	@Override
	public Object doInAction(RedisConnection connection) {

		try {
			return connection.hGet(rawKey, rawField);
		} catch (InvalidDataAccessApiUsageException e) {
			throw HashGetAll.toHashUnsupported(e, connection, rawKey, key);
		}
	}
}
