package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import com.isesol.framework.cache.client.executor.redis.conn.*;
import org.apache.logging.log4j.*;
import org.springframework.dao.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

import static org.springframework.data.redis.connection.DataType.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#getMap(String) getMap} <code>doInTemplate</code> 方法的回调
 */
public class HashGetAll<V> extends MapRedisAction<V> {

	static Logger LOG = LogManager.getLogger(HashGetAll.class.getName());

	private final String key;

	private final byte[] rawKey;

	public HashGetAll(CacheSerializable serializable, String key) {

		super(serializable);
		this.key = key;
		this.rawKey = serializable.toRawKey(key);
	}

	static RuntimeException toHashUnsupported(
			RuntimeException e, RedisConnection connection, byte[] rawKey, String key) {

		if (RedisTransaction.isInTransactionContext(connection)) {
			return e;
		}

		DataType type = connection.type(rawKey);
		if (!HASH.equals(type)) {
			return new UnsupportedOperationException(
					"key is '" + key + "' it is '" + type + "'" + " type that is a HASH type unsupported operation");
		}
		return e;
	}

	@Override
	public Map<byte[], byte[]> doInAction(RedisConnection connection) {

		try {
			if (!RedisTransaction.isInTransactionContext(connection) && !connection.exists(rawKey)) {
				LOG.info("key '{}' was not found, return null", key);
				return null;
			}
			return connection.hGetAll(rawKey);
		} catch (InvalidDataAccessApiUsageException e) {
			throw toHashUnsupported(e, connection, rawKey, key);
		}
	}
}
