package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.springframework.dao.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#mgetMapEntry(String, String...) mgetMapEntry}
 * <code>doInTemplate</code> 方法的回调
 */
public class HashMGet<V> extends ListRedisAction<V> {

	private final String key;

	private final byte[] rawKey;

	private final byte[][] rawFields;

	public HashMGet(CacheSerializable serializable, String key, String[] fields) {

		super(serializable);
		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		this.rawFields = serializable.toRawFields(key, fields);
	}

	@Override
	public Collection<byte[]> doInAction(RedisConnection connection) {

		try {
			return connection.hMGet(rawKey, rawFields);
		} catch (InvalidDataAccessApiUsageException e) {
			throw HashGetAll.toHashUnsupported(e, connection, rawKey, key);
		}
	}

}
