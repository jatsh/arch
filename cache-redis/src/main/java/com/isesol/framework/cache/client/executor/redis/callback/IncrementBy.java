package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.dao.*;
import org.springframework.data.redis.connection.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#incrementBy(String, int) incrementBy(String, int)} 和<br/>
 * {@link RedisCacheExecutor RedisCacheExecutor#incrementBy(String, String, int) incrementBy(String, String, int)}
 * <code>doInTemplate</code> 方法的回调
 */
public class IncrementBy extends OriginalRedisAction<Number> {

	static Logger LOG = LogManager.getLogger(IncrementBy.class.getName());

	private final String key;

	private final String field;

	private final byte[] rawKey;

	private final byte[] rawField;

	private final int incrementBy;

	public IncrementBy(CacheSerializable serializable, String key, String field, int incrementBy) {

		this.key = key;
		this.field = field;
		this.incrementBy = incrementBy;
		this.rawKey = serializable.toRawKey(key);
		this.rawField = Tools.isBlank(field) ? null : serializable.toRawField(key, field);
	}

	@Override
	public Number doInAction(RedisConnection connection) {

		if (isStringIncrement()) {
			return stringIncrementBy(connection);
		} else {
			return hashIncrementBy(connection);
		}
	}

	protected Number stringIncrementBy(RedisConnection connection) {

		try {
			Long result = null;
			if (incrementBy == 1) {
				result = connection.incr(rawKey);
			} else {
				result = connection.incrBy(rawKey, incrementBy);
			}
			LOG.debug("[incrementBy] key '{}', result = {}", key, result);
			return result;
		} catch (InvalidDataAccessApiUsageException e) {
			throw new UnsupportedOperationException(
					"key '" + key + "'" + " increment by " + incrementBy + " failed", e);
		}
	}

	protected Number hashIncrementBy(RedisConnection connection) {

		try {
			Long result = connection.hIncrBy(rawKey, rawField, incrementBy);
			LOG.debug(
					"[incrementBy] key '{}', mapKey '{}', incrementBy = {}, result = {}", key, field, incrementBy,
					result);
			return result;
		} catch (InvalidDataAccessApiUsageException e) {
			throw new UnsupportedOperationException(
					"key '" + key + "', mapKey '" + field + "'" + " increment by " + incrementBy + " failed", e);
		}
	}

	protected boolean isStringIncrement() {

		return (rawField == null);
	}
}
