package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

public class IsMember extends OriginalRedisAction<Boolean> {

	static Logger LOG = LogManager.getLogger(IsMember.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final byte[] rawValue;

	public IsMember(CacheSerializable serializable, String key, String value) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		// this.rawValue = serializable.toRawField(key, value);
		this.rawValue = serializable.toRawValue(key, value);
	}

	@Override
	public Boolean doInAction(RedisConnection connection) {

		Boolean sIsMember = connection.sIsMember(rawKey, rawValue);
		LOG.debug("value '{}' sIsMember? {}", key, sIsMember);
		return sIsMember;
	}
}