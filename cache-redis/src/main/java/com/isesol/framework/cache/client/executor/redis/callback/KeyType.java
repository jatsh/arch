package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

/**
 * 获取 key 的数据类型，作为 <code>doInTemplateRequiresNew</code> 方法的回调
 */
public class KeyType extends OriginalRedisAction<DataType> {

	static Logger LOG = LogManager.getLogger(KeyType.class.getName());

	private final String key;

	private final byte[] rawKey;

	public KeyType(CacheSerializable serializable, String key) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
	}

	@Override
	public DataType doInAction(RedisConnection connection) {

		DataType type = connection.type(rawKey);
		LOG.debug("key '{}' type is '{}'", key, type);
		return type;
	}
}
