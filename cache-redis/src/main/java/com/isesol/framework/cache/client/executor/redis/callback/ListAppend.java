package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#listAppend(String, Object, int) listAppend(expireSeconds)}和<br/>
 * {@link RedisCacheExecutor RedisCacheExecutor#listAppend(String, Object, Date) listAppend(expireAt)}
 * <code>doInTemplate</code> 方法的回调
 */
public class ListAppend extends VoidTxRedisAction {

	static Logger LOG = LogManager.getLogger(ListAppend.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final byte[] rawElement;

	private final Expires expires;

	public ListAppend(
			CacheSerializable serializable, String key, Object element, Integer expireSeconds, Date expireAt) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		this.rawElement = serializable.toRawValue(key, element);
		this.expires = new Expires(key, rawKey, expireSeconds, expireAt);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		Long length = connection.rPush(rawKey, rawElement);
		LOG.debug("key = {}, list after push length is {}", key, length);

		expires.expire(connection);
	}
}
