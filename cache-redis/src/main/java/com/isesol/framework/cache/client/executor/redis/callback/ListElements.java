package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.adaptor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import com.isesol.framework.cache.client.executor.redis.conn.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.dao.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#listElements(String) listElements}
 * <code>doInTemplate</code> 方法的回调
 */
public class ListElements<E> extends ListRedisAction<E> {

	private static final int RANGE_ALL_START = 0;

	private static final int RANGE_ALL_END = -1;

	static Logger LOG = LogManager.getLogger(ListElements.class.getName());

	private final DataTypeAdaptors adaptors;

	private final String key;

	private final byte[] rawKey;

	public ListElements(CacheSerializable serializable, DataTypeAdaptors adaptors, String key) {

		super(serializable);
		this.adaptors = adaptors;
		this.key = key;
		this.rawKey = serializable.toRawKey(key);
	}

	@Override
	public Collection<byte[]> doInAction(RedisConnection connection) {

		try {
			Collection<byte[]> elements = connection.lRange(rawKey, RANGE_ALL_START, RANGE_ALL_END);
			if (isNeedConfirmExists(connection, elements)) {
				Boolean exists = connection.exists(rawKey);
				if (Boolean.FALSE.equals(exists)) {
					LOG.debug("[listElements] key '{}' list length is 0 and key not exists, return null", key);
					return null;
				}
			}
			return elements;
		} catch (InvalidDataAccessApiUsageException e) {
			DataType type = connection.type(rawKey);
			LOG.warn(
					"[GetElements] key '{}' is '{}' type that is not 'LIST' type, attemp using DataTypeAdaptor get " +
					"cached object", key, type);
			DataTypeAdaptor adaptor = adaptors.getAdaptor(type);
			return adaptor.adaptGetElements(rawKey);
		}
	}

	private boolean isNeedConfirmExists(RedisConnection connection, Collection<byte[]> elements) {

		return (!RedisTransaction.isInTransactionContext(connection) && Tools.isBlank(elements));
	}

}
