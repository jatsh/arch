package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import com.isesol.framework.cache.client.executor.redis.conn.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#listLength(String) listLength} <code>doInTemplate</code> 方法的回调
 */
public class ListLength extends OriginalRedisAction<Long> {

	static Logger LOG = LogManager.getLogger(ListLength.class.getName());

	private final String key;

	private final byte[] rawKey;

	public ListLength(CacheSerializable serializable, String key) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
	}

	@Override
	public Long doInAction(RedisConnection connection) {

		Long length = connection.lLen(rawKey);
		if (!RedisTransaction.isInTransactionContext(connection) && Long.valueOf(0).equals(length)) {
			Boolean exists = connection.exists(rawKey);
			LOG.debug("key '{}' length is 0, current connection is not in transaction, key exists? {}", key, exists);
			if (Boolean.FALSE.equals(exists)) {
				LOG.debug("key '{}' length is 0, but key not exists, return -1", key);
				return -1L;
			}
		}
		return length;
	}
}
