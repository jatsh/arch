package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

/**
 * {@link RedisCacheExecutor#listPopHead(String) listPopHead(String)} <code>doInTemplate</code> 方法的回调
 */
public class ListPop<E> extends ObjectRedisAction<E> {

	static Logger LOG = LogManager.getLogger(ListPop.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final boolean isHead;

	public ListPop(CacheSerializable serializable, String key) {

		this(serializable, key, true);
	}

	public ListPop(CacheSerializable serializable, String key, boolean isHead) {

		super(serializable);
		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		this.isHead = isHead;
	}

	@Override
	public Object doInAction(RedisConnection connection) {

		Object head = isHead ? connection.lPop(rawKey) : connection.rPop(rawKey);
		LOG.debug("key = {}, isHead = {}, head object = {}", key, isHead, head);
		return head;
	}
}
