package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#listRemoveFirst(String, Object) listRemoveFirst}
 * <code>doInTemplate</code> 方法的回调
 */
public class ListRemoveFirst extends VoidRedisAction {

	private static final int LIST_REMOVE_FIRST = 1;

	static Logger LOG = LogManager.getLogger(ListRemoveFirst.class.getName());

	private final String key;

	private final Object element;

	private final byte[] rawKey;

	private final byte[] rawElement;

	public ListRemoveFirst(CacheSerializable serializable, String key, Object element) {

		this.key = key;
		this.element = element;
		this.rawKey = serializable.toRawKey(key);
		this.rawElement = serializable.toRawValue(key, element);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		Number count = connection.lRem(rawKey, LIST_REMOVE_FIRST, rawElement);
		LOG.debug("key = {}, remove count {}, element = {}", key, count, element);
	}
}
