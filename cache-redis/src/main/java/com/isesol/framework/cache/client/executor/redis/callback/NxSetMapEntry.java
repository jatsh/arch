package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

public class NxSetMapEntry extends OriginalRedisAction<Boolean> {

	static Logger LOG = LogManager.getLogger(NxSetMapEntry.class.getName());

	private final String key;

	private final String field;

	private final byte[] rawKey;

	private final byte[] rawField;

	private final byte[] rawValue;

	private final Expires expires;

	public NxSetMapEntry(
			CacheSerializable serializable, String key, String field, Object value, Integer expireSeconds,
			Date expireAt) {

		this.key = key;
		this.field = field;
		this.rawKey = serializable.toRawKey(key);
		this.rawField = serializable.toRawField(key, field);
		this.rawValue = serializable.toRawHashValue(key, field, value);
		this.expires = new Expires(key, rawKey, expireSeconds, expireAt);
	}

	@Override
	public Boolean doInAction(RedisConnection connection) {

		Boolean result = connection.hSetNX(rawKey, rawField, rawValue);
		LOG.debug("nxSetMapEntry, key = {}, field = {}, result = {}", key, field, result);
		if (Boolean.TRUE.equals(result)) {
			expires.expire(connection);
			return true;
		}
		return false;
	}
}
