package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * {@link RedisCacheExecutor#nxSet(String, Object) nxSet}、
 * {@link RedisCacheExecutor#nxSet(String, Object, int) nxSet(expires)} 和
 * {@link  RedisCacheExecutor#nxSet(String, Object, Date) nxSet(expireAt)}
 * <code>doInTemplate</code> 方法的回调
 */
public class NxStringSet extends OriginalRedisAction<Boolean> {

	static Logger LOG = LogManager.getLogger(NxStringSet.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final byte[] rawValue;

	private final Expires expires;

	public NxStringSet(
			CacheSerializable serializable, String key, Object value, Integer expireSeconds, Date expireAt) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		this.rawValue = serializable.toRawValue(key, value);
		this.expires = new Expires(key, rawKey, expireSeconds, expireAt);
	}

	@Override
	public Boolean doInAction(RedisConnection connection) {

		Boolean result = connection.setNX(rawKey, rawValue);
		LOG.debug("setnx, key = {}, result = {}", key, result);
		if (Boolean.TRUE.equals(result)) {
			expires.expire(connection);
			return true;
		}
		return false;
	}
}
