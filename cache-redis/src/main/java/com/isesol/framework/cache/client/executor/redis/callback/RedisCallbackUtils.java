package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.operation.*;
import com.isesol.framework.cache.client.executor.redis.*;

/**
 * Redis 工具类，主要用于回调接口的参数处理。
 */
public final class RedisCallbackUtils {

	private RedisCallbackUtils() {

	}

	/**
	 * 获取
	 * {@link RedisCacheExecutor#treeSort(String, SortedCond)} 中排序条件中 score
	 * 的起始值。如果排序条件中的起始值为 <code>null</code> 值时，则使用负无穷大
	 * （{@link Double#NEGATIVE_INFINITY}） 作为 score 的最小值。
	 *
	 * @param cond 排序条件
	 *
	 * @return 排序条件中 score 的最小值
	 */
	public static double getDefaultZSetMin(SortedCond cond) {

		if (isMinNull(cond)) {
			return Double.NEGATIVE_INFINITY;
		}
		return cond.getMin();
	}

	/**
	 * 获取
	 * {@link RedisCacheExecutor#treeSort(String, SortedCond)} 中排序条件中 score
	 * 的最大值。如果排序条件中的起始值为 <code>null</code> 值时，则使用 正无穷大 （
	 * {@link Double#POSITIVE_INFINITY}） 作为 score 的最大值。
	 *
	 * @param cond 排序条件
	 *
	 * @return 排序条件中 score 的最小值
	 */
	public static double getDefaultZSetMax(SortedCond cond) {

		if (isMaxNull(cond)) {
			return Double.POSITIVE_INFINITY;
		}
		return cond.getMax();
	}

	private static boolean isMinNull(SortedCond cond) {

		return (cond == null || cond.getMin() == null);
	}

	private static boolean isMaxNull(SortedCond cond) {

		return (cond == null || cond.getMax() == null);
	}
}
