package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

public class SetAdd extends VoidTxRedisAction {

	static Logger LOG = LogManager.getLogger(SetAdd.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final byte[] rawElement;

	private final Expires expires;

	public SetAdd(CacheSerializable serializable, String key, Object element, Integer expireSeconds, Date expireAt) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		this.rawElement = serializable.toRawValue(key, element);
		this.expires = new Expires(key, rawKey, expireSeconds, expireAt);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		Long addResult = connection.sAdd(rawKey, rawElement);
		LOG.debug("key = {}, addResult = {}", key, addResult);
		expires.expire(connection);
	}

}
