package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * {@link RedisCacheExecutor#setMap(String, Map, int) setMap(expireSeconds)} 和
 * {@link  RedisCacheExecutor#setMap(String, Map, Date) setMap(expireAt)}
 * <code>doInTemplate</code> 方法的回调
 */
public class SetMap<V> extends VoidRedisAction {

	static Logger LOG = LogManager.getLogger(SetMap.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final Map<byte[], byte[]> rawHash;

	private final Expires expires;

	public SetMap(
			CacheSerializable serializable, String key, Map<String, V> map, Integer expireSeconds, Date expireAt) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		this.rawHash = serializable.toRawHash(key, map);
		this.expires = new Expires(key, rawKey, expireSeconds, expireAt);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		connection.hMSet(rawKey, rawHash);
		LOG.debug("hMSet finished, key = '{}'", key);

		expires.expire(connection);
	}
}
