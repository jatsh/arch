package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * {@link RedisCacheExecutor#setMapEntry(String, Map) SetMapEntry}
 * <code>doInTemplate</code> 方法的回调
 */
public class SetMapEntry<V> extends VoidRedisAction {

	static Logger LOG = LogManager.getLogger(SetMapEntry.class.getName());

	private final int defaultExpires;

	private final String key;

	private final DataType type;

	private final byte[] rawKey;

	private final Map<byte[], byte[]> rawHash;

	private final String mapKeys;

	public SetMapEntry(
			CacheSerializable serializable, String key, Map<String, V> mapEntry, DataType type, int defaultExpires) {

		this.defaultExpires = defaultExpires;

		this.key = key;
		this.type = type;

		this.rawKey = serializable.toRawKey(key);
		this.rawHash = serializable.toRawHash(key, mapEntry);
		this.mapKeys = Arrays.toString(mapEntry.keySet().toArray(new String[mapEntry.size()]));

	}

	@Override
	public void doInAction(RedisConnection connection) {

		// key 为 HASH 类型时，新增数据
		if (DataType.HASH.equals(type)) {
			connection.hMSet(rawKey, rawHash);
			return;
		}

		// key 为 NONE 类型时，表示 key 不存在，需要设置 key 的过期时间
		if (DataType.NONE.equals(type)) {
			connection.hMSet(rawKey, rawHash);
			connection.expire(rawKey, defaultExpires);
			LOG.warn(
					"key '{}' was not found, set default expires in {}, mapKeys is '{}'", key, defaultExpires,
					mapKeys);
			return;
		}

		// 其他类型，不支持抛异常！
		throw new UnsupportedOperationException(
				"key '" + key + "' has value, but type of " + "the value is '{" + type +
				"}' type (not Hash), cannot set the map entries");
	}
}
