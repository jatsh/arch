package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * {@link RedisCacheExecutor#setMapEntry(String, Map, int) setMapEntry(int)} 和
 * {@link RedisCacheExecutor#setMapEntry(String, Map, Date) setMapEntry(Date)} <code>doInTemplate</code> 方法的回调
 */
public class SetMapEntryExpires<V> extends VoidTxRedisAction {

	static Logger LOG = LogManager.getLogger(SetMapEntryExpires.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final Map<byte[], byte[]> rawHash;

	private final String fields;

	private final Expires expires;

	public SetMapEntryExpires(
			CacheSerializable serializable, String key, Map<String, V> mapEntry, Integer expireSeconds, Date
			expireAt) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		this.rawHash = serializable.toRawHash(key, mapEntry);
		this.expires = new Expires(key, rawKey, expireSeconds, expireAt);
		if (LOG.isDebugEnabled()) {
			this.fields = mapEntry.keySet().toString();
		} else {
			this.fields = null;
		}
	}

	@Override
	public void doInAction(RedisConnection connection) {

		connection.hMSet(rawKey, rawHash);
		LOG.debug("key = {}, HASH set fields: {}", key, fields);
		expires.expire(connection);
	}
}
