package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

public class SetRemove extends VoidRedisAction {

	static Logger LOG = LogManager.getLogger(SetRemove.class.getName());

	private final String key;

	private final Object element;

	private final byte[] rawKey;

	private final byte[] rawElement;

	public SetRemove(CacheSerializable serializable, String key, Object element) {

		this.key = key;
		this.element = element;
		this.rawKey = serializable.toRawKey(key);
		this.rawElement = serializable.toRawValue(key, element);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		Long remResult = connection.sRem(rawKey, rawElement);
		LOG.debug("[setRemove] key = {}, remResult = {}, element = {}", key, remResult, element);
	}
}
