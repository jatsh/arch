package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.adaptor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.dao.*;
import org.springframework.data.redis.connection.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#get(String) } <code>doInTemplate</code> 方法的回调
 */
public class StringGet<V> extends ObjectRedisAction<V> {

	static Logger LOG = LogManager.getLogger(StringGet.class.getName());

	private final DataTypeAdaptors adaptors;

	private final String key;

	private final byte[] rawKey;

	public StringGet(CacheSerializable serializable, DataTypeAdaptors adaptors, String key) {

		super(serializable);
		this.adaptors = adaptors;
		this.key = key;
		this.rawKey = serializable.toRawKey(key);
	}

	@Override
	public Object doInAction(RedisConnection connection) {

		try {
			return connection.get(rawKey);
		} catch (InvalidDataAccessApiUsageException e) {
			DataType type = connection.type(rawKey);
			LOG.warn(
					"key '{}' is '{}' type that is not 'String' type, attemp using DataTypeAdaptor get cached object",
					key, type);
			DataTypeAdaptor adaptor = adaptors.getAdaptor(type);
			return adaptor.adaptGet(rawKey);
		}
	}
}
