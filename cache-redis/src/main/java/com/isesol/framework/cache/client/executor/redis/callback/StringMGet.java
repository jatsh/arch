package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#mget(String...) mget} <code>doInTemplate</code> 方法的回调
 */
public class StringMGet extends ListRedisAction<Object> {

	private final byte[][] rawKeys;

	public StringMGet(CacheSerializable serializable, String... keys) {

		super(serializable);
		this.rawKeys = serializable.toRawKeys(keys);
	}

	@Override
	public Collection<byte[]> doInAction(RedisConnection connection) {

		return connection.mGet(rawKeys);
	}
}
