package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#set(String, Object, Date) set(expireAt)}
 * <code>doInTemplate</code> 方法的回调
 */
public class StringSetExpireAt extends VoidTxRedisAction {

	static Logger LOG = LogManager.getLogger(StringSetExpireAt.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final byte[] rawValue;

	private final Expires expires;

	public StringSetExpireAt(CacheSerializable serializable, String key, Object value, Date expireAt) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		this.rawValue = serializable.toRawValue(key, value);
		this.expires = new Expires(key, rawKey, null, expireAt);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		connection.set(rawKey, rawValue);
		expires.expire(connection);
		LOG.debug("set value, key = {}, expireAt = {}", key, expires.getExpireAt());
	}
}
