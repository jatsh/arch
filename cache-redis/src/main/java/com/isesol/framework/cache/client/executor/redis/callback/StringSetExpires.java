package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#set(String, Object, int) set(expireSeconds)}
 * <code>doInTemplate</code> 方法的回调
 */
public class StringSetExpires extends VoidRedisAction {

	static Logger LOG = LogManager.getLogger(StringSetExpires.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final byte[] rawValue;

	private final int expireSeconds;

	public StringSetExpires(CacheSerializable serializable, String key, Object value, int expireSeconds) {

		this.key = key;
		this.expireSeconds = expireSeconds;
		this.rawKey = serializable.toRawKey(key);
		this.rawValue = serializable.toRawValue(key, value);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		if (expireSeconds > 0) {
			connection.setEx(rawKey, expireSeconds, rawValue);
			LOG.debug("set value, key = {}, expireSeconds = {}", key, expireSeconds);
		} else {
			connection.set(rawKey, rawValue);
			LOG.debug("set value, key = {}", key);
		}
	}
}
