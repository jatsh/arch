package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#ttl(String) ttl} <code>doInTemplate</code> 方法的回调
 */
public class TimeToLive extends OriginalRedisAction<Long> {

	static Logger LOG = LogManager.getLogger(TimeToLive.class.getName());

	private final String key;

	private final byte[] rawKey;

	public TimeToLive(CacheSerializable serializable, String key) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
	}

	@Override
	public Long doInAction(RedisConnection connection) {

		Long ttl = connection.ttl(rawKey);
		LOG.debug("key '{}' time to live remain seconds is {}", key, ttl);
		return ttl;
	}
}
