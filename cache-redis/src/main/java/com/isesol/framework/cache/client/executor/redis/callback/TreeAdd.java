package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#treeAdd(String, Object, double, int) treeAdd(expireSeconds)} 和<br/>
 * {@link RedisCacheExecutor RedisCacheExecutor#treeAdd(String, Object, double, Date) treeAdd(expireAt)}
 * <code>doInTemplate</code> 方法的回调
 */
public class TreeAdd extends VoidTxRedisAction {

	static Logger LOG = LogManager.getLogger(TreeAdd.class.getName());

	private final String key;

	private final double score;

	private final byte[] rawKey;

	private final byte[] rawElement;

	private final Expires expires;

	public TreeAdd(
			CacheSerializable serializable, String key, Object element, double score, Integer expireSeconds,
			Date expireAt) {

		this.key = key;
		this.score = score;
		this.rawKey = serializable.toRawKey(key);
		this.rawElement = serializable.toRawValue(key, element);
		this.expires = new Expires(key, rawKey, expireSeconds, expireAt);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		Boolean addResult = connection.zAdd(rawKey, score, rawElement);
		LOG.debug("key = {}, addResult = {}", key, addResult);
		expires.expire(connection);
	}

}
