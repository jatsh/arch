package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#treeElementScore(String, Object) treeElementScore}
 * <code>doInTemplate</code> 方法的回调
 */
public class TreeElementScore extends OriginalRedisAction<Double> {

	static Logger LOG = LogManager.getLogger(TreeElementScore.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final byte[] rawElement;

	public TreeElementScore(CacheSerializable serializable, String key, Object element) {

		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		this.rawElement = serializable.toRawValue(key, element);
	}

	@Override
	public Double doInAction(RedisConnection connection) {

		Double score = connection.zScore(rawKey, rawElement);
		LOG.debug("key '{}' score is {}", key, score);
		return score;
	}

}
