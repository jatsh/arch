package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#treeRemove(String, Object) treeRemove}
 * <code>doInTemplate</code> 方法的回调
 */
public class TreeRemove extends VoidRedisAction {

	static Logger LOG = LogManager.getLogger(TreeRemove.class.getName());

	private final String key;

	private final Object element;

	private final byte[] rawKey;

	private final byte[] rawElement;

	public TreeRemove(CacheSerializable serializable, String key, Object element) {

		this.key = key;
		this.element = element;
		this.rawKey = serializable.toRawKey(key);
		this.rawElement = serializable.toRawValue(key, element);
	}

	@Override
	public void doInAction(RedisConnection connection) {

		Long remResult = connection.zRem(rawKey, rawElement);
		LOG.debug("[treeRemove] key = {}, remResult = {}, element = {}", key, remResult, element);
	}
}
