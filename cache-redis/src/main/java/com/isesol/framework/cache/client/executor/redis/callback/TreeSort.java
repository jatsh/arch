package com.isesol.framework.cache.client.executor.redis.callback;

import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.operation.*;
import com.isesol.framework.cache.client.executor.redis.*;
import com.isesol.framework.cache.client.executor.redis.action.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;
import org.springframework.data.redis.connection.RedisZSetCommands.*;

import java.util.*;

/**
 * {@link RedisCacheExecutor RedisCacheExecutor#treeSort(String, SortedCond) treeSort}
 * <code>doInTemplate</code> 方法的回调
 */
public class TreeSort<E> extends SortedTreeRedisAction<E> {

	static Logger LOG = LogManager.getLogger(TreeSort.class.getName());

	private final String key;

	private final byte[] rawKey;

	private final SortedCond condition;

	public TreeSort(CacheSerializable serializable, String key, SortedCond condition) {

		super(serializable);
		this.key = key;
		this.rawKey = serializable.toRawKey(key);
		this.condition = condition;
	}

	@Override
	public RedisSortedTree doInAction(RedisConnection connection) {

		double min = RedisCallbackUtils.getDefaultZSetMin(condition);
		double max = RedisCallbackUtils.getDefaultZSetMax(condition);

		LOG.debug(
				"[treeSort] key = {}, isAsc = {}, offset = {}, count = {}, min = {}, max = {}", key, condition.isAsc(),
				condition.getOffset(), condition.getCount(), min, max);


		Long total = connection.zCount(rawKey, min, max);

		Set<Tuple> tuples = null;
		if (condition.isAsc()) {
			tuples = connection.zRangeByScoreWithScores(rawKey, min, max, condition.getOffset(), condition.getCount());
		} else {
			tuples = connection.zRevRangeByScoreWithScores(
					rawKey, min, max, condition.getOffset(), condition.getCount());
		}
		return new RedisSortedTree(total, tuples);
	}
}
