package com.isesol.framework.cache.client.executor.redis.conn;

import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * 忽略事务 API 操作的 Redis 连接对象，主要用于防止事务体内再次进行事务操作。
 */
class IgnoreTxOperationRedisConnection extends DefaultStringRedisConnection implements RedisTxCommands {

	static Logger LOG = LogManager.getLogger(IgnoreTxOperationRedisConnection.class.getName());

	IgnoreTxOperationRedisConnection(RedisConnection connection) {

		super(connection);
	}

	@Override
	public final void multi() {

		LOG.debug("ignore 'multi' operation");
	}

	@Override
	public final List<Object> exec() {

		LOG.debug("ignore 'exec' operation");
		return Collections.emptyList();
	}

	@Override
	public final void discard() {

		LOG.debug("ignore 'discard' operation");
	}

	@Override
	public final void watch(byte[]... keys) {

		LOG.debug("ignore 'watch' operation");
	}

	@Override
	public final void unwatch() {

		LOG.debug("ignore 'unwatch' operation");
	}
}
