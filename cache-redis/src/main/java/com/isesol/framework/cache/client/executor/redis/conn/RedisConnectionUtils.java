package com.isesol.framework.cache.client.executor.redis.conn;

import org.springframework.data.redis.connection.*;

/**
 * Redis 连接对象工具类
 */
final class RedisConnectionUtils {

	private RedisConnectionUtils() {

	}

	/**
	 * 检查 Redis 连接对象是否已经忽略了 Redis 的事务操作 API。通过该方法可以确定当前连接对象是否处于事务环境中。
	 *
	 * @param connection Redis 连接对象
	 *
	 * @return Redis 连接对象是否忽略了 Redis 事务操作 API
	 */
	static boolean isIgnoreTxConnection(RedisConnection connection) {

		return (connection instanceof IgnoreTxOperationRedisConnection);
	}

	/**
	 * 移除 Redis 连接对象中的事务操作 API
	 *
	 * @param connection Redis 连接对象
	 *
	 * @return 忽略了 Redis 事务操作的 Redis 连接对象
	 */
	static RedisConnection removeTxCommands(RedisConnection connection) {

		if (connection == null) {
			return null;
		}

		if (isIgnoreTxConnection(connection)) {
			return connection;
		}

		return new IgnoreTxOperationRedisConnection(connection);
	}
}
