package com.isesol.framework.cache.client.executor.redis.conn;

import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.data.redis.connection.*;

import java.util.*;

/**
 * 封装了 Redis 事务操作。
 */
public final class RedisTransaction {

	static Logger LOG = LogManager.getLogger(RedisTransaction.class.getName());

	/**
	 * 原始的 Redis 连接对象
	 */
	private RedisConnection connection;

	/**
	 * 忽略 Redis 事务 API 操作的连接对象
	 */
	private RedisConnection withoutTxConnection;

	/**
	 * <p>
	 * 通过一个 Redis 连接对象构建 Redis 事务操作对象。
	 * </p>
	 *
	 * @param connection 原始的 Redis 连接对象
	 */
	private RedisTransaction(RedisConnection connection) {

		this.connection = connection;
	}

	/**
	 * 通过原始的 Redis 连接对象，构建 Redis 事务操作对象。
	 *
	 * @param connection Redis 连接对象
	 *
	 * @return Redis 事务操作对象
	 */
	private static RedisTransaction getRedisTransaction(RedisConnection connection) {

		return new RedisTransaction(connection);
	}

	/**
	 * 判断当前连接是否在事务上下文环境中
	 *
	 * @param connection {@link RedisConnection} 对象
	 *
	 * @return {@link RedisConnection} 是否处于事务上下文环境中
	 */
	public static boolean isInTransactionContext(RedisConnection connection) {

		return RedisConnectionUtils.isIgnoreTxConnection(connection);
	}

	/**
	 * <p>
	 * 在一个事务体内进行 Redis 的操作，方法在事务完成后不需获取事务执行后的返回结果。
	 * <p>
	 * <p>
	 * 若 Redis 连接对象已经处于事务体内时， 则事务回调模板在执行时不再开启事务，而是继续在之前的事务体内完成。
	 * </p>
	 *
	 * @param connection Redis 连接对象
	 * @param template 事务操作回调模板
	 *
	 * @throws RedisTransactionException 由于异常原因造成的事务操作失败，或者是事务失败时事务回滚失败
	 */
	public static void doInTransaction(RedisConnection connection, TxTemplate template) {

		doInTransactionWithResults(connection, template);
	}

	/**
	 * <p>
	 * 在一个事务体内进行 Redis 的操作，方法在事务完成返回事务体内 Redis 各命令的执行结果。
	 * <p>
	 * <p>
	 * 若 Redis 连接对象已经处于事务体内时，则事务回调模板在执行时不再开启事务，而是继续在之前的事务体内完成。
	 * </p>
	 *
	 * @param connection Redis 连接对象
	 * @param template 事务操作回调模板
	 *
	 * @return 事务执行后各 Redis 命令的执行结果集。如果操作已处于一个事务环境中时，返回大小为 0 的结果
	 *
	 * @throws RedisTransactionException 由于异常原因造成的事务操作失败，或者是事务失败时事务回滚失败
	 */
	public static List<Object> doInTransactionWithResults(RedisConnection connection, TxTemplate template) {

		LOG.debug("[doInTransaction] RedisConnection type is '{}'", connection);

		// 当前连接已处于一个事务环境中时，不再开启事务
		if (isInTransactionContext(connection)) {
			LOG.trace(
					"[doInTransaction] Current connection is already in transaction context, do not need to open the" +
					" " +
					"transaction");
			template.doInTransaction(connection);
			return Collections.emptyList();
		}

		RedisTransaction tx = getRedisTransaction(connection);

		tx.start();

		try {
			template.doInTransaction(tx.getWithoutTxConnection());
			return tx.commit();
		} catch (Exception e) {
			throw tx.rollbackWithException(e);
		}
	}

	private static String serializeList(List<Object> list) {

		if (list == null) {
			return null;
		}
		StringBuilder builder = new StringBuilder();
		builder.append('[');
		for (int i = 0, k = list.size(); i < k; i++) {
			if (i > 0) {
				builder.append(',').append(' ');
			}
			Object object = list.get(i);
			if (object instanceof byte[]) {
				builder.append(Tools.toString((byte[]) object));
			} else {
				builder.append(Tools.truncate(object));
			}
		}
		builder.append(']');
		return builder.toString();
	}

	/**
	 * 开启事务
	 */
	private void start() {

		connection.multi();
		if (LOG.isTraceEnabled()) {
			LOG.trace("Start transaction in '{}' on thread '{}'", connection, Thread.currentThread());
		}
	}

	/**
	 * 提交事务
	 *
	 * @return 提交事务后命令执行的返回结果
	 */
	private List<Object> commit() {

		List<Object> list = connection.exec();
		if (LOG.isTraceEnabled()) {
			LOG.trace(
					"Commit transaction in '{}' on thread '{}', tx executing result '{}'", connection,
					Thread.currentThread(), serializeList(list));
		}
		return list;
	}

	/**
	 * <p>
	 * 处理由于异常原因导致的，撤消当前事务。
	 * </p>
	 *
	 * @param exception 由于异常原因导致的需要撤销的事务操作
	 *
	 * @return Redis 事务失败的异常信息
	 */
	private RuntimeException rollbackWithException(Exception exception) {

		try {
			connection.discard();
			if (LOG.isTraceEnabled()) {
				LOG.trace(
						"Rollback transaction in '{}' on thread '{}', cause exception '{}'", connection,
						Thread.currentThread(), exception);
			}
		} catch (Exception e) {
			throw new RedisTransactionException("Transaction rollback failed", e);
		}

		LOG.info("Rollback transaction finished, caused by {}", exception);

		if (exception == null) {
			return null;
		}

		if (exception instanceof RuntimeException) {
			return (RuntimeException) exception;
		}

		return new RedisTransactionException(
				"Transactional operation executing failed" + ", transaction has been rollbacked", exception);
	}

	/**
	 * <p>
	 * 获取忽略 Redis 事务 API 操作的连接对象。
	 * </p>
	 *
	 * @return 忽略 Redis 事务 API 操作的连接对象
	 */
	private RedisConnection getWithoutTxConnection() {

		if (withoutTxConnection == null) {
			withoutTxConnection = RedisConnectionUtils.removeTxCommands(connection);
			if (LOG.isTraceEnabled()) {
				LOG.trace("Generated without transactional '{}' from '{}'", withoutTxConnection, connection);
			}
		}

		return withoutTxConnection;
	}
}
