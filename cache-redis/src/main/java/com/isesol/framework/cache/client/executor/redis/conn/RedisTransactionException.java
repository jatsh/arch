package com.isesol.framework.cache.client.executor.redis.conn;

import org.springframework.dao.*;

/**
 * Redis 事务处理失败而产生的异常，或者是数据回滚时产生异常
 */
public class RedisTransactionException extends DataAccessException {

	public RedisTransactionException(String msg, Throwable cause) {

		super(msg, cause);
	}
}
