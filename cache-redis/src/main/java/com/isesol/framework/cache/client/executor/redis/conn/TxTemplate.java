package com.isesol.framework.cache.client.executor.redis.conn;

import org.springframework.data.redis.connection.*;

/**
 * Redis 事务回调模板，用于处理需要在一个事务体内执行的操作。
 */
public interface TxTemplate {

	/**
	 * 在事务体内执行 Redis 操作。
	 *
	 * @param txconnection Redis 连接对象
	 */
	void doInTransaction(RedisConnection txconnection);
}
