package com.isesol.framework.cache.client.executor.serializer;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;

public abstract class AbstractCacheSerializer<T> implements CacheSerializer<T> {

	static Logger log = LogManager.getLogger(AbstractCacheSerializer.class.getName());


	@Override
	public byte[] serialize(T t) {

		if (t == null) {
			log.warn("serialize Object is null, return null");
			return null;
		}
		try {
			return doSerialize(t);
		} catch (Exception e) {
			throw new CacheSerializeException("serialize error, object: " + t, e);
		}
	}

	@Override
	public T deserialize(byte[] bytes) {

		if (bytes == null) {
			log.warn("deserialize byte array is null, return null");
			return null;
		}
		try {
			return doDeserialize(bytes);
		} catch (Exception e) {
			throw new CacheSerializeException(
					"deserialize error, byte array (hex decoded): " + Tools.toHexSpace(bytes), e);
		}
	}

	protected abstract byte[] doSerialize(T t);

	protected abstract T doDeserialize(byte[] bytes);
}
