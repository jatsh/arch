package com.isesol.framework.cache.client.executor.serializer;

import com.isesol.framework.cache.client.exception.*;

/**
 * Basic interface serialization and deserialization of Objects to byte arrays
 * (binary data).
 * <p>
 * It is recommended that implementations are designed to handle null
 * objects/empty arrays on serialization and deserialization side.
 * <p>
 * Note that does not accept null keys or values but can return null replies
 * (for non existing keys).
 *
 * @see org.springframework.data.redis.serializer.RedisSerializer
 */
public interface CacheSerializer<T> {

	/**
	 * Serialize the given object to binary data.
	 *
	 * @param t object to serialize
	 *
	 * @return the equivalent binary data
	 *
	 * @throws CacheSerializeException
	 */
	byte[] serialize(T t);

	/**
	 * Deserialize an object from the given binary data.
	 *
	 * @param bytes object binary representation
	 *
	 * @return the equivalent object instance
	 *
	 * @throws CacheSerializeException
	 */
	T deserialize(byte[] bytes);
}
