package com.isesol.framework.cache.client.executor.serializer;

import com.alibaba.fastjson.*;
import com.alibaba.fastjson.parser.*;
import com.alibaba.fastjson.serializer.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;
import org.springframework.data.redis.serializer.*;

import java.util.*;

import static com.alibaba.fastjson.parser.Feature.*;
import static com.alibaba.fastjson.serializer.SerializerFeature.*;

public class FastjsonSerializer implements CacheSerializer<Object>, InitializingBean {

	protected static final byte[] EMPTY_ARRAY = new byte[0];

	static Logger log = LogManager.getLogger(FastjsonSerializer.class.getName());

	private SerializerFeature[] serializerFeatures;

	private Feature[] features;

	public FastjsonSerializer() {

	}

	@Override
	public void afterPropertiesSet() {

		if (Tools.isBlank(serializerFeatures)) {
			serializerFeatures = new SerializerFeature[]{WriteClassName};
		}

		if (Tools.isBlank(features)) {
			features = new Feature[]{AllowSingleQuotes, DisableASM};
		}
	}

	@Override
	public byte[] serialize(Object t) {

		if (t == null) {
			log.warn("Serialize null object will be return zero length byte array");
			return EMPTY_ARRAY;
		}
		try {

			Object converted = convertUnmodifiable(t);

			return JSON.toJSONBytes(converted, serializerFeatures);

		} catch (Exception e) {
			log.error("Serialize object to JSON cause error, {}", t, e);
			throw new SerializationException("Could not write JSON: " + e.getMessage(), e);
		}
	}

	@Override
	public Object deserialize(byte[] bytes) {

		if (Tools.isBlank(bytes)) {
			log.debug("Deserialize empty bytes or null value will be return null value");
			return null;
		}
		try {
			return JSON.parse(bytes, features);
		} catch (Exception e) {
			log.error("Deserialize JSON bytes to object cause error, {}", Tools.toString(bytes), e);
			throw new SerializationException("Could not read JSON: " + e.getMessage(), e);
		}
	}

	public void setSerializerFeatures(SerializerFeature[] serializerFatures) {

		this.serializerFeatures = Tools.cloneArray(serializerFatures);
	}

	public void setFeatures(Feature[] features) {

		this.features = Tools.cloneArray(features);
	}

	protected Object convertUnmodifiable(Object object) {

		String className = object.getClass().getName();

		if ("java.util.Collections$UnmodifiableMap".equals(className)) {

			return new HashMap<Object, Object>((Map<?, ?>) object);
		}

		if ("java.util.Collections$UnmodifiableList".equals(className)) {

			return new ArrayList<Object>((List<?>) object);
		}

		if ("java.util.Collections$UnmodifiableSet".equals(className)) {

			return new HashSet<Object>((Set<?>) object);
		}

		return object;
	}
}
