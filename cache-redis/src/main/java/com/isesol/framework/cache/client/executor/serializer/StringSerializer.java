package com.isesol.framework.cache.client.executor.serializer;

import com.isesol.framework.cache.client.util.*;

import java.nio.charset.*;

public class StringSerializer extends AbstractCacheSerializer<String> {

	private final Charset charset;

	public StringSerializer() {

		this(Tools.DEFAULT_CHARSET);
	}

	public StringSerializer(String charsetName) {

		this(Charset.forName(charsetName));
	}

	public StringSerializer(Charset charset) {

		this.charset = charset;
	}

	@Override
	protected byte[] doSerialize(String t) {

		return t.getBytes(charset);
	}

	@Override
	protected String doDeserialize(byte[] bytes) {

		return new String(bytes, charset);
	}

}
