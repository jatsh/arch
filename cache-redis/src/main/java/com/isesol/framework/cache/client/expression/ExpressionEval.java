package com.isesol.framework.cache.client.expression;
import com.isesol.framework.cache.client.annotation.*;

/**
 * 表达式解析器。采用方法运行时的调用参数，以及缓存方法上标识的表达式，解析缓存方法标注中的 key 值，以及缓存条件值。
 *
 * @see EnableCacheable
 * EnableCacheable
 * @see ClearingCached
 * ClearingCached
 */
public interface ExpressionEval {

	/**
	 * 根据缓存方法实际运行时的参数，计算缓存方法标注上的 <code>key</code> 值。
	 *
	 * @param parameters 方法调用参数列表
	 *
	 * @return 根据方法调用参数，计算表达式的结果
	 *
	 * @see EnableCacheable#key()
	 * EnableCacheable#key()
	 * @see ClearingCached#key()
	 * ClearingCached#key()
	 */
	Object computeExpressionKey(Object[] parameters);

	/**
	 * 根据缓存方法实际运时的参数，计算缓存方法标注上的 <code>condition</code> 表达式， 以确定方法是否需要进行缓存操作处理。
	 *
	 * @param parameters 方法调用参数列表
	 *
	 * @return 缓存方法标识根据当前调用方法的参数值确定是否需要进行缓存操作
	 *
	 * @see EnableCacheable#condition()
	 * EnableCacheable#condition()
	 * @see ClearingCached#condition()
	 * ClearingCached#condition()
	 */
	boolean computeCondition(Object[] parameters);
}
