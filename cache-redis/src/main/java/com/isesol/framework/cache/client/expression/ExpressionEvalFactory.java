package com.isesol.framework.cache.client.expression;

import java.lang.reflect.*;

public interface ExpressionEvalFactory {

	ExpressionEval createEval(String key, String condition, Method method);
}
