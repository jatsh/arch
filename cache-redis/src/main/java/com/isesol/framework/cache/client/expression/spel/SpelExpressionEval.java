package com.isesol.framework.cache.client.expression.spel;

import com.isesol.framework.cache.client.expression.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.expression.*;
import org.springframework.expression.spel.standard.*;
import org.springframework.expression.spel.support.*;
import org.springframework.util.*;

import java.lang.reflect.*;
import java.util.*;

public class SpelExpressionEval implements ExpressionEval {

	static Logger log = LogManager.getLogger(SpelExpressionEval.class.getName());

	/**
	 * SpEL expression parser
	 */
	private static ExpressionParser parser = new SpelExpressionParser();

	/**
	 * Target method
	 */
	private Method method;

	/**
	 * Cache key SpEL expression object
	 */
	private Expression key;

	/**
	 * Cache condition SpEL expression object
	 */
	private Expression condition;

	/**
	 * Cache key original expression
	 */
	private String keyExpr;

	/**
	 * Cache condition original expression
	 */
	private String condExpr;

	public SpelExpressionEval(String key, String condition, Method method) {

		this.keyExpr = key;
		this.condExpr = condition;
		this.key = parse(key);
		this.condition = parse(condition);
		this.method = method;
	}

	public ExpressionParser getParser() {

		return parser;
	}

	public Expression getKey() {

		return key;
	}

	public Expression getCondition() {

		return condition;
	}

	public String getKeyExpr() {

		return keyExpr;
	}

	public String getCondExpr() {

		return condExpr;
	}

	@Override
	public Object computeExpressionKey(Object[] parameters) {

		if (key == null) {

			return keyExpressionNull(parameters);
		}

		if (parameters == null || parameters.length == 0) {

			if (log.isDebugEnabled()) {
				log.debug(
						"compute key '{}', parameters is null or empty, method: {}, return {}", keyExpr,
						getMethodSignature(), Arrays.toString(parameters));
			}

			return Arrays.toString(parameters);
		}

		EvaluationContext context = createContext(parameters);
		Object result = key.getValue(context);

		if (log.isDebugEnabled()) {
			log.debug(
					"compute key result '{}', key '{}', parameters: {}, method: {}", result, keyExpr, Arrays.toString(
							parameters), getMethodSignature());
		}

		return result;
	}

	@Override
	public boolean computeCondition(Object[] parameters) {

		if (condition == null) {
			log.debug("compute condition '{}', condition expression object is null, return true", condExpr);
			return true;
		}

		if (parameters == null) {
			log.warn("compute condition '{}', parameters is null, return false", condExpr);
			return false;
		}

		EvaluationContext context = createContext(parameters);
		Object result = condition.getValue(context);

		if (log.isDebugEnabled()) {
			log.debug(
					"compute condition result is '{}', cond '{}', parameters: {}, method: {}", result, condExpr,
					Arrays.toString(parameters), getMethodSignature());
		}

		if (result instanceof Boolean) {

			return (Boolean) result;
		}

		return conditionResultNotBoolean(result, parameters);
	}

	protected Object keyExpressionNull(Object[] parameters) {

		log.warn(
				"compute key [" + keyExpr + "], key expression object is null" + ", method: " +
				CacheClientUtils.getSimpleSignature(method) + ", return: " + Arrays.toString(parameters));

		return Arrays.toString(parameters);
	}

	protected boolean conditionResultNotBoolean(Object result, Object[] parameters) {

		log.warn(
				"compute condition result type is [" + (
						result == null ? null : result.getClass()) + "]" +
				" that is not boolean type, so the condition result as FALSE returning" + ", cond: [" + condExpr +
				"]" +
				", parameters: " + Arrays.toString(parameters) + ", method: " + getMethodSignature());

		return false;
	}

	private String getMethodSignature() {

		return CacheClientUtils.getSimpleSignature(method).toString();
	}

	private EvaluationContext createContext(Object[] parameters) {

		EvaluationContext context = new StandardEvaluationContext();

		for (int i = 0; i < parameters.length; i++) {

			String name = MethodArgumentNames.getName(method, i);

			if (name != null) {

				context.setVariable(name, parameters[i]);
			}

			context.setVariable("a" + i, parameters[i]);
			context.setVariable("p" + i, parameters[i]);
		}

		return context;
	}

	private Expression parse(String expression) {

		if (!StringUtils.hasText(expression)) {

			return null;
		}

		return parser.parseExpression(expression);
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("keyExpr", keyExpr);
		builder.append("condExpr", condExpr);
		return builder.toString();
	}
}
