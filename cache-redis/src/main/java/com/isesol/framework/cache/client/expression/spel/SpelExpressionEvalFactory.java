package com.isesol.framework.cache.client.expression.spel;

import com.isesol.framework.cache.client.expression.*;

import java.lang.reflect.*;

public class SpelExpressionEvalFactory implements ExpressionEvalFactory {

	@Override
	public ExpressionEval createEval(String key, String condition, Method method) {

		return new SpelExpressionEval(key, condition, method);
	}
}
