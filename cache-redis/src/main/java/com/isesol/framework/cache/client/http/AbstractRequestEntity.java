package com.isesol.framework.cache.client.http;

import com.isesol.framework.cache.client.util.*;

import java.io.*;
import java.nio.charset.*;

public abstract class AbstractRequestEntity implements RequestEntity {

	@Override
	public byte[] getBytesEntity(Charset charset) {

		String entity = getEntity(charset);
		if (entity == null || entity.length() == 0) {
			return new byte[0];
		}
		return entity.getBytes(charset);
	}

	@Override
	public void writeTo(OutputStream out, Charset charset) throws IOException {

		byte[] bys = getBytesEntity(charset);
		if (Tools.isBlank(bys)) {
			return;
		}
		out.write(bys);
	}

	public String getDefaultEncodingEntity() {

		return getEntity(HttpRequestor.DEFAULT_CHARSET);
	}

	@Override
	public String toString() {

		return getDefaultEncodingEntity();
	}
}
