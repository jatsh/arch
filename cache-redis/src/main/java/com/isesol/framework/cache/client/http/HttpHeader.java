package com.isesol.framework.cache.client.http;

/**
 * HTTP 请求首部数据
 */
public final class HttpHeader {

	/**
	 * HTTP 请求首部的名称
	 */
	private String name;

	/**
	 * HTTP 请求首部的数据值
	 */
	private String value;

	HttpHeader(String name, String value) {

		this.name = name;
		this.value = value;
	}

	/**
	 * <p>
	 * 获取 HTTP 请求首部的首部名称
	 * </p>
	 *
	 * @return HTTP 请求首部的首部名称
	 */
	public String getName() {

		return name;
	}

	/**
	 * <p>
	 * 获取 HTTP 请求首部的数据值
	 * </p>
	 *
	 * @return HTTP 请求首部的数据值
	 */
	public String getValue() {

		return value;
	}

	@Override
	public String toString() {

		if (name == null) {
			return "HttpHeader [" + value + "]";
		}
		return "HttpHeader [" + name + ": " + value + "]";
	}
}
