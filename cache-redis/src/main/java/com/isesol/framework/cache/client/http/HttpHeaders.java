package com.isesol.framework.cache.client.http;

import com.isesol.framework.cache.client.util.*;

import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 * HTTP 报文头
 */
public final class HttpHeaders {

	/**
	 * 位于 <code>Content-Type</code> 报头中字符编码的正则表达式匹配模式
	 */
	private static final Pattern CONTENT_TYPE_CHARSET = Pattern.compile("(?i:charset)\\s*=\\s*([^\\s]+)");

	/**
	 * HTTP 报头集合
	 */
	private Map<CaseInsensiveKey, List<String>> headers;

	public HttpHeaders() {

	}

	public HttpHeaders(Map<String, List<String>> headers) {

		addHeaders(headers);
	}

	/**
	 * <p>
	 * 添加一个 HTTP 报头数据
	 * </p>
	 *
	 * @param name 报头名称（报头的名称中的字母字符不区分大小写）
	 * @param value 报头值
	 *
	 * @throws IllegalArgumentException 报头名称、报头值为 <code>null</code> 或者为空白字符
	 */
	public void addHeader(String name, String value) {

		Assert.notEmpty(name, "name");
		Assert.notEmpty(value, "value");
		internalAddHeader(name, value);
	}

	/**
	 * <p>
	 * 获取位于 <code>Content-Type</code> 报头中的字符编码
	 * </p>
	 *
	 * @return 位于 <code>Content-Type</code> 报头中的字符编码，若该报头中不存在字符编码时， 则返回
	 * <code>null</code> 值
	 */
	public String getContentTypeCharset() {

		String contentType = getHeader("Content-Type");
		if (Tools.isBlank(contentType)) {
			return null;
		}
		Matcher matcher = CONTENT_TYPE_CHARSET.matcher(contentType);
		while (matcher.find()) {
			return matcher.group(1).trim();
		}
		return null;
	}

	/**
	 * <p>
	 * 通过报头名称获取报头名称为该名称的第一个报文值
	 * </p>
	 *
	 * @param name 报头名称（报头的名称中的字母字符不区分大小写）
	 *
	 * @return 指定报头值，若报头不存在时，返回 <code>null</code> 值。若指定报头名称的报头有多个时，则返回第一个
	 *
	 * @throws IllegalArgumentException 报头名称为 <code>null</code> 或者为空白字符
	 */
	public String getHeader(String name) {

		Assert.notEmpty(name, "name");
		List<String> values = getHeaders(name);
		if (values == null) {
			return null;
		}
		return values.get(0);
	}

	/**
	 * <p>
	 * 通过报头名称获取报头名称为该名称所有的报文值
	 * </p>
	 *
	 * @param name 报头名称（报头的名称中的字母字符不区分大小写）
	 *
	 * @return 指定报头名称所有的报头值，若报头不存在时，返回 <code>null</code> 值。返回的是
	 * <code>List</code> 类型的数据，该 <code>List</code> 是只读的，不允许被修改
	 *
	 * @throws IllegalArgumentException 报头名称为 <code>null</code> 或者为空白字符
	 */
	public List<String> getHeaders(String name) {

		Assert.notEmpty(name, "name");
		if (Tools.isBlank(headers)) {
			return null;
		}
		List<String> values = headers.get(new CaseInsensiveKey(name));
		if (Tools.isBlank(values)) {
			return null;
		}
		return Collections.unmodifiableList(values);
	}

	/**
	 * <p>
	 * 获取所有的报头数据
	 * </p>
	 *
	 * @return 所有的报头数据。若无任何报头数据时，将返回空值。该方法永远不会返回 <code>null</code> 值。 返回的
	 * <code>Map</code> 数据类型是只读取，不允许被修改
	 */
	public Map<String, List<String>> getHeaders() {

		if (Tools.isBlank(headers)) {
			return Collections.emptyMap();
		}
		Map<String, List<String>> unmodified = new LinkedHashMap<String, List<String>>();
		for (Map.Entry<CaseInsensiveKey, List<String>> entry : headers.entrySet()) {
			List<String> values = entry.getValue();
			if (Tools.isBlank(values)) {
				continue;
			}
			unmodified.put(entry.getKey().getKey(), Collections.unmodifiableList(entry.getValue()));
		}
		return Collections.unmodifiableMap(unmodified);
	}

	/**
	 * <p>
	 * 获取所有报头数据的 {@link Iterable} 对象，方便用于迭代循环所有的报头数据
	 * </p>
	 *
	 * @return 所有报头数据 {@link Iterable} 对象。返回的对象是只读的，不允许被修改
	 */
	public Iterable<HttpHeader> getHeadersIterable() {

		if (Tools.isBlank(headers)) {
			return Collections.emptyList();
		}
		final List<HttpHeader> headersList = new LinkedList<HttpHeader>();
		Tools.iterateCallback(headers, new HttpHeaderAssembler(headersList));
		return new NullSafeReadOnlyIterable<HttpHeader>(headersList);
	}

	private void addHeaders(Map<String, List<String>> headers) {

		Tools.iterateCallback(
				headers, new Tools.MapCollectionCallback<String, String>() {
					@Override
					public void doCallback(String key, String value) {

						internalAddHeader(key, value);
					}
				});
	}

	private void internalAddHeader(String name, String value) {

		if (headers == null) {
			headers = new LinkedHashMap<CaseInsensiveKey, List<String>>();
		}
		CaseInsensiveKey key = new CaseInsensiveKey(name);
		List<String> values = headers.get(key);
		if (values == null) {
			values = new LinkedList<String>();
			headers.put(key, values);
		}
		values.add(value);
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();

		if (Tools.isBlank(headers)) {
			return builder.append("null").toString();
		}

		int offset = 0;
		for (HttpHeader header : getHeadersIterable()) {
			if (offset++ > 0) {
				builder.append("\r\n");
			}
			if (!Tools.isBlank(header.getName())) {
				builder.append(header.getName()).append(": ");
			}
			builder.append(header.getValue());
		}

		return builder.toString();
	}

	private static class HttpHeaderAssembler implements Tools.MapCollectionCallback<CaseInsensiveKey, String> {

		private List<HttpHeader> headersList;

		public HttpHeaderAssembler(List<HttpHeader> headersList) {

			this.headersList = headersList;
		}

		@Override
		public void doCallback(CaseInsensiveKey key, String value) {

			headersList.add(new HttpHeader(key.getKey(), value));
		}
	}

	private static class CaseInsensiveKey implements Serializable, Comparable<CaseInsensiveKey> {

		private String key;

		public CaseInsensiveKey(String key) {

			this.key = key;
		}

		public String getKey() {

			return key;
		}

		@Override
		public int compareTo(CaseInsensiveKey o) {

			if (key == null) {
				return -1;
			}
			if (o.key == null) {
				return Integer.MAX_VALUE;
			}
			return key.toLowerCase().compareTo(o.key.toLowerCase());
		}

		@Override
		public int hashCode() {

			final int prime = 31;
			int result = 1;
			result = prime * result + (
					(key == null) ? 0 : key.toLowerCase().hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {

			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof CaseInsensiveKey)) {
				return false;
			}
			CaseInsensiveKey other = (CaseInsensiveKey) obj;
			if (key == null) {
				if (other.key != null) {
					return false;
				}
			} else if (!key.equalsIgnoreCase(other.key)) {
				return false;
			}
			return true;
		}

		@Override
		public String toString() {

			return key;
		}
	}
}
