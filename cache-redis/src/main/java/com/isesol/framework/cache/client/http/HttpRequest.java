package com.isesol.framework.cache.client.http;

import com.isesol.framework.cache.client.util.*;

import java.io.*;
import java.nio.charset.*;
import java.util.*;

/**
 * HTTP 请求数据。包括请求数据的字符编码、HTTP 请求报头，以及 HTTP 请求实体部分。
 *
 * @see HttpHeader
 * @see HttpHeaders
 * @see HttpResponse
 */
public final class HttpRequest {

	/**
	 * HTTP 请求数据的字符编码
	 */
	private Charset charset;

	/**
	 * HTTP 请求报头
	 */
	private HttpHeaders requestHeader;

	/**
	 * HTTP 请求实体
	 */
	private RequestEntity requestEntity;

	public HttpRequest() {

		this(HttpRequestor.DEFAULT_CHARSET);
	}

	public HttpRequest(String charsetName) {

		this(Charset.forName(Tools.trim(charsetName)));
	}

	public HttpRequest(Charset charset) {

		Assert.notNull(charset, "charset");
		this.charset = charset;
		this.requestHeader = new HttpHeaders();
	}

	/**
	 * <p>
	 * 为一个 HTTP 请求添加一个请求报头数据
	 * </p>
	 *
	 * @param name 请求报头名称
	 * @param value 请求报头值
	 *
	 * @throws IllegalArgumentException 请求报头名称、报头值为 <code>null</code> 或者为空白字符
	 */
	public void addHeader(String name, String value) {

		Assert.notEmpty(name, "name");
		Assert.notEmpty(value, "value");
		requestHeader.addHeader(name, value);
	}

	/**
	 * <p>
	 * 通过一个请求报头名称获取该报头的值
	 * </p>
	 *
	 * @param name 报头名称
	 *
	 * @return 指定报头值，若报头不存在时，返回 <code>null</code> 值
	 *
	 * @throws IllegalArgumentException 报头名称为 <code>null</code> 或者为空白字符
	 */
	public String getHeader(String name) {

		Assert.notEmpty(name, "name");
		return requestHeader.getHeader(name);
	}

	public List<String> getHeaders(String name) {

		Assert.notEmpty(name, "name");
		return requestHeader.getHeaders(name);
	}

	public Map<String, List<String>> getHeaders() {

		return requestHeader.getHeaders();
	}

	public Iterable<HttpHeader> getHeadersIterable() {

		return requestHeader.getHeadersIterable();
	}

	public void writeRequestEntityTo(OutputStream out) throws IOException {

		if (requestEntity == null) {
			return;
		}
		requestEntity.writeTo(out, charset);
	}

	public boolean hasRequestEntity() {

		return (requestEntity != null);
	}

	public String getStringRequestEntity() {

		if (requestEntity == null) {
			return null;
		}
		return requestEntity.getEntity(charset);
	}

	public void setStringRequestEntity(String str) {

		if (str == null) {
			return;
		}
		this.requestEntity = new StringRequestEntity(str);
	}

	public String appendQueryRequestEntity(String endpoint) {

		String entity = getStringRequestEntity();
		if (Tools.isBlank(entity)) {
			return endpoint;
		}
		return endpoint + '?' + entity;
	}

	public byte[] getBytesRequestEntity() {

		if (requestEntity == null) {
			return null;
		}
		return requestEntity.getBytesEntity(charset);
	}

	public void setRequestEntity(RequestEntity requestEntity) {

		this.requestEntity = requestEntity;
	}

	public Charset getCharset() {

		return charset;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("HttpRequest inspect information:");
		builder.append("\n------------------------------------------------");
		builder.append("\n").append(requestHeader);
		builder.append("\n\n").append(requestEntity);
		builder.append("\n------------------------------------------------");
		return builder.toString();
	}
}
