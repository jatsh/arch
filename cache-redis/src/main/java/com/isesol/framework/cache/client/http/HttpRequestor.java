package com.isesol.framework.cache.client.http;

import com.isesol.framework.cache.client.util.*;

import java.io.*;
import java.nio.charset.*;

/**
 * HTTP 请求处理器，目前仅支持 GET 和 POST 的 HTTP 请求方法。
 */
public interface HttpRequestor {

	/**
	 * HTTP 请求/响应默认字符编码（UTF-8）
	 */
	Charset DEFAULT_CHARSET = Tools.DEFAULT_CHARSET;

	/**
	 * <p>
	 * 执行 HTTP/GET 请求。
	 * </p>
	 *
	 * @param request HTTP/GET 请求参数
	 *
	 * @return HTTP/GET 请求响应数据
	 *
	 * @throws IOException HTTP 请求网络通信异常
	 */
	HttpResponse doGet(HttpRequest request) throws IOException;

	/**
	 * <p>
	 * 执行 HTTP/POST 请求。
	 * </p>
	 *
	 * @param request HTTP/POST 请求参数
	 *
	 * @return HTTP/POST 请求响应数据
	 *
	 * @throws IOException HTTP 请求网络通信异常
	 */
	HttpResponse doPost(HttpRequest request) throws IOException;
}
