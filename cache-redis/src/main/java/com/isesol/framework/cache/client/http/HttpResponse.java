package com.isesol.framework.cache.client.http;

import com.isesol.framework.cache.client.util.*;

import java.nio.charset.*;
import java.util.*;

/**
 * HTTP/1.1 响应数据。包括响应状态码、响应状态信息、响应报头，以及响应报体。
 */
public final class HttpResponse {

	/**
	 * HTTP 响应状态码（OK） -- 200
	 */
	public static final int STATUS_OK = 200;

	/**
	 * HTTP 响应状态码（Not Modified） -- 304
	 */
	public static final int STATUS_NOT_MODIFIED = 304;

	/**
	 * HTTP 响应状态码（Not Found） -- 404
	 */
	public static final int STATUS_NOT_FOUND = 404;

	/**
	 * HTTP 响应状态码（Internal Server Error） -- 500
	 */
	public static final int STATUS_INTERNAL_SERVER_ERROR = 500;

	/**
	 * HTTP 响应状态码为 Success 系列（2xx）的最小值 -- 200
	 */
	private static final int STATUS_SUCCESS_MIN = 200;

	/**
	 * HTTP 响应状态码为 Success 系列（2xx）的最大值 -- 299
	 */
	private static final int STATUS_SUCCESS_MAX = 299;

	/**
	 * HTTP 响应状态码
	 */
	private int responseCode;

	/**
	 * HTTP 响应状态信息
	 */
	private String responseMessage;

	/**
	 * HTTP 响应报头
	 */
	private HttpHeaders responseHeader;

	/**
	 * HTTP 响应实体
	 */
	private byte[] responseData;

	public HttpResponse(
			int responseCode, String responseMessage, byte[] responseData, Map<String, List<String>> headers) {

		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
		this.responseData = Tools.cloneArray(responseData);
		this.responseHeader = new HttpHeaders(headers);
	}

	public void addHeader(String name, String value) {

		throw new UnsupportedOperationException("http response object unsupports the operation");
	}

	/**
	 * 获取 HTTP 响应状态码
	 *
	 * @return HTTP 响应状态码
	 */
	public int getResponseCode() {

		return responseCode;
	}

	/**
	 * 获取 HTTP 响应状态信息
	 *
	 * @return HTTP 响应状态信息
	 */
	public String getResponseMessage() {

		return responseMessage;
	}

	/**
	 * 获取 HTTP 响应是否是“成功”的状态码
	 *
	 * @return 响应状态码是否为“2xx”或者“304”，以确定响应是否成功
	 */
	public boolean isSuccessResponse() {

		if (responseCode >= STATUS_SUCCESS_MIN && responseCode <= STATUS_SUCCESS_MAX) {
			return true;
		}
		if (responseCode == STATUS_NOT_MODIFIED) {
			return true;
		}
		return false;
	}

	/**
	 * 获取原始的响应实体数据
	 *
	 * @return 字节数组格式原始的响应实体数据。若响应实体不存在时，返回 <code>null</code> 值。
	 */
	public byte[] getResponseData() {

		return Tools.cloneArray(responseData);
	}

	/**
	 * 指定字符编码获取响应实体数据
	 *
	 * @param charsetName 字符编码
	 *
	 * @return 响应实体数据
	 *
	 * @throws IllegalArgumentException 字符编码值为 <code>null</code> 值，或者为空白字符
	 * @throws UnsupportedCharsetException 不支持的字符编码
	 * @see #getResponseData()
	 * @see #getResponseData(Charset)
	 * @see #getContentTypeResponse()
	 * @see #getDefaultEncodingResponse()
	 */
	public String getResponseData(String charsetName) {

		Assert.notEmpty(charsetName, "charsetName");
		return getResponseData(Charset.forName(Tools.trim(charsetName)));
	}

	/**
	 * 指定字符编码获取响应实体数据
	 *
	 * @param charset 字符集对象
	 *
	 * @return 响应实体数据。若响应实体不存在时，返回 <code>null</code> 值；若响应实体数据为空时，返回空串
	 *
	 * @throws IllegalArgumentException 字符集对象为 <code>null</code> 值
	 * @see #getResponseData()
	 * @see #getResponseData(String)
	 * @see #getContentTypeResponse()
	 * @see #getDefaultEncodingResponse()
	 */
	public String getResponseData(Charset charset) {

		Assert.notNull(charset, "charset");
		if (responseData == null) {
			return null;
		}
		if (responseData.length == 0) {
			return "";
		}
		return new String(responseData, charset);
	}

	/**
	 * 通过响应报头 <code>Content-Type</code> 值中 <code>charset</code>
	 * 所指定的编码值获取字符串格式的响应实体数据。
	 * <p>或无法从 <code>Content-Type</code> 获取字符编码时，以 HTTP 响应默认的字符编码进行处理。
	 * </p>
	 *
	 * @return 响应实体数据。若响应实体不存在时，返回 <code>null</code> 值；若响应实体数据为空时，返回空串
	 *
	 * @see #getResponseData()
	 * @see #getResponseData(Charset)
	 * @see #getResponseData(String)
	 * @see #getDefaultEncodingResponse()
	 */
	public String getContentTypeResponse() {

		String charsetName = responseHeader.getContentTypeCharset();
		if (Tools.isBlank(charsetName)) {
			charsetName = HttpRequestor.DEFAULT_CHARSET.name();
		}
		return getResponseData(charsetName);
	}

	/**
	 * 获取以默认的 HTTP 响应字符编码
	 * {@link HttpRequestor#DEFAULT_CHARSET
	 * HttpRequestor#DEFAULT_CHARSET} 解码后的响应实体数据
	 *
	 * @return 默认字符编码的 HTTP 响应实体数据。若响应实体不存在时，返回 <code>null</code>
	 * 值；若响应实体数据为空时，返回空串
	 *
	 * @see HttpRequestor#DEFAULT_CHARSET
	 * HttpRequestor#DEFAULT_CHARSET
	 * @see #getResponseData()
	 * @see #getResponseData(Charset)
	 * @see #getResponseData(String)
	 * @see #getContentTypeResponse()
	 */
	public String getDefaultEncodingResponse() {

		return getResponseData(HttpRequestor.DEFAULT_CHARSET);
	}

	@Override
	public String toString() {

		final StringBuilder builder = new StringBuilder();
		builder.append("HttpResponse inspect information:");
		builder.append("\n------------------------------------------------");
		builder.append("\r\n").append(responseHeader);
		builder.append("\r\n\r\n").append(getContentTypeResponse());
		builder.append("\n------------------------------------------------");
		return builder.toString();
	}
}
