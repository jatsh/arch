package com.isesol.framework.cache.client.http;

import com.isesol.framework.cache.client.util.*;

import java.io.*;
import java.net.*;
import java.nio.charset.*;
import java.util.*;

/**
 * 名值对请求实体。可用于 HTTP/GET 请求 URL 中的查询字符串，以及 HTTP/POST 请求实体数为
 * <code>application/x-www-form-urlencoded</code> 类型的数据。
 *
 * @see HttpRequest#getStringRequestEntity
 * @see HttpRequest#appendQueryRequestEntity
 */
public class NamePairRequestEntity extends AbstractRequestEntity implements RequestEntity {

	private Map<String, List<String>> pairs;

	public NamePairRequestEntity() {

	}

	public void add(String name, int value) {

		add(name, String.valueOf(value));
	}

	public void add(String name, long value) {

		add(name, String.valueOf(value));
	}

	public void add(String name, String value) {

		if (pairs == null) {
			pairs = new LinkedHashMap<String, List<String>>();
		}
		List<String> values = pairs.get(name);
		if (values == null) {
			values = new LinkedList<String>();
			pairs.put(name, values);
		}
		values.add(value);
	}

	@Override
	public String getEntity(Charset charset) {

		if (Tools.isBlank(pairs)) {
			return null;
		}
		StringBuilder builder = new StringBuilder();
		int p = 0;
		String charsetName = charset.name();
		try {
			for (Map.Entry<String, List<String>> entry : pairs.entrySet()) {
				List<String> values = entry.getValue();
				if (values == null) {
					continue;
				}
				if (p++ > 0) {
					builder.append('&');
				}
				String name = entry.getKey();
				for (String value : values) {
					builder.append(name).append('=');
					builder.append(URLEncoder.encode(value, charsetName));
				}
			}
		} catch (UnsupportedEncodingException ignored) {
		}
		return builder.toString();
	}
}
