package com.isesol.framework.cache.client.http;

import java.io.*;
import java.nio.charset.*;

/**
 * HTTP 请求实体。该接口抽象了 HTTP 请求实体数据。
 *
 * @see HttpRequest
 * @see HttpResponse
 */
public interface RequestEntity {

	/**
	 * 指定字符编码格式，获取字符串格式表示的请求实体数据。原始的实体数据可能是一个字节数组。
	 *
	 * @param charset 请求实体的字符编码
	 *
	 * @return 指定编码字符串所表示的请求实体数据
	 */
	String getEntity(Charset charset);

	/**
	 * 指定字符编码格式，获取字节数据格式表示的请求实体数据。原始的实体数据可能是一个字符串。
	 *
	 * @param charset 请求实体的字符编码
	 *
	 * @return 指定编码字符串所表示的请求实体数据
	 */
	byte[] getBytesEntity(Charset charset);

	/**
	 * 将请求实体数据以指定的编码格式写入输出流中。该接口的功能可以将请求实体数据写入 HTTP 请求
	 *
	 * @param out
	 * @param charset
	 */
	void writeTo(OutputStream out, Charset charset) throws IOException;
}
