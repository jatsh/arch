package com.isesol.framework.cache.client.http;

import java.nio.charset.*;

public class StringRequestEntity extends AbstractRequestEntity implements RequestEntity {

	private String str;

	public StringRequestEntity(String str) {

		this.str = str;
	}

	@Override
	public String getEntity(Charset charset) {

		return str;
	}
}
