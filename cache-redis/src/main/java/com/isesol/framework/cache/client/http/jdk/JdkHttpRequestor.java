package com.isesol.framework.cache.client.http.jdk;

import com.isesol.framework.cache.client.http.*;
import com.isesol.framework.cache.client.util.EclipseTools.*;
import org.apache.logging.log4j.*;

import java.io.*;
import java.net.*;
import java.util.*;


/**
 * 使用 JDK 中 HTTP 客户端实现的 HTTP 请求处理器。
 */
class JdkHttpRequestor implements HttpRequestor {

	private static final int STREAM_BUFFER_SIZE = 4096;

	static Logger log = LogManager.getLogger(JdkHttpRequestor.class.getName());

	private String endpoint;

	/**
	 * HTTP request read response timeout in milliseconds
	 */
	private int readTimeout;

	/**
	 * HTTP request connect server timeout in milliseconds
	 */
	private int connectTimeout;

	JdkHttpRequestor(String endpoint, int readTimeout, int connectTimeout) {

		this.endpoint = endpoint;
		this.readTimeout = readTimeout;
		this.connectTimeout = connectTimeout;
	}

	public String getEndpoint() {

		return endpoint;
	}

	public int getReadTimeout() {

		return readTimeout;
	}

	public int getConnectTimeout() {

		return connectTimeout;
	}

	@Override
	public HttpResponse doGet(HttpRequest request) throws IOException {

		return doRequest(request, HttpRequestType.GET);
	}

	@Override
	public HttpResponse doPost(HttpRequest request) throws IOException {

		return doRequest(request, HttpRequestType.POST);
	}

	public HttpResponse doRequest(HttpRequest request, HttpRequestType type) throws IOException {

		log.trace("Request:\n{}", request);

		HttpURLConnection httpConnection = null;
		try {
			String requestUrl = type.getRequestUrl(endpoint, request);
			URL url = new URL(requestUrl);
			httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setConnectTimeout(getConnectTimeout());
			httpConnection.setReadTimeout(getReadTimeout());
			addRequestHeaders(httpConnection, request);
			type.sendHttpBody(httpConnection, request);
			httpConnection.connect();
			HttpResponse response = createResponse(httpConnection);

			log.trace("Response:\n{}", response);

			return response;
		} finally {
			if (httpConnection != null) {
				httpConnection.disconnect();
			}
		}
	}

	protected HttpResponse createResponse(HttpURLConnection httpConnection) throws IOException {

		int responseCode = httpConnection.getResponseCode();
		String responseMessage = httpConnection.getResponseMessage();
		InputStream in = httpConnection.getInputStream();
		byte[] responseData = readResponse(in);
		return createResponse(responseCode, responseMessage, responseData, httpConnection.getHeaderFields());
	}

	protected HttpResponse createResponse(
			int responseCode, String responseMessage, byte[] responseData, Map<String, List<String>> headers) {

		return new HttpResponse(responseCode, responseMessage, responseData, headers);
	}

	protected void addRequestHeaders(HttpURLConnection httpConnection, HttpRequest request) {

		httpConnection.addRequestProperty("Connection", "close");
		for (HttpHeader header : request.getHeadersIterable()) {
			httpConnection.addRequestProperty(header.getName(), header.getValue());
		}
	}

	protected byte[] readResponse(InputStream in) throws IOException {

		ByteArrayOutputStream ba = new ByteArrayOutputStream();
		byte[] bys = new byte[STREAM_BUFFER_SIZE];
		for (int n = -1; (n = in.read(bys)) != -1; ) {
			ba.write(bys, 0, n);
		}
		return ba.toByteArray();
	}

	@Override
	public String toString() {

		ToString builder = new ToString(this);
		builder.append("endpoint", endpoint);
		builder.append("readTimeout", readTimeout);
		builder.append("connectTimeout", connectTimeout);
		return builder.toString();
	}

	/**
	 * <p>
	 * HTTP 请求类型
	 * </p>
	 */
	protected static enum HttpRequestType {

		/**
		 * HTTP/POST 请求
		 */
		POST {
			@Override
			public void sendHttpBody(HttpURLConnection httpConnection, HttpRequest request) throws IOException {

				OutputStream out = null;
				try {
					httpConnection.setDoOutput(true);
					out = httpConnection.getOutputStream();
					request.writeRequestEntityTo(out);
				} finally {
					if (out != null) {
						out.close();
					}
				}
			}
		},

		/**
		 * HTTP/GET 请求
		 */
		GET {
			@Override
			public String getRequestUrl(String endpoint, HttpRequest request) {

				return request.appendQueryRequestEntity(endpoint);
			}
		};

		public String getRequestUrl(String endpoint, HttpRequest request) {

			return endpoint;
		}

		public void sendHttpBody(HttpURLConnection httpConnection, HttpRequest request) throws IOException {

		}
	}
}
