package com.isesol.framework.cache.client.http.jdk;

import com.isesol.framework.cache.client.http.*;
import com.isesol.framework.cache.client.util.*;
import org.springframework.beans.factory.*;

public class JdkHttpRequestorFactoryBean implements FactoryBean<HttpRequestor>, InitializingBean {

	private static final int DEFAULT_CONNECT_TIMEOUT = 15000;

	private static final int DEFAULT_READ_TIMEOUT = 15000;

	private JdkHttpRequestor jdkHttpRequestor;

	private String endpoint;

	private int connectTimeout = DEFAULT_CONNECT_TIMEOUT;

	private int readTimeout = DEFAULT_READ_TIMEOUT;

	public JdkHttpRequestorFactoryBean() {

	}

	public void setEndpoint(String endpoint) {

		this.endpoint = endpoint;
	}

	public void setConnectTimeout(int connectTimeout) {

		this.connectTimeout = connectTimeout;
	}

	public void setReadTimeout(int readTimeout) {

		this.readTimeout = readTimeout;
	}

	@Override
	public final void afterPropertiesSet() {

		Assert.notLesser(readTimeout, 0, "readTimeout");
		Assert.notLesser(connectTimeout, 0, "connectTimeout");
		doAfterPropertiesSet();
		this.endpoint = initEndpointUrl();
		// Assert.notEmpty(endpoint, "endpoint");
		this.jdkHttpRequestor = new JdkHttpRequestor(endpoint, readTimeout, connectTimeout);
	}

	@Override
	public HttpRequestor getObject() {

		return jdkHttpRequestor;
	}

	@Override
	public Class<?> getObjectType() {

		return JdkHttpRequestor.class;
	}

	@Override
	public boolean isSingleton() {

		return true;
	}

	protected String initEndpointUrl() {

		return endpoint;
	}

	protected void doAfterPropertiesSet() {

	}
}
