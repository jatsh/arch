package com.isesol.framework.cache.client.interceptor;

import com.isesol.framework.cache.client.annotation.*;
import org.springframework.context.*;

import java.lang.reflect.*;

public interface CacheMetaRegistry extends ApplicationContextAware {

	MethodCacheMeta register(CachingOperation option, Method method, Class<?> targetClass);

	MethodCacheMeta getMeta(Method method, Class<?> targetClass);
}
