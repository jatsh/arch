package com.isesol.framework.cache.client.interceptor;

import org.aopalliance.intercept.*;

public class CacheableAdvice extends MethodCacheableSupport implements MethodInterceptor {

	@Override
	public Object invoke(final MethodInvocation invocation) throws Throwable {

		MethodCacheInvoker invoker = new MethodCacheInvoker() {

			@Override
			public Object invoke() throws Throwable {

				return invocation.proceed();
			}
		};

		return execute(invoker, invocation.getThis(), invocation.getMethod(), invocation.getArguments());
	}
}
