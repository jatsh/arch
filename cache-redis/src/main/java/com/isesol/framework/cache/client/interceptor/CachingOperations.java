package com.isesol.framework.cache.client.interceptor;

import com.isesol.framework.cache.client.annotation.*;
import com.isesol.framework.cache.client.expression.*;
import com.isesol.framework.cache.client.util.EclipseTools.*;

import java.io.*;
import java.util.*;

public class CachingOperations implements Serializable {

	private Map<CachingType, ExpressionCachingOperation> operations;

	public CachingOperations() {

		this.operations = new HashMap<CachingType, ExpressionCachingOperation>(CachingType.count() + 1, 1);
	}

	public void addCachingOperation(CachingOperation operation, ExpressionEval expression) {

		if (operation != null) {

			addOperation(operation, expression);
		}
	}

	public boolean hasCaching(CachingType type) {

		return operations.containsKey(type);
	}

	public boolean isExecutionCaching(CachingType type, Object[] arguments) {

		return computeCondition(type, arguments);

	}

	public String getKeyId(CachingType type) {

		ExpressionCachingOperation exprOperation = operations.get(type);

		if (exprOperation == null) {

			return null;
		}

		return exprOperation.getKeyId();
	}

	public CachingOperation getCachingOperation(CachingType type) {

		ExpressionCachingOperation exprOperation = operations.get(type);

		if (exprOperation == null) {

			return null;
		}

		return exprOperation.getOperation();
	}

	public Object computeExpressionKey(CachingType type, Object[] arguments) {

		ExpressionCachingOperation exprOperation = operations.get(type);

		if (exprOperation == null) {

			return null;
		}

		return exprOperation.computeExpressionKey(arguments);
	}

	public boolean computeCondition(CachingType type, Object[] parameters) {

		ExpressionCachingOperation exprOperation = operations.get(type);

		if (exprOperation == null) {

			return false;
		}

		return exprOperation.computeCondition(parameters);
	}

	private void addOperation(CachingOperation operation, ExpressionEval expression) {

		operations.put(operation.getType(), new ExpressionCachingOperation(operation, expression));
	}

	@Override
	public String toString() {

		ToString builder = new ToString(this);
		builder.append("operations", operations);
		return builder.toString();
	}
}
