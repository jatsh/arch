package com.isesol.framework.cache.client.interceptor;

import com.isesol.framework.cache.client.annotation.*;
import com.isesol.framework.cache.client.expression.*;
import com.isesol.framework.cache.client.listener.application.*;
import com.isesol.framework.cache.client.util.*;
import com.isesol.framework.cache.client.util.Assert;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;
import org.springframework.context.*;
import org.springframework.util.*;

import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.*;

public class DefaultCacheMetaRegistry implements CacheMetaRegistry, InitializingBean {

	static Logger log = LogManager.getLogger(DefaultCacheMetaRegistry.class.getName());

	private final Map<Object, MethodCacheMeta> metaCache;

	private ApplicationContext applicationContext;

	private ExpressionEvalFactory evalFactory;

	public DefaultCacheMetaRegistry() {

		this.metaCache = new ConcurrentHashMap<Object, MethodCacheMeta>();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {

		this.applicationContext = applicationContext;
	}

	public ExpressionEvalFactory getEvalFactory() {

		return evalFactory;
	}

	public void setEvalFactory(ExpressionEvalFactory evalFactory) {

		Assert.notNull(evalFactory, "evalFactory");
		this.evalFactory = evalFactory;
	}

	@Override
	public void afterPropertiesSet() {

		Assert.notNull(evalFactory, "evalFactory");
		Assert.notNull(applicationContext, "applicationContext");
	}

	@Override
	public MethodCacheMeta register(CachingOperation operation, Method method, Class<?> targetClass) {

		Object metaKey = generateMethodMetaCacheKey(method, targetClass);

		MethodCacheMeta meta = metaCache.get(metaKey);

		if (meta == null) {
			meta = new MethodCacheMeta(targetClass, method);
			metaCache.put(metaKey, meta);
		}

		if (meta.hasCaching(operation.getType())) {
			log.debug(
					"Ignore register method cache meta, current context meta exists. method: {}, operation: {}",
					CacheClientUtils.getName(method), operation);
			return meta;
		}

		ExpressionEval expression = createEval(method, operation);
		meta.addCachingOperation(operation, expression);
		applicationContext.publishEvent(new CachingOperationEvent(operation, method, targetClass));

		if (log.isDebugEnabled()) {
			log.debug(
					"Register method cache meta, target class: {}, method: {}, operation: {}", CacheClientUtils.getName(targetClass),
					CacheClientUtils.getName(method), operation);
		}

		return meta;
	}

	@Override
	public MethodCacheMeta getMeta(Method method, Class<?> targetClass) {

		Object metaKey = generateMethodMetaCacheKey(method, targetClass);

		return metaCache.get(metaKey);
	}

	protected ExpressionEval createEval(Method method, CachingOperation option) {

		return evalFactory.createEval(option.getKey(), option.getCondition(), method);
	}

	protected Object generateMethodMetaCacheKey(Method method, Class<?> targetClass) {

		return new DefaultMethodMetaCacheKey(method, targetClass);
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append(getClass().getName()).append(':');
		int index = 1;
		for (Map.Entry<Object, MethodCacheMeta> entry : metaCache.entrySet()) {
			builder.append('\n').append(index++).append(". ");
			builder.append(entry.getKey()).append(" --> ").append(entry.getValue());
		}
		return builder.toString();
	}

	private static class DefaultMethodMetaCacheKey {

		private final Class<?> returnType;

		private final String methodName;

		private final Class<?>[] parameterTypes;

		private final Class<?> targetClass;

		public DefaultMethodMetaCacheKey(Method method, Class<?> targetClass) {

			this.returnType = method.getReturnType();
			this.methodName = method.getName();
			this.parameterTypes = method.getParameterTypes();
			this.targetClass = targetClass;
		}

		@Override
		public int hashCode() {

			final int prime = 29;
			int result = 1;
			result = prime * result + (
					(methodName == null) ? 0 : methodName.hashCode());
			result = prime * result + Arrays.hashCode(parameterTypes);
			result = prime * result + (
					(returnType == null) ? 0 : returnType.hashCode());
			result = prime * result + (
					(targetClass == null) ? 0 : targetClass.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object other) {

			if (this == other) {
				return true;
			}
			if (!(other instanceof DefaultMethodMetaCacheKey)) {
				return false;
			}
			DefaultMethodMetaCacheKey otherKey = (DefaultMethodMetaCacheKey) other;
			return ObjectUtils.nullSafeEquals(this.targetClass, otherKey.targetClass) && ObjectUtils.nullSafeEquals(
					this.returnType, otherKey.returnType) && ObjectUtils.nullSafeEquals(
					this.methodName, otherKey.methodName) && ObjectUtils.nullSafeEquals(
					this.parameterTypes, otherKey.parameterTypes);
		}

		@Override
		public String toString() {

			StringBuilder builder = new StringBuilder();
			builder.append(
					targetClass == null ? null : targetClass.getName()).append('#');
			builder.append(
					returnType == null ? null : returnType.getSimpleName()).append(' ');
			builder.append(methodName).append(" (");
			CacheClientUtils.appendSimpleClassNames(parameterTypes, null, builder);
			builder.append(")");
			return builder.toString();
		}
	}
}
