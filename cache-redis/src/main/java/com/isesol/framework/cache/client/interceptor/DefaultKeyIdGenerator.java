package com.isesol.framework.cache.client.interceptor;

import com.isesol.framework.cache.client.annotation.*;
import com.isesol.framework.cache.client.util.*;

import java.security.*;

public class DefaultKeyIdGenerator implements KeyIdGenerator {

	private static final String DEFAULT_DIGEST_ALGORITHM = "MD5";

	private String separator;

	public DefaultKeyIdGenerator() {

	}

	protected static String md5(byte[] bys) {

		if (bys == null) {
			return null;
		}
		byte[] digest = getDigest(DEFAULT_DIGEST_ALGORITHM).digest(bys);

		return Tools.toHex(digest);
	}

	protected static MessageDigest getDigest(String algorithm) {

		try {
			return MessageDigest.getInstance(algorithm);
		} catch (NoSuchAlgorithmException ex) {
			throw new IllegalStateException("Could not find MessageDigest with algorithm \"" + algorithm + "\"", ex);
		}
	}

	@Override
	public String getSeperator() {

		return (
				separator == null ? ":" : separator);
	}

	public void setSeperator(String separator) {

		this.separator = separator;
	}

	@Override
	public KeyId generate(CachingOperation operation) {

		String originalKey = operation.getKeyId();
		Object keyId = buildKeyId(originalKey);
		return new KeyId(originalKey, keyId);
	}

	protected Object buildKeyId(Object originalKey) {

		if (originalKey instanceof String) {
			return md5(CacheClientUtils.toBytes((String) originalKey));
		}
		return originalKey;
	}

	protected StringBuilder generateMethodArguments(StringBuilder builder, Class<?>[] parameterTypes) {

		if (parameterTypes == null) {
			return builder;
		}
		int p = 0;
		for (Class<?> clazz : parameterTypes) {
			if (p++ > 0) {
				builder.append(',');
			}
			appendClass(builder, clazz, true);
		}
		return builder;
	}

	protected StringBuilder generateMethodName(StringBuilder builder, String name) {

		return builder.append(name);
	}

	protected StringBuilder generateClass(StringBuilder builder, Class<?> declaringClass) {

		return appendClass(builder, declaringClass, false);
	}

	protected StringBuilder appendClass(StringBuilder builder, Class<?> clazz, boolean isSimple) {

		return builder.append(
				isSimple ? clazz.getSimpleName() : clazz.getName());
	}
}
