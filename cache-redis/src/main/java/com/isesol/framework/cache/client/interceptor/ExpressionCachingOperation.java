package com.isesol.framework.cache.client.interceptor;

import com.isesol.framework.cache.client.annotation.*;
import com.isesol.framework.cache.client.expression.*;
import com.isesol.framework.cache.client.util.EclipseTools.*;

import java.io.*;

class ExpressionCachingOperation implements Serializable {

	private CachingOperation operation;

	private transient ExpressionEval expression;

	ExpressionCachingOperation(CachingOperation operation, ExpressionEval expression) {

		this.operation = operation;
		this.expression = expression;
	}

	CachingOperation getOperation() {

		return operation;
	}

	Object computeExpressionKey(Object[] arguments) {

		if (expression != null) {

			return expression.computeExpressionKey(arguments);
		}

		return null;
	}

	boolean computeCondition(Object[] arguments) {

		if (expression != null) {

			return expression.computeCondition(arguments);
		}

		return false;
	}

	String getKeyId() {

		return operation.getKeyId();
	}

	@Override
	public String toString() {

		ToString builder = new ToString(this);
		builder.append("operation", operation);
		builder.append("expression", expression);
		return builder.toString();
	}
}
