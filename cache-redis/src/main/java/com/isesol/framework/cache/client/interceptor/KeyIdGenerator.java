package com.isesol.framework.cache.client.interceptor;

import com.isesol.framework.cache.client.annotation.*;
import org.springframework.util.*;

import java.io.*;

/**
 * According to {@link MethodCacheMeta} object generating {@link KeyId} object.
 * <code>KeyId</code> object is basic immutable key object using calculate cache
 * key with another cache key parameters.
 */
public interface KeyIdGenerator {

	/**
	 * <p>
	 * Generate {@link KeyId} object, the object is immutable that should be
	 * cached by {@link CachingOperation} object
	 * </p>
	 *
	 * @param operation Method caching operation meta data
	 *
	 * @return Immutable basic key object
	 */
	KeyId generate(CachingOperation operation);

	/**
	 * <p>
	 * Returing key id separator that also concating other variable parameters
	 * as part of cache key.
	 * </p>
	 *
	 * @return Key item separator string
	 */
	String getSeperator();

	/**
	 * <p>
	 * Immutable part of cache key, the object generating final cache key with
	 * other variable part of cache key. The object should be cached by
	 * {@link MethodCacheMeta} object.
	 * </p>
	 */
	class KeyId implements Serializable {

		/**
		 * Calculated original immutable key
		 */
		private Object original;

		/**
		 * Calculated transforming original immutable key, original can convert
		 * to the value but not the oppsite. The value purpose compress orignal
		 * key length
		 */
		private Object keyId;

		public KeyId(Object original, Object keyId) {

			this.original = original;
			this.keyId = keyId;
		}

		public Object getOriginal() {

			return original;
		}

		public Object getKeyId() {

			return keyId;
		}

		@Override
		public int hashCode() {

			final int prime = 31;
			int result = 1;
			result = prime * result + (
					(keyId == null) ? 0 : keyId.hashCode());
			result = prime * result + (
					(original == null) ? 0 : original.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object object) {

			if (this == object) {
				return true;
			}
			if (!(object instanceof KeyId)) {
				return false;
			}
			KeyId other = (KeyId) object;
			return ObjectUtils.nullSafeEquals(this.keyId, other.keyId) && ObjectUtils.nullSafeEquals(
					this.original, other.original);
		}

		@Override
		public String toString() {

			return keyId + " <-> " + original;
		}
	}
}
