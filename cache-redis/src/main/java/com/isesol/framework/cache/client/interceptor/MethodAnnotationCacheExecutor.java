package com.isesol.framework.cache.client.interceptor;

import com.isesol.framework.cache.client.executor.*;

/**
 * 缓存客户端 方法级缓存操作执行器。用于缓存客户端方法级别缓存操作的缓存 key 包装方法缓存的标识数据。
 */
public class MethodAnnotationCacheExecutor extends AbstractCacheExecutor {

	/**
	 * 缓存客户端方法级缓存所产生的缓存 key 的标识
	 */
	private static final String CACHE_EXECUTOR_TYPE = "method";

	public MethodAnnotationCacheExecutor() {

		super(CACHE_EXECUTOR_TYPE);
	}
}
