package com.isesol.framework.cache.client.interceptor;

import com.isesol.framework.cache.client.util.EclipseTools.*;

public final class MethodCacheKey {

	private String keyId;

	private Object evalKey;

	public MethodCacheKey(String keyId, Object evalKey) {

		this.keyId = keyId;
		this.evalKey = evalKey;
	}

	public String getKeyId() {

		return keyId;
	}

	public Object getEvalKey() {

		return evalKey;
	}

	public String generateKey(String separator) {

		return keyId + separator + evalKey;
	}

	@Override
	public String toString() {

		ToString builder = new ToString(this);
		builder.append("keyId", keyId);
		builder.append("evalKey", evalKey);
		return builder.toString();
	}
}
