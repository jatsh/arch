package com.isesol.framework.cache.client.interceptor;

import com.isesol.framework.cache.client.annotation.*;
import com.isesol.framework.cache.client.expression.*;
import com.isesol.framework.cache.client.util.*;

import java.lang.reflect.*;

/**
 * 方法调用缓存注解元数据。主要方法级缓存目标类、目标方法，以及标注的元数据。
 */
public class MethodCacheMeta {

	private Class<?> targetClass;

	private Method method;

	private CachingOperations operations;

	public MethodCacheMeta(Class<?> targetClass, Method method) {

		this.operations = new CachingOperations();
		this.targetClass = targetClass;
		this.method = method;
	}

	public void addCachingOperation(CachingOperation option, ExpressionEval expression) {

		operations.addCachingOperation(option, expression);
	}

	public Class<?> getTargetClass() {

		return targetClass;
	}

	public String getMethodName() {

		return method.getName();
	}

	public Class<?> getMethodReturnType() {

		return method.getReturnType();
	}

	public Class<?>[] getMethodParameterTypes() {

		return method.getParameterTypes();
	}

	public Class<?>[] getMethodExceptionTypes() {

		return method.getExceptionTypes();
	}

	public boolean hasCaching(CachingType type) {

		return operations.hasCaching(type);
	}

	public boolean isExecutionCaching(CachingType type, Object[] arguments) {

		return operations.isExecutionCaching(type, arguments);
	}

	public CachingOperation getCachingOperation(CachingType type) {

		return operations.getCachingOperation(type);
	}

	public Object computeExpressionKey(CachingType type, Object[] arguments) {

		return operations.computeExpressionKey(type, arguments);
	}

	public String getKeyId(CachingType type) {

		return operations.getKeyId(type);
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("MethodCacheMeta@").append(Integer.toHexString(System.identityHashCode(this)));
		builder.append(", signature = ");
		if (method == null) {
			builder.append("null");
		} else {
			builder.append(
					targetClass == null ? null : targetClass.getSimpleName()).append('#');
			CacheClientUtils.appendSimpleSignature(method, builder);
		}
		return builder.toString();
	}
}
