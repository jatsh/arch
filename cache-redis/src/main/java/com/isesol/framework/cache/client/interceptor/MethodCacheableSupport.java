package com.isesol.framework.cache.client.interceptor;

import com.isesol.framework.cache.client.annotation.*;
import com.isesol.framework.cache.client.app.*;
import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.aop.framework.*;
import org.springframework.beans.factory.*;

import java.lang.reflect.*;
import java.text.*;
import java.util.*;

public class MethodCacheableSupport implements InitializingBean, CacheClientApplicationAware {

	static Logger log = LogManager.getLogger(MethodCacheableSupport.class.getName());

	private volatile boolean initialized;

	private CacheClientApplication application;

	private CacheMetaRegistry registry;

	private TransactionCacheExecutor executor;

	public MethodCacheableSupport() {

	}

	public CacheMetaRegistry getRegistry() {

		return registry;
	}

	public void setRegistry(CacheMetaRegistry registry) {

		this.registry = registry;
	}

	public TransactionCacheExecutor getExecutor() {

		return executor;
	}

	public void setExecutor(TransactionCacheExecutor executor) {

		this.executor = executor;
	}

	@Override
	public void setCacheClientApplication(CacheClientApplication application) {

		this.application = application;
	}

	public CacheClientApplication getApplication() {

		return application;
	}

	@Override
	public final void afterPropertiesSet() {

		Assert.notNull(getApplication(), "application");
		Assert.notNull(getRegistry(), "registry");
		Assert.notNull(getExecutor(), "cacheKeyGenerator");
		doAfterPropertiesSet();
		this.initialized = true;
		log.info("{}", this);
	}

	protected void doAfterPropertiesSet() {

	}

	protected Object execute(MethodCacheInvoker invoker, Object target, Method method, Object[] arguments) throws
	                                                                                                       Throwable {

		if (!initialized) {
			log.warn("Object is initialing, ignore cacheable method invoke, invoke original method directly");
			return invoker.invoke();
		}

		Class<?> targetClass = AopProxyUtils.ultimateTargetClass(target);
		if (targetClass == null && target != null) {
			targetClass = target.getClass();
		}

		// Find cache meta from registry
		MethodCacheMeta cacheMeta = getRegistry().getMeta(method, targetClass);

		if (log.isDebugEnabled()) {
			log.debug(
					"[getMeta] original target class: {}, target method: {}#{}, meta: {}",
					target == null ? "null" : target.getClass(), targetClass.getSimpleName(), method.getName(),
					cacheMeta);
		}

		// cache meta was not found, the method invoking should be not cached
		if (cacheMeta == null) {
			return invoker.invoke();
		}

		Object returnObject = null;

		if (canEnableCaching(cacheMeta, method, arguments)) {
			MethodCacheKey methodKey = createCacheKey(CachingType.ENABLE, cacheMeta, arguments);
			CachingOperation operation = cacheMeta.getCachingOperation(CachingType.ENABLE);
			returnObject = doEnableCacheable(methodKey.generateKey(WrapKey.KEY_SEPARATOR), operation, invoker, method);
		} else {
			returnObject = invoker.invoke();
		}

		if (canClearingCached(cacheMeta, arguments)) {
			MethodCacheKey methodKey = createCacheKey(CachingType.CLEARING, cacheMeta, arguments);
			doClearingCached(methodKey.generateKey(WrapKey.KEY_SEPARATOR));
		}

		return returnObject;
	}

	/**
	 * <p>
	 * Determine whether enable caching, the cache operation must be the
	 * following conditions:
	 * <ul>
	 * <li>{@link EnableCacheable
	 *
	 * @param meta method caching meta data
	 * @param method target method
	 * @param arguments method invoked arguments runtime
	 *
	 * @return whether enable cacheing method invoked result
	 *
	 * @EnableCacheable} annotation annotated on method;</li>
	 * <li>the annotation 'condition' value evaluated result is true with method
	 * invoke argument values runtime;</li>
	 * <li>the method return type is not 'void'.</li>
	 * </ul>
	 * </p>
	 */
	private boolean canEnableCaching(MethodCacheMeta cacheMeta, Method method, Object[] arguments) {

		if (void.class.equals(method.getReturnType())) {
			return false;
		}
		return cacheMeta.isExecutionCaching(CachingType.ENABLE, arguments);
	}

	/**
	 * <p>
	 * Determine whether clearing cached, the cache operation must be the
	 * following conditions:
	 * <ul>
	 * <li>{@link ClearingCached
	 *
	 * @param meta method caching meta data
	 * @param arguments method invoked arguments runtime
	 *
	 * @return whether clearing cached method invoked result previously
	 *
	 * @ClearingCached} annotation annotated on method;</li>
	 * <li>the annotation 'condition' value evaluated result is true with method
	 * invoke argument values runtime.</li>
	 * </ul>
	 * </p>
	 */
	private boolean canClearingCached(MethodCacheMeta cacheMeta, Object[] arguments) {

		return cacheMeta.isExecutionCaching(CachingType.CLEARING, arguments);
	}

	protected void doClearingCached(String cacheKey) {

		del(cacheKey);
	}

	protected Object doEnableCacheable(
			String cacheKey, CachingOperation operation, MethodCacheInvoker invoker, Method method) throws Throwable {

		// Get object from cache
		Object cachedObject = executorGet(cacheKey);

		if (cachedObject == null) {
			if (log.isDebugEnabled()) {
				log.debug(
						"Cannot hit cached object according to key '{}', method signature: {}" +
						", will execute target object original method", cacheKey, CacheClientUtils.getSimpleSignature(
								method));
			}
			return invokeOriginalMethod(cacheKey, operation, invoker);
		}

		// cached object is not null
		// return the cached object when the object type matches method return
		// type
		if (method.getReturnType().isInstance(cachedObject)) {
			if (log.isDebugEnabled()) {
				log.debug(
						"Hit cached object according to key '{}', method signature: {}, object is [{}]", cacheKey,
						CacheClientUtils.getSimpleSignature(method), cachedObject);
			}
			return cachedObject;
		}

		// the cached object is compatible with method return type
		Object compatibleObject = CompatibleTypeConverter.convertCompatible(cachedObject, method.getReturnType());

		if (compatibleObject != null) {
			if (log.isInfoEnabled()) {
				log.info(
						"Hit cached object according to key '{}', cached object type ({})" +
						" is compatible with the method return type ({})", cacheKey, cachedObject.getClass().getName(),
						method.getReturnType().getName());
			}
			return compatibleObject;
		}

		// cached object type is not same with method return value type
		// the branch is incredible!! why????
		log.error(
				"Hit cached object according to key '{}' that the type '{}' is not same with " +
				"current method return value type is '{}', do not return the cached object and " +
				"will execute target object original method, method infomation: {}", cacheKey,
				cachedObject.getClass().getName(), method.getReturnType().getName(),
				CacheClientUtils.getSimpleSignature(method));

		return invokeOriginalMethod(cacheKey, operation, invoker);
	}

	/**
	 * <p>
	 * Invoke target class original method, and save the object to cache server.
	 * </p>
	 *
	 * @param cacheKey Cache data basic key
	 * @param operation method annotation operations
	 * @param invoker method invoker
	 *
	 * @return method invoked result
	 *
	 * @throws Throwable framework throws
	 */
	private Object invokeOriginalMethod(String cacheKey, CachingOperation operation, MethodCacheInvoker invoker) throws
	                                                                                                             Throwable {

		Object cachedObject = invoker.invoke();

		log.debug(
				"Method executing result will save(set) to cache, key '{}'" +
				", method annotated operation: {}, method executed result: {}", cacheKey, operation, cachedObject);

		if (cachedObject == null) {
			log.info(
					"Method invoke result is null, null value should not be cached, key '{}', operation: {}", cacheKey,
					operation);
			return null;
		}

		Date expireAt = operation.getExpireAtDate();

		// Annotated annotation has not 'expireAt', set cache entry expiration
		// time with 'expireSeconds' value
		if (expireAt == null) {
			log.debug(
					"Save(set) to cache that annotated method has not option 'expireAt'" +
					", will use 'expireSeconds' setting, key '{}', expireSeconds {}", cacheKey,
					operation.getExpireSeconds());
			executorSet(cacheKey, cachedObject, operation.getExpireSeconds());
			return cachedObject;
		}

		if (expireAt.getTime() > System.currentTimeMillis()) {
			// 'expireAt' absolute datetime after the current time
			if (log.isDebugEnabled()) {
				log.debug(
						"Save(set) to cache that annotated method has option 'expireAt'" +
						", will use the value as cache expireAt value, key '{}', expireAt '{}'", cacheKey,
						new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(expireAt));
			}
			executorSet(cacheKey, cachedObject, expireAt, operation.getExpireAt());
		} else if (log.isWarnEnabled()) {
			// 'expireAt' datetime before the current time, NEVER SAVE IT
			log.warn(
					"Save(set) to cache that annotated method option 'expireAt' lesser than" +
					" current time, WILL DO NOT SAVE TO CACHE, key '{}', option expireAt '{}'", cacheKey,
					new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(expireAt));
		}

		return cachedObject;
	}

	protected MethodCacheKey createCacheKey(CachingType type, MethodCacheMeta meta, Object[] arguments) {

		String keyId = meta.getKeyId(type);
		Object evalKey = meta.computeExpressionKey(type, arguments);
		return new MethodCacheKey(keyId, evalKey);
	}

	public Object executorGet(String key) {

		try {
			return getExecutor().get(key);
		} catch (Exception e) {
			log.error("execute get method cause error, get key '{}'", key, e);
			return null;
		}
	}

	public void executorSet(String key, Object value, int expireSeconds) {

		try {
			getExecutor().set(key, value, expireSeconds);
		} catch (Exception e) {
			log.error(
					"execute cache set with expireSeconds cause error" + ", key '{}', value '{}', expireSeconds '{}'",
					key, value, expireSeconds, e);
		}
	}

	public void executorSet(String key, Object value, Date expireAt, String expireAtStr) {

		try {
			getExecutor().set(key, value, expireAt);
		} catch (Exception e) {
			log.error(
					"execute cache set with expireAt cause error, key '{}', value '{}', expireAt '{}'", key, value,
					expireAtStr, e);
		}
	}

	public void del(String key) {

		try {
			getExecutor().del(key);
		} catch (CacheClientException e) {
			log.error("execute cache del cause error, key '{}'", key, e);
		}
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("initialized", initialized);
		builder.append("application", getApplication());
		builder.append("registry", getRegistry());
		builder.append("executor", getExecutor());
		return builder.toString();
	}

	protected interface MethodCacheInvoker {
		Object invoke() throws Throwable;
	}
}
