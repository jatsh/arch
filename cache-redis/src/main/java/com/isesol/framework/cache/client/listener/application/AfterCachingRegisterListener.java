package com.isesol.framework.cache.client.listener.application;

import com.isesol.framework.cache.client.util.*;
import org.springframework.beans.factory.*;
import org.springframework.context.*;
import org.springframework.context.event.*;

public class AfterCachingRegisterListener implements ApplicationListener<ContextRefreshedEvent>, InitializingBean {

	private ValidateClearingKeyId validateClearingKeyId;

	public AfterCachingRegisterListener() {

	}

	public void setValidateClearingKeyId(ValidateClearingKeyId validateClearingKeyId) {

		this.validateClearingKeyId = validateClearingKeyId;
	}

	@Override
	public void afterPropertiesSet() {

		Assert.notNull(validateClearingKeyId, "validateClearingKeyId");
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {

		validateClearingKeyId.validate();

		validateClearingKeyId.clearKeyIds();
	}

	public interface ValidateClearingKeyId {
		void validate();

		void clearKeyIds();
	}
}
