package com.isesol.framework.cache.client.listener.application;

import com.isesol.framework.cache.client.annotation.*;
import org.springframework.context.*;

import java.lang.reflect.*;

public class CachingOperationEvent extends ApplicationEvent {

	private final CachingOperation operation;

	private final transient Method method;

	private final Class<?> targetClass;

	public CachingOperationEvent(CachingOperation operation, Method method, Class<?> targetClass) {

		super(operation);
		this.operation = operation;
		this.method = method;
		this.targetClass = targetClass;
	}

	public CachingOperation getOperation() {

		return operation;
	}

	public String getMethodName() {

		return method.getName();
	}

	public String getTargetClassName() {

		return targetClass.getName();
	}
}
