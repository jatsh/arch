package com.isesol.framework.cache.client.listener.application;

import com.isesol.framework.cache.client.annotation.*;
import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.context.*;

import java.util.*;
import java.util.concurrent.*;

public class CachingOperationListener implements ApplicationListener<CachingOperationEvent>, AfterCachingRegisterListener.ValidateClearingKeyId {

	static Logger log = LogManager.getLogger(CachingOperationListener.class.getName());

	private Map<String, String> enableKeyIds;

	private Map<String, String> clearingKeyIds;

	public CachingOperationListener() {

		this.enableKeyIds = new ConcurrentHashMap<String, String>();
		this.clearingKeyIds = new ConcurrentHashMap<String, String>();
	}

	@Override
	public void onApplicationEvent(CachingOperationEvent event) {

		CachingOperation operation = event.getOperation();

		if (operation == null) {

			log.warn("CachingOperation object is null, cannot add to keyId list, ignore it");

			return;
		}

		String keyId = event.getOperation().getKeyId();

		String info = event.getTargetClassName() + '#' + event.getMethodName();

		if (CachingType.ENABLE.equals(operation.getType())) {

			if (enableKeyIds.containsKey(keyId)) {

				throw new EnableKeyIdDuplicateException(keyId, info, enableKeyIds.get(keyId));
			}

			enableKeyIds.put(keyId, info);

		} else if (CachingType.CLEARING.equals(operation.getType())) {

			clearingKeyIds.put(keyId, info);
		}

		log.debug("Add '{}' keyId: '{}', info: {}", operation.getType(), keyId, info);
	}

	@Override
	public void validate() {

		if (Tools.isBlank(clearingKeyIds)) {
			log.warn("Validate clearing keyId, clearing keyId list has not any keyId, do not validate clearing keyId");
			return;
		}

		log.debug(
				"Validate clearing keyId, clearing keyId count: {}, enable cacheable keyId count: {}",
				clearingKeyIds.size(), enableKeyIds.size());

		for (Map.Entry<String, String> entry : clearingKeyIds.entrySet()) {

			String info = enableKeyIds.get(entry.getKey());

			if (info == null) {

				throw new ClearingKeyIdReferenceException(entry.getKey(), entry.getValue());
			}

			log.debug(
					"Validate clearing keyId ok, keyId '{}' on method: {}" +
					", reference enable cacheable method info: {}", entry.getKey(), entry.getValue(), info);
		}
	}

	@Override
	public void clearKeyIds() {

		log.info(
				"Clear key id list, enable count: {}, clearing count: {}", enableKeyIds.size(), clearingKeyIds.size());

		enableKeyIds.clear();

		clearingKeyIds.clear();

		log.info("Clear key id list, enable and clearing has been cleared");
	}
}
