package com.isesol.framework.cache.client.log;

/**
 * 缓存警告日志服务
 */
public interface CacheLogService {

	void log(LogType type, String message);

	void log(LogType type, String message, Throwable e);

	/**
	 * 日志服务类型
	 */
	public static enum LogType {

		/**
		 * 大数据量的缓存数据报警类型
		 */
		LARGE_BYTES("cache.client.large");

		private String record;

		private LogType(String record) {

			this.record = record;
		}

		public String getRecord() {

			return record;
		}
	}
}
