package com.isesol.framework.cache.client.log.impl;

import com.isesol.framework.cache.client.log.*;
import org.apache.logging.log4j.*;

public class LoggingCacheLog implements CacheLogService {

	static Logger log = LogManager.getLogger(LoggingCacheLog.class.getName());

	@Override
	public void log(LogType type, String message) {

		log.warn("{}", message);
	}

	@Override
	public void log(LogType type, String message, Throwable e) {

		log.warn("{}", message, e);
	}
}
