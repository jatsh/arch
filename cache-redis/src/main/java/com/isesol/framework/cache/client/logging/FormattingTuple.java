package com.isesol.framework.cache.client.logging;

public class FormattingTuple {

	private static String appCode;

	private String message;

	private Throwable throwable;

	public FormattingTuple(String message) {

		this(message, null);
	}

	public FormattingTuple(String message, Throwable throwable) {

		this.message = message;
		this.throwable = throwable;
	}

	public static void injectAppCode(String appCode) {

		FormattingTuple.appCode = '[' + appCode + "] ";
	}

	static String filterMessage(String message) {

		if (appCode == null) {
			return HideUtils.securityHidden(message);
		}
		return appCode + HideUtils.securityHidden(message);
	}

	static String filterMessage(FormattingTuple tuple) {

		if (tuple == null) {
			return null;
		}
		return filterMessage(tuple.getMessage());
	}

	public String getMessage() {

		return message;
	}

	public Throwable getThrowable() {

		return throwable;
	}
}
