package com.isesol.framework.cache.client.logging;

public final class HideUtils {

	private HideUtils() {

	}

	public static String securityHidden(final String message) {

		String msg = message;

		if (message == null || message.trim().length() == 0) {

			return msg;
		}

		if (msg.indexOf("\"password\"") > -1) {

			msg = msg.replaceAll("(?i)(\"\\s*password\\s*\")\\s*:\\s*\"[^\"]*?\"", "$1:\"******\"");
		}

		msg = msg.replaceAll("([0-9]{1,3})(?:\\.[0-9]{1,3}){2}\\.([0-9]{1,3})", "$1.***.***.$2");

		return msg;
	}
}
