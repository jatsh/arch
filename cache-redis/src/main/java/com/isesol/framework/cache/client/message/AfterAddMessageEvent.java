package com.isesol.framework.cache.client.message;

import com.isesol.framework.cache.client.listener.event.*;
import com.isesol.framework.cache.client.util.EclipseTools.*;

/**
 * 消息收集器在接收客户端所产生的消息后，触发消息监听器所产生的事件。
 *
 * @see MessageCollectorListener
 * @see CacheClientMessage
 */
public class AfterAddMessageEvent implements CacheListenerEvent {

	/**
	 * 缓存客户端产生的消息数据
	 */
	private CacheClientMessage message;

	public AfterAddMessageEvent(CacheClientMessage message) {

		this.message = message;
	}

	/**
	 * 获取缓存客户端产生的消息数据
	 *
	 * @return 缓存客户端产生的消息数据
	 */
	public CacheClientMessage getMessage() {

		return message;
	}

	@Override
	public String toString() {

		ToString builder = new ToString(this);
		builder.append("message", message);
		return builder.toString();
	}
}
