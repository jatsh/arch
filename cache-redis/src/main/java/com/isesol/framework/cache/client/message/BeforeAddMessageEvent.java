package com.isesol.framework.cache.client.message;

import com.isesol.framework.cache.client.listener.event.*;
import com.isesol.framework.cache.client.util.EclipseTools.*;

/**
 * 消息收集器在接收客户端所产生的消息前，触发消息监听器所产生的事件。
 *
 * @see MessageCollectorListener
 */
public class BeforeAddMessageEvent implements CacheListenerEvent {

	/**
	 * 缓存客户端消息内容
	 */
	private String message;

	/**
	 * 缓存客户端产生的异常数据
	 */
	private Throwable throwable;

	public BeforeAddMessageEvent(String message, Throwable throwable) {

		super();
		this.message = message;
		this.throwable = throwable;
	}

	/**
	 * 获取缓存客户端所产生的消息内容
	 *
	 * @return 缓存客户端所产生的消息内容
	 */
	public String getMessage() {

		return message;
	}

	/**
	 * 获取缓存客户端所产生的异常数据
	 *
	 * @return 缓存客户端所产生的异常数据，返回值有可能为 <code>null</code> 值
	 */
	public Throwable getThrowable() {

		return throwable;
	}

	@Override
	public String toString() {

		ToString builder = new ToString(this);
		builder.append("message", message);
		builder.append("throwable", throwable);
		return builder.toString();
	}
}
