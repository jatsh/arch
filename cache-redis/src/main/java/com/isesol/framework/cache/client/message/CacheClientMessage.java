package com.isesol.framework.cache.client.message;

import com.isesol.framework.cache.client.util.*;
import com.isesol.framework.cache.client.util.EclipseTools.*;

import java.io.*;
import java.util.*;

/**
 * 缓存客户端消息数据
 * 含有标识一条消息的全局唯一消息编号、消息产生时的时间、客户端应用代码、客户端应用节点代码、
 * 产生消息所调用的 API、 数据、具体的消息内容，以及异常消息中的异常信息。
 */
public class CacheClientMessage implements Serializable {

	/**
	 * 全局唯一的消息编号
	 */
	private final String id;

	/**
	 * 消息产生的 Unix 纪元毫秒时间
	 */
	private final long time;

	/**
	 * 缓存客户端应用代码（App Code）
	 */
	private final String app;

	/**
	 * 缓存客户端应用节点代码（Node Code）
	 */
	private final String node;

	/**
	 * 产生消息时调用的 API 信息
	 */
	private final String api;

	/**
	 * 消息内容
	 */
	private final String message;

	/**
	 * 异常信息
	 */
	private final String exception;

	public CacheClientMessage(String id, String app, String node, String api, String message, String exception) {

		this.id = id;
		this.time = System.currentTimeMillis();
		this.app = app;
		this.node = node;
		this.api = api;
		this.message = message;
		this.exception = exception;
	}

	/**
	 * 全局唯一的消息编号，用于唯一标识一条缓存客户端的消息信息，数据值由缓存客户端自动生成。
	 *
	 * @return 全局唯一的消息编号
	 */
	public String getId() {

		return id;
	}

	/**
	 * 获取消息收集器收集到消息时的 Unix 纪元毫秒时间。
	 *
	 * @return 消息产生的 Unix 纪元毫秒时间
	 */
	public long getTime() {

		return time;
	}

	/**
	 * 获取缓存客户端应用代码（App Code），即在配置中所使用的 <code>app-code</code>。
	 *
	 * @return 缓存客户端应用代码（App Code）
	 */
	public String getApp() {

		return app;
	}

	/**
	 * 获取缓存客户端应用节点代码（Node Code），节点代码由缓存客户端根据 APP CODE 产生，
	 * 标识同一应用集群部署时唯一的节点代码，用于标识应用具体的节点信息。
	 *
	 * @return 缓存客户端应用节点代码（Node Code）
	 */
	public String getNode() {

		return node;
	}

	/**
	 * 获取产生缓存客户端消息时所调用的 API 数据，该数据主要由类名和方法签名构成。
	 *
	 * @return 产生缓存客户端消息时所调用的 API 数据
	 */
	public String getApi() {

		return api;
	}

	/**
	 * 获取缓存客户端消息内容。
	 *
	 * @return 缓存客户端消息内容
	 */
	public String getMessage() {

		return message;
	}

	/**
	 * 获取缓存客户端在产生异常时异常中的信息。
	 *
	 * @return 缓存客户端在产生异常时异常中的信息
	 */
	public String getException() {

		return exception;
	}

	@Override
	public String toString() {

		ToString builder = new ToString("CacheClientMessage: ");
		builder.append("id", id);
		builder.append("time", Tools.formatWithMillis(new Date(time))).append('(').append(time).append(')');
		builder.append("app", app);
		builder.append("node", node);
		builder.append("api", api);
		builder.append("message", message);
		builder.append("exception", exception);
		return builder.toString();
	}
}
