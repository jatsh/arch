package com.isesol.framework.cache.client.message;

import com.isesol.framework.cache.client.util.EclipseTools.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

/**
 * 缓存客户端内存队列。该队列是线程安全的，不需要进行额外的同步处理。
 */
public class CacheClientQueue<T> implements Serializable {

	/**
	 * 阻塞队列
	 */
	private BlockingQueue<T> queue;

	/**
	 * 使用无界链表阻塞队列构建缓存客户端内存队列
	 */
	public CacheClientQueue() {

		this.queue = new LinkedBlockingQueue<T>();
	}

	/**
	 * 获取内存队列中的数据量
	 *
	 * @return 内存队列中的数据量
	 */
	public int size() {

		return queue.size();
	}

	/**
	 * 清除内存队列中所有的数据
	 */
	public void clear() {

		queue.clear();
	}

	/**
	 * 将数据追加在内存队列的尾部。如果数据为 <code>null</code> 值时会忽略该条数据，不会添加至队列中。
	 *
	 * @param message 需要放入队列中的数据
	 */
	public void add(T message) {

		if (message == null) {

			return;
		}

		queue.add(message);
	}

	/**
	 * 从队列头部获取一条数据。如果队列为空（没有数据）时，将返回 <code>null</code> 值。
	 *
	 * @return 从队列头部获取的一条数据，该返回值可能为空
	 */
	public T poll() {

		return queue.poll();
	}

	/**
	 * 从队列头部获取指定数据量的数据。如果队列为空（没有数据）时，将返回 <code>null</code> 值。
	 *
	 * @param size 需要获取队列数据的最大数量
	 *
	 * @return 从队列头部获取的数据，返回的数据量大小为小于或等于参数 <code>size</code> 的值。 若队列为空将返回
	 * <code>null</code> 值。
	 */
	public List<T> poll(final int size) {

		if (size < 0) {

			throw new IllegalArgumentException(
					"argument 'size' value is " + size + ", it value must be greater than or equals 0");
		}

		if (queue.size() == 0) {

			return null;
		}

		if (size == 0) {

			return new ArrayList<T>(0);
		}

		List<T> messages = new LinkedList<T>();

		for (int i = 0; i < size; i++) {

			T message = poll();

			if (message == null) {

				break;
			}

			messages.add(message);
		}

		return messages;
	}

	@Override
	public String toString() {

		ToString builder = new ToString(this);
		builder.append("queue", queue);
		return builder.toString();
	}
}
