package com.isesol.framework.cache.client.message;
import com.isesol.framework.cache.client.executor.*;

/**
 * <p>
 * 缓存客户端消息收集器。用于收集缓存客户端运行时产生的异常，以及其他一些关于缓存客户端的一些信息。
 * </p>
 * <p>
 * 消息收集器的实现应为异步方式处理，即缓存客户端在消息产生时，使用消息收集器异步地在内存中收集所产生的消息，
 * 当达到消息发送策略所要求的阀值时，将内存中的消息发送至远程的消息接收端，同时清除内存中已经发送的消息。
 * </p>
 *
 * @see CacheClientMessage
 * @see MessageCollectorAware
 * @see SendStrategy
 * @see MessageSender
 * @see CacheExecutorAdvice
 * CacheExecutorAdvice
 */
public interface MessageCollector extends MessageCollectorListenerAware {

	/**
	 * 收集缓存客户端运行信息
	 *
	 * @param api 使用的缓存客户端 API 描述
	 * @param message 运行的简要信息
	 *
	 * @return 全局唯一的消息编号
	 */
	String collectMessage(String api, String message);

	/**
	 * 收集缓存客户端运行时异常信息
	 *
	 * @param api 使用的缓存客户端 API 描述
	 * @param message 运行的简要信息
	 * @param t 异常信息
	 *
	 * @return 全局唯一的消息编号
	 */
	String collectMessage(String api, String message, Throwable t);
}
