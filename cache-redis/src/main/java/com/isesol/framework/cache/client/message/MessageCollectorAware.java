package com.isesol.framework.cache.client.message;

/**
 * 可注入消息收集器的接口。接口的实现表示允许缓存客户端注入 {@link MessageCollector} 对象。
 */
public interface MessageCollectorAware {

	/**
	 * 设置缓存客户端消息收集器
	 *
	 * @param messageCollector 缓存客户端消息收集器
	 */
	void setMessageCollector(MessageCollector messageCollector);
}
