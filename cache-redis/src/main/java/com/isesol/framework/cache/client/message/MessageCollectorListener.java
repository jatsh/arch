package com.isesol.framework.cache.client.message;

/**
 * 消息收集监听器，用于消息收集器在处理缓存客户端产生的消息前后事件进行处理。
 *
 * @see MessageCollector
 * @see SendStrategy
 */
public interface MessageCollectorListener {

	/**
	 * 消息收集器在接收客户端所产生的消息前触发。
	 *
	 * @param event 触发消息监听器所产生的上下文数据
	 */
	void beforeAddMessage(BeforeAddMessageEvent event);

	/**
	 * 消息收集器在接收客户端所产生的消息后触发。
	 *
	 * @param event 触发消息监听器所产生的上下文数据
	 */
	void afterAddMessage(AfterAddMessageEvent event);
}
