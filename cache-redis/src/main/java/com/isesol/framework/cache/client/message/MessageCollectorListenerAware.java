package com.isesol.framework.cache.client.message;

/**
 * 消息收集监听器处理，用于添加/注册新的消息收集监听器。
 *
 * @see MessageCollector
 */
public interface MessageCollectorListenerAware {

	/**
	 * 添加一个消息收集监听器
	 *
	 * @param listener 消息收集监听器
	 */
	void addCollectorListener(MessageCollectorListener listener);
}
