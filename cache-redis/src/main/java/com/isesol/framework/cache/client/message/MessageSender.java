package com.isesol.framework.cache.client.message;

import java.util.*;

/**
 * 缓存客户端消息发送器。用于将缓存客户端内部消息队列中的数据发送至远程的服务端。 具体的发送时机由 {@link SendStrategy} 指定。
 *
 * @see SendStrategy
 * @see MessageCollector
 */
public interface MessageSender {

	/**
	 * 将缓存客户端内部队列中的数据发送至远程的服务端，发送完成后清队内部队列中的数据， 以释放队列所占有用的内存空间。
	 *
	 * @param appCode 业务应用代码
	 * @param messages 需要发送的消息数据集
	 */
	void send(String appCode, List<CacheClientMessage> messages);
}
