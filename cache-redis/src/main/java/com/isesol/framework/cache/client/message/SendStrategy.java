package com.isesol.framework.cache.client.message;

/**
 * 缓存客户端消息发送策略。该策略作为一个 {@link MessageCollectorListener} 类型，
 * 其在消息收集处理的各阶段进行触发，以便于消息发送策略决定消息的发送时机。
 *
 * @see MessageSender
 * @see MessageCollector
 */
public interface SendStrategy extends MessageCollectorListener {
}
