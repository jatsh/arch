package com.isesol.framework.cache.client.message.collector;

import com.isesol.framework.cache.client.app.*;
import com.isesol.framework.cache.client.message.*;
import com.isesol.framework.cache.client.thread.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;

/**
 * 异步客户端缓存消息收集器。在接收到缓存客户端所产生的消息时，异步地将消息放入缓存客户端的内存队列中。 默认情况下，异步处理由缓存客户端线程执行器（
 * {@link CacheClientThreadExecutor
 * CacheClientThreadExecutor} 有 5 个线程的线程池处理。
 */
public class AsyncMessageCollector extends ThreadExecutorMessageCollector implements CacheClientApplicationAware {

	/**
	 * 异步消息处理器线程名称
	 */
	public static final String ASYNC_COLLECTOR_THREAD_NAME = "AsyncCollector";

	/**
	 * 异步消息处理器线程池默认的线程数量
	 */
	public static final int DEFAULT_ASYNC_THREAD_NUMBER = 5;

	static Logger log = LogManager.getLogger(AsyncMessageCollector.class.getName());

	private CacheClientApplication application;

	public AsyncMessageCollector() {

		super();
	}

	@Override
	public void setCacheClientApplication(CacheClientApplication application) {

		this.application = application;
	}

	@Override
	protected void doAfterPropertiesSet() {

		Assert.notNull(application, "application");
	}

	@Override
	public String collectMessage(String api, final String msg, Throwable t) {

		log.trace("[collectMessage] message = {}, api = {}, throwable = {}", api, msg, t);

		String message = msg;

		if (Tools.isBlank(message)) {

			message = (
					t == null ? "<NO_MESSAGE>" : t.getMessage());
		}

		fireBeforeAddListener(message, t);

		String messageId = generateMessageId();

		execute(new MessageReceiver(messageId, api, message, t));

		return messageId;
	}

	protected String generateMessageId() {

		return ServerInfoUtils.generateIdentifier();
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("application", application);
		builder.appendParent(super.toString());
		return builder.toString();
	}

	private class MessageReceiver implements Runnable {

		private final String messageId;

		private final String api;

		private final String message;

		private String exceptionName;

		protected MessageReceiver(String messageId, String api, String message, Throwable e) {

			this.messageId = messageId;
			this.api = api;
			this.message = message;
			if (e != null) {
				this.exceptionName = e.getClass().getName();
			}
		}

		@Override
		public void run() {

			CacheClientMessage msg = new CacheClientMessage(
					messageId, application.getAppCode(), application.getNodeCode(), api, message, exceptionName);
			store(msg);
			fireAfterAddListener(msg);
		}
	}
}
