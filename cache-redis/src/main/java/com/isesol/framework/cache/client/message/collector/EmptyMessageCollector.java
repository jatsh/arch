package com.isesol.framework.cache.client.message.collector;

import com.isesol.framework.cache.client.message.*;

/**
 * 不作任何处理的消息收集器，用于关闭消息收集器的处理。
 */
public class EmptyMessageCollector implements MessageCollector {

	private static final String EMPTY_RESULT = "<EMPTY>";

	@Override
	public void addCollectorListener(MessageCollectorListener listener) {

	}

	@Override
	public String collectMessage(String api, String message) {

		return EMPTY_RESULT;
	}

	@Override
	public String collectMessage(String api, String message, Throwable t) {

		return EMPTY_RESULT;
	}
}
