package com.isesol.framework.cache.client.message.collector;

import com.isesol.framework.cache.client.message.*;
import com.isesol.framework.cache.client.util.*;
import org.springframework.beans.factory.*;

/**
 * 内存队列数据存储器。用于将数据存放至在内存中创建的队列中。
 *
 * @see CacheClientQueue
 */
public class QueueStored<T> implements InitializingBean {

	/**
	 * 缓存客户端内存队列
	 */
	private CacheClientQueue<T> queue;

	public QueueStored() {

	}

	public void setQueue(CacheClientQueue<T> queue) {

		this.queue = queue;
	}

	@Override
	public void afterPropertiesSet() {

		Assert.notNull(queue, "queue");
	}

	/**
	 * 往内存队列尾部追加数据
	 *
	 * @param data 需要追加至内存队列尾部的数据，如果值为 <code>null</code> 时，将忽略该数据
	 */
	public void store(T data) {

		queue.add(data);
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("queue", queue);
		builder.appendParent(super.toString());
		return builder.toString();
	}
}
