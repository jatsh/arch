package com.isesol.framework.cache.client.message.collector;

import com.isesol.framework.cache.client.message.*;
import com.isesol.framework.cache.client.thread.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;

import java.util.*;

/**
 * 线程执行器构建的缓存客户端消息收集器
 */
public abstract class ThreadExecutorMessageCollector extends QueueStored<CacheClientMessage>
		implements MessageCollector, InitializingBean, DisposableBean {

	static Logger log = LogManager.getLogger(ThreadExecutorMessageCollector.class.getName());

	/**
	 * 消息收集监听器
	 */
	private final List<MessageCollectorListener> listeners;

	/**
	 * 客户端缓存线程执行器
	 */
	private CacheClientThreadExecutor threadExecutor;

	public ThreadExecutorMessageCollector() {

		this.listeners = new LinkedList<MessageCollectorListener>();
	}

	public void setThreadExecutor(CacheClientThreadExecutor threadExecutor) {

		this.threadExecutor = threadExecutor;
	}

	@Override
	public final void afterPropertiesSet() {

		super.afterPropertiesSet();
		Assert.notNull(threadExecutor, "threadExecutor");
		doAfterPropertiesSet();
	}

	@Override
	public final void destroy() {

		if (threadExecutor != null) {
			threadExecutor.destroy();
		}

		doDestory();
	}

	/**
	 * 设置消息的发送策略。{@link SendStrategy} 对象作为消息收集监听器的一种。
	 *
	 * @param sendStrategy 消息的发送策略
	 */
	public void setSendStrategy(SendStrategy sendStrategy) {

		addCollectorListener(sendStrategy);
	}

	@Override
	public final void addCollectorListener(MessageCollectorListener listener) {

		Assert.notNull(listener, "listener");
		listeners.add(listener);

		if (log.isTraceEnabled()) {
			log.trace("[addCollectorListener] listener = {}, listeners count = {}", listener, listeners.size());
		}
	}

	@Override
	public final String collectMessage(String api, String message) {

		return collectMessage(api, message, null);
	}

	/**
	 * 使用线程执行器执行任务
	 *
	 * @param command 需要执行的任务对象
	 */
	public final void execute(Runnable command) {

		threadExecutor.execute(command);
	}

	/**
	 * 触发消息收集前的事件
	 *
	 * @param message
	 * @param e
	 */
	protected final void fireBeforeAddListener(String message, Throwable e) {

		if (Tools.isBlank(listeners)) {
			return;
		}

		BeforeAddMessageEvent event = new BeforeAddMessageEvent(message, e);

		for (MessageCollectorListener listener : listeners) {
			listener.beforeAddMessage(event);
		}
	}

	/**
	 * 触发消息收集后的事件
	 *
	 * @param message
	 */
	protected final void fireAfterAddListener(CacheClientMessage message) {

		if (Tools.isBlank(listeners)) {
			return;
		}

		AfterAddMessageEvent event = new AfterAddMessageEvent(message);

		for (MessageCollectorListener listener : listeners) {
			listener.afterAddMessage(event);
		}
	}

	protected void doAfterPropertiesSet() {

	}

	protected void doDestory() {

	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("listeners", listeners);
		builder.append("threadExecutor", threadExecutor);
		builder.appendParent(super.toString());
		return builder.toString();
	}
}
