package com.isesol.framework.cache.client.message.sender;

import com.isesol.framework.cache.client.util.*;

import java.io.*;

/**
 * 缓存客户端消息数据发送至本地控制台中，在控制上直接输出消息数据
 *
 * @see LoggingMessageSender
 */
public class ConsoleMessageSender extends LocalMessageSender {

	/**
	 * 控制台类型 -- 标准控制台（std）
	 */
	public static final String TYPE_STD = "std";

	/**
	 * 控制台类型 -- 错误控制台（err）
	 */
	public static final String TYPE_ERR = "err";

	/**
	 * 控制台类型，默认值为标准控制台
	 */
	private String type = TYPE_STD;

	private PrintStream stream;

	public ConsoleMessageSender() {

	}

	public String getType() {

		return type;
	}

	public void setType(String type) {

		this.type = type;
	}

	@Override
	protected void doAfterPropertiesSet() {

		super.doAfterPropertiesSet();
		Assert.notEmpty(type, "type");
		this.stream = TYPE_ERR.equalsIgnoreCase(type) ? System.err : System.out;
	}

	@Override
	protected void doSend(String appCode, String message) {

		stream.println(appCode + ": " + message);
	}
}
