package com.isesol.framework.cache.client.message.sender;

import com.isesol.framework.cache.client.*;
import com.isesol.framework.cache.client.message.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.*;

/**
 * 消息发送失败时，自动持久化处理的消息发送器，以便于消息的发送的可靠性。
 * <p>
 * 在发送消息的过程中，若产生了异常或者错误时，将消息持久化到磁盘文件中，便于下一次到达触发发送的时机时，再与当前的数据一并发送。
 * </p>
 */
public abstract class FailedPersistenceSender extends JsonMessageConvert implements MessageSender, InitializingBean {

	/**
	 * 发送失败时，消息持久化文件名的前缀
	 */
	private static final String FAILED_FILENAME_PREFIX = ServerInfoUtils.getPid() + "-" + Long.toString(
			Tools.toSeconds(
					ServerInfoUtils.getStartTime()), Character.MAX_RADIX) + "-";

	private static final AtomicInteger INCREMENT = new AtomicInteger(0);

	static Logger log = LogManager.getLogger(FailedPersistenceSender.class.getName());

	/**
	 * 发送失败消息持久化文件存放目录
	 */
	private File root = new File(Constant.FAIL_FILE_PATH);

	/**
	 * 消息持久化消息行之间的分隔符
	 */
	private String lineSeperator = "\n";

	/**
	 * 之前失败时数据持久化的文件
	 */
	private File persistence;

	public FailedPersistenceSender() {

	}

	public File getRoot() {

		return root;
	}

	public void setRoot(File root) {

		Assert.notNull(root, "root");
		this.root = root;
	}

	public String getLineSeperator() {

		return lineSeperator;
	}

	/**
	 * <p>
	 * Default value is <code>\n</code>.
	 * </p>
	 *
	 * @param lineSeperator
	 */
	public void setLineSeperator(String lineSeperator) {

		Assert.notEmpty(lineSeperator, "lineSeperator");

		this.lineSeperator = lineSeperator;
	}

	public synchronized File getPersistence() {

		return persistence;
	}

	@Override
	public final void afterPropertiesSet() {

		if (log.isInfoEnabled()) {

			log.info("root = {}, line separator = {}", getRoot().getAbsolutePath(), Tools.toHex(getLineSeperator()));
		}

		doAfterPropertiesSet();
	}

	@Override
	public final synchronized void send(String appCode, List<CacheClientMessage> messages) {

		if (log.isDebugEnabled()) {

			log.debug("[send] start, message items: {}, persistence: {}", Tools.size(messages), persistence);
		}

		// messages as strings to send
		List<String> list = new LinkedList<String>();

		// send messages
		boolean sendStatus = sendMessages(appCode, persistence, list, messages);

		// send messages is success, set persistence to null
		if (sendStatus) {

			persistence = null;

			return;
		}

		// send messages is failed,
		// all messages that contains old persistence and current messages
		// continue to file persistence

		// rename current persistence file name to failed after send
		afterProcess(persistence, FailedResourceEvent.FAILED_AFTER_SEND);

		// persistence messages to file, the persistence field will be reset to
		// a new file object
		try {

			persistence = writeFailedMessages(list);

			log.info("[send] send failed generating a new persistence file: {}", persistence);
		} catch (Exception e) {

			log.warn("[send] want write failed message to file: {}, cause exception and retry again", persistence, e);

			try {

				persistence = writeFailedMessages(list);

				log.warn("[send] retry send failed generating a new persistence file: {}", persistence);
			} catch (Exception e1) {

				log.error(
						"[send] writeFailedMessages again cause exception, ignored it" +
						", wrote file: {}, contents:\n{}", persistence, concatMessages(list), e1);
			}
		}
	}

	/**
	 * <p>
	 * Write messages to perstence file when messages send failed
	 * </p>
	 *
	 * @param messages Messages are sended
	 *
	 * @return Persistence file object that messages has been wroten
	 *
	 * @throws IOException
	 */
	protected File writeFailedMessages(List<String> messages) throws IOException {

		if (Tools.isBlank(messages)) {

			log.warn("[writeFailedMessages] messages is null or empty, need not write failed");
			return null;
		}

		// checking root directory, prepare to write persistence file
		if (!getRoot().exists()) {

			boolean mkdirResult = getRoot().mkdirs();

			if (!mkdirResult) {

				mkdirResult = getRoot().mkdirs();
			}

			log.debug(
					"[writeFailedMessages] root directory not exists, make the directories: {}" + ", mkdir result: {}",
					getRoot(), mkdirResult);
		}

		BufferedWriter writer = null;
		File file = null;

		try {

			file = new File(getRoot(), createFailedFilename());

			if (log.isDebugEnabled()) {

				log.debug(
						"[writeFailedMessages] created new file to write {} failed message(s), file: {}", Tools.size(
								messages), file);
			}

			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), Tools.DEFAULT_CHARSET));
			String msgs = concatMessages(messages);
			writer.write(msgs);
			writer.flush();

			if (log.isDebugEnabled()) {

				log.debug(
						"[writeFailedMessages] write failed messages to file: {} finished" + ", write {} character(s)",
						file, Tools.size(msgs));
			}

			return file;
		} finally {

			if (writer != null) {

				writer.close();
			}
		}
	}

	/**
	 * <p>
	 * Concat string format messages to a string, the messages are seperated by
	 * {@link #getLineSeperator()}
	 * </p>
	 *
	 * @param messages All messages
	 *
	 * @return a string contains all messages
	 */
	protected String concatMessages(List<String> messages) {

		if (Tools.isBlank(messages)) {

			log.warn("[concatMessages] messages is null or empty, messages: {}", messages);

			return null;
		}

		StringBuilder builder = new StringBuilder();

		for (String message : messages) {

			builder.append(message).append(getLineSeperator());
		}

		String str = builder.toString();

		log.trace("[concatMessages] concat messages:\n{}", str);

		return str;
	}

	/**
	 * <p>
	 * Create persistence file name, the file name must be uniqued to avoid
	 * connflit.
	 * </p>
	 *
	 * @return a new file name
	 */
	protected String createFailedFilename() {

		StringBuilder builder = new StringBuilder();

		builder.append(FAILED_FILENAME_PREFIX);
		builder.append(Tools.toSeconds(System.currentTimeMillis()));
		builder.append('-');
		builder.append(INCREMENT.incrementAndGet());
		return builder.toString();
	}

	protected boolean sendMessages(
			String appCode, File resource, List<String> list, List<CacheClientMessage> messages) {

		if (log.isDebugEnabled()) {

			log.debug("[sendMessages] failed file: {}, string messages size: {}", resource, Tools.size(list));
		}

		try {

			if (isValidFile(resource)) {

				log.debug("[sendMessages] resource has value, failed file: {}", resource);

				readFailedMessages(resource, list);

				afterProcess(resource, FailedResourceEvent.AFTER_READ);
			} else if (resource != null) {

				log.warn(
						"[sendMessages] resource has value, but it not exists or is not file" + ", failed file: {}",
						resource);
			}

			serializeLine(messages, list);

			if (Tools.isBlank(list)) {

				return true;
			}

			return doSend(appCode, list);
		} catch (Exception e) {

			log.error(
					"Unexcepted exception that is unimaginable!!! failed file: {}, list = {}, messages = {}", resource,
					list, messages, e);

			return false;
		}
	}

	protected boolean doSend(String appCode, List<String> messages) {

		String msgs = concatMessages(messages);

		return doSend(appCode, msgs, messages.size());
	}

	protected File afterProcess(File resource, FailedResourceEvent event) {

		log.debug("[afterProcess] failed file: {}, event: {}", resource, event);

		File newResource = null;

		try {

			if (resource == null || !resource.exists()) {

				log.warn(
						"[afterProcess] failed file is null or not exists, failed file: {}, event: {}", resource,
						event);

				return null;
			}

			newResource = event.getRenameFile(resource);

			boolean renameResult = resource.renameTo(newResource);

			log.debug(
					"[afterProcess] failed file rename to '{}', result '{}', from '{}', event: {}", newResource,
					renameResult, resource, event);

			return newResource;
		} catch (Exception e) {

			log.warn(
					"[afterProcess] failed file rename to '{}' from '{}', event: {} cause error" +
					", the error can be ignored", newResource, resource, event, e);
			return null;
		}
	}

	/**
	 * <p>
	 * Reads messages in persistence file, and append the messages to the
	 * current batch messages.
	 * </p>
	 *
	 * @param resource The last time the persistent file failed to send
	 * @param messages The messages need to be sent
	 *
	 * @throws IOException
	 */
	protected void readFailedMessages(File resource, List<String> messages) throws IOException {

		if (!isValidFile(resource)) {

			log.warn(
					"[readFailedMessages] failed file not exists or resource is not file" + ", failed file: {}",
					resource);
			return;
		}

		if (log.isDebugEnabled()) {

			log.debug(
					"[readFailedMessages] failed file is not null represents the last error generated" +
					", failed file: {}, modified time: {}, file size: {}", resource, resource.lastModified(),
					resource.length());
		}

		// Reads messages from failed persistence file, and appends the messages
		// to the current messages

		BufferedReader reader = null;

		try {

			reader = new BufferedReader(new InputStreamReader(new FileInputStream(resource), Tools.DEFAULT_CHARSET));

			int count = 0;

			for (String str = null; (str = reader.readLine()) != null; ) {

				if (Tools.isBlank(str)) {

					continue;
				}

				messages.add(str.trim());

				count++;
			}

			log.debug("[readFailedMessages] read {} message(s)", count);
		} finally {

			if (reader != null) {

				reader.close();
			}
		}
	}

	/**
	 * <p>
	 * Checking whethere file is valid
	 * </p>
	 *
	 * @param file file object
	 *
	 * @return
	 */
	private boolean isValidFile(File file) {

		if (file == null) {

			return false;
		}

		if (!file.exists()) {

			return false;
		}

		if (!file.isFile()) {

			return false;
		}

		return true;
	}

	protected void doAfterPropertiesSet() {

	}

	protected abstract boolean doSend(String appCode, String messages, int count);
}
