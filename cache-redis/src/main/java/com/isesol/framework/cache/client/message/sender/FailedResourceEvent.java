package com.isesol.framework.cache.client.message.sender;

import java.io.*;

public enum FailedResourceEvent {

	AFTER_READ("read"),

	FAILED_AFTER_SEND("failed");

	private String event;

	private FailedResourceEvent(String event) {

		this.event = event;
	}

	public String getEvent() {

		return event;
	}

	public File getRenameFile(File file) {

		return new File(file.getParentFile(), file.getName() + "." + event);
	}

}
