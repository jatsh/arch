package com.isesol.framework.cache.client.message.sender;

import com.isesol.framework.cache.client.*;
import com.isesol.framework.cache.client.connector.*;
import com.isesol.framework.cache.client.http.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;

/**
 * 通过 HTTP 协议发送缓存客户端消息数据至远程的服务端。
 * <p>
 * 客户端在向 HTTP URL POST 数据时，在 HTTP 请求头
 * <code>{@value #CACHE_MESSAGE_COUNT}</code> 中将发送的消息数量作为其值
 * </p>
 * <p>
 * 同时提交数据的 HTTP 请求头 <code>Content-Type</code> 值设为 <code>{@value #DEFAULT_CONTENT_TYPE}</code>。
 * </p>
 * <p>
 * 服务端在接收数据后处理数据，同时将处理的消息数量写入 HTTP 响应报文体中。客户端获取 HTTP 响应数据，
 * 获取服务端处理的消息数量，若该值与发送的消息数量一致时表示服务端接收成功，不一致时可以认为服务端接收或处理失败， 需要进行发送失败持久化处理。
 * </p>
 */
public class HttpMessageSender extends FailedPersistenceSender implements DisposableBean {

	static Logger log = LogManager.getLogger(HttpMessageSender.class.getName());

	/**
	 * HTTP 请求处理器
	 */
	private HttpRequestor httpRequestor;

	/**
	 * 应用关闭通知器
	 */
	private ClientShutdownNotifier shutdownNotifier;

	public HttpMessageSender() {

	}


	public static final String CACHE_MESSAGE_COUNT= Constant.CACHE_MESSAGE_COUNT;

	public static final String DEFAULT_CONTENT_TYPE= Constant.DEFAULT_CONTENT_TYPE;


	public HttpRequestor getHttpRequestor() {

		return httpRequestor;
	}

	public void setHttpRequestor(HttpRequestor httpRequestor) {

		this.httpRequestor = httpRequestor;
	}

	public ClientShutdownNotifier getShutdownNotifier() {

		return shutdownNotifier;
	}

	public void setShutdownNotifier(ClientShutdownNotifier shutdownNotifier) {

		this.shutdownNotifier = shutdownNotifier;
	}

	@Override
	protected void doAfterPropertiesSet() {

		Assert.notNull(getHttpRequestor(), "httpRequestor");
	}

	@Override
	public void destroy() {

		if (getShutdownNotifier() != null) {
			getShutdownNotifier().notifyShutdown();
		}
	}

	@Override
	protected boolean doSend(String appCode, String messages, int count) {

		log.debug("[doSend] start, appCode '{}', message count: {}", appCode, count);

		try {

			HttpRequest request = new HttpRequest(Tools.DEFAULT_CHARSET);

			String sendMessages = concatSendMessages(messages, count);

			RequestEntity entity = new StringRequestEntity(sendMessages);
			request.setRequestEntity(entity);

			request.addHeader("Content-Type", defaultContentType());
			request.addHeader(Constant.CACHE_APP_CODE, appCode);
			request.addHeader(CACHE_MESSAGE_COUNT, String.valueOf(count));

			HttpResponse response = getHttpRequestor().doPost(request);

			return isSuccessSent(response, count);
		} catch (Exception e) {

			log.error("[doSend] send messages cause exception, messages count: {}", count, e);

			return false;
		}
	}

	protected boolean isSuccessSent(HttpResponse response, int count) {

		log.trace("[isSuccessSent] count: {}", count);

		if (response == null) {

			log.debug("[isSuccessSent] HTTP response is null, request number of messages is {}", count);

			return false;
		}

		if (!response.isSuccessResponse()) {

			log.warn(
					"[isSuccessSent] HTTP response code is not success code (2xx)" +
					", response code is '{}', request message count is {}", response.getResponseCode(), count);

			return false;
		}

		String responseString = response.getContentTypeResponse();

		if (Tools.isBlank(responseString)) {

			log.warn("[isSuccessSent] Response content is null or empty" + ", send number of messages is {}", count);

			return false;
		}

		log.debug("[isSuccessSent] Response content '{}', send number of messages is {}", responseString, count);

		try {

			int acceptCount = Integer.parseInt(responseString.trim());

			log.info(
					"[isSuccessSent] Response accept number of messages is {}" + ", send number of messages is {}",
					acceptCount, count);

			return (count == acceptCount);

		} catch (NumberFormatException e) {

			log.warn(
					"[isSuccessSent] HTTP response content '{}' is not numberic" +
					", can not get server process messages count, send number of messages is {}", responseString
							.trim(),
					count);

			return false;
		}
	}

	protected String concatSendMessages(String messages, int count) {

		return messages.trim();
	}

	protected String defaultContentType() {

		return DEFAULT_CONTENT_TYPE;
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("root", getRoot());
		builder.append("lineSeperator", getLineSeperator());
		builder.append("httpRequestor", getHttpRequestor());
		return builder.toString();
	}
}
