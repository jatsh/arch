package com.isesol.framework.cache.client.message.sender;

import com.alibaba.fastjson.*;
import com.isesol.framework.cache.client.message.*;
import com.isesol.framework.cache.client.util.*;

import java.util.*;

/**
 * JSON 消息转换器。用于将 {@link CacheClientMessage} 对象序列化成为 JSON 格式的字符串。
 */
public class JsonMessageConvert {

	public String serialize(CacheClientMessage message) {

		if (message == null) {

			return null;
		}

		return JSON.toJSONString(message);
	}

	public List<String> serializeLine(List<CacheClientMessage> messages, final List<String> flist) {

		List<String> list = flist;

		if (list == null) {

			list = new LinkedList<String>();
		}

		if (Tools.isBlank(messages)) {

			return list;
		}

		for (CacheClientMessage message : messages) {

			if (message == null) {

				continue;
			}

			list.add(serialize(message));
		}

		return list;
	}
}
