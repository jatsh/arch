package com.isesol.framework.cache.client.message.sender;

/**
 * 通过本地发送缓存客户端消息数据。
 */
public abstract class LocalMessageSender extends FailedPersistenceSender {

	/**
	 * 是否处于调试模式的永远发送成功
	 */
	private boolean debugSendSuccess = true;

	public LocalMessageSender() {

	}

	public boolean isDebugSendSuccess() {

		return debugSendSuccess;
	}

	/**
	 * <p>
	 * 设置是否处于调试模式，在本地发送处理完成后的返回值。主要用于调试失败转存的逻辑。默认值为 <code>true</code>，
	 * 表示发送永远成功不会进入发送失败转存逻辑。
	 * </p>
	 *
	 * @param debugSendSuccess
	 */
	public void setDebugSendSuccess(boolean debugSendSuccess) {

		this.debugSendSuccess = debugSendSuccess;
	}

	@Override
	protected final boolean doSend(String appCode, String messages, int count) {

		String message = "accept local messages, appCode: " + appCode + ", count: " + count + ", debugSendSuccess: " +
		                 debugSendSuccess + ":\n" + messages;
		doSend(appCode, message);
		return debugSendSuccess;
	}

	protected abstract void doSend(String appCode, String message);
}
