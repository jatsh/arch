package com.isesol.framework.cache.client.message.sender;

import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;


/**
 * 将日志作为缓存客户端消息发送目的地的消息发送器。消息以 <code>INFO</code> 级别日志输出，默认的消息记录器为
 * {@link #recorder}
 *
 * @see ConsoleMessageSender
 */
public class LoggingMessageSender extends LocalMessageSender {

	/**
	 * 默认日志记录器（message.send.recorder）
	 */
	public static final String DEFAULT_RECORDER = "message.send.recorder";

	static Logger log = LogManager.getLogger(LoggingMessageSender.class.getName());

	/**
	 * 日志记录器，默认值为“message.send.recorder”
	 */
	private String recorder = DEFAULT_RECORDER;


	public LoggingMessageSender() {

	}

	public String getRecorder() {

		return recorder;
	}

	public void setRecorder(String recorder) {

		this.recorder = recorder;
	}

	@Override
	protected void doAfterPropertiesSet() {

		super.doAfterPropertiesSet();
		Assert.notNull(recorder, "recorder");

	}

	@Override
	protected void doSend(String appCode, String message) {

		log.info("{}: {}", appCode, message);
	}
}
