package com.isesol.framework.cache.client.message.sender;

import com.isesol.framework.cache.client.app.*;
import com.isesol.framework.cache.client.message.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;

import java.util.*;

/**
 * 拥有缓存客户端内部消息队列的队列消息发送器。
 * <p>
 * 该消息器仅作为缓存客户端内部消息队列和具体的消息发送器的包装。其中内部的消息队列应与
 * {@link MessageCollector MessageCollector} 中的进行数据共享。
 * </p>
 * <p>
 * {@link MessageCollector MessageCollector} 作为该共享内部队列的生产者， 这个类则为该队列的消费者。
 * </p>
 *
 * @see MessageCollector
 */
public class MessageQueueSender
		implements CacheClientApplicationAware, MessageSender, InitializingBean, DisposableBean {

	static Logger log = LogManager.getLogger(MessageQueueSender.class.getName());

	/**
	 * 与 {@link MessageCollector MessageCollector} 共享的缓存客户端内部消息队列
	 */
	private CacheClientQueue<CacheClientMessage> queue;

	/**
	 * 承担消息发送任务的消息发送器
	 */
	private MessageSender sender;

	private CacheClientApplication application;

	public MessageQueueSender() {

	}

	public void setQueue(CacheClientQueue<CacheClientMessage> queue) {

		this.queue = queue;
	}

	public void setSender(MessageSender sender) {

		this.sender = sender;
	}

	@Override
	public void setCacheClientApplication(CacheClientApplication application) {

		this.application = application;
	}

	@Override
	public void afterPropertiesSet() {

		Assert.notNull(queue, "queue");
		Assert.notNull(sender, "sender");
		Assert.notNull(application, "application");
	}

	protected final void send(List<CacheClientMessage> messages) {

		send(application.getAppCode(), messages);
	}

	protected final void send(int max) {

		send(application.getAppCode(), max);
	}

	@Override
	public final void send(String appCode, List<CacheClientMessage> messages) {

		if (Tools.isBlank(messages)) {

			return;
		}

		if (log.isDebugEnabled()) {

			StringBuilder builder = new StringBuilder();

			builder.append("send messages, count = " + messages.size());

			int offset = 1;

			for (CacheClientMessage message : messages) {

				builder.append("\n  ").append(offset++).append(". ").append(message);
			}

			log.debug(builder.toString());

		}

		sender.send(appCode, messages);
	}

	/**
	 * 获取消息队列中消息的数量
	 *
	 * @return 消息队列中消息的数量
	 */
	public int getMessageCount() {

		return queue.size();
	}

	/**
	 * 限定发送数据量进行发送，以防止在大量数据的情况下，由于大量的数据而产生发送失败的情况。
	 *
	 * @param max 发送消费的最大数量
	 */
	public void send(String appCode, int max) {

		if (max < 1) {

			return;
		}

		List<CacheClientMessage> messages = queue.poll(max);

		if (Tools.isBlank(messages)) {
			return;
		}

		send(appCode, messages);
	}

	@Override
	public final void destroy() {

		if (log.isInfoEnabled()) {

			log.info("Before destory, send the remainder of messages, count: {}", getMessageCount());
		}

		// 在容器关闭或者销毁时，将队列中尚未达到发送条件的数据发送
		send(Integer.MAX_VALUE);

		doDestory();

		log.info("after destory, destory finished");
	}

	protected void doDestory() {

	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("queue", queue);
		builder.append("sender", sender);
		return builder.toString();
	}
}
