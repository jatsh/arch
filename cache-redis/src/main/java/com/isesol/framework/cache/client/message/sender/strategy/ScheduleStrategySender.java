package com.isesol.framework.cache.client.message.sender.strategy;

import com.isesol.framework.cache.client.message.*;
import com.isesol.framework.cache.client.message.sender.*;
import com.isesol.framework.cache.client.thread.*;
import com.isesol.framework.cache.client.thread.impl.*;
import com.isesol.framework.cache.client.util.*;
import org.springframework.beans.factory.*;

import java.util.concurrent.atomic.*;

/**
 * <p>
 * Cache client message send strategy that according to schedule period time in
 * seconds. A fixed period of time to send all messages in the memory, after
 * send clearing the messages in memory.
 * </p>
 * <p>
 * The send strategy default using schedule thread pool that conatains 2
 * threads, means that the strategy can perform 2 tasks at the same time.
 * </p>
 */
public class ScheduleStrategySender extends MessageQueueSender implements SendStrategy, DisposableBean {

	/**
	 * <p>
	 * Default max send the number of messages, default value is 500
	 * </p>
	 */
	public static final int DEFAULT_SEND_MAX_NUMBERS = 500;

	/**
	 * <p>
	 * Default send period seconds, default value is 600 seconds (5 minutes)
	 * </p>
	 */
	public static final int DEFAULT_SEND_PERIODS = 600;

	/**
	 * <p>
	 * Default schedule thread pool contains the number of thread, default value
	 * is 2
	 * </p>
	 */
	public static final int DEFAULT_SEND_THREADS_COUNT = 2;

	/**
	 * Start running monitor, this value represents the send handler has been
	 * launched
	 */
	private final AtomicBoolean initMonitor = new AtomicBoolean(false);

	/**
	 * <p>
	 * Max send the number of messages in periods
	 * </p>
	 */
	private int max = DEFAULT_SEND_MAX_NUMBERS;

	/**
	 * Messages in memory that were sent period in seconds. When the numbers of
	 * messages in memory exceeds the value to start sending process
	 */
	private int periods = DEFAULT_SEND_PERIODS;

	/**
	 * The number of thread in message send thead pool
	 */
	private int sendThreadsCount = DEFAULT_SEND_THREADS_COUNT;

	/**
	 * Message sending thread pool
	 */
	private CacheClientThreadExecutor threadExecutor;

	public ScheduleStrategySender() {

	}

	/**
	 * <p>
	 * Setting max send the number of messages in periods. The value must be a
	 * positive integer. Default value is definied in constant
	 * {@link #DEFAULT_SEND_MAX_NUMBERS}
	 * </p>
	 *
	 * @param max Send period in seconds
	 *
	 * @throws IllegalArgumentException periods value is not a positive integer
	 */
	public void setMax(int max) {

		Assert.notLesser(max, 1, "max");
		this.max = max;
	}

	/**
	 * <p>
	 * Setting messages in memory that were sent period in seconds. The value
	 * must be a positive integer. Default value is definied in constant
	 * {@link #DEFAULT_SEND_PERIODS}
	 * </p>
	 *
	 * @param periods Send period in seconds
	 *
	 * @throws IllegalArgumentException periods value is not a positive integer
	 */
	public void setPeriods(int periods) {

		Assert.notLesser(periods, 1, "periods");
		this.periods = periods;
	}

	/**
	 * <p>
	 * Setting the number of thread in message sending schedule thead pool. The
	 * value must be a positive integer. Default value is definied in constant
	 * {@link #DEFAULT_SEND_THREADS_COUNT}
	 * </p>
	 *
	 * @param sendThreadsCount Number of thread in message sending schedule thead pool
	 *
	 * @throws IllegalArgumentException sendThreadsCount value is not a positive integer
	 */
	public void setSendThreadsCount(int sendThreadsCount) {

		Assert.notLesser(sendThreadsCount, 1, "sendThreadsCount");
		this.sendThreadsCount = sendThreadsCount;
	}

	@Override
	public void beforeAddMessage(BeforeAddMessageEvent event) {

	}

	@Override
	public void afterAddMessage(AfterAddMessageEvent event) {

		if (!initMonitor.compareAndSet(false, true)) {

			return;
		}

		Runnable command = new Runnable() {

			@Override
			public void run() {

				send(max);
			}
		};

		String prefixName = "Schedule[" + (periods / Tools.PER_MINUTE_IN_SECONDS) + "]";

		threadExecutor = new ScheduleCacheClientThreadExecutor(prefixName, sendThreadsCount, periods, command);
		threadExecutor.start();
	}

	@Override
	protected void doDestory() {

		if (threadExecutor != null) {

			threadExecutor.destroy();
		}
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("initMonitor", initMonitor);
		builder.append("max", max);
		builder.append("periods", periods);
		builder.append("sendThreadsCount", sendThreadsCount);
		builder.append("threadExecutor", threadExecutor);
		builder.appendParent(super.toString());
		return builder.toString();
	}
}
