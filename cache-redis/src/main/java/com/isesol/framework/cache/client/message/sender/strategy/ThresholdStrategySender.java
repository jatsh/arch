package com.isesol.framework.cache.client.message.sender.strategy;

import com.isesol.framework.cache.client.message.*;
import com.isesol.framework.cache.client.message.sender.*;
import com.isesol.framework.cache.client.thread.*;
import com.isesol.framework.cache.client.thread.impl.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.factory.*;

/**
 * <p>
 * Cache client message send strategy that according to currentthe number of
 * messages in memory. When the number of messages exceeds setting threshold
 * value, and sends these messages, clears the data in memory at the same time.
 * </p>
 * <p>
 * The send strategy default using thread pool that conatains 2 threads.
 * </p>
 */
public class ThresholdStrategySender extends MessageQueueSender implements SendStrategy, DisposableBean {

	/**
	 * <p>
	 * Default max numbers of message that will be send in memory, default value
	 * is 100
	 * </p>
	 */
	public static final int DEFAULT_MESSAGE_THRESHOLD = 100;

	/**
	 * <p>
	 * Default thread pool contains the number of thread, default value is 2
	 * </p>
	 */
	public static final int DEFAULT_SEND_THREADS_COUNT = 2;

	static Logger log = LogManager.getLogger(ThresholdStrategySender.class.getName());

	/**
	 * A maximum number on the message sent. When the numbers of messages in
	 * memory exceeds the value to start sending process
	 */
	private int threshold = DEFAULT_MESSAGE_THRESHOLD;

	/**
	 * The number of thread in message send thead pool
	 */
	private int sendThreadsCount = DEFAULT_SEND_THREADS_COUNT;

	/**
	 * Message sending thread pool
	 */
	private CacheClientThreadExecutor threadExecutor;

	public ThresholdStrategySender() {

	}

	/**
	 * <p>
	 * Setting the maximum number on the message sent. The value must be a
	 * positive integer. Default value is definied in constant
	 * {@link #DEFAULT_MESSAGE_THRESHOLD}
	 * </p>
	 *
	 * @param threshold A maximum number on the message sent
	 *
	 * @throws IllegalArgumentException Threshold value is not a positive integer
	 */
	public void setThreshold(int threshold) {

		Assert.notLesser(threshold, 1, "threshold");
		this.threshold = threshold;
	}

	/**
	 * <p>
	 * Setting the number of thread in message sending thead pool. The value
	 * must be a positive integer. Default value is definied in constant
	 * {@link #DEFAULT_MESSAGE_THRESHOLD}
	 * </p>
	 *
	 * @param sendThreadsCount Number of thread in message sending thead pool
	 *
	 * @throws IllegalArgumentException sendThreadsCount value is not a positive integer
	 */
	public void setSendThreadsCount(int sendThreadsCount) {

		Assert.notLesser(sendThreadsCount, 1, "sendThreadsCount");
		this.sendThreadsCount = sendThreadsCount;
	}

	@Override
	public void afterPropertiesSet() {

		super.afterPropertiesSet();

		// building thead pool, using fixed thread pool stragery
		threadExecutor = new PooledCacheClientThreadExecutor("Threshold[" + threshold + "]", sendThreadsCount);
	}

	@Override
	public void beforeAddMessage(BeforeAddMessageEvent event) {

		log.trace("[event=BeforeAddMessage] event = {}", event);
	}

	@Override
	public void afterAddMessage(AfterAddMessageEvent event) {

		if (log.isTraceEnabled()) {
			log.trace("[event=AfterAddMessage] event = {}, threshold = {}", event, threshold);
		}

		// check messages count
		if (getMessageCount() < threshold) {
			return;
		}

		// asynchronized executing send in thread pool
		threadExecutor.execute(
				new Runnable() {

					@Override
					public void run() {

						send(threshold);
					}
				});
	}

	@Override
	protected void doDestory() {

		if (threadExecutor != null) {

			threadExecutor.destroy();
		}
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("threshold", threshold);
		builder.append("sendThreadsCount", sendThreadsCount);
		builder.append("threadExecutor", threadExecutor);
		builder.appendParent(super.toString());
		return builder.toString();
	}
}
