package com.isesol.framework.cache.client.processor;

import com.isesol.framework.cache.client.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.aop.*;

import java.lang.reflect.*;

/**
 * <p>
 * 确定是否需要创建 AOP 代理对象的类过滤器。该类扫描指定类中所有的方法，以确定该类的对象是否需要创建 AOP 代理对象。
 * </p>
 * <p>
 * 这个 <code>ClassFilter</code> 主要用于方法缓存注解扫描的 <code>Pointcut</code> 的实现类中， 作为
 * <code>{@link Pointcut#getClassFilter() Pointcut#getClassFilter()}</code>
 * 方法的返回对象。
 * </p>
 *
 * @see CacheablePointcut
 * @see CacheableProxyCreator
 */
public abstract class AbstractMethodsScanningClassFilter implements ClassFilter {

	static Logger log = LogManager.getLogger(AbstractMethodsScanningClassFilter.class.getName());

	public static final String PACKAGE_PREFIXES = Constant.PACKAGE_PREFIXES_STRING;

	/**
	 * 确定类是否满足创建代理对象的条件。对类中所有 public 的方法逐一进行判断，用于分析类中所有方法的信息。
	 * <p>对于非
	 * <code>{@value #PACKAGE_PREFIXES}</code>
	 * 开头包名的类永远返回 <code>false</code>
	 * ，即不为该类的基础对象产生代理增强对象。
	 * </p>
	 *
	 * @param clazz 确定是否需要创建代理对象的类信息
	 *
	 * @return 方法参数中类类型的对象是否需要创建 AOP 代理对象。<br />
	 * <code>true</code>：参数指定的类满足创建代理对象的条件<br />
	 * <code>false</code>：参数指定的类不满足创建代理对象的条件，或者该类的包名不以
	 * <code>com.ddatsh.</code> 开头
	 */
	@Override
	public boolean matches(Class<?> clazz) {

		Assert.notNull(clazz, "clazz");

		if (!CacheClientUtils.isCacheAbleClass(clazz)) {
			log.trace("Class is not cacheable class, type is '{}', ignored it method annotation scanning", clazz);
			return false;
		}

		if (CacheClientUtils.isCacheInternalClass(clazz)) {
			log.info(
					"Class is internal cache class (prefix of package name with '{}'), type is '{}', " +
					"ignored it method annotation scanning", Constant.CACHE_PACKAGE_PREFIX, clazz);
			return false;
		}

		log.debug("Matches class is  {}", clazz);

		Method[] methods = clazz.getMethods();

		if (Tools.isBlank(methods)) {

			return false;
		}

		boolean match = false;

		for (Method method : methods) {

			match |= matchesMethod(method, clazz);
		}

		if (match && log.isInfoEnabled()) {

			log.info("Matches class is proxied, class: {}", clazz);
		}

		return match;
	}

	private boolean matchesMethod(Method method, Class<?> clazz) {

		if (Modifier.isStatic(method.getModifiers())) {

			log.debug("Static method may be ignored, method: {}, class: {}", method, clazz);

			return false;
		}

		boolean result = matches(method, clazz);

		if (result && log.isInfoEnabled()) {

			log.info(
					"Matches method is cacheable method, method: {}, class: {}", method.getName(),
					clazz.getName());
		}

		return result;
	}

	/**
	 * 确定类中的方法是否满足创建代理对象的条件，比如，含有特定的方法名、方法标注等信息用于判断。
	 *
	 * @param method 目标类中 public 的方法
	 * @param targetClass 目标类对象
	 *
	 * @return 该方法所在的类产生的对象是否需要在其上创建 AOP 代理对象。<br />
	 * <code>true</code>：方法满足创建代理对象的条件<br />
	 * <code>false</code>：方法不满足创建代理对象的条件
	 *
	 * @see CacheablePointcut#matches(Method, Class)
	 */
	protected abstract boolean matches(Method method, Class<?> targetClass);
}
