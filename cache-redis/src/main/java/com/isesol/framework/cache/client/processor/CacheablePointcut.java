package com.isesol.framework.cache.client.processor;

import com.isesol.framework.cache.client.annotation.*;
import com.isesol.framework.cache.client.interceptor.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.aop.*;
import org.springframework.beans.factory.*;
import org.springframework.core.*;
import org.springframework.core.annotation.*;

import java.lang.annotation.*;
import java.lang.reflect.*;

public class CacheablePointcut extends AbstractMethodsScanningClassFilter implements InitializingBean, Pointcut {

	static Logger log = LogManager.getLogger(CacheablePointcut.class.getName());

	private CacheMetaRegistry registry;

	public CacheablePointcut() {

	}

	public CacheMetaRegistry getRegistry() {

		return registry;
	}

	public void setRegistry(CacheMetaRegistry registry) {

		this.registry = registry;
	}

	@Override
	public final void afterPropertiesSet() {

		Assert.notNull(getRegistry(), "registry");
		doAfterPropertiesSet();
	}

	@Override
	public ClassFilter getClassFilter() {

		return this;
	}

	@Override
	public MethodMatcher getMethodMatcher() {

		return MethodMatcher.TRUE;
	}

	@Override
	protected boolean matches(Method method, Class<?> targetClass) {

		Method originalMethod = BridgeMethodResolver.findBridgedMethod(method);

		boolean match = false;

		Class<?> clazz = method.getReturnType();

		for (CachingType caching : CachingType.values()) {

			if (CachingType.ENABLE.equals(caching) && void.class.equals(clazz)) {

				if (log.isDebugEnabled()) {

					log.debug(
							"@EnableCacheable cannot accept method that the return type is 'void'" +
							", ignore it, method: {}#{}", targetClass.getSimpleName(), method.getName());
				}

				continue;
			}

			match |= hasCacheableAnnotation(originalMethod, targetClass, caching.getType());
		}

		return match;
	}

	private boolean hasCacheableAnnotation(
			Method method, Class<?> targetClass, Class<? extends Annotation> annotationType) {

		Annotation annotation = AnnotationUtils.findAnnotation(method, annotationType);

		if (annotation != null) {

			register(annotation, method, targetClass);

			return true;
		}

		return false;
	}

	protected boolean register(Annotation annotation, Method method, Class<?> targetClass) {

		CachingOperation operation = CachingOperation.create(annotation);

		if (operation != null) {
			if (log.isDebugEnabled()) {
				log.debug("find find annotation {} on method {}", CacheClientUtils.getName(method), operation);
			}
			getRegistry().register(operation, method, targetClass);
			return true;
		}

		return false;
	}

	protected void doAfterPropertiesSet() {

	}
}
