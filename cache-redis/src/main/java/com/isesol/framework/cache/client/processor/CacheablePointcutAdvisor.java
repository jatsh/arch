package com.isesol.framework.cache.client.processor;

import com.isesol.framework.cache.client.util.*;
import org.aopalliance.aop.*;
import org.springframework.aop.*;
import org.springframework.aop.support.*;
import org.springframework.beans.factory.*;
import org.springframework.core.*;

import java.io.*;

public class CacheablePointcutAdvisor extends AbstractGenericPointcutAdvisor implements Serializable, InitializingBean {

	private static final int DEFAULT_ORDER = Ordered.HIGHEST_PRECEDENCE + 100;

	private Integer order;

	private Pointcut pointcut;

	public CacheablePointcutAdvisor() {

	}

	@Override
	public Pointcut getPointcut() {

		return pointcut;
	}

	public void setPointcut(Pointcut pointcut) {

		Assert.notNull(pointcut, "pointcut");
		this.pointcut = pointcut;
	}

	@Override
	public void setAdvice(Advice advice) {

		Assert.notNull(advice, "advice");
		super.setAdvice(advice);
	}

	@Override
	public void afterPropertiesSet() {

		Assert.notNull(getPointcut(), "pointcut");
		Assert.notNull(getAdvice(), "advice");
	}

	@Override
	public int getOrder() {

		if (order == null) {
			return DEFAULT_ORDER;
		}
		return order;
	}

	@Override
	public void setOrder(int order) {

		this.order = order;
	}

	@Override
	public String toString() {

		return getClass().getSimpleName() + "@" + "pointcut = [" + getPointcut() + "], " + "advice = [" + getAdvice() +
		       "], " + "order = [" + getOrder() + "]";
	}
}
