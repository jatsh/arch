package com.isesol.framework.cache.client.processor;

import com.isesol.framework.cache.client.util.*;
import com.isesol.framework.cache.client.util.EclipseTools.*;
import org.aopalliance.aop.*;
import org.apache.logging.log4j.*;
import org.springframework.aop.*;
import org.springframework.aop.framework.*;
import org.springframework.aop.framework.autoproxy.*;
import org.springframework.aop.support.*;
import org.springframework.aop.target.*;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.config.*;
import org.springframework.core.*;
import org.springframework.util.*;

public class CacheableProxyCreator extends ProxyConfig
		implements BeanClassLoaderAware, BeanFactoryAware, BeanPostProcessor, PriorityOrdered, InitializingBean {

	private static final Class<?>[] INFRASTRUCTURE_CLASSES = {Advice.class, Advisor.class, AopInfrastructureBean
			.class};

	static Logger log = LogManager.getLogger(CacheableProxyCreator.class.getName());

	/**
	 * BeanFactory instance, the object is injected by spring container
	 * automatically
	 */
	private transient BeanFactory beanFactory;

	/**
	 * BeanFactoryClassLoader instance, the object is injected by spring
	 * container automatically
	 */
	private transient ClassLoader classloader;

	/**
	 * cacheable annotation advisor
	 */
	private CacheablePointcutAdvisor advisor;

	/**
	 * Ignore bean names that create cacheable proxy object
	 */
	private String[] ignoreBeanNames;

	public CacheableProxyCreator() {

	}

	public CacheablePointcutAdvisor getAdvisor() {

		return advisor;
	}

	public void setAdvisor(CacheablePointcutAdvisor advisor) {

		this.advisor = advisor;
	}

	public void setIgnoreBeanNames(String[] ignoreBeanNames) {

		this.ignoreBeanNames = Tools.cloneArray(ignoreBeanNames);
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) {

		this.beanFactory = beanFactory;
	}

	@Override
	public void setBeanClassLoader(ClassLoader classLoader) {

		this.classloader = classLoader;
	}

	@Override
	public void afterPropertiesSet() {

		com.isesol.framework.cache.client.util.Assert.notNull(advisor, "advisor");

		log.info("afterPropertiesSet() has been invoked, {}", this);
	}

	@Override
	public int getOrder() {

		return Ordered.LOWEST_PRECEDENCE;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) {

		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) {

		return createProxyIfNecessary(bean, beanName);
	}

	protected Object createProxyIfNecessary(Object bean, String beanName) {

		if (ignoreProxyBeanName(beanName)) {

			logProxy("Ignore to create proxy", bean, beanName);

			return bean;
		}

		if (isInfrastructureClass(bean.getClass(), beanName)) {

			logProxy("Infrastructure class, that not attempt to create proxy object", bean, beanName);

			return bean;
		}

		if (shouldSkipProxy(bean, beanName)) {

			logProxy("Should skip proxy", bean, beanName);

			return bean;
		}

		if (!isEligableProxyBean(bean, beanName)) {

			logProxy("Object bean is not an eligable bean for creating proxy object", bean, beanName);

			return bean;
		}

		if (!canApply(bean)) {

			return bean;
		}

		logProxy("Prepare to create cacheable proxy object", bean, beanName);

		return createProxy(bean.getClass(), beanName, new SingletonTargetSource(bean));
	}

	private void logProxy(String message, Object bean, String beanName) {

		log.info("{}, bean name '{}', bean class '{}'", message, beanName, bean);
	}

	protected Object createProxy(Class<?> beanClass, String beanName, TargetSource targetSource) {

		ProxyFactory proxyFactory = new ProxyFactory();

		proxyFactory.copyFrom(this);

		if (!shouldProxyTargetClass(beanClass, beanName)) {

			Class<?>[] targetInterfaces = ClassUtils.getAllInterfacesForClass(beanClass, this.classloader);

			for (Class<?> targetInterface : targetInterfaces) {

				proxyFactory.addInterface(targetInterface);
			}
		}

		proxyFactory.addAdvice(advisor.getAdvice());

		proxyFactory.setTargetSource(targetSource);

		return proxyFactory.getProxy(this.classloader);
	}

	protected boolean ignoreProxyBeanName(String beanName) {

		if (Tools.isBlank(ignoreBeanNames)) {

			return false;
		}

		for (String ignored : ignoreBeanNames) {

			if (!Tools.isBlank(ignored)) {

				continue;
			}

			if (ignored.equals(beanName)) {

				return true;
			}
		}

		return false;
	}

	protected boolean shouldSkipProxy(Object bean, String beanName) {

		return false;
	}

	protected boolean isInfrastructureClass(Class<?> beanClass, String beanName) {

		boolean result = false;

		for (Class<?> infrastructure : INFRASTRUCTURE_CLASSES) {

			if (infrastructure.isAssignableFrom(beanClass)) {

				result = true;

				break;
			}
		}

		return result;
	}

	protected boolean shouldProxyTargetClass(Class<?> beanClass, String beanName) {

		if (isProxyTargetClass()) {

			return true;
		}

		if (beanFactory instanceof ConfigurableListableBeanFactory) {

			return AutoProxyUtils.shouldProxyTargetClass((ConfigurableListableBeanFactory) beanFactory, beanName);
		}

		return false;
	}

	protected void customizeProxyFactory(ProxyFactory proxyFactory) {

	}

	protected boolean isEligableProxyBean(Object bean, String beanName) {

		return true;
	}

	protected boolean canApply(Object target) {

		return AopUtils.canApply(getAdvisor().getPointcut(), target.getClass());
	}

	@Override
	public String toString() {

		ToString builder = new ToString(this);
		builder.append("beanFactory", beanFactory);
		builder.append("classloader", classloader);
		builder.append("frozen", isFrozen());
		builder.append("proxyTargetClass", isProxyTargetClass());
		builder.append("exposeProxy", isExposeProxy());
		builder.append("opaque", isOpaque());
		builder.append("optimize", isOptimize());
		builder.append("advisor", advisor);
		builder.append("ignoreBeanNames", ignoreBeanNames);
		return builder.toString();
	}
}
