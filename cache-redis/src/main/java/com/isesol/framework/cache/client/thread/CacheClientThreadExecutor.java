package com.isesol.framework.cache.client.thread;

import org.springframework.beans.factory.*;

import java.util.concurrent.*;

public interface CacheClientThreadExecutor extends Executor, DisposableBean {

	void start();

	@Override
	void destroy();
}
