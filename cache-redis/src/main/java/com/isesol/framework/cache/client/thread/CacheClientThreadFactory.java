package com.isesol.framework.cache.client.thread;

import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

public class CacheClientThreadFactory implements ThreadFactory {

	/**
	 * Thread amount counter
	 */
	private static final AtomicInteger GLOBAL_INCR = new AtomicInteger(0);

	private static final String NAME = "cache-";

	private final String prefix;

	private AtomicInteger inner = new AtomicInteger(0);

	private boolean daemon = false;

	public CacheClientThreadFactory(String name, boolean daemon) {

		this.prefix = name;
		this.daemon = daemon;
	}

	@Override
	public Thread newThread(Runnable r) {

		Thread thread = new Thread(r);
		thread.setName(createThreadName());
		thread.setDaemon(daemon);
		return thread;
	}

	protected String createThreadName() {

		return NAME + GLOBAL_INCR.incrementAndGet() + "-" + prefix + "-" + inner.incrementAndGet();
	}
}
