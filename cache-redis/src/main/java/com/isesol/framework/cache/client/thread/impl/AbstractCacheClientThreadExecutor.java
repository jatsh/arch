package com.isesol.framework.cache.client.thread.impl;

import com.isesol.framework.cache.client.thread.*;

import java.util.concurrent.atomic.*;

public abstract class AbstractCacheClientThreadExecutor implements CacheClientThreadExecutor {

	private final AtomicBoolean startMonitor;

	private final AtomicBoolean shutdownMonitor;

	public AbstractCacheClientThreadExecutor() {

		this.startMonitor = new AtomicBoolean(false);
		this.shutdownMonitor = new AtomicBoolean(false);
	}

	@Override
	public final void start() {

		if (startMonitor.compareAndSet(false, true)) {
			doStart();
		}
	}

	@Override
	public void execute(Runnable command) {

	}

	@Override
	public final void destroy() {

		if (shutdownMonitor.compareAndSet(false, true)) {
			doDestory();
		}
	}

	protected void doStart() {

	}

	protected void doDestory() {

	}
}
