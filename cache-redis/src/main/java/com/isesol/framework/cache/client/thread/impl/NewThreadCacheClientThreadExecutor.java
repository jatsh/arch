package com.isesol.framework.cache.client.thread.impl;

import com.isesol.framework.cache.client.thread.*;

import java.util.concurrent.*;

public class NewThreadCacheClientThreadExecutor extends AbstractCacheClientThreadExecutor {

	private ThreadFactory factory = new CacheClientThreadFactory("thread", false);

	@Override
	public void execute(Runnable command) {

		factory.newThread(command).start();
	}
}
