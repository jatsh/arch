package com.isesol.framework.cache.client.thread.impl;

import com.isesol.framework.cache.client.thread.*;
import com.isesol.framework.cache.client.util.EclipseTools.*;

import java.util.concurrent.*;

public class PooledCacheClientThreadExecutor extends AbstractCacheClientThreadExecutor {

	private final ExecutorService executor;

	private final String name;

	private final int threadCount;

	public PooledCacheClientThreadExecutor(String name, int threadCount) {

		this.name = name;
		this.threadCount = threadCount;
		this.executor = Executors.newFixedThreadPool(threadCount, new CacheClientThreadFactory(name, false));
	}

	@Override
	public void execute(Runnable command) {

		executor.execute(command);
	}

	@Override
	protected void doDestory() {

		if (executor != null) {
			executor.shutdown();
		}
	}

	@Override
	public String toString() {

		ToString builder = new ToString(this);
		builder.append("name", name);
		builder.append("threadCount", threadCount);
		builder.append("executor", executor);
		return builder.toString();
	}
}
