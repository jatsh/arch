package com.isesol.framework.cache.client.thread.impl;

import com.isesol.framework.cache.client.thread.*;
import com.isesol.framework.cache.client.util.EclipseTools.*;

import java.util.concurrent.*;

public class ScheduleCacheClientThreadExecutor extends AbstractCacheClientThreadExecutor {

	private final String name;

	private final int seconds;

	private final int threadCount;

	private final Runnable command;

	private final ScheduledExecutorService schedule;

	public ScheduleCacheClientThreadExecutor(String name, int threadCount, int seconds, Runnable command) {

		this.name = name;
		this.threadCount = threadCount;
		this.seconds = seconds;
		this.command = command;
		this.schedule = Executors.newScheduledThreadPool(threadCount, new CacheClientThreadFactory(name, false));
	}

	@Override
	protected void doStart() {

		schedule.scheduleAtFixedRate(command, 0, seconds, TimeUnit.SECONDS);
	}

	@Override
	protected void doDestory() {

		if (schedule != null) {

			schedule.shutdown();
		}
	}

	@Override
	public String toString() {

		ToString builder = new ToString(this);
		builder.append("name", name);
		builder.append("threadCount", threadCount);
		builder.append("seconds", seconds);
		builder.append("command", command);
		builder.append("schedule", schedule);
		return builder.toString();
	}
}
