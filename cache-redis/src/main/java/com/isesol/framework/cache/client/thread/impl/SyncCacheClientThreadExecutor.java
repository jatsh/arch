package com.isesol.framework.cache.client.thread.impl;

public class SyncCacheClientThreadExecutor extends AbstractCacheClientThreadExecutor {

	@Override
	public void execute(Runnable command) {

		command.run();
	}
}
