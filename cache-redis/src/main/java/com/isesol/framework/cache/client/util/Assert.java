package com.isesol.framework.cache.client.util;

import java.util.*;

public final class Assert {

	private static final String IS_REQUIRED = "is required";

	private Assert() {

	}

	public static void notLesser(int num, int min, String name) {

		if (num < min) {
			throw new IllegalArgumentException("'" + name + "' value must be greater than or equals " + min);
		}
	}

	public static void notEmpty(String value, String name) {

		if (Tools.isBlank(value)) {
			throw new IllegalArgumentException(
					"'" + name + "' " + IS_REQUIRED +
					", the String must has text, it must not be null, empty, or blank");
		}
	}

	public static void notEmpty(Collection<?> value, String name) {

		if (Tools.isBlank(value)) {
			throw new IllegalArgumentException(
					"'" + name + "' " + IS_REQUIRED +
					", the Collection must has element, it must contain at least 1 element");
		}
	}

	public static void notEmpty(Map<?, ?> value, String name) {

		if (Tools.isBlank(value)) {
			throw new IllegalArgumentException(
					"'" + name + "' " + IS_REQUIRED + ", the Map must has entry, it must contain at least 1 entry");
		}
	}

	public static void notEmpty(Object[] value, String name) {

		if (Tools.isBlank(value)) {
			throw new IllegalArgumentException(
					"'" + name + "' " + IS_REQUIRED +
					", the array must has element, it must contain at least 1 element");
		}
	}

	public static void notNull(Object value, String name) {

		if (value == null) {
			throw new IllegalArgumentException("'" + name + "' " + IS_REQUIRED + ", the object must not be null");
		}
	}
}
