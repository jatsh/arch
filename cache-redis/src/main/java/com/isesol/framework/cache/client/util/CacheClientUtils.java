package com.isesol.framework.cache.client.util;

import com.isesol.framework.cache.client.*;
import org.apache.logging.log4j.*;

import java.beans.*;
import java.lang.reflect.*;
import java.util.*;

public final class CacheClientUtils {

	static Logger LOG = LogManager.getLogger(CacheClientUtils.class.getName());

	private CacheClientUtils() {

	}

	public static String getName(Class<?> clazz) {

		if (clazz == null) {
			return null;
		}
		return clazz.getSimpleName();
	}

	public static String getName(Method method) {

		if (method == null) {
			return null;
		}
		return getSimpleSignature(method).toString();
	}

	public static String getName(Field field) {

		if (field == null) {
			return null;
		}
		return field.getName();
	}

	public static boolean hasNull(Object... objects) {

		if (objects == null || objects.length == 0) {
			return true;
		}
		for (Object object : objects) {
			if (object == null) {
				return true;
			}
		}
		return false;
	}

	public static boolean isCacheAbleClass(Class<?> clazz) {

		Assert.notNull(clazz, "clazz");

		if (clazz.getPackage() == null) {
			return false;
		}

		String packageName = clazz.getPackage().getName();
		for (String prefix : Constant.PACKAGE_PREFIXES) {

			if (packageName.startsWith(prefix)) {

				return true;
			}
		}

		return false;
	}

	public static boolean isCacheInternalClass(Class<?> clazz) {

		Assert.notNull(clazz, "clazz");

		// int.class, byte.class, and etc.
		if (clazz.getPackage() == null) {
			return false;
		}

		return clazz.getPackage().getName().startsWith(Constant.CACHE_PACKAGE_PREFIX) && !clazz.getName().endsWith(
				"Test") && !clazz.getName().startsWith("Test");
	}

	public static String toString(byte[] bys) {

		if (bys == null) {
			return null;
		}
		return new String(bys, Tools.DEFAULT_CHARSET);
	}

	public static byte[] toBytes(CharSequence str) {

		if (str == null) {
			return null;
		}
		return str.toString().getBytes(Tools.DEFAULT_CHARSET);
	}

	public static StringBuilder getSimpleSignature(Method method) {

		return appendSimpleSignature(method, null);
	}

	public static StringBuilder appendSimpleSignature(Method method, final StringBuilder build) {

		StringBuilder builder = build;

		if (builder == null) {
			builder = new StringBuilder();
		}

		if (method == null) {
			return builder.append("null");
		}
		builder.append(method.getDeclaringClass().getSimpleName()).append('#');
		builder.append(method.getReturnType().getSimpleName()).append(' ');
		builder.append(method.getName()).append('(');
		appendSimpleClassNames(method.getParameterTypes(), null, builder);
		builder.append(')');
		appendSimpleClassNames(method.getExceptionTypes(), " throws ", builder);
		return builder;
	}

	public static StringBuilder appendSimpleClassNames(Class<?>[] clazzs, String prefix, StringBuilder builder) {

		if (clazzs == null || clazzs.length == 0) {
			return builder;
		}
		if (prefix != null) {
			builder.append(prefix);
		}
		for (int i = 0; i < clazzs.length; i++) {
			if (i > 0) {
				builder.append(", ");
			}
			builder.append(clazzs[i].getSimpleName());
		}
		return builder;
	}

	public static boolean parseBoolean(String str, boolean defaultValue) {

		if (Tools.isBlank(str)) {
			return defaultValue;
		}

		return Boolean.valueOf(str);
	}

	public static int parseInt(String str, int defaultValue) {

		if (Tools.isBlank(str)) {

			return defaultValue;
		}

		try {

			return Integer.parseInt(str);
		} catch (Exception e) {

			return defaultValue;
		}
	}

	public static StringBuilder repeatSpace(int num, StringBuilder builder) {

		return repeat(num, "  ", builder);
	}

	public static StringBuilder repeat(int num, String str, final StringBuilder build) {

		StringBuilder builder = build;

		if (builder == null) {

			builder = new StringBuilder();
		}

		for (int i = 0; i < num; i++) {

			builder.append(str);
		}

		return builder;
	}

	public static Map<String, Object> getObjectProperties(Object object) {

		Map<String, Object> descriptors = new LinkedHashMap<String, Object>();

		if (object == null) {

			return descriptors;
		}

		try {

			BeanInfo info = Introspector.getBeanInfo(object.getClass(), Object.class);

			PropertyDescriptor[] properties = info.getPropertyDescriptors();

			for (PropertyDescriptor property : properties) {

				Object value = "<CannotRead>";

				Method read = property.getReadMethod();

				if (read != null) {

					value = read.invoke(object);
				}

				descriptors.put(property.getName(), value);
			}

		} catch (Exception e) {

			LOG.warn(
					"[getObjectProperties] cause exception, object type '{}', object = [{}]", object.getClass(),
					object,
					e);
		}

		return descriptors;
	}
}