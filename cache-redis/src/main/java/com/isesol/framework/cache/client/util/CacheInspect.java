package com.isesol.framework.cache.client.util;

import com.alibaba.fastjson.*;
import com.alibaba.fastjson.serializer.*;
import org.aopalliance.intercept.*;

import java.lang.reflect.*;
import java.util.*;

public class CacheInspect {

	private static final int MAX_CACHE_ITEMS = 64;

	private static final String DEFAULT_LINE_SEPERATOR = "\n";

	private static final String CACHE_EXECUTOR_TX_METHOD_NAME = "executeInTransaction";

	private static final ConcurrentLRUCache<Method, String> METHOD_SIGNATURE_CACHE =
			new ConcurrentLRUCache<Method, String>(
					MAX_CACHE_ITEMS);

	private final String lineSeperator;

	private final int maxLength;

	private final Method method;

	private final Object target;

	private final Object[] arguments;

	private String details;

	public CacheInspect(MethodInvocation invocation) {

		this(invocation, Tools.DEFAULT_MAX_LENGTH, DEFAULT_LINE_SEPERATOR);
	}

	public CacheInspect(MethodInvocation invocation, String lineSeperator) {

		this(invocation, Tools.DEFAULT_MAX_LENGTH, lineSeperator);
	}

	public CacheInspect(MethodInvocation invocation, int maxLength, String lineSeperator) {

		this.maxLength = maxLength < 1 ? Tools.DEFAULT_MAX_LENGTH : maxLength;
		this.lineSeperator = lineSeperator;
		this.method = invocation.getMethod();
		this.target = invocation.getThis();
		this.arguments = invocation.getArguments();
	}

	public String getLineSeperator() {

		return lineSeperator;
	}

	public int getMaxLength() {

		return maxLength;
	}

	public String getInvokeMethodSignature() {

		return inspectInvokeMethodSimpleArgs();
	}

	public String getInvokeDetails() {

		if (details == null) {
			StringBuilder builder = new StringBuilder();
			// inspect invoke class name
			builder.append('[').append(target.getClass().getSimpleName()).append("] ");
			// inspect method signature
			builder.append(inspectInvokeMethodSimpleArgs());

			if (!CACHE_EXECUTOR_TX_METHOD_NAME.equals(method.getName())) {
				builder.append(", ");
				// inspect method invoke arguments
				inspectArgsValue(builder);
			}

			details = builder.toString();
		}

		return details;
	}

	private String inspectInvokeMethodSimpleArgs() {

		String signature = METHOD_SIGNATURE_CACHE.get(method);

		if (signature == null) {
			StringBuilder inner = new StringBuilder();
			// inspect method name
			inner.append(method.getName()).append('(');
			inspectMethodArgumentNames(inner);
			inner.append(')');
			signature = inner.toString();
			METHOD_SIGNATURE_CACHE.put(method, signature);
		}

		return signature;
	}

	private StringBuilder inspectMethodArgumentNames(StringBuilder builder) {

		Class<?>[] argTypes = method.getParameterTypes();

		if (Tools.isBlank(argTypes)) {
			return builder;
		}

		for (int i = 0; i < argTypes.length; i++) {
			if (i > 0) {
				builder.append(", ");
			}
			builder.append(MethodArgumentNames.getName(target, method, i));
		}

		return builder;
	}

	private StringBuilder inspectArgsValue(final StringBuilder build) {

		StringBuilder builder = build;

		if (builder == null) {
			builder = new StringBuilder();
		}

		builder.append("parameters: ");

		for (int i = 0; i < arguments.length; i++) {
			if (i > 0) {
				builder.append(", ");
			}
			builder.append(MethodArgumentNames.getName(target, method, i)).append(" = ");
			if (arguments[i] == null) {
				builder.append('[').append("null").append(']');
			} else if (arguments[i] instanceof String) {
				builder.append('[').append(Tools.truncate((String) arguments[i], getMaxLength())).append(']');
			} else if (arguments[i] instanceof Date) {
				builder.append('[').append(Tools.formatWithMillis((Date) arguments[i])).append(']');
			} else {
				appendObject(builder, arguments[i]);
			}
		}

		return builder;
	}

	private StringBuilder appendObject(final StringBuilder build, Object object) {

		StringBuilder builder = build;

		if (builder == null) {
			builder = new StringBuilder();
		}

		try {
			String value = JSON.toJSONString(convertUnmodifiable(object), SerializerFeature.WriteClassName);
			if (value == null) {
				return null;
			}
			if (value.indexOf("\"password\"") > -1) {
				value = value.replaceAll("\"password\":\"[^\"]+\"", "\"password\":\"******\"");
			}
			return Tools.truncate(value, getMaxLength(), builder);
		} catch (Exception e) {
			return builder;
		}
	}

	StringBuilder inspectClasses(Class<?>[] classes, boolean isThrows, StringBuilder builder) {

		if (Tools.isBlank(classes)) {
			return builder;
		}

		if (isThrows) {
			builder.append(" throws ");
		}

		for (int i = 0; i < classes.length; i++) {
			if (i > 0) {
				builder.append(", ");
			}
			builder.append(classes[i].getSimpleName());
			if (!isThrows) {
				String name = MethodArgumentNames.getName(target, method, i);
				if (!Tools.isBlank(name)) {
					builder.append(' ').append(name);
				}
			}
		}

		return builder;
	}

	private Object convertUnmodifiable(Object object) {

		if (object == null) {
			return object;
		}

		String className = object.getClass().getName();

		if ("java.util.Collections$UnmodifiableMap".equals(className)) {
			return new HashMap<Object, Object>((Map<?, ?>) object);
		}

		if ("java.util.Collections$UnmodifiableList".equals(className)) {
			return new ArrayList<Object>((List<?>) object);
		}

		if ("java.util.Collections$UnmodifiableSet".equals(className)) {
			return new HashSet<Object>((Set<?>) object);
		}

		return object;
	}
}
