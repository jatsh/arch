package com.isesol.framework.cache.client.util;

import java.math.*;
import java.util.*;
import java.util.concurrent.atomic.*;

/**
 * <p>
 * 兼容类型转换器，对于不同类型但类型可兼容的数据进行转换。可兼容的类型根据数据类型不同，主要分为：
 * <p>
 * <ul>
 * <li><b>向上转型：</b> 将所有的子类型的数据向上转型为该类型的父类型数据</li>
 * <li><b>数值类型：</b> 将 {@link Number} 子类型的数据（{@link Byte}, {@link Short},
 * {@link Integer}, {@link Long}, {@link Float}, {@link Double},
 * {@link BigInteger}, {@link BigDecimal}, {@link AtomicInteger},
 * {@link AtomicLong}）转换为该类型的其他数据，或者基本数值类型（<code>byte</code>, <code>short</code>, <code>int</code>,
 * <code>long</code>, <code>float</code>,
 * <code>double</code>）</li>
 * <li><b>布尔类型：</b> 将 {@link Boolean} 类型的数据转换基本数据类型（<code>boolean</code>）</li>
 * <li><b>字符类型：</b> 将 {@link Character} 类型的数据转换基本数据类型（<code>char</code>）</li>
 * <li><b>字符串类型：</b> 将长度为 1 的字符串数据（{@link String}）转换为基本数据类型（<code>char</code>
 * ），或者包装类型（{@link Character}）</li>
 * </ul>
 * <p>
 * <p>
 * 对于原始数据类型，所应用的转换器类型为：
 * <ul>
 * <li>{@link Byte}, {@link Short}, {@link Integer}, {@link Long}, {@link Float}, {@link Double}, {@link BigInteger},
 * {@link BigDecimal} 使用数值类型转换器</li>
 * <li>{@link Boolean} 使用布尔类型转换器</li>
 * <li>{@link Character} 使用字符类型转换器</li>
 * <li>{@link String} 使用字符串类型转换器</li>
 * </ul>
 * </p>
 * <p>
 * <p>
 * 转换器工作流程依次为：
 * <ol>
 * <li>如果目标类型为 <code>null</code> 值，或者为 <code>void</code> 类型时，以 <code>null</code>
 * 值作为转换结果</li>
 * <li>如果转换数据对象为 <code>null</code> 值时，若目标类型为基本类型时，抛出
 * {@link CompatibleTypeConvertException}； 若目标类型为对象类型则以 <code>null</code>
 * 值作为转换结果</li>
 * <li>如果转换数据对象为目标类型时，则该数据对象直接作为转换结果</li>
 * <li>如果转换数据对象类型尚未在兼容类型转换器中注册过时，若目标类型为基本类型时，抛出
 * {@link CompatibleTypeConvertException}； 若目标类型为对象类型则以 <code>null</code>
 * 值作为转换结果</li>
 * <li>根据不同类型的转换器进行数据类型转换操作，无法作转换时，若目标类型为基本类型时，抛出
 * {@link CompatibleTypeConvertException}； 若目标类型为对象类型则以 <code>null</code>
 * 值作为转换结果</li>
 * </ol>
 * </p>
 */
public abstract class CompatibleTypeConverter {

	/**
	 * 数值类型转换器
	 */
	private static final CompatibleTypeConverter NUMBER = new NumberTypeConverter();

	/**
	 * 布尔类型转换器
	 */
	private static final CompatibleTypeConverter BOOLEAN = new CompatibleTypeConverter(Boolean.class) {
		@Override
		protected Object doConvert(Object returnResult, Class<?> returnType) {

			if (returnType == boolean.class || returnType == Boolean.class) {
				return returnResult;
			}
			if (returnType == AtomicBoolean.class) {
				return new AtomicBoolean((Boolean) returnResult);
			}
			illegalPrimitiveTarget(returnType, "BOOLEAN");
			return null;
		}
	};

	/**
	 * 字符类型转换器
	 */
	private static final CompatibleTypeConverter CHARACTER = new CompatibleTypeConverter(Character.class) {
		@Override
		protected Object doConvert(Object returnResult, Class<?> returnType) {

			if (!(returnResult instanceof Character)) {
				return null;
			}
			if (returnType == char.class || returnType == Character.class) {
				return returnResult;
			}
			illegalPrimitiveTarget(returnType, "CHARACTER");
			return null;
		}
	};

	/**
	 * 字符串类型转换器
	 */
	private static final CompatibleTypeConverter STRING = new CompatibleTypeConverter(String.class) {
		@Override
		protected Object doConvert(Object returnResult, Class<?> returnType) {

			String str = (String) returnResult;
			if ((returnType == char.class || returnType == Character.class) && str.length() == 1) {
				return str.charAt(0);
			}
			illegalPrimitiveTarget(returnType, "STRING");
			return null;
		}
	};

	/**
	 * 兼容类型转换器容器
	 */
	private static final Map<Class<?>, CompatibleTypeConverter> COMPATIBLE =
			new HashMap<Class<?>, CompatibleTypeConverter>();

	static {

		// 根据转换数据的类型注册数据类型转换器
		COMPATIBLE.put(Byte.class, NUMBER);
		COMPATIBLE.put(Short.class, NUMBER);
		COMPATIBLE.put(Integer.class, NUMBER);
		COMPATIBLE.put(Long.class, NUMBER);
		COMPATIBLE.put(Float.class, NUMBER);
		COMPATIBLE.put(Double.class, NUMBER);
		COMPATIBLE.put(BigInteger.class, NUMBER);
		COMPATIBLE.put(BigDecimal.class, NUMBER);
		COMPATIBLE.put(Boolean.class, BOOLEAN);
		COMPATIBLE.put(Character.class, CHARACTER);
		COMPATIBLE.put(String.class, STRING);
	}

	/**
	 * 转换器应用的数据类型
	 */
	private final Class<?> type;

	private CompatibleTypeConverter(Class<?> type) {

		this.type = type;
	}

	public static Object convertCompatible(Object returnResult, Class<?> returnType) {

		if (returnType == null || returnType == void.class) {
			return null;
		}

		if (returnResult == null) {
			illegalPrimitiveConvert(returnType, "Data value is null");
			return null;
		}

		if (returnType.isInstance(returnResult)) {
			return returnResult;
		}

		CompatibleTypeConverter convert = COMPATIBLE.get(returnResult.getClass());

		if (convert == null) {
			illegalPrimitiveConvert(
					returnType, "Data value type '" + returnResult.getClass().getName() +
					            "' does not register in compatible type converter");
			return null;
		}

		return convert.convert(returnResult, returnType);
	}

	private static void illegalPrimitiveConvert(Class<?> returnType, String message) {

		if (returnType != null && returnType.isPrimitive()) {
			throw new CompatibleTypeConvertException(
					message + ", target type is " + "'" + returnType.getName() + ".class' that is primitive type");
		}
	}

	private static void illegalPrimitiveTarget(Class<?> returnType, String converterType) {

		illegalPrimitiveConvert(
				returnType,
				converterType + " converter cannot convert data value " + "to type '" + returnType.getName() +
				".class' type");
	}

	private Object convert(Object returnResult, Class<?> returnType) {

		if (!type.isInstance(returnResult)) {
			illegalPrimitiveConvert(returnType, "Data value is not a excepted type '" + type.getName() + "'");
			return null;
		}

		return doConvert(returnResult, returnType);
	}

	protected abstract Object doConvert(Object returnResult, Class<?> returnType);

	private interface NumberConverter {
		Object convert(Number result);
	}

	private static final class NumberTypeConverter extends CompatibleTypeConverter {

		private static final NumberConverter BYTE = new NumberConverter() {
			@Override
			public Object convert(Number result) {

				return (result instanceof Byte) ? result : result.byteValue();
			}
		};

		private static final NumberConverter SHORT = new NumberConverter() {
			@Override
			public Object convert(Number result) {

				return (result instanceof Short) ? result : result.shortValue();
			}
		};

		private static final NumberConverter INT = new NumberConverter() {
			@Override
			public Object convert(Number result) {

				return (result instanceof Integer) ? result : result.intValue();
			}
		};

		private static final NumberConverter LONG = new NumberConverter() {
			@Override
			public Object convert(Number result) {

				return (result instanceof Long) ? result : result.longValue();
			}
		};

		private static final NumberConverter FLOAT = new NumberConverter() {
			@Override
			public Object convert(Number result) {

				return (result instanceof Float) ? result : result.floatValue();
			}
		};

		private static final NumberConverter DOUBLE = new NumberConverter() {
			@Override
			public Object convert(Number result) {

				return (result instanceof Double) ? result : result.doubleValue();
			}
		};

		private static final NumberConverter B_INT = new NumberConverter() {
			@Override
			public Object convert(Number result) {

				return (result instanceof BigInteger) ? result : new BigInteger(String.valueOf(result));
			}
		};

		private static final NumberConverter B_DEC = new NumberConverter() {
			@Override
			public Object convert(Number result) {

				return (result instanceof BigDecimal) ? result : new BigDecimal(String.valueOf(result));
			}
		};

		private static final NumberConverter A_INT = new NumberConverter() {
			@Override
			public Object convert(Number result) {

				return (result instanceof AtomicInteger) ? result : new AtomicInteger(result.intValue());
			}
		};

		private static final NumberConverter A_LONG = new NumberConverter() {
			@Override
			public Object convert(Number result) {

				return (result instanceof AtomicLong) ? result : new AtomicLong(result.intValue());
			}
		};

		private static final Map<Class<?>, NumberConverter> NUMBER_CONVERTERS =
				new HashMap<Class<?>, NumberConverter>();

		static {
			NUMBER_CONVERTERS.put(byte.class, BYTE);
			NUMBER_CONVERTERS.put(short.class, SHORT);
			NUMBER_CONVERTERS.put(int.class, INT);
			NUMBER_CONVERTERS.put(long.class, LONG);
			NUMBER_CONVERTERS.put(float.class, FLOAT);
			NUMBER_CONVERTERS.put(double.class, DOUBLE);

			NUMBER_CONVERTERS.put(Byte.class, BYTE);
			NUMBER_CONVERTERS.put(Short.class, SHORT);
			NUMBER_CONVERTERS.put(Integer.class, INT);
			NUMBER_CONVERTERS.put(Long.class, LONG);
			NUMBER_CONVERTERS.put(Float.class, FLOAT);
			NUMBER_CONVERTERS.put(Double.class, DOUBLE);

			NUMBER_CONVERTERS.put(BigInteger.class, B_INT);
			NUMBER_CONVERTERS.put(BigDecimal.class, B_DEC);
			NUMBER_CONVERTERS.put(AtomicInteger.class, A_INT);
			NUMBER_CONVERTERS.put(AtomicLong.class, A_LONG);

		}

		public NumberTypeConverter() {

			super(Number.class);
		}

		@Override
		protected Object doConvert(Object returnResult, Class<?> returnType) {

			Number result = (Number) returnResult;
			NumberConverter converter = NUMBER_CONVERTERS.get(returnType);
			if (converter != null) {
				return converter.convert(result);
			}
			illegalPrimitiveTarget(returnType, "NUMBER");
			return null;
		}
	}

	public static final class CompatibleTypeConvertException extends RuntimeException {

		public CompatibleTypeConvertException(String message, Throwable cause) {

			super(message, cause);
		}

		public CompatibleTypeConvertException(String message) {

			super(message);
		}
	}
}
