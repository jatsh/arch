package com.isesol.framework.cache.client.util;

import java.util.*;
import java.util.concurrent.locks.*;

public class ConcurrentLRUCache<K, V> {

	private final int max;

	private final Map<K, V> delegate = new LinkedHashMap<K, V>() {

		@Override
		protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {

			return size() > max;
		}
	};

	private final Lock readLock;

	private final Lock writeLock;

	public ConcurrentLRUCache(int max) {

		this.max = max;
		ReadWriteLock rw = new ReentrantReadWriteLock();
		this.readLock = rw.readLock();
		this.writeLock = rw.writeLock();
	}

	public V get(Object key) {

		readLock.lock();
		try {
			return delegate.get(key);
		} finally {
			readLock.unlock();
		}
	}

	public V put(K key, V value) {

		writeLock.lock();
		try {
			return delegate.put(key, value);
		} finally {
			writeLock.unlock();
		}
	}

	public int length() {

		readLock.lock();
		try {
			return delegate.size();
		} finally {
			readLock.unlock();
		}
	}
}
