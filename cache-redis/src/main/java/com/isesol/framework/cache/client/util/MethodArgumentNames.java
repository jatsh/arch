package com.isesol.framework.cache.client.util;

import org.springframework.core.*;
import org.springframework.util.*;

import java.lang.reflect.*;

/**
 * 获取方法参数名称。需要获取方法参数名称的类在编译时，编译选项需要增加 <code>-g</code> 参数开启调试信息，否则无法获取方法的参数名称。
 */
public final class MethodArgumentNames {

	/**
	 * 参数名称查找器
	 */
	private static final ParameterNameDiscoverer PARAMETER_DISCOVERER = new
			LocalVariableTableParameterNameDiscoverer();

	private MethodArgumentNames() {

	}

	/**
	 * 获取方法指定位置参数的参数名称
	 *
	 * @param method 方法对象
	 * @param index 参数位置，第一个参数值为 0
	 *
	 * @return 方法指定位置参数的参数名称。如果无法获取方法参数名称时将返回 <code>null</code> 值
	 *
	 * @throws IllegalArgumentException <code>method</code> 参数为 <code>null</code> 值， 或者
	 * <code>index</</code> 参数值小于 0
	 */
	public static String getName(Method method, int index) {

		return getName(null, method, index);
	}

	/**
	 * 获取方法指定的名称，通过目标对象获取最为匹配对象类该方法的参数名称。适用于在有继承体系的类或接口中，
	 * 方法被重写/重新声明的情况（重写后可能修改过方法的参数名称）。
	 *
	 * @param target 目标对象。目标对象为 <code>null</code> 值时，该方法将退化成
	 * {@link #getName(Method, int)} 方法
	 * @param fmethod 方法对象
	 * @param index 方法指定位置参数的参数名称。如果无法获取方法参数名称时将返回 <code>null</code> 值
	 *
	 * @return 方法指定位置参数的参数名称。如果无法获取方法参数名称时将返回 <code>null</code> 值
	 *
	 * @throws IllegalArgumentException <code>method</code> 参数为 <code>null</code> 值， 或者
	 * <code>index</</code> 参数值小于 0
	 */
	public static String getName(Object target, final Method fmethod, int index) {

		Assert.notNull(fmethod, "method");
		Assert.notLesser(index, 0, "index");

		Method method = fmethod;

		if (target != null) {
			method = ClassUtils.getMostSpecificMethod(method, target.getClass());
		}

		return nullSafetyGetName(method, index);
	}

	/**
	 * 空指针及索引点安全地获取方法的参数名称
	 *
	 * @param method 方法对象
	 * @param index 参数名称位于方法参数列表的位置
	 *
	 * @return 方法指定位置的参数名称。如果无法获取方法的参数名称，或者位置索引不在有效范围之内时， 将返回 <code>null</code>
	 * 值
	 */
	private static String nullSafetyGetName(Method method, int index) {

		String[] names = getNames(method);

		if (Tools.isBlank(names)) {
			return null;
		}

		if (index > names.length) {
			return null;
		}

		return names[index];
	}

	/**
	 * 获取方法所有的参数名称数组
	 *
	 * @param method 方法对象
	 *
	 * @return 方法参数名称集合。如果无法获取方法的参数名称时，将返回 <code>null</code> 值
	 */
	private static String[] getNames(Method method) {

		if (method == null) {
			return null;
		}

		return PARAMETER_DISCOVERER.getParameterNames(method);
	}
}
