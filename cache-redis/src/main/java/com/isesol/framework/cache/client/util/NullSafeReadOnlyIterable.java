package com.isesol.framework.cache.client.util;

import java.util.*;

public class NullSafeReadOnlyIterable<E> implements Iterable<E> {

	private static final Iterator<Object> EMPTY_ITERATOR = new Iterator<Object>() {

		@Override
		public boolean hasNext() {

			return false;
		}

		@Override
		public Object next() {

			throw new NoSuchElementException();
		}

		@Override
		public void remove() {

		}
	};

	private final Iterator<E> iterator;

	public NullSafeReadOnlyIterable(Iterable<E> iterable) {

		this.iterator = wrapIterator(iterable);
	}

	@SuppressWarnings("unchecked")
	public static <E> Iterator<E> wrapIterator(Iterable<E> iterable) {

		if (iterable == null) {
			return (Iterator<E>) EMPTY_ITERATOR;
		}
		return new ReadOnlyIterator<E>(iterable);
	}

	@Override
	public Iterator<E> iterator() {

		return iterator;
	}

	private static class ReadOnlyIterator<E> implements Iterator<E> {

		private Iterator<E> iterator;

		public ReadOnlyIterator(Iterable<E> iterable) {

			this.iterator = iterable.iterator();
		}

		@Override
		public boolean hasNext() {

			return iterator.hasNext();
		}

		@Override
		public E next() {

			return iterator.next();
		}

		@Override
		public void remove() {

		}
	}
}
