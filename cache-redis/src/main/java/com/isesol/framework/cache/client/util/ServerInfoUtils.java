package com.isesol.framework.cache.client.util;

import java.lang.management.*;
import java.net.*;
import java.security.*;
import java.util.*;
import java.util.concurrent.atomic.*;

import static com.isesol.framework.cache.client.util.Tools.toHex;
import static com.isesol.framework.cache.client.util.Tools.toHexChar;
import static com.isesol.framework.cache.client.util.Tools.toSeconds;

/**
 * 服务器信息工具类
 */
public final class ServerInfoUtils {

	private static ServerInfo serverInfo = new ServerInfo();

	private ServerInfoUtils() {

	}

	/**
	 * <p>
	 * 生成用于信息追踪使用的标识符， 标识符由 6 项数据构成，数据结构：<br/>
	 * PID@HOSTNAME-RANDOM(36)@STARTTIME(36)-TICTAC(36)@INCR(Hex)：
	 * <ul>
	 * <li>PID - Java 进程号</li>
	 * <li>HOSTNAME - 主机名</li>
	 * <li>RANDOM - 以 36 进制表示的 0～4096 之间的随机数</li>
	 * <li>STARTTIME - 以 36 进制表示的 JVM 启动时的 Unix 毫秒纪元时间</li>
	 * <li>TICTAC - 以 36 进制表示的 JVM 启动到当前的秒数（值在 0～0xfffffff 之间循环使用）</li>
	 * <li>INCR - 以 16 进制的递增序列（值在 0～0xffffff 之间循环使用）</li>
	 * </ul>
	 * </p>
	 */
	public static String generateIdentifier() {

		return serverInfo.generateIdentifier();
	}

	/**
	 * 获取服务器的主机名
	 *
	 * @return 服务器的主机名，若无法获取时返回 localhost
	 */
	public static String getHostname() {

		return serverInfo.getHostname();
	}

	/**
	 * 获取应用的进程编号
	 *
	 * @return 进程编号，若无法获取时返回 0 值
	 */
	public static int getPid() {

		return serverInfo.getPid();
	}

	/**
	 * 获取以十六进制字符表示的网卡 MAC 地址
	 *
	 * @return 网卡地址，无法获取时返回 null 值
	 */
	public static String getHexMacAddress() {

		return serverInfo.getHexMacAddress();
	}

	/**
	 * 获取应用的用户名
	 *
	 * @return 应用的用户名
	 */
	public static String getUsername() {

		return serverInfo.getUsername();
	}

	/**
	 * 获取应用所在操作第统用户目录
	 *
	 * @return 应用所在操作第统用户目
	 */
	public static String getUserHome() {

		return serverInfo.getUserHome();
	}

	/**
	 * 获取应用的工作目录
	 *
	 * @return 应用的工作目录
	 */
	public static String getUserDir() {

		return serverInfo.getUserDir();
	}

	/**
	 * 获取应用的启动时间
	 *
	 * @return 以 Unix 纪元毫秒值表示的启动时间
	 */
	public static long getStartTime() {

		return serverInfo.getStartTime();
	}

	/**
	 * 获取 JVM 虚拟机编号，该编号由 <进程号>@<主机名> 组成
	 *
	 * @return JVM 虚拟机编号
	 */
	public static String getVmid() {

		return serverInfo.getVmid();
	}

	/**
	 * 获取服务器标识，生成格式如下：<HOSTNAME>:<USERNAME>:<HEX_MAC>
	 *
	 * @return 服务器标识
	 */
	public static String getServerIdentifier() {

		return serverInfo.getServerIdentifier();
	}

	/**
	 * 获取服务器所有信息
	 *
	 * @return 服务器所有信息
	 */
	public static String getAllInfo() {

		return serverInfo.getAllInfo();
	}

	private static class ServerInfo {

		/**
		 * counter
		 */
		private static final AtomicInteger COUNTER = new AtomicInteger();

		private static final int RANDOM_SCOPE = 0xfff;

		private static final int IDENTIFIER_MAX_LENGTH = 13;

		private static final int IDENTIFIER_TICTAC_MASK = 0xfffffff;

		/**
		 * 主机名
		 */
		private final String hostname;

		/**
		 * 进程号
		 */
		private final int pid;

		/**
		 * mac 地址
		 */
		private final String hexMacAddress;

		private final String userDir;

		private final String userHome;

		private final String vmid;

		private final long startTime;

		private final String username;

		private final String serverIdentifier;

		private final String prefix;

		private String allInfo;

		public ServerInfo() {

			this.userDir = System.getProperty("user.dir");
			this.userHome = System.getProperty("user.home");
			this.hexMacAddress = macAddress();
			this.pid = pid();
			this.hostname = hostname();
			this.vmid = vmid();
			this.startTime = startTime();
			this.username = System.getProperty("user.name");
			this.serverIdentifier = hostname + ':' + username + ':' + hexMacAddress;
			this.prefix = vmid + '-' + Integer.toString(
					new SecureRandom().nextInt(RANDOM_SCOPE), Character.MAX_RADIX) +
			              '@' + Long.toString(startTime, Character.MAX_RADIX) + '-';
		}

		public String getHostname() {

			return hostname;
		}

		public int getPid() {

			return pid;
		}

		public String getHexMacAddress() {

			return hexMacAddress;
		}

		public String getUserDir() {

			return userDir;
		}

		public String getUserHome() {

			return userHome;
		}

		public String getVmid() {

			return vmid;
		}

		public long getStartTime() {

			return startTime;
		}

		public String getUsername() {

			return username;
		}

		public String getAllInfo() {

			if (allInfo == null) {
				allInfo = serverInfo();
			}
			return allInfo;
		}

		public String getServerIdentifier() {

			return serverIdentifier;
		}

		public String generateIdentifier() {

			StringBuilder builder = new StringBuilder(prefix.length() + IDENTIFIER_MAX_LENGTH);
			builder.append(prefix);
			builder.append(Integer.toString(getTictac() & IDENTIFIER_TICTAC_MASK, Character.MAX_RADIX));
			builder.append('@');
			int inc = COUNTER.incrementAndGet();
			builder.append(toHexChar(inc, Tools.LOW_HALF_6));
			builder.append(toHexChar(inc, Tools.LOW_HALF_5));
			builder.append(toHexChar(inc, Tools.LOW_HALF_4));
			builder.append(toHexChar(inc, Tools.LOW_HALF_3));
			builder.append(toHexChar(inc, Tools.LOW_HALF_2));
			builder.append(toHexChar(inc));
			return builder.toString();
		}

		private long startTime() {

			return ManagementFactory.getRuntimeMXBean().getStartTime();
		}

		private int pid() {

			String vid = vmid();
			if (vid == null) {
				return 0;
			}
			int at = vid.indexOf('@');
			if (at < 0) {
				return 0;
			}
			try {
				return Integer.parseInt(vid.substring(0, at));
			} catch (Exception e) {
				return 0;
			}
		}

		private String hostname() {

			String vid = vmid();
			if (vid == null) {
				return "localhost";
			}
			int at = vid.indexOf('@');
			if (at < 0) {
				return "localhost";
			}
			try {
				return vid.substring(at + 1);
			} catch (Exception e) {
				return "localhost";
			}
		}

		private String vmid() {

			return ManagementFactory.getRuntimeMXBean().getName();
		}

		private String macAddress() {

			Enumeration<NetworkInterface> en = null;
			try {
				en = NetworkInterface.getNetworkInterfaces();
			} catch (SocketException e) {
			}
			if (en == null) {
				return null;
			}
			while (en.hasMoreElements()) {
				try {
					byte[] bytes = en.nextElement().getHardwareAddress();
					if (bytes == null || bytes.length == 0) {
						continue;
					}
					return toHex(bytes);
				} catch (SocketException e1) {
				}
			}
			return null;
		}

		private String serverInfo() {

			StringBuilder builder = new StringBuilder();
			builder.append("    server hostname = ").append(hostname);
			builder.append("\n      VM process id = ").append(pid);
			builder.append("\n        MAC address = ").append(hexMacAddress);
			builder.append("\n           username = ").append(username);
			builder.append("\n user run directory = ").append(userDir);
			builder.append("\nuser home directory = ").append(userHome);
			builder.append("\n  server identifier = ").append(serverIdentifier);
			builder.append("\n  server start time = ").append(String.format("%tF %<tT,%<tL (%<d)", startTime));
			builder.append("\n        VM inner id = ").append(vmid);
			builder.append("\n  identifier prefix = ").append(prefix);
			return builder.toString();
		}

		private int getTictac() {

			return (int) toSeconds(System.currentTimeMillis() - startTime);
		}
	}
}
