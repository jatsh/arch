package com.isesol.framework.cache.client.util.property;

import com.isesol.framework.cache.client.util.*;
import org.springframework.beans.*;
import org.springframework.jmx.access.*;

import java.beans.*;
import java.lang.reflect.*;

/**
 * 属性调用器，用于调用属性的 set 或者 get 方法，以实现动态设置或者读取属性值
 */
public class PropertyInvoker {

	/**
	 * JavaBeans 属性描述对象
	 */
	private PropertyDescriptor property;

	PropertyInvoker(PropertyDescriptor property) {

		this.property = property;
	}

	/**
	 * 设置对象的属性值
	 *
	 * @param target 目标对象
	 * @param stringValue 字符串格式的属性值
	 * @param typeConvert 类型转换器
	 *
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	void setValue(Object target, String stringValue, SimpleTypeConverter typeConvert) throws
	                                                                                  IllegalAccessException,
	                                                                                  InvocationTargetException {

		Object value = typeConvert.convertIfNecessary(stringValue, property.getPropertyType());

		property.getWriteMethod().invoke(target, value);
	}

	/**
	 * 调用对象该属性的 get 方法读取属性值
	 *
	 * @param target 目标对象
	 *
	 * @return
	 *
	 * @throws NullPointerException 目标对象为空时
	 * @throws UnsupportedOperationException 目标对象属性无 get 方法时
	 * @throws InvocationFailureException 反射调用异常
	 */
	Object getValue(Object target) {

		Assert.notNull(target, "target");

		Method reader = property.getReadMethod();

		if (reader == null) {

			throw new UnsupportedOperationException("property " + getPropertyName() + " unsupport get method");
		}

		try {

			return reader.invoke(target);
		} catch (Exception e) {

			throw new InvocationFailureException("get property [" + getPropertyName() + "] value failed", e);
		}

	}

	public String getPropertyName() {

		return property.getName();
	}

	public Class<?> getPropertyClass() {

		return property.getPropertyType();
	}
}
