package com.isesol.framework.cache.client.util.property;

/**
 * 属性调用器初始化异常
 */
public class PropertyInvokerException extends RuntimeException {

	public PropertyInvokerException(String message, Throwable cause) {

		super(message, cause);
	}

	public PropertyInvokerException(String message) {

		super(message);
	}
}
