package com.isesol.framework.cache.client.util.property;

import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.springframework.beans.*;

import java.beans.*;
import java.util.*;

/**
 * {@link PropertyInvoker} 管理器
 */
public class PropertyManager {

	static Logger log = LogManager.getLogger(PropertyManager.class.getName());

	/**
	 * {@link PropertyInvoker} 对象集合。key 为小写化的属性名，value 为属性调用器对象
	 */
	private final Map<String, PropertyInvoker> propertyInvokers = new TreeMap<String, PropertyInvoker>();

	/**
	 * 类型转换器
	 */
	private final SimpleTypeConverter typeConvert = new SimpleTypeConverter();

	/**
	 * 属性管理器所属的类对象
	 */
	private final Class<?> clazz;

	public PropertyManager(Class<?> clazz) {

		Assert.notNull(clazz, "clazz");

		this.clazz = clazz;

		doInitPropertyDescriptors();
	}

	/**
	 * 返回属性管理器目标类
	 *
	 * @return 属性管理器目标类
	 */
	public Class<?> getTargetClass() {

		return clazz;
	}

	/**
	 * 设置对象值
	 *
	 * @param target 目标对象
	 * @param property 小写化的属性名
	 * @param stringValue 字符串格式的属性值
	 *
	 * @return 成功时返回标准的属性名称
	 *
	 * @throws NullPointerException target 或 property 为空
	 * @throws ClassCastException target 与该对象属性所属类的类型不一致
	 * @throws PropertyInvokerException 为 target 设置时产生异常或者无法获取属性调用器对象
	 */
	public String setPropertyValue(Object target, String property, String stringValue) {

		Assert.notNull(target, "target");
		Assert.notEmpty(property, "property");

		if (!clazz.isInstance(target)) {

			throw new ClassCastException("setPropertyValue failed, target: " + target + " is not " + clazz + " type");
		}

		PropertyInvoker invoker = getPropertyDescriptor(property.toLowerCase());

		if (invoker == null) {

			throw new PropertyInvokerException(
					"setPropertyValue failed, class: [" + clazz + "] " + "no contains property: [" + property +
					"], ignore the method invoke");
		}

		try {

			invoker.setValue(target, stringValue, typeConvert);

			return invoker.getPropertyName();
		} catch (Exception e) {

			throw new PropertyInvokerException(
					"set object [" + target + "] property [" + invoker.getPropertyName() + "] value [" + stringValue +
					"] cause exception", e);
		}
	}

	public PropertyInvoker getPropertyDescriptor(String property) {

		return propertyInvokers.get(property);
	}

	public void logProperties(Object target) {

		if (!log.isInfoEnabled()) {

			return;
		}

		if (target == null) {
			log.warn("logProperties, parameter target is null");
			return;
		}

		if (!clazz.isInstance(target)) {
			throw new PropertyInvokerException("logProperties, target: " + target + " is not " + clazz + " type");
		}

		StringBuilder builder = new StringBuilder();
		builder.append("logProperties, " + target + " properties:\n");

		for (Map.Entry<String, PropertyInvoker> entry : propertyInvokers.entrySet()) {

			PropertyInvoker invoker = entry.getValue();

			builder.append(invoker.getPropertyName()).append(" = ").append(invoker.getValue(target)).append("\n");
		}
		log.info("{}", builder);
	}

	/**
	 * 初始化 {@link #propertyInvokers} 数据
	 */
	private void doInitPropertyDescriptors() {

		try {

			BeanInfo beanInfo = createBeanInfo();

			PropertyDescriptor[] pds = beanInfo.getPropertyDescriptors();

			for (PropertyDescriptor pd : pds) {

				if (pd.getWriteMethod() == null) {

					log.debug("{} property '{}' has not write method, ignore it!", clazz, pd);

					continue;
				}

				addPropertyInvoker(pd);
			}

		} catch (Exception e) {

			throw new PropertyInvokerException("doInitPropertyDescriptors error, class: [" + clazz + "]", e);
		}
	}

	private BeanInfo createBeanInfo() throws IntrospectionException {

		if (clazz == Object.class) {

			return Introspector.getBeanInfo(clazz);
		}

		return Introspector.getBeanInfo(clazz, Object.class);
	}

	private void addPropertyInvoker(PropertyDescriptor pd) {

		propertyInvokers.put(pd.getName().toLowerCase(), new PropertyInvoker(pd));
	}
}
