<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<xsd:schema
        xmlns="http://www.isesol.com/schema/cache"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema"
        xmlns:beans="http://www.springframework.org/schema/beans"
        targetNamespace="http://www.isesol.com/schema/cache"
        elementFormDefault="qualified"
        attributeFormDefault="unqualified">
    <xsd:import namespace="http://www.springframework.org/schema/beans"
                schemaLocation="http://www.springframework.org/schema/beans/spring-beans.xsd"/>
    <xsd:import namespace="http://www.springframework.org/schema/tool"
                schemaLocation="http://www.springframework.org/schema/tool/spring-tool.xsd"/>

    <xsd:element name="annotation-driven" type="annotation-driven-type"/>

    <xsd:element name="cache-executor" type="cache-executor-type">
        <xsd:annotation>
            <xsd:documentation>
                缓存执行器基本配置，以及 AOP 相关的配置
            </xsd:documentation>
        </xsd:annotation>
    </xsd:element>

    <xsd:complexType name="annotation-driven-type">
        <xsd:attribute name="cache-executor-ref" type="NonEmptyString" default="cacheExecutor"/>
        <xsd:attribute name="registry-ref" type="NonEmptyString"/>
        <xsd:attribute name="eval-facgtory-ref" type="NonEmptyString"/>
        <xsd:attribute name="keyid-gen-ref" type="NonEmptyString"/>
        <xsd:attribute name="ignore-bean-names" type="NonEmptyString"/>
        <xsd:attributeGroup ref="proxy-config"/>
    </xsd:complexType>

    <xsd:complexType name="cache-executor-type">
        <xsd:all>
            <xsd:element name="server-address" type="server-address-type" minOccurs="0"/>
            <xsd:element name="server" type="server-type" minOccurs="0"/>
            <xsd:element name="key-serializer" type="serializer-type" minOccurs="0"/>
            <xsd:element name="value-serializer" type="serializer-type" minOccurs="0"/>
            <xsd:element name="message-collector" type="message-collector-type" minOccurs="0"/>
        </xsd:all>

        <xsd:attribute name="appcode" use="required" type="NonEmptyString">
            <xsd:annotation>
                <xsd:documentation>
                    应用代码（必需），应用代码应是唯一能表示一个工程应用的标识
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>

        <xsd:attribute name="id" default="cacheExecutor" type="NonEmptyString">
            <xsd:annotation>
                <xsd:documentation>
                    通过该配置所创建  CacheExecutor 对象的 Spring Bean ID，
                    应用中可以使用该 ID 将 CacheExecutor 注入到 Spring 的 Bean中使用。（默认值：cacheExecutor）
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>

        <xsd:attribute name="default-expires" default="1800" type="xsd:int">
            <xsd:annotation>
                <xsd:documentation>
                    默认的数据缓存有效期（单位：秒）。
                    数据缓存未指定有效期或者有效期无效时使用该值。（默认值：1800 = 30 分钟）
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>

        <xsd:attribute name="cache-app-ref" type="NonEmptyString">
            <xsd:annotation>
                <xsd:documentation>
                    应用节点代码生成器引用的 Spring Bean ID，
                    该引用 ID 所表示的 Spring Bean 需要实现 CacheClientApplication 接口，
                    未指定时使用默认实现  DefaultCacheClientApplication 作为应用节点代码生成器
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>

        <xsd:attribute name="alert-bytes-threshold" default="1048576" type="xsd:int">
            <xsd:annotation>
                <xsd:documentation>
                        缓存数据值的按实际缓存字节大小的日志报警阀值。
                        当存储的缓存值大小超过该值时在日志文件中输出报警（默认值：1048576 = 1MB），
                        需要配置名为“cache.client.large”的日志记录器用于专门记录该类型的日志数据
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>

        <xsd:attributeGroup ref="proxy-config"/>
    </xsd:complexType>

    <xsd:complexType name="server-address-type">
        <xsd:choice minOccurs="0">
            <xsd:element name="local" type="cache-local-type"/>
            <xsd:element name="http" type="cache-http-type"/>
        </xsd:choice>
        <xsd:attribute name="ref" type="xsd:string"/>
    </xsd:complexType>

    <xsd:complexType name="server-type">
        <xsd:sequence>
            <xsd:element ref="beans:props" minOccurs="0" maxOccurs="unbounded"/>
        </xsd:sequence>
        <xsd:attribute name="ref" type="xsd:string"/>
    </xsd:complexType>

    <xsd:complexType name="serializer-type">
        <xsd:attribute name="ref" use="required" type="NonEmptyString"/>
    </xsd:complexType>

    <xsd:complexType name="message-collector-type">
        <xsd:all>
            <xsd:element name="send-type" type="send-type-type">
                <xsd:annotation>
                    <xsd:documentation>
                        缓存客户端运行数据的发送方式
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
            <xsd:element name="send-strategy" type="send-strategy-type">
                <xsd:annotation>
                    <xsd:documentation>
                        缓存客户端运行数据的发送策略
                    </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
        </xsd:all>
        <xsd:attribute name="ref" type="xsd:string">
            <xsd:annotation>
                <xsd:documentation>
                    通过实现 MessageCollector 接口，通过 Spring 管理 Bean 的 id 值
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="collector-threads-count" default="5">
            <xsd:annotation>
                <xsd:documentation>
                    缓存应用客户端运行数据若采用 async 方式时，数据收集线程的数量（默认值：5）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="mode" default="async">
            <xsd:annotation>
                <xsd:documentation>
                    缓存客户端运行数据的收集模式（默认值：async）
        </xsd:documentation>
            </xsd:annotation>
            <xsd:simpleType>
                <xsd:restriction base="xsd:string">
                    <xsd:enumeration value="async">
                        <xsd:annotation>
                            <xsd:documentation>
                                异步收集缓存客户端运行数据，该方式的操作对于原始的业务应用影响较少
                            </xsd:documentation>
                        </xsd:annotation>
                    </xsd:enumeration>
                    <xsd:enumeration value="close">
                        <xsd:annotation>
                            <xsd:documentation>
                                    关闭缓存客户端运行数据收集，即不收集缓存业务应用客户端产生的运行数据
                            </xsd:documentation>
                        </xsd:annotation>
                    </xsd:enumeration>
                </xsd:restriction>
            </xsd:simpleType>
        </xsd:attribute>
    </xsd:complexType>

    <xsd:complexType name="cache-local-type">
        <xsd:attribute name="address" use="required"/>
    </xsd:complexType>

    <xsd:complexType name="cache-http-type">
        <xsd:attributeGroup ref="http-type"/>
    </xsd:complexType>

    <xsd:complexType name="send-type-type">
        <xsd:choice minOccurs="0">
            <xsd:element name="send-to-log" type="send-to-log-type">
                <xsd:annotation>
                    <xsd:documentation>
                        设置客户端运行数据发送方式为“日志”，即将客户端运行数据直接在客户端以日志的方式输出
          </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
            <xsd:element name="send-to-http" type="send-to-http-type">
                <xsd:annotation>
                    <xsd:documentation>
                        设置客户端运行数据发送方式为“HTTP”，即将客户端运行数据通过 HTTP 协议的方式发送至远程接收端
          </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
            <xsd:element name="send-to-console" type="send-to-console-type">
                <xsd:annotation>
                    <xsd:documentation>
                        设置客户端运行数据发送方式为“控制台”，即将客户端运行数据直接在控制台中输出
          </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
        </xsd:choice>
        <xsd:attribute name="ref" type="xsd:string">
            <xsd:annotation>
                <xsd:documentation>
                    通过扩展 MessageSender
                    接口实现的客户端运行数据发送方式的 Spring Bean 的 id 值
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
    </xsd:complexType>

    <xsd:complexType name="send-strategy-type">
        <xsd:choice minOccurs="0">
            <xsd:element name="strategy-threshold" type="strategy-threshold-type">
                <xsd:annotation>
                    <xsd:documentation>
                            设置客户端运行数据发送策略为“阀值”，即当数据量达到一定的阀值时，将积累的数据发送至指定处
          </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
            <xsd:element name="strategy-schedule" type="strategy-schedule-type">
                <xsd:annotation>
                    <xsd:documentation>
                            设置客户端运行数据发送策略为“周期”，即以固定的时间间隔将数据发送至指定处
          </xsd:documentation>
                </xsd:annotation>
            </xsd:element>
        </xsd:choice>
        <xsd:attribute name="ref" type="xsd:string"/>
    </xsd:complexType>

    <xsd:complexType name="strategy-threshold-type">
        <xsd:attribute name="threshold" default="100" type="xsd:int">
            <xsd:annotation>
                <xsd:documentation>
                    客户端运行数据发送阀值，即当数据记录数累计到该属性所设定的数据时，将数据发送至指定处。（默认值：100）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="send-threads" default="2" type="xsd:int">
            <xsd:annotation>
                <xsd:documentation>
                    发送消息线程池中线程的数量。（默认值：2）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
    </xsd:complexType>

    <xsd:complexType name="strategy-schedule-type">
        <xsd:attribute name="periods" default="600" type="xsd:int">
            <xsd:annotation>
                <xsd:documentation>
                    指定客户端运行数据发送的周期（单位：秒）（默认值：600）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="send-max" default="500" type="xsd:int">
            <xsd:annotation>
                <xsd:documentation>
                指定客户端运行数据发送的最大数量，以防止在间隔期内产生大量的数据，给接收该数据的服务端造成流量压力。（默认值：500）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="send-threads" default="2" type="xsd:int">
            <xsd:annotation>
                <xsd:documentation>
                    发送消息线程池中线程的数量。（默认值：2）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
    </xsd:complexType>

    <xsd:complexType name="send-to-log-type">
        <xsd:attributeGroup ref="send-to-local-type"/>
        <xsd:attribute name="recorder" default="message.sender.recorder" type="NonEmptyString"/>
    </xsd:complexType>

    <xsd:complexType name="send-to-console-type">
        <xsd:attributeGroup ref="send-to-local-type"/>
        <xsd:attribute name="type" default="out">
            <xsd:simpleType>
                <xsd:restriction base="xsd:string">
                    <xsd:enumeration value="out"/>
                    <xsd:enumeration value="err"/>
                </xsd:restriction>
            </xsd:simpleType>
        </xsd:attribute>
    </xsd:complexType>

    <xsd:complexType name="send-to-http-type">
        <xsd:attributeGroup ref="send-to-type"/>
        <xsd:attributeGroup ref="http-endpoint-optional-type"/>
    </xsd:complexType>

    <xsd:attributeGroup name="http-type">
        <xsd:attribute name="endpoint" use="required" type="NonEmptyString">
            <xsd:annotation>
                <xsd:documentation>
                    HTTP 请求 URL 地址
                </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="connect-timeout" default="15000" type="xsd:int">
            <xsd:annotation>
                <xsd:documentation>
                    HTTP 请求连接超时时间（单位：毫秒）（默认值：15000）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="read-timeout" default="15000" type="xsd:int">
            <xsd:annotation>
                <xsd:documentation>
                    HTTP 读取数据响应超时时间（单位：毫秒）（默认值：15000）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
    </xsd:attributeGroup>

    <xsd:attributeGroup name="http-endpoint-optional-type">
        <xsd:attribute name="endpoint" type="xsd:string">
            <xsd:annotation>
                <xsd:documentation>
                    HTTP 请求 URL 地址
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="connect-timeout" default="15000" type="xsd:int">
            <xsd:annotation>
                <xsd:documentation>
                    HTTP 请求连接超时时间（单位：毫秒）（默认值：15000）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
        <xsd:attribute name="read-timeout" default="15000" type="xsd:int">
            <xsd:annotation>
                <xsd:documentation>
                    HTTP 读取数据响应超时时间（单位：毫秒）（默认值：15000）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
    </xsd:attributeGroup>

    <xsd:attributeGroup name="send-to-local-type">
        <xsd:attributeGroup ref="send-to-type"/>
        <xsd:attribute name="success" default="true" type="xsd:boolean"/>
    </xsd:attributeGroup>

    <xsd:attributeGroup name="send-to-type">
        <xsd:attribute name="format" default="json">
            <xsd:simpleType>
                <xsd:restriction base="xsd:string">
                    <xsd:enumeration value="json"/>
                </xsd:restriction>
            </xsd:simpleType>
        </xsd:attribute>
    </xsd:attributeGroup>

    <xsd:simpleType name="NonEmptyString">
        <xsd:restriction base="xsd:string">
            <xsd:pattern value=".*[^\s].*"/>
        </xsd:restriction>
    </xsd:simpleType>

    <xsd:attributeGroup name="proxy-config">

        <xsd:attribute name="proxy-target-class" type="xsd:boolean" default="true">
            <xsd:annotation>
                <xsd:documentation>
                        Spring AOP 动态代理属性配置，指定是否强制在类级别上创建代理对象。
                        默认值为 true，表示强制在类级别上创建代理对象。
                        false 时表示根据需要被代理的对象是否有接口自动地确定是在类级别上创建代理对象，还是在接口级别上创建代理对象。
                        参考 org.springframework.aop.framework.ProxyConfig 中的 proxyTargetClass 属性。
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>

        <xsd:attribute name="freeze-proxy" type="xsd:boolean" default="false">
            <xsd:annotation>
                <xsd:documentation>
                    Spring AOP 动态代理属性配置，
                    默认值为 false，
                    参考 org.springframework.aop.framework.ProxyConfig 中的 frozen 属性。（默认值：false）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>

        <xsd:attribute name="optimize" type="xsd:boolean" default="false">
            <xsd:annotation>
                <xsd:documentation>
                     Spring AOP 动态代理属性配置，参考 org.springframework.aop.framework.ProxyConfig
                     中的 optimize 属性。（默认值：false）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>

        <xsd:attribute name="opaque" type="xsd:boolean" default="false">
            <xsd:annotation>
                <xsd:documentation>
                    Spring AOP 动态代理属性配置，指定是否创建透明的代理对象，
                    即代理对象是否可以转换成为 org.springframework.aop.framework.Advised 接口，以便于获取代理对象内部的状态。
                    默认值为 false，表示不创建透明的代理对象，即不可以转换成为 Advised 接口类型。
                    参考 org.springframework.aop.framework.ProxyConfig 中的 opaque 属性。
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>

        <xsd:attribute name="expose-proxy" type="xsd:boolean" default="false">
            <xsd:annotation>
                <xsd:documentation>
                    Spring AOP 动态代理属性配置，指定是否将代理对象暴露在 org.springframework.aop.framework.AopContext 中的 ThreadLocal 中，
                    默认值为 false，表示不暴露代理对象，即无法从 AopContext 中获取代理对象。
                    参考 org.springframework.aop.framework.ProxyConfig 中的 exposeProxy 属性。（默认值：false）
        </xsd:documentation>
            </xsd:annotation>
        </xsd:attribute>
    </xsd:attributeGroup>

</xsd:schema>