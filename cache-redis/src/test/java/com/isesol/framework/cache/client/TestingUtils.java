package com.isesol.framework.cache.client;

import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.testng.*;

import java.lang.reflect.*;
import java.util.*;

public final class TestingUtils {

	static Logger LOGGER = LogManager.getLogger(TestingUtils.class.getName());

	private TestingUtils() {

	}

	public static void logBefore(Method method, Object[] params) {

		StringBuilder builder = new StringBuilder();
		builder.append("=================================================");
		builder.append("\n").append(method.getDeclaringClass().getSimpleName()).append("#").append(method.getName());

		if (!Tools.isBlank(params)) {
			builder.append("\n-------------------------------------------------");
			for(int i = 0; i < params.length; i++){
				builder.append("\n");
				append(builder, MethodArgumentNames.getName(method, i));
				builder.append(" = ");
				if (params[i] instanceof byte[]) {
					append(builder, Arrays.toString((byte[]) params[i]));
				} else if (params[i] instanceof short[]) {
					append(builder, Arrays.toString((short[]) params[i]));
				} else if (params[i] instanceof char[]) {
					append(builder, Arrays.toString((char[]) params[i]));
				} else if (params[i] instanceof int[]) {
					append(builder, Arrays.toString((int[]) params[i]));
				} else if (params[i] instanceof long[]) {
					append(builder, Arrays.toString((long[]) params[i]));
				} else if (params[i] instanceof float[]) {
					append(builder, Arrays.toString((float[]) params[i]));
				} else if (params[i] instanceof double[]) {
					append(builder, Arrays.toString((double[]) params[i]));
				} else if (params[i] instanceof boolean[]) {
					append(builder, Arrays.toString((boolean[]) params[i]));
				} else if (params[i] instanceof Object[]) {
					append(builder, Arrays.toString((Object[]) params[i]));
				} else {
					if (params[i] instanceof Collection<?>) {
						builder.append("size: ").append(( (Collection<?>) params[i] ).size()).append(": ");
					} else if (params[i] instanceof Map<?, ?>) {
						builder.append("size: ").append(( (Map<?, ?>) params[i] ).size()).append(": ");
					}
					append(builder, String.valueOf(params[i]));
				}
			}
		}

		builder.append("\n=================================================");

		LOGGER.info("\n{}\n", builder);
	}

	public static void logAfter(ITestResult result) {

		ITestNGMethod method = result.getMethod();
		StringBuilder builder = new StringBuilder();
		builder.append("-------------------------------------------------\n")
				.append(Arrays.toString(method.getGroups())).append("#").append(method.getPriority()).append("\n");
		builder.append("-------------------------------------------------\n")
				.append(method.getTestClass().getRealClass().getSimpleName()).append("#")
				.append(method.getMethodName()).append("\n");
		builder.append("-------------------------------------------------\n").append("time: ")
				.append(result.getEndMillis() - result.getStartMillis()).append(" ms").append(", result: ")
				.append(toStatus(result.getStatus())).append("\n");
		builder.append("-------------------------------------------------");
		LOGGER.info("\n{}\n", builder);
	}

	private static String toStatus(int status) {

		switch ( status ) {
			case 1:
				return "SUCCESS";
			case 2:
				return "FAILURE";
			case 3:
				return "SKIP";
			case 4:
				return "SUCCESS_PERCENTAGE_FAILURE";
			case 16:
				return "START";
			default:
				return "UNKNOWN";
		}
	}

	private static void append(StringBuilder builder, String str) {

		builder.append(Tools.truncate(str));
	}
}
