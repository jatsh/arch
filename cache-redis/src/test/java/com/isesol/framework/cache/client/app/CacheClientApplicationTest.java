package com.isesol.framework.cache.client.app;

import com.google.common.truth.*;
import com.isesol.framework.cache.client.*;
import com.isesol.framework.cache.client.util.*;
import org.springframework.test.context.*;
import org.springframework.test.context.testng.*;
import org.testng.*;
import org.testng.annotations.*;

import javax.annotation.*;
import java.lang.reflect.*;

@ContextConfiguration(locations = "classpath:/app/application-context.xml")
public class CacheClientApplicationTest extends AbstractTestNGSpringContextTests {

    private static final String APP_CODE = "dev";

    @Resource
    private CacheClientApplication application;

    @BeforeMethod
    public void beforeMethod(Method method, Object[] params) {

        TestingUtils.logBefore(method, params);
    }

    @AfterMethod
    public void afterMethod(ITestResult result) {

        TestingUtils.logAfter(result);
    }

    @Test(groups = "application")
    public void testGetAppCode() {

        Truth.assertThat(application.getAppCode()).isEqualTo(APP_CODE);
    }

    @Test(groups = "application")
    public void testGetAppNodeCode() {

        String hostname = ServerInfoUtils.getHostname();
        String code = CacheClientApplicationTest.class.getResource("").getPath().replace(":", "");
        int length = CacheClientApplicationTest.class.getPackage().getName().length() + 1;
        code = code.substring(0, code.length() - length);

       // Truth.assertThat(application.getNodeCode()).is(APP_CODE + ':' + hostname + ':' + code);
    }
}
