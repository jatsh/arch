package com.isesol.framework.cache.client.connector;

import com.google.common.truth.*;
import com.isesol.framework.cache.client.*;
import org.springframework.test.context.*;
import org.springframework.test.context.testng.*;
import org.testng.*;
import org.testng.annotations.*;

import javax.annotation.*;
import java.lang.reflect.*;

@ContextConfiguration(locations = "classpath:/connector/application-context.xml")
public class CacheServerLocatorTest extends AbstractTestNGSpringContextTests {

	@Resource
	private CacheServerLocator localLocator;

	private CacheServerConnector connector;

	private CacheServerAddress[] address;

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {

		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {

		TestingUtils.logAfter(result);
	}

	@Test(groups = "connector",priority = 1)
	public void testGetCacheServerConnector() {

		this.connector = localLocator.getCacheServer();
		Truth.assertThat(this.connector).isNotNull();
	}

	@Test(groups = "connector",priority = 2)
	public void testGetAddress() {

		this.address = connector.getAddress();
		Truth.assertThat(address).isNotEmpty();
		Truth.assertThat(address.length).isEqualTo(1);
	}

	@Test(groups = "connector",priority = 3)
	public void testGetAddressInfo() {

		Truth.assertThat(address[0].getHost()).isEqualTo("127.0.0.1");
		Truth.assertThat(address[0].getPort()).isEqualTo(6379);
		Truth.assertThat(address[0].getPassword()).isEqualTo("pass");
	}
}
