package com.isesol.framework.cache.client.connector;

import com.isesol.framework.cache.client.*;
import com.isesol.framework.cache.client.app.*;
import com.isesol.framework.cache.client.connector.notifier.*;
import com.isesol.framework.cache.client.http.*;
import com.isesol.framework.cache.client.util.*;
import org.easymock.*;
import org.testng.*;
import org.testng.annotations.*;

import java.io.*;
import java.lang.reflect.*;

public class ClientShutdownHttpNotifierTest {

	private CacheClientApplication application = new TestCacheClientApplication("dev");

	private ClientShutdownHttpNotifier notifier;

	private HttpRequestor httpRequestor;

	@BeforeClass
	public void before() {
		this.notifier = createNotifier();
	}

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {
		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {
		TestingUtils.logAfter(result);
	}

	@Test(groups = "connector", priority = 7500, dataProvider = "ClientShutdownNotifier#notifyShutdown", dataProviderClass = ConnectorDataProvider.class)
	public void testNotifyShutdown(Integer id, Integer code, String respContent, Boolean throwException)
			throws IOException {
		replay(code, respContent, throwException);
		notifier.notifyShutdown();
		EasyMock.verify(httpRequestor);
	}

	private void replay(Integer code, String respContent, Boolean throwException) throws IOException {

		httpRequestor = EasyMock.createMock(HttpRequestor.class);
		notifier.setRequestor(httpRequestor);
		notifier.afterPropertiesSet();

		httpRequestor.doPost(EasyMock.isA(HttpRequest.class));

		if (throwException) {
			EasyMock.expectLastCall().andThrow(new IOException("Mock IOExcpetion"));
			EasyMock.replay(httpRequestor);
			return;
		}

		HttpResponse response = new HttpResponse(code, "OK", Tools.toBytes(respContent), null);
		EasyMock.expectLastCall().andReturn(response).once();
		EasyMock.replay(httpRequestor);
	}

	private ClientShutdownHttpNotifier createNotifier() {
		ClientShutdownHttpNotifier httpNotifier = new ClientShutdownHttpNotifier();
		httpNotifier.setCacheClientApplication(application);
		return httpNotifier;
	}

	private static final class TestCacheClientApplication implements CacheClientApplication {

		private final String appCode;
		private final String nodeCode;

		public TestCacheClientApplication(String appCode) {

			this.appCode = appCode;
			this.nodeCode = appCode + "-Node";
		}

		@Override
		public String getAppCode() {
			return appCode;
		}

		@Override
		public String getNodeCode() {
			return nodeCode;
		}

	}
}
