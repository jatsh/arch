package com.isesol.framework.cache.client.connector;

import org.testng.annotations.DataProvider;

public class ConnectorDataProvider {

	@DataProvider(name = "ClientShutdownNotifier#notifyShutdown")
	public static Object[][] provideNotifyShutdown() {

		int n = 30001;

		int[] codes = { 200, 500 };
		String[] responseContents = { "true", "false", null, "ERROR" };
		boolean[] exceptions = { false, true };

		Object[][] data = new Object[codes.length * responseContents.length * exceptions.length][];

		int index = 0;
		for(int code : codes){
			for(String responseContent : responseContents){
				for(boolean exception : exceptions){
					data[index++] = new Object[]{ n++, code, responseContent, exception };
				}
			}
		}

		return data;
	}
}
