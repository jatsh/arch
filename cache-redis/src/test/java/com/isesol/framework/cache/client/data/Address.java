package com.isesol.framework.cache.client.data;

import java.io.Serializable;

public class Address implements Serializable {

	private String country;

	private String state;

	private String city;

	private String district;

	private String road;

	private String houseNumber;

	private String floor;

	private String postCode;

	public Address() {

	}

	public Address(String country, String state, String city, String district, String road, String houseNumber,
	               String floor, String postCode) {

		super();
		this.country = country;
		this.state = state;
		this.city = city;
		this.district = district;
		this.road = road;
		this.houseNumber = houseNumber;
		this.floor = floor;
		this.postCode = postCode;
	}

	public String getCountry() {

		return country;
	}

	public void setCountry(String country) {

		this.country = country;
	}

	public String getState() {

		return state;
	}

	public void setState(String state) {

		this.state = state;
	}

	public String getCity() {

		return city;
	}

	public void setCity(String city) {

		this.city = city;
	}

	public String getDistrict() {

		return district;
	}

	public void setDistrict(String district) {

		this.district = district;
	}

	public String getRoad() {

		return road;
	}

	public void setRoad(String road) {

		this.road = road;
	}

	public String getHouseNumber() {

		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {

		this.houseNumber = houseNumber;
	}

	public String getFloor() {

		return floor;
	}

	public void setFloor(String floor) {

		this.floor = floor;
	}

	public String getPostCode() {

		return postCode;
	}

	public void setPostCode(String postCode) {

		this.postCode = postCode;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( city == null ) ? 0 : city.hashCode() );
		result = prime * result + ( ( country == null ) ? 0 : country.hashCode() );
		result = prime * result + ( ( district == null ) ? 0 : district.hashCode() );
		result = prime * result + ( ( floor == null ) ? 0 : floor.hashCode() );
		result = prime * result + ( ( houseNumber == null ) ? 0 : houseNumber.hashCode() );
		result = prime * result + ( ( postCode == null ) ? 0 : postCode.hashCode() );
		result = prime * result + ( ( road == null ) ? 0 : road.hashCode() );
		result = prime * result + ( ( state == null ) ? 0 : state.hashCode() );
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (district == null) {
			if (other.district != null)
				return false;
		} else if (!district.equals(other.district))
			return false;
		if (floor == null) {
			if (other.floor != null)
				return false;
		} else if (!floor.equals(other.floor))
			return false;
		if (houseNumber == null) {
			if (other.houseNumber != null)
				return false;
		} else if (!houseNumber.equals(other.houseNumber))
			return false;
		if (postCode == null) {
			if (other.postCode != null)
				return false;
		} else if (!postCode.equals(other.postCode))
			return false;
		if (road == null) {
			if (other.road != null)
				return false;
		} else if (!road.equals(other.road))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}

	@Override
	public String toString() {

		return "Room " + floor + " No." + houseNumber + " " + road + ", " + district + " " + city + " " + state + " "
				+ country + " (" + postCode + ")";
	}
}
