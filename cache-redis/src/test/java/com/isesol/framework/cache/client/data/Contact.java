package com.isesol.framework.cache.client.data;

import java.io.Serializable;

public class Contact implements Serializable {

	public static enum ContactType {

		MOBILE, TEL, FAX, EMAIL, IM;

		public Contact create(ContactScope scope, String contact, boolean important) {

			return new Contact(this, scope, contact, important);
		}
	}

	public static enum ContactScope {
		WORKING, PRIVATE, COMMON, HOME
	}

	private ContactType type;

	private ContactScope scope;

	private String contact;

	private boolean important;

	public Contact() {

		super();
	}

	private Contact(ContactType type, ContactScope scope, String contact, boolean important) {

		super();
		this.type = type;
		this.scope = scope;
		this.contact = contact;
		this.important = important;
	}

	public ContactType getType() {

		return type;
	}

	public void setType(ContactType type) {

		this.type = type;
	}

	public ContactScope getScope() {

		return scope;
	}

	public void setScope(ContactScope scope) {

		this.scope = scope;
	}

	public String getContact() {

		return contact;
	}

	public void setContact(String contact) {

		this.contact = contact;
	}

	public boolean isImportant() {

		return important;
	}

	public void setImportant(boolean important) {

		this.important = important;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( contact == null ) ? 0 : contact.hashCode() );
		result = prime * result + ( important ? 1231 : 1237 );
		result = prime * result + ( ( scope == null ) ? 0 : scope.hashCode() );
		result = prime * result + ( ( type == null ) ? 0 : type.hashCode() );
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (contact == null) {
			if (other.contact != null)
				return false;
		} else if (!contact.equals(other.contact))
			return false;
		if (important != other.important)
			return false;
		if (scope != other.scope)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append(type).append("(").append(scope).append("): ");
		builder.append(contact);
		if (important) {
			builder.append(" *");
		}
		return builder.toString();
	}
}