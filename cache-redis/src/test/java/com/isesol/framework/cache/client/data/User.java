package com.isesol.framework.cache.client.data;

import com.isesol.framework.cache.client.data.Contact.*;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.atomic.*;

public class User implements Serializable {

	private static final AtomicLong increment = new AtomicLong(0);

	public static final User TOM = createUser("tom", "2000-01-01");

	public static final User JERRY = createUser("jerry", "2005-01-01");

	public static final User JERRY_NEW = createUser("jerry", "2005-02-01");

	public static final User BOB = createUser("bob", "2003-01-01");

	@SuppressWarnings("serial")
	public static final Map<String, User> ALL_USERS = Collections.unmodifiableMap(new HashMap<String, User>() {

		{
			put(TOM.getName(), TOM);
			put(JERRY.getName(), JERRY);
			put(BOB.getName(), BOB);
		}
	});

	@SuppressWarnings("serial")
	public static final Map<String, User> USERS = Collections.unmodifiableMap(new HashMap<String, User>() {

		{
			put(TOM.getName(), TOM);
			put(JERRY.getName(), JERRY);
		}
	});

	@SuppressWarnings("serial")
	public static final Map<String, User> USER_NEWS = Collections.unmodifiableMap(new HashMap<String, User>() {

		{
			put(BOB.getName(), BOB);
			put(JERRY_NEW.getName(), JERRY);
		}
	});

	private Long id;

	private String name;

	private boolean male;

	private Date birthday;

	private List<Address> addrs;

	private List<Contact> contacts;

	public User() {

		super();
	}

	public User(Long id, String name, boolean male, String birthday) {

		super();
		this.id = id;
		this.name = name;
		this.male = male;
		try {
			this.birthday = new SimpleDateFormat("yyyy-MM-dd").parse(birthday);
		} catch ( ParseException e ) {
			throw new IllegalArgumentException("birthday '" + birthday + "' is invalid", e);
		}
	}

	private static User createUser(String name, String date) {

		User user = new User(increment.incrementAndGet(), name, false, date);
		user.addAddress(new Address("China", "Shanghai", "Shanghai", "Pudong", "QingXi Rd.", "No x", "x room", "200000"));
		user.addAddress(new Address("China", "Shanghai", "Shanghai", "Pudong", "ZhangJiaJie Street.", "No x", "x room",
				"200000"));
		user.addContact(ContactType.TEL.create(ContactScope.WORKING, "(021)88888888-100", false));
		user.addContact(ContactType.TEL.create(ContactScope.COMMON, "1350000000", true));
		user.addContact(ContactType.TEL.create(ContactScope.HOME, "(021)50410405", true));
		user.addContact(ContactType.EMAIL.create(ContactScope.WORKING, "admin@xx.xx", true));
		user.addContact(ContactType.EMAIL.create(ContactScope.PRIVATE, "admin@ddatsh.com", true));
		user.addContact(ContactType.IM.create(ContactScope.PRIVATE, "124315391", true));
		return user;
	}

	public void addAddress(Address addr) {

		if (addrs == null) {
			addrs = new LinkedList<Address>();
		}
		addrs.add(addr);
	}

	public void addContact(Contact contact) {

		if (contacts == null) {
			contacts = new LinkedList<Contact>();
		}
		contacts.add(contact);
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public boolean isMale() {

		return male;
	}

	public void setMale(boolean male) {

		this.male = male;
	}

	public Date getBirthday() {

		return birthday;
	}

	public void setBirthday(Date birthday) {

		this.birthday = birthday;
	}

	public List<Address> getAddrs() {

		return addrs;
	}

	public void setAddrs(List<Address> addrs) {

		this.addrs = addrs;
	}

	public List<Contact> getContacts() {

		return contacts;
	}

	public void setContacts(List<Contact> contacts) {

		this.contacts = contacts;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( addrs == null ) ? 0 : addrs.hashCode() );
		result = prime * result + ( ( birthday == null ) ? 0 : birthday.hashCode() );
		result = prime * result + ( ( contacts == null ) ? 0 : contacts.hashCode() );
		result = prime * result + ( ( id == null ) ? 0 : id.hashCode() );
		result = prime * result + ( male ? 1231 : 1237 );
		result = prime * result + ( ( name == null ) ? 0 : name.hashCode() );
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (addrs == null) {
			if (other.addrs != null)
				return false;
		} else if (!addrs.equals(other.addrs))
			return false;
		if (birthday == null) {
			if (other.birthday != null)
				return false;
		} else if (!birthday.equals(other.birthday))
			return false;
		if (contacts == null) {
			if (other.contacts != null)
				return false;
		} else if (!contacts.equals(other.contacts))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (male != other.male)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append(User.class.getSimpleName());
		builder.append("@");
		builder.append(Integer.toHexString(System.identityHashCode(this)));
		builder.append(" [");
		if (id != null) {
			builder.append("id=");
			builder.append(id);
			builder.append(", ");
		}
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		builder.append("male=");
		builder.append(male);
		builder.append(", ");
		if (birthday != null) {
			builder.append("birthday=");
			builder.append(birthday);
			builder.append(", ");
		}
		if (addrs != null) {
			builder.append("addrs=");
			builder.append(addrs);
			builder.append(", ");
		}
		if (contacts != null) {
			builder.append("contacts=");
			builder.append(contacts);
		}
		builder.append("]");
		return builder.toString();
	}
}
