package com.isesol.framework.cache.client.executor;

import com.isesol.framework.cache.client.data.*;
import com.isesol.framework.cache.client.executor.api.*;
import com.isesol.framework.cache.client.executor.operation.*;
import com.isesol.framework.cache.client.util.*;
import org.testng.annotations.*;

import java.math.*;
import java.util.*;

public class CacheExecutorDataProvider {

	public static final Integer EXPIRES_SHORT = 1;

	public static final Integer EXPIRES_LONG = 3600;

	public static final SortedCond SORTED_CONDITION_ALL = new SortedCond(true);

	private static final String USER_KEY = "user.key";

	@SuppressWarnings("serial")
	private static final Map<String, User> INCLUDES_EMPTY_MAP = new HashMap<String, User>() {

		{
			put(User.TOM.getName(), User.TOM);
			put(User.JERRY.getName(), User.JERRY);
			put(null, User.BOB);
			put("", User.BOB);
			put("   ", User.BOB);
		}
	};

	public static String getUserKey() {

		return USER_KEY;
	}

	public static String getUserKey(int num) {

		return USER_KEY + ':' + num;
	}

	public static User getUserBob() {

		return User.BOB;
	}

	public static Map<String, User> getUserMap() {

		return User.USERS;
	}

	public static Map<String, User> getUserMapIncludesEmptyKey() {

		return INCLUDES_EMPTY_MAP;
	}

	public static Date createDate(Integer incrementSeconds) {

		if (incrementSeconds == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.add(Calendar.SECOND, incrementSeconds);
		return c.getTime();
	}

	@DataProvider(name = "set-object", parallel = true)
	public static Object[][] provideSetObject() {

		String[] keys = { getUserKey(), null, "  " };
		Object[] values = { null, getUserBob() };
		Integer[] expires = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };
		Integer[] expireAts = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };

		Object[][] results = new Object[keys.length * values.length * ( expires.length + expireAts.length )][];

		int incr = 1001;
		int index = 0;

		for(String key : keys){
			for(Object value : values){
				for(Integer expire : expires){
					results[index++] = new Object[]{ incr++, getKey(key, incr - 1), value, expire, null };
				}
				for(Integer expireAt : expireAts){
					results[index++] = new Object[]{ incr++, getKey(key, incr - 1), value, null, expireAt };
				}
			}
		}

		return results;
	}

	@DataProvider(name = "set-map", parallel = true)
	public static Object[][] provideSetMap() {

		String[] keys = { getUserKey(), null, "  " };
		Object[] values = { null, getUserMap(), new HashMap<String, User>(), getUserMapIncludesEmptyKey() };
		Integer[] expires = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };
		Integer[] expireAts = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };
		Boolean[] usingSets = { false, true };

		Object[][] results = new Object[keys.length * values.length * usingSets.length
				* ( expires.length + expireAts.length )][];

		int incr = 2001;
		int index = 0;

		for(String key : keys){
			for(Object value : values){
				for(Boolean usingSet : usingSets){
					for(Integer expire : expires){
						results[index++] = new Object[]{ incr++, getKey(key, incr - 1), value, expire, null, usingSet };
					}
					for(Integer expireAt : expireAts){
						results[index++] = new Object[]{ incr++, getKey(key, incr - 1), value, null, expireAt,
								usingSet };
					}
				}
			}
		}

		return results;
	}

	@DataProvider(name = "set-map-entry", parallel = true)
	public static Object[][] provideSetMapEntry() {

		Map<?, ?>[] existsMaps = { null, getUserMap() };
		Integer[] expires = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };
		Integer[] expireAts = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };

		final String key = getUserKey();
		final String[] fields = { null, "", "   ", User.JERRY.getName() };
		final User jerryNew = User.JERRY_NEW;

		Object[][] results = new Object[fields.length * existsMaps.length * ( expires.length + expireAts.length )][];

		int incr = 3001;
		int index = 0;

		for(Map<?, ?> existsMap : existsMaps){
			for(String field : fields){
				for(Integer expire : expires){
					results[index++] = new Object[]{ incr++, getKey(key, incr - 1), existsMap, field, jerryNew,
							expire, null, exceptTTLGreater(existsMap, expire, null) };
				}
				for(Integer expireAt : expireAts){
					results[index++] = new Object[]{ incr++, getKey(key, incr - 1), existsMap, field, jerryNew, null,
							expireAt, exceptTTLGreater(existsMap, null, expireAt) };
				}
			}
		}

		return results;
	}

	@DataProvider(name = "set-map-entries", parallel = true)
	public static Object[][] provideSetMapEntries() {

		Map<?, ?>[] existsMaps = { null, getUserMap() };
		Integer[] expires = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };
		Integer[] expireAts = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };

		Map<?, ?>[] entries = { User.USER_NEWS, getUserMapIncludesEmptyKey() };

		Object[][] results = new Object[existsMaps.length * entries.length * ( expires.length + expireAts.length )][];

		int incr = 4001;
		int index = 0;

		final String key = getUserKey();

		for(Map<?, ?> existsMap : existsMaps){
			for(Map<?, ?> entry : entries){
				for(Integer expire : expires){
					results[index++] = new Object[]{ incr++, getKey(key, incr - 1), existsMap, entry, expire, null,
							exceptTTLGreater(existsMap, expire, null) };
				}
				for(Integer expireAt : expireAts){
					results[index++] = new Object[]{ incr++, getKey(key, incr - 1), existsMap, entry, null, expireAt,
							exceptTTLGreater(existsMap, null, expireAt) };
				}
			}
		}

		return results;
	}

	private static long exceptTTLGreater(Map<?, ?> existsMap, Integer expire, Integer expireAt) {

		if (( expire == null && expireAt == null ) || ( expireAt != null && expireAt <= 0 )
				|| ( expire != null && expire <= 0 )) {

			if (existsMap == null) {
				return TransactionCacheExecutor.DEFAULT_EXPIRATION_SECONDS - 5;
			} else {
				return EXPIRES_LONG * 3 - 5;
			}
		}
		if (expireAt != null && expireAt > 0) {
			return expireAt - 5;
		}
		if (expire != null && expire > 0) {
			return expire - 5;
		}
		return EXPIRES_LONG * 3;
	}

	@DataProvider(name = "has-key")
	public static Object[][] provideHasKey() {

		return new Object[][]{ { 5001, null, true, getUserBob(), false }, { 5002, null, false, getUserBob(), false },
				{ 5003, " ", true, getUserBob(), false }, { 5004, " ", false, getUserBob(), false },
				{ 5005, "", true, getUserBob(), false }, { 5006, "", false, getUserBob(), false },
				{ 5007, "test12345", false, getUserBob(), false }, { 5008, "test12345", true, getUserBob(), false },
				{ 5009, getUserKey(), false, getUserBob(), false }, { 5010, getUserKey(), true, getUserBob(), true } };
	}

	@DataProvider(name = "expireSeconds")
	public static Object[][] provideExpireSeconds() {

		return new Object[][]{

				{ 6001, null, 120, 300 }, { 6002, null, 120, 40 },

				{ 6101, getUserKey(), 120, 300 }, { 6102, getUserKey(), 120, 40 },

				{ 6201, "          ", 120, 300 }, { 6202, "          ", 120, 40 } };
	}

	@DataProvider(name = "expireAt")
	public static Object[][] provideExpireAt() {

		return new Object[][]{

				{ 7001, null, 120, EXPIRES_LONG }, { 7002, null, 120, -EXPIRES_LONG },

				{ 7101, getUserKey(), 120, EXPIRES_LONG }, { 7102, getUserKey(), 120, -EXPIRES_LONG },

				{ 7201, "  ", 120, EXPIRES_LONG }, { 7202, "  ", 120, -EXPIRES_LONG } };
	}

	@DataProvider(name = "del")
	public static Object[][] provideDel() {

		return new Object[][]{ { 8001, null, getUserBob(), false }, { 8002, null, getUserMap(), false },
				{ 8003, null, getUserMap(), true },

				{ 8101, "    ", getUserBob(), false }, { 8102, "    ", getUserMap(), false },
				{ 8103, "    ", getUserMap(), true },

				{ 8201, "self.user", getUserBob(), false }, { 8202, "self.user", getUserMap(), false },
				{ 8203, "self.user", getUserMap(), true },

				{ 8301, getUserKey(), getUserBob(), false }, { 8302, getUserKey(), getUserMap(), false },
				{ 8303, getUserKey(), getUserMap(), true } };
	}

	@DataProvider(name = "delMapKey", parallel = true)
	public static Object[][] provideDelMapKey() {

		final int N = 15;

		Map<String, String> data = new HashMap<String, String>();

		for(int i = 0; i < N; i++){
			data.put("field-" + i, String.valueOf(i));
		}

		Object[][] result = new Object[N + 13][];

		int n = 14001;

		int index = 0;

		result[index++] = new Object[]{ n++, getKey("key-", n - 1), data, new String[]{ "" }, -1 };
		result[index++] = new Object[]{ n++, getKey("key-", n - 1), data, new String[]{ "  " }, -1 };
		result[index++] = new Object[]{ n++, getKey("key-", n - 1), data, new String[]{ null }, -1 };
		result[index++] = new Object[]{ n++, getKey("key-", n - 1), data, new String[]{ "field-0", null, "field-1" },
				-1 };
		result[index++] = new Object[]{ n++, getKey("key-", n - 1), data, new String[]{ "field-0", "", "field-1" },
				-1 };
		result[index++] = new Object[]{ n++, getKey("key-", n - 1), data,
				new String[]{ "field-0", "   ", "field-1" }, -1 };
		result[index++] = new Object[]{ n++, getKey("key-", n - 1), data,
				new String[]{ "field-0", "field-1", "field-50" }, N - 2 };
		result[index++] = new Object[]{ n++, getKey("key-", n - 1), data,
				new String[]{ "field-0", "field-1", "field-50", "field-51" }, N - 2 };
		result[index++] = new Object[]{ n++, getKey("key-", n - 1), data,
				new String[]{ "field-18", "field-22", "field-50", "field-51" }, N };

		for(int i = 0; i < N + 4; i++){
			result[index++] = new Object[]{ n++, getKey("key-", n - 1), data, toFields(i + 1), N - i - 1 };
		}

		return result;
	}

	private static String[] toFields(int n) {

		String[] strs = new String[n];
		for(int i = 0; i < n; i++){
			strs[i] = "field-" + i;
		}
		return strs;
	}

	@DataProvider(name = "mget")
	public static Object[][] provideMGet() {

		Object[] values = new Object[]{ "test", false, 'a', Integer.MAX_VALUE, Long.MAX_VALUE, Double.MAX_VALUE,
				Float.MAX_VALUE, Math.PI, getUserBob(), getUserMap(), User.USER_NEWS,
				new ArrayList<User>(getUserMap().values()) };

		Object[][] result = new Object[values.length][];

		int n = 8501;

		int offset = 0;

		while ( offset < values.length ) {
			result[offset] = new Object[]{ n++, getUserKey(), values, offset + 1 };
			offset++;
		}

		return result;
	}

	@DataProvider(name = "incrementBy", parallel = true)
	public static Object[][] provideIncrementBy() {

		int n = 9001;

		int[] values = { Integer.MIN_VALUE, -500, -1, 0, 1, 500 };
		String[] keys = { null, "", "   ", "increment blank", "increment" };

		Object[][] provides = new Object[values.length * keys.length][];

		int index = 0;

		for(String key : keys){
			for(int value : values){
				provides[index++] = new Object[]{ n++, getKey(key, n - 1), value };
			}
		}

		return provides;
	}

	@DataProvider(name = "incrementMapBy", parallel = true)
	public static Object[][] provideIncrementMapBy() {

		int n = 9501;

		int[] values = { Integer.MIN_VALUE, -500, -1, 0, 1, 500 };
		String[] keys = { null, "", "   ", "increment blank", "increment" };

		Object[][] provides = new Object[values.length * keys.length * keys.length][];

		int index = 0;

		for(String key : keys){
			for(String mapKey : keys){
				for(int value : values){
					provides[index++] = new Object[]{ n++, getKey(key, n - 1), mapKey, value };
				}
			}
		}

		return provides;
	}

	@DataProvider(name = "incrementTypeError", parallel = true)
	public static Object[][] provideIncrementTypeError() {

		int n = 9601;

		Integer[] incrementBys = { null, Integer.MIN_VALUE, -500, -1, 0, 1, 500 };
		String[] fields = { "field", "null" };

		Object[][] provides = new Object[incrementBys.length * fields.length][];

		int index = 0;

		for(String field : fields){
			for(Integer incrementBy : incrementBys){
				provides[index++] = new Object[]{ n++, getKey("user", n - 1), field, incrementBy };
			}
		}

		return provides;
	}

	@DataProvider(name = "executeInTransaction", parallel = true)
	public static Object[][] provideExecuteInTransaction() {

		String key = "exec:tx";
		String[] fields = { "field", null };
		Object[] values = { User.BOB, null };
		Integer[] expireSeconds = { EXPIRES_LONG, null };
		Integer[] incrementSeconds = { EXPIRES_LONG, null };
		int[] exceptionAfterStatements = { -1, 0, 1, 2, 5, 8, 9, 10, 11 };

		int testCount = fields.length * values.length * exceptionAfterStatements.length
				* ( expireSeconds.length + incrementSeconds.length );

		Object[][] results = new Object[testCount][];

		int incr = 10001;

		int index = 0;
		for(String field : fields){
			for(Object value : values){
				for(Integer expireSecond : expireSeconds){
					for(int exceptionAfterStatement : exceptionAfterStatements){
						results[index++] = new Object[]{ incr++, getKey(key, incr - 1), field, expireSecond, null,
								exceptionAfterStatement, value };
					}
				}
				for(Integer incrementSecond : incrementSeconds){
					for(int exceptionAfterStatement : exceptionAfterStatements){
						results[index++] = new Object[]{ incr++, getKey(key, incr - 1), field, null, incrementSecond,
								exceptionAfterStatement, value };
					}
				}
			}
		}

		return results;
	}

	@DataProvider(name = "list", parallel = true)
	public static Object[][] provideList() {

		String[] keys = { "user", null, "   ", "" };
		Object[][] allElements = new Object[][]{ { User.JERRY, User.BOB, User.TOM, "string value" },
				{ User.JERRY, User.BOB, User.TOM, null }, { User.JERRY, null, null, null },
				{ null, User.BOB, User.TOM, "string value" }, { null, null, User.TOM, "string value" },
				{ null, null, null, "string value" }, { null, null, null, null }, };
		Integer[] expireSeconds = { EXPIRES_LONG, EXPIRES_SHORT, null, 0, -EXPIRES_LONG };
		Integer[] incrementSeconds = { EXPIRES_LONG, EXPIRES_SHORT, null, 0, -EXPIRES_LONG };

		Object[][] results = new Object[keys.length * allElements.length
				* ( expireSeconds.length + incrementSeconds.length )][];

		int incr = 11001;

		int index = 0;
		for(String key : keys){
			for(Object[] elements : allElements){
				boolean argError = hasArgError(key, elements);
				for(Integer expireSecond : expireSeconds){
					results[index++] = new Object[]{ incr++, getKey(key, incr - 1), elements, expireSecond, null,
							argError };
				}
				for(Integer incrementSecond : incrementSeconds){
					results[index++] = new Object[]{ incr++, getKey(key, incr - 1), elements, null, incrementSecond,
							argError };
				}
			}
		}

		return results;
	}

	@DataProvider(name = "listPop", parallel = true)
	public static Object[][] provideListPop() {

		String[] keys = { "user", "user2", null, "   ", "" };
		Object[][] allElements = { { User.BOB, User.JERRY, User.TOM }, {}, null };

		Object[][] results = new Object[keys.length * allElements.length][];

		int incr = 11501;

		int index = 0;
		for(String key : keys){
			boolean isArgException = Tools.isBlank(key);
			for(Object[] elements : allElements){
				results[index++] = new Object[]{ incr++, getKey("user", incr - 1), elements, getKey(key, incr - 1),
						isArgException };
			}
		}

		return results;
	}

	@SuppressWarnings("rawtypes")
	@DataProvider(name = "zset", parallel = true)
	public static Object[][] provideZSet() {

		final SortedCond[] conditions = { new SortedCond(true), new SortedCond(true, 2, -1),
				new SortedCond(true, 2, 5), new SortedCond(true, -10D, 10D, 2, 3),
				new SortedCond(true, -10D, null, 2, -1), new SortedCond(true, null, 10D, 2, 3), new SortedCond(false),
				new SortedCond(false, 2, -1), new SortedCond(false, 2, 5), new SortedCond(false, -10D, 10D, 2, 3),
				new SortedCond(false, -10D, null, 2, -1), new SortedCond(false, null, 10D, 2, 3), };

		String[] keys = { "user", null, "   ", "" };
		SortedElement[][] allElements = new SortedElement[][]{ SortedTestElements.getElements(),
				SortedTestElements.getNullElements(0), SortedTestElements.getNullElements(5),
				SortedTestElements.getLastNullElements() };
		Integer[] expireSeconds = { EXPIRES_LONG, EXPIRES_SHORT, null };
		Integer[] incrementSeconds = { EXPIRES_LONG, EXPIRES_SHORT, null };

		Object[][] results = new Object[keys.length * allElements.length
				* ( expireSeconds.length + incrementSeconds.length ) + 1 * // key
				// not
				// error
				// data
				1 * // element not error data
				( expireSeconds.length + incrementSeconds.length ) * // expire
				( conditions.length - 1 )][];

		int incr = 12001;

		int index = 0;
		for(String key : keys){
			for(SortedElement[] elements : allElements){
				boolean argError = hasArgError(key, elements);
				for(Integer expireSecond : expireSeconds){
					if (argError) {
						results[index++] = new Object[]{ incr++, getKey(key, incr - 1), elements, expireSecond, null,
								argError, null, null };
					} else {
						for(SortedCond condition : conditions){
							results[index++] = new Object[]{ incr++, getKey(key, incr - 1), elements, expireSecond,
									null, argError, condition, SortedTestElements.sort(condition) };
						}
					}
				}
				for(Integer incrementSecond : incrementSeconds){
					if (argError) {
						results[index++] = new Object[]{ incr++, getKey(key, incr - 1), elements, null,
								incrementSecond, argError, null, null };
					} else {
						for(SortedCond condition : conditions){
							results[index++] = new Object[]{ incr++, getKey(key, incr - 1), elements, null,
									incrementSecond, argError, condition, SortedTestElements.sort(condition) };
						}
					}
				}
			}
		}

		return results;
	}

	@SuppressWarnings("rawtypes")
	@DataProvider(name = "zset-ignore", parallel = true)
	public static Object[][] provideZSetIgnore() {

		String key = "zsetIgnoreKey";
		Integer[] expires = { 0, -EXPIRES_SHORT, -EXPIRES_LONG };
		Integer[] expireAts = { 0, -EXPIRES_SHORT, -EXPIRES_LONG };
		Object[][] results = new Object[expires.length + expireAts.length][];
		SortedElement[] elements = SortedTestElements.getElements();
		int incr = 13001;
		int index = 0;
		for(Integer expire : expires){
			results[index++] = new Object[]{ incr++, key, elements, expire, null };
		}
		for(Integer expireAt : expireAts){
			results[index++] = new Object[]{ incr++, key, elements, null, expireAt };
		}
		return results;
	}

	@DataProvider(name = "nx-set", parallel = true)
	public static Object[][] provideNxSet() {

		boolean[] inits = { true, false };
		Object[] values = { User.JERRY, User.TOM, null };
		String[] keys = { null, "", "   ", getUserKey() };
		Integer[] expires = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };
		Integer[] expireAts = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };

		Object[][] results = new Object[inits.length * values.length * keys.length
				* ( expires.length + expireAts.length )][];

		int incr = 50000;
		int index = 0;
		for(String key : keys){
			for(boolean init : inits){
				for(Object value : values){
					for(Integer expire : expires){
						results[index++] = new Object[]{ incr++, init, getKey(key, incr - 1), value, expire, null };
					}
					for(Integer expireAt : expireAts){
						results[index++] = new Object[]{ incr++, init, getKey(key, incr - 1), value, null, expireAt };
					}
				}
			}
		}
		return results;
	}

	@DataProvider(name = "nx-set-map-entry", parallel = true)
	public static Object[][] provideNxSetMapEntry() {

		boolean[] inits = { true, false };
		String[] keys = { null, "", "   ", getUserKey() };
		String[] fields = { null, "", "   ", "field" };
		Object[] values = { User.JERRY, User.TOM, null };
		Integer[] expires = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };
		Integer[] expireAts = { null, EXPIRES_SHORT, EXPIRES_LONG, 0, -EXPIRES_SHORT, -EXPIRES_LONG };

		Object[][] results = new Object[inits.length * values.length * keys.length * fields.length
				* ( expires.length + expireAts.length )][];

		int incr = 51000;
		int index = 0;
		for(String key : keys){
			for(String field : fields){
				for(boolean init : inits){
					for(Object value : values){
						for(Integer expire : expires){
							results[index++] = new Object[]{ incr++, init, getKey(key, incr - 1), field, value,
									expire, null };
						}
						for(Integer expireAt : expireAts){
							results[index++] = new Object[]{ incr++, init, getKey(key, incr - 1), field, value, null,
									expireAt };
						}
					}
				}
			}
		}
		return results;
	}

	private static String getKey(String key, int num) {

		if (Tools.isBlank(key)) {
			return key;
		}
		return key + num;
	}

	@SuppressWarnings( { "rawtypes", "unchecked" })
	private static final class SortedTestElements {

		private static int index = 0;

		private static final SortedElement[] ELEMENTS = {
				new SortedElement<Object>("p" + ( index++ ), -Double.MAX_VALUE), // 0
				// 10
				new SortedElement<Object>("p" + ( index++ ), -10000), // 1 9
				new SortedElement<Object>("p" + ( index++ ), -10), // 2 8
				new SortedElement<Object>("p" + ( index++ ), -Math.PI), // 3 7
				new SortedElement<Object>("p" + ( index++ ), -Math.E), // 4 6
				new SortedElement<Object>("p" + ( index++ ), 0), // 5 5
				new SortedElement<Object>("p" + ( index++ ), Math.E), // 6 4
				new SortedElement<Object>("p" + ( index++ ), Math.PI), // 7 3
				new SortedElement<Object>("p" + ( index++ ), 10), // 8 2
				new SortedElement<Object>("p" + ( index++ ), 10000), // 9 1
				new SortedElement<Object>("p" + ( index++ ), Double.MAX_VALUE), // 10
				// 0
		};

		public static SortedElement[] getElements() {

			return ELEMENTS.clone();
		}

		public static SortedElement[] getLastNullElements() {

			return getNullElements(ELEMENTS.length - 1);
		}

		public static SortedElement[] getNullElements(int... nullIndex) {

			SortedElement[] elements = getElements();
			for(int i = 0; i < nullIndex.length; i++){
				elements[nullIndex[i]] = new SortedElement(null, elements[nullIndex[i]].getScore());
			}
			return elements;
		}

		public static SortedTreeExcept sort(SortedCond cond) {

			int start = search(cond.getMin(), true);
			int end = search(cond.getMax(), false);

			int count = cond.getCount() < 0 ? end - start - cond.getOffset() + 1 : cond.getCount();

			SortedElement[] elements = new SortedElement[count];

			if (cond.isAsc()) {
				for(int i = start + cond.getOffset(), p = 0; p < elements.length; p++, i++){
					elements[p] = ELEMENTS[i];
				}
			} else {
				for(int i = end - cond.getOffset(), p = 0; p < elements.length; p++, i--){
					elements[p] = ELEMENTS[i];
				}
			}

			SortedTreeExcept except = new SortedTreeExcept(end - start + 1, count, elements);

			return except;
		}

		private static int search(Double value, boolean isMin) {

			if (isMin) {
				if (value == null) {
					return 0;
				}
				for(int i = 0; i < ELEMENTS.length; i++){
					double score = ELEMENTS[i].getScore();
					if (value <= score) {
						return i;
					}
				}
				return ELEMENTS.length;
			} else {
				if (value == null) {
					return ELEMENTS.length - 1;
				}
				for(int i = ELEMENTS.length - 1; i >= 0; i--){
					double score = ELEMENTS[i].getScore();
					if (value >= score) {
						return i;
					}
				}
				return 0;
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private static boolean hasArgError(String key, Object[] elements) {

		if (Tools.isBlank(key)) {

			return true;
		}
		for(Object element : elements){
			if (element == null) {

				return true;
			}
			if (element instanceof SortedElement) {
				if (( (SortedElement) element ).getElement() == null) {

					return true;
				}
			}
		}
		return false;
	}

	@DataProvider(name = "cache-primitive")
	public static Object[][] providePrimitive() {

		return new Object[][]{
				{ 21001, "dev.byte", (byte) 'a' },
				{ 21002, "dev.short", (short) 'a' },
				{ 21003, "dev.char", (char) 'a' },
				{ 21004, "dev.int", (int) 'a' },
				{ 21005, "dev.long", (long) 'a' },
				{ 21006, "dev.float", (float) 'a' },
				{ 21007, "dev.double", (double) 'a' },
				{ 2100, "dev.boolean", false },
				{ 21008, "dev.biginteger", new BigInteger("10101010101010101010101010101010101010101010101010") },
				{ 21008, "dev.bigdecimal",
						new BigDecimal("10101010101010101010101010101010101010101010101010.7897787878787878787") } };
	}

	@DataProvider(name = "cache-method-primitive", parallel = true)
	public static Object[][] providePrimitives() {

		return new Object[][]{

				{ 22001, byte.class, (byte) 50 }, { 22002, short.class, (short) 50 }, { 22003, char.class, (char) 'a' },
				{ 22004, int.class, (int) 50 }, { 22005, long.class, (long) 50 }, { 22006, float.class, (float) 50 },
				{ 22007, double.class, (double) 50 }, { 22008, boolean.class, true } };
	}

	@DataProvider(name = "wrap-key")
	public static Object[][] provideWrapKey() {

		return new Object[][]{ { 23001, "test1" }, { 23002, "test2" }, { 23003, "test3" }, { 23004, "test4" },
				{ 23005, "test5" }, { 23006, "test6" }, { 23007, "test7" }, { 23008, "test8" }, { 23009, "test9" },
				{ 23010, "test10" } };
	}

	@DataProvider(name = "wrap-key-exception")
	public static Object[][] provideWrapKeyException() {

		return new Object[][]{ { 23101, null }, { 23102, "" }, { 23103, " " }, { 23104, "   " } };
	}

	@DataProvider(name = "wrap-keys")
	public static Object[][] provideWrapKeys() {

		return new Object[][]{ { 23201, new String[]{ "test1" } }, { 23202, new String[]{ "test1", "test2" } },
				{ 23203, new String[]{ "test1", "test2", "test3" } },
				{ 23204, new String[]{ "test1", "test2", "test3", "test4" } },
				{ 23205, new String[]{ "test1", "test2", "test3", "test4", "test5" } },
				{ 23206, new String[]{ "test1", "test2", "test3", "test4", "test5", "test6" } },
				{ 23207, new String[]{ "test1", "test2", "test3", "test4", "test5", "test6", "test7" } } };
	}

	@DataProvider(name = "wrap-keys-exception")
	public static Object[][] provideWrapKeysException() {

		return new Object[][]{ { 23301, null }, { 23302, new String[0] }, { 23303, new String[]{ null } },
				{ 23304, new String[]{ "test1", "" } }, { 23305, new String[]{ "test1", "test2", null } },
				{ 23306, new String[]{ "test1", "test2", "test3", "  " } },
				{ 23307, new String[]{ "test1", "test2", "test3", "test4", " " } } };
	}
}
