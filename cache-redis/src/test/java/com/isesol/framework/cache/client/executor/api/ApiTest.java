package com.isesol.framework.cache.client.executor.api;

import com.isesol.framework.cache.client.data.*;
import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.*;
import org.testng.*;
import org.testng.annotations.*;

import java.util.*;
import java.util.concurrent.*;

public class ApiTest extends BasicCacheExecutorTest {

	private static final char[] CHS = "0123456789abcdefghijklnnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

	@Test
	public void tempTest() throws CacheClientException {

		final String KEY = "k0";
		try {
			for(int i = 0; i < 10; i++){
				boolean result = executor.nxSetMapEntry(KEY, "f0", User.BOB, 2000);
				LOGGER.info("tempTest: {}. {}", i, result);
			}
			LOGGER.info("tempTest: key '{}' ttl is '{}'", KEY, executor.ttl(KEY));
		} finally {
			executor.del(KEY);
		}
	}

	@Test
	public void testLargeMessage() throws CacheClientException {

		final int length = 1024 * 1024;
		String value = generate(length);
		Assert.assertEquals(value.length(), length);
		executor.set("large.string.1M", value);
		executor.del("large.string.1M");
	}

	@Test(enabled = true, priority = 100, dataProvider = "cache-primitive", dataProviderClass = CacheExecutorDataProvider.class)
	public void testPrimitive(int id, String key, Object value) throws CacheClientException {

		executor.set(key, value);
		executor.del(key);
	}

	@Test
	public void testTransaction() throws CacheClientException {

		final Map<String, User> map = new HashMap<String, User>();
		map.put("tom", User.TOM);
		execute("first", map);
		execute("after", map);
		execute("err", map);
		execute("last", map);
	}

	private void execute(final String prefix, final Map<String, User> map) throws CacheClientException {

		LOGGER.info("----------------> prefix = {}", prefix);
		try {
			executor.executeInTransaction(new TransactionAction() {

				@Override
				public void doInTxAction(CacheExecutorOperation operation) throws CacheClientException {

					LOGGER.info("doInTransaction '{}', operation = {}", prefix, operation);
					executor.set(prefix + "0", User.TOM);

					if ("err".equals(prefix)) {
						throw new RuntimeException("manual throw exception");
					}

					executor.set(prefix + "1", User.TOM);

					LOGGER.info("executeInTransaction '{}' execute success", prefix);
				}
			});
		} catch ( CacheClientException e ) {
			LOGGER.error("executeInTransaction '{}' execute failed", prefix, e);
		}

		try {

			Object object0 = executor.get(prefix + '0');
			LOGGER.info("Get MapEntry, key = {}0, value = {}", prefix, object0);

			Object object1 = executor.get(prefix + '1');
			LOGGER.info("Get MapEntry, key = {}1, value = {}", prefix, object1);
		} catch ( CacheClientException e ) {
			LOGGER.error("executeInTransaction '{}' execute get failed", prefix, e);
		} finally {
			executor.del(prefix + '0', prefix + '1');
		}
	}

	@Test
	public void test() throws CacheClientException, InterruptedException {

		final int N = 10000;
		final CountDownLatch latch = new CountDownLatch(N);
		ExecutorService es = Executors.newFixedThreadPool(150);

		long t0, t1;

		t0 = System.currentTimeMillis();

		for(int i = 0; i < N; i++){
			final int n = i;
			es.submit(new Runnable() {

				@Override
				public void run() {

					try {
						String str = "test:" + n;
						executor.set(str, User.BOB, 5);
					} catch ( Exception e ) {
						e.printStackTrace();
					} finally {
						latch.countDown();
					}
				}
			});
		}

		latch.await();

		t1 = System.currentTimeMillis();

		es.shutdown();

		System.out.println("\n\n############performance test, average time: " + ( t1 - t0 ) / 1D / N + "ms\n\n");
	}

	@Test
	public void testListPop() throws CacheClientException {

		Object[] elements = { User.TOM, User.JERRY, User.BOB };
		for(Object element : elements){
			executor.listAppend("k0", element);
		}

		for(int i = 0; i < elements.length - 1; i++){
			Object value = executor.listPopHead("k0");
			Assert.assertNotNull(value);
			Assert.assertEquals(value, elements[i]);
		}

		Long length = executor.listLength("k0");

		LOGGER.info("testListPop, key 'k0' length is '{}'", length);
		executor.del("k0");
	}

	@Test
	public void testDelMapKeyTx() throws CacheClientException {

		final String key = "key";

		final Map<String, String> map = new HashMap<String, String>();
		map.put("k0", "0");
		map.put("k1", "1");
		map.put("k2", "2");

		try {

			executor.setMap(key, map);

			executor.executeInTransaction(new TransactionAction() {

				@Override
				public void doInTxAction(CacheExecutorOperation operation) throws CacheClientException {

					operation.delMapKey(key, "k0");
					// if ( "key".equals( key ) ) {
					// throw new IllegalStateException( "Manual threw exception"
					// );
					// }
					operation.delMapKey(key, "k1");
				}
			});
		} finally {
			executor.del(key);
		}
	}

	private String generate(int length) {

		char[] chs = new char[length];
		Random ran = new Random();
		for(int i = 0; i < chs.length; i++){
			chs[i] = CHS[ran.nextInt(CHS.length)];
		}
		return new String(chs);
	}
}
