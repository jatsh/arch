package com.isesol.framework.cache.client.executor.api;

import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.operation.*;

import java.util.*;

import static com.isesol.framework.cache.client.executor.CacheExecutorDataProvider.EXPIRES_SHORT;
import static com.isesol.framework.cache.client.executor.CacheExecutorDataProvider.createDate;

public final class ApiTestUtil {

	private ApiTestUtil() {

	}

	public static void set(CacheExecutor executor, String key, Object value, Integer expireSeconds,
						   Integer incrementSeconds) throws CacheClientException {

		if (expireSeconds != null) {
			executor.set(key, value, expireSeconds);
		} else if (incrementSeconds != null) {
			executor.set(key, value, createDate(incrementSeconds));
		} else {
			executor.set(key, value);
		}
	}

	public static void setMap(CacheExecutor executor, String key, Map<String, ?> map, Integer expireSeconds,
	                          Integer incrementSeconds) throws CacheClientException {

		if (expireSeconds != null) {
			executor.setMap(key, map, expireSeconds);
		} else if (incrementSeconds != null) {
			executor.setMap(key, map, createDate(incrementSeconds));
		} else {
			executor.setMap(key, map);
		}
	}

	public static void setMapEntry(CacheExecutor executor, String key, String field, Object value,
	                               Integer expireSeconds, Integer incrementSeconds) throws CacheClientException {

		if (incrementSeconds == null && expireSeconds == null) {
			executor.setMapEntry(key, field, value);
		} else if (incrementSeconds != null) {
			executor.setMapEntry(key, field, value, createDate(incrementSeconds));
		} else if (expireSeconds != null) {
			executor.setMapEntry(key, field, value, expireSeconds);
		}
	}

	public static void setMapEntries(CacheExecutor executor, String key, Map<String, ?> entries, Integer expireSeconds,
	                                 Integer incrementSeconds) throws CacheClientException {

		if (incrementSeconds == null && expireSeconds == null) {
			executor.setMapEntry(key, entries);
		} else if (incrementSeconds != null) {
			executor.setMapEntry(key, entries, createDate(incrementSeconds));
		} else if (expireSeconds != null) {
			executor.setMapEntry(key, entries, expireSeconds);
		}
	}

	public static void addListElements(CacheExecutor executor, String key, Object[] elements, Integer expireSeconds,
	                                   Integer incrementSeconds) throws CacheClientException {

		if (incrementSeconds != null) {
			Date expireAt = createDate(incrementSeconds);
			for(Object element : elements){
				executor.listAppend(key, element, expireAt);
			}
		} else if (expireSeconds != null) {
			for(Object element : elements){
				executor.listAppend(key, element, expireSeconds);
			}
		} else {
			for(Object element : elements){
				executor.listAppend(key, element);
			}
		}
	}

	public static void addZSetElements(CacheExecutor executor, String key, SortedElement<Object>[] elements,
	                                   Integer expireSeconds, Integer incrementSeconds) throws CacheClientException {

		if (incrementSeconds != null) {
			Date expireAt = createDate(incrementSeconds);
			for(SortedElement<Object> tree : elements){
				executor.treeAdd(key, tree.getElement(), tree.getScore(), expireAt);
			}
		} else if (expireSeconds != null) {
			for(SortedElement<Object> tree : elements){
				executor.treeAdd(key, tree.getElement(), tree.getScore(), expireSeconds);
			}
		} else {
			for(SortedElement<Object> tree : elements){
				executor.treeAdd(key, tree.getElement(), tree.getScore());
			}
		}
	}

	public static boolean nxSet(CacheExecutor executor, String key, Object value, Integer expireSeconds,
	                            Integer incrementSeconds) throws CacheClientException {

		if (expireSeconds != null) {
			return executor.nxSet(key, value, expireSeconds);
		}
		if (incrementSeconds != null) {
			return executor.nxSet(key, value, createDate(incrementSeconds));
		}
		return executor.nxSet(key, value);
	}

	public static boolean nxSetMapEntry(CacheExecutor executor, String key, String field, Object value,
	                                    Integer expireSeconds, Integer incrementSeconds) throws CacheClientException {

		if (expireSeconds != null) {
			return executor.nxSetMapEntry(key, field, value, expireSeconds);
		}
		if (incrementSeconds != null) {
			return executor.nxSetMapEntry(key, field, value, createDate(incrementSeconds));
		}
		return executor.nxSetMapEntry(key, field, value);
	}

	public static boolean isIgnore(Integer expireSeconds, Integer incrementSeconds) {

		if (incrementSeconds != null && incrementSeconds < 1) {
			return true;
		}
		if (incrementSeconds == null && expireSeconds != null && expireSeconds < 1) {
			return true;
		}
		return false;
	}

	public static boolean isShortExpires(Integer expireSeconds, Integer expireAtIncrement) {

		return EXPIRES_SHORT.equals(expireSeconds) || EXPIRES_SHORT.equals(expireAtIncrement);
	}

	public static void sleep() {

		try {
			Thread.sleep(EXPIRES_SHORT * 1000 * 3);
		} catch ( InterruptedException e ) {
			Thread.currentThread().interrupt();
		}
	}
}
