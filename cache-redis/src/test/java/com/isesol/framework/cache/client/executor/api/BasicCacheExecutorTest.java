package com.isesol.framework.cache.client.executor.api;

import com.isesol.framework.cache.client.*;
import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.*;
import org.apache.logging.log4j.*;
import org.springframework.test.context.*;
import org.springframework.test.context.testng.*;
import org.testng.*;
import org.testng.annotations.*;

import javax.annotation.*;
import java.lang.reflect.*;

@ContextConfiguration(locations = "classpath:/executor/api/application-context.xml")
public class BasicCacheExecutorTest extends AbstractTestNGSpringContextTests {

	Logger LOGGER = LogManager.getLogger(this.getClass());

	//	@Autowired
//	@Qualifier("cacheExecutor")
//	protected CacheExecutor executor;
//
	@SuppressWarnings("SpringJavaAutowiringInspection")
	@Resource(name = "cacheExecutor")
	protected CacheExecutor executor;

	@BeforeMethod
	public void before() throws CacheClientException {

		executor.del(CacheExecutorDataProvider.getUserKey());

	}

	@AfterMethod
	public void after() throws CacheClientException {

		executor.del(CacheExecutorDataProvider.getUserKey());
	}

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {

		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {

		TestingUtils.logAfter(result);
	}

	@Test(priority = 1)
	public void testExecutorNotNull() {

		Assert.assertNotNull(executor);
	}
}
