package com.isesol.framework.cache.client.executor.api;

import com.isesol.framework.cache.client.data.*;
import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.executor.exception.*;
import com.isesol.framework.cache.client.executor.operation.*;
import com.isesol.framework.cache.client.util.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.math.*;
import java.util.*;

import static com.isesol.framework.cache.client.executor.CacheExecutorDataProvider.*;

public class CacheExecutorTest extends BasicCacheExecutorTest {

	@SuppressWarnings("unchecked")
	@Test(groups = "cache-executor", priority = 100, dataProvider = "set-object", dataProviderClass = CacheExecutorDataProvider.class)
	public void testSetObject(Integer id, String key, Object value, Integer expireSeconds, Integer incrementSeconds)
			throws CacheClientException, InterruptedException {

		try {

			boolean isIgnore = isIgnore(expireSeconds, incrementSeconds);
			Exception x = null;
			try {
				if (value instanceof Map) {
					ApiTestUtil.setMap(executor, key, (Map<String, ?>) value, expireSeconds, incrementSeconds);
				} else {
					ApiTestUtil.set(executor, key, value, expireSeconds, incrementSeconds);
				}
			} catch (Exception e) {
				x = e;
			}

			if (Tools.isBlank(key)) {
				Assert.assertTrue(x instanceof IllegalArgumentException);
				return;
			}

			if (isShortExpires(expireSeconds, incrementSeconds)) {
				sleep();
			}

			Object actual = executor.get(key);

			if (isShortExpires(expireSeconds, incrementSeconds)) {
				Assert.assertNull(actual);
				return;
			}

			if (isIgnore) {
				Assert.assertNull(actual);
				return;
			}

			if (value != null) {
				Assert.assertNotNull(actual);
			}

			Assert.assertEquals(actual, value);
		} finally {
			if (!Tools.isBlank(key)) {
				executor.del(key);
			}
		}
	}

	@Test(groups = "cache-executor", priority = 200, dataProvider = "set-map", dataProviderClass = CacheExecutorDataProvider.class)
	public void testSetMap(Integer id, String key, Map<String, User> map, Integer expireSeconds,
						   Integer incrementSeconds, boolean usingSetObject) throws CacheClientException, InterruptedException {

		boolean isIgnore = isIgnore(expireSeconds, incrementSeconds);

		boolean isExpired = isShortExpires(expireSeconds, incrementSeconds);

		try {

			Exception x = null;

			try {
				if (usingSetObject) {
					ApiTestUtil.set(executor, key, map, expireSeconds, incrementSeconds);
				} else {
					ApiTestUtil.setMap(executor, key, map, expireSeconds, incrementSeconds);
				}
			} catch (Exception e) {
				x = e;
			}

			if (Tools.isBlank(key) || (!Tools.isBlank(map) && isEmptyKey(map))) {
				Assert.assertNotNull(x);
				Assert.assertTrue(x instanceof IllegalArgumentException);
				return;
			}

			if (isExpired) {
				sleep();
			}

			Map<String, User> getMap = null;
			try {
				getMap = executor.getMap(key);
			} catch (CacheClientException e) {
				if (usingSetObject) {
					Assert.assertEquals(UnsupportedOperationException.class, e.getCause().getClass());
					Object object = executor.get(key);
					if (isIgnore) {
						Assert.assertNotNull(object);
					}
					return;
				}
			}

			if (isExpired || isIgnore || Tools.isBlank(getMap)) {
				Assert.assertNull(getMap);
				return;
			}

			if (map != null) {
				Assert.assertNotNull(getMap);
			}

			Assert.assertEquals(getMap, map);
		} finally {
			if (!Tools.isBlank(key)) {
				executor.del(key);
			}
		}
	}

	@Test(groups = "cache-executor", priority = 300, dataProvider = "set-map-entry", dataProviderClass = CacheExecutorDataProvider.class, description = "setMapEntry(key, mapKey, value) & setMapEntry(key, mapKey, value, int) & setMapEntry(key, mapKey, value, Date)")
	public void testSetMapEntry(Integer id, String key, Map<String, User> existsMap, String field, User user,
			Integer expireSeconds, Integer incrementAt, Long exceptedTTLGreater) throws CacheClientException {

		try {

			Integer existsExpires = null;

			if (existsMap != null) {
				existsExpires = EXPIRES_LONG * 3;
				executor.setMap(key, existsMap, existsExpires);
			}

			boolean isExpire = isShortExpires(expireSeconds, incrementAt);

			Exception x = null;

			try {
				ApiTestUtil.setMapEntry(executor, key, field, user, expireSeconds, incrementAt);
			} catch (Exception e) {
				x = e;
			}

			if (Tools.isBlank(field)) {
				Assert.assertNotNull(x);
				Assert.assertTrue(x instanceof IllegalArgumentException);
				return;
			}

			if (isExpire) {
				sleep();
			}

			User cachedUser = executor.getMapEntry(key, field);

			LOGGER.info("id = {}, cached: {} -> {}", id, field, cachedUser);

			if (isExpire) {
				Assert.assertFalse(executor.hasKey(key));
				Assert.assertNull(cachedUser);
				return;
			}

			Assert.assertNotNull(cachedUser);
			Assert.assertEquals(cachedUser, user);

			Long ttl = executor.ttl(key);
			Assert.assertNotNull(ttl);
			Assert.assertTrue(ttl > exceptedTTLGreater);

			if (existsExpires != null) {
				Map<String, User> users = executor.getMap(key);
				Assert.assertNotNull(users);
				Assert.assertEquals(users.size(), existsMap.size());
			}
		} finally {
			if (key != null) {
				executor.del(key);
			}
		}
	}

	@Test(groups = "cache-executor", priority = 400, dataProvider = "set-map-entries", dataProviderClass = CacheExecutorDataProvider.class, description = "setMapEntry(key, map)")
	public void testSetMapEntries(Integer id, String key, Map<String, User> existsMap, Map<String, User> entries,
			Integer expireSeconds, Integer incrementAt, Long exceptedTTLGreater) throws CacheClientException {

		try {

			Integer existsExpires = null;

			if (existsMap != null) {
				existsExpires = EXPIRES_LONG * 3;
				executor.setMap(key, existsMap, existsExpires);
			}

			boolean isExpire = isShortExpires(expireSeconds, incrementAt);

			Exception x = null;

			try {
				ApiTestUtil.setMapEntries(executor, key, entries, expireSeconds, incrementAt);
			} catch (Exception e) {
				x = e;
			}

			if (isEmptyKey(entries)) {
				Assert.assertNotNull(x);
				Assert.assertTrue(x instanceof IllegalArgumentException);
				return;
			}

			if (isExpire) {
				sleep();
			}

			if (isExpire) {
				Assert.assertFalse(executor.hasKey(key));
				return;
			}

			for (Map.Entry<String, User> entry : entries.entrySet()) {
				User cachedUser = executor.getMapEntry(key, entry.getKey());
				LOGGER.info("id = {}, cached: {} -> {}", id, entry.getKey(), cachedUser);
				Assert.assertNotNull(cachedUser);
				Assert.assertEquals(cachedUser, entry.getValue());
			}

			Long ttl = executor.ttl(key);
			LOGGER.info("id = {}, key = {}, ttl = {}", id, key, ttl);
			Assert.assertNotNull(ttl);
			Assert.assertTrue(ttl > exceptedTTLGreater);

			if (existsMap != null) {
				Map<String, User> map = new HashMap<String, User>(existsMap);
				map.putAll(entries);
				Map<String, User> users = executor.getMap(key);
				Assert.assertNotNull(users);
				Assert.assertEquals(users.size(), map.size());
			}
		} finally {
			if (key != null) {
				executor.del(key);
			}
		}
	}

	@Test(groups = "cache-executor", priority = 500, dataProvider = "has-key", dataProviderClass = CacheExecutorDataProvider.class, description = "hasKey(key)")
	public void testHasKey(Integer id, String key, boolean isPreAdd, User user, boolean excepted)
			throws CacheClientException {

		Exception argumentException = null;

		if (isPreAdd) {

			try {

				executor.set(getUserKey(), user);

			} catch (Exception ignore) {
			}
		}

		try {

			boolean result = executor.hasKey(key);

			Assert.assertEquals(result, excepted);

			// clearing testing data
			if (result) {

				executor.del(key);
			}
		} catch (Exception e) {

			LOGGER.error("id = " + id + ", key = '" + key + "'", e);

			argumentException = e;
		}

		if (Tools.isBlank(key)) {

			Assert.assertTrue(argumentException instanceof IllegalArgumentException);

			return;
		}

		Assert.assertNull(argumentException);
	}

	@Test(groups = "cache-executor", priority = 600, dataProvider = "expireSeconds", dataProviderClass = CacheExecutorDataProvider.class, description = "expire(key, expireSeconds)")
	public void testExpire(Integer id, final String key, final int setSeconds, final int expireSeconds)
			throws CacheClientException {

		try {
			executor.set(key, getUserBob(), setSeconds);
		} catch (Exception ignore) {
		}

		Exception exceptedException = null;
		try {
			executor.expire(key, expireSeconds);
		} catch (IllegalArgumentException e) {
			exceptedException = e;
		}

		if (Tools.isBlank(key)) {

			Assert.assertTrue(exceptedException instanceof IllegalArgumentException);

			return;
		}

		Long ex1 = executor.ttl(key);

		Assert.assertNotNull(ex1);

		if (expireSeconds > setSeconds) {

			Assert.assertTrue(ex1 > setSeconds);
		}

		if (expireSeconds < setSeconds) {

			Assert.assertTrue(ex1 < (setSeconds / 2));
		}
	}

	@Test(groups = "cache-executor", priority = 700, dataProvider = "expireAt", dataProviderClass = CacheExecutorDataProvider.class, description = "expireAt(key, expireAt)")
	public void testExpireAt(Integer id, final String key, final int setSeconds, final int secondsIncrement)
			throws CacheClientException {

		try {
			executor.set(key, getUserBob(), setSeconds);
		} catch (Exception ignore) {
		}

		Exception exceptedException = null;
		try {
			executor.expireAt(key, createDate(secondsIncrement));
		} catch (IllegalArgumentException e) {

			LOGGER.info("testExpireAt, id = " + id + ", key = '" + key + "'", e);
			exceptedException = e;
		}

		if (Tools.isBlank(key)) {

			Assert.assertTrue(exceptedException instanceof IllegalArgumentException);

			return;
		}

		Long ex1 = executor.ttl(key);

		LOGGER.info("testExpireAt, id = " + id + ", key = '" + key + "'" + ", expires = " + setSeconds
				+ "', after expireAt ttl = " + ex1);

		Assert.assertNotNull(ex1);

		if (secondsIncrement > setSeconds) {

			Assert.assertTrue(ex1 > setSeconds);
		}

		if (secondsIncrement < setSeconds) {

			Assert.assertTrue(ex1 <= setSeconds && ex1 > setSeconds - 5);
		}
	}

	@Test(groups = "cache-executor", priority = 800, dataProvider = "del", dataProviderClass = CacheExecutorDataProvider.class, description = "del(keys...)")
	@SuppressWarnings("unchecked")
	public void testDel(Integer id, final String key, Object value, boolean usingHash) throws CacheClientException {

		if (usingHash && value instanceof Map) {

			executor.setMap(getUserKey(), (Map<String, ?>) value);
		} else {

			executor.set(getUserKey(), value);
		}

		Exception x = null;

		try {

			executor.del(key);
		} catch (Exception e) {

			x = e;
		}

		if (Tools.isBlank(key)) {

			Assert.assertTrue(x instanceof IllegalArgumentException);

			return;
		}

		Object object = executor.get(getUserKey());

		if (getUserKey().equals(key)) {

			Assert.assertNull(object);
		} else {

			Assert.assertNotNull(object);
			Assert.assertEquals(object, value);
		}
	}

	@Test(groups = "cache-executor", priority = 850, dataProvider = "delMapKey", dataProviderClass = CacheExecutorDataProvider.class, description = "delMapKeys(key, fields...)")
	public void testDelMapKey(Integer id, final String key, Map<String, String> data, final String[] mapKeys, int remain)
			throws CacheClientException {

		try {
			executor.setMap(key, data);
			Exception excepted = null;
			try {

				executor.executeInTransaction(new TransactionAction() {
					@Override
					public void doInTxAction(CacheExecutorOperation operation) throws CacheClientException {
						for (String mapKey : mapKeys) {
							operation.delMapKey(key, mapKey);
						}
					}
				});
			} catch (Exception e) {
				excepted = e;
			}

			if (Tools.isBlank(key) || hasEmpty(mapKeys)) {
				Assert.assertNotNull(excepted);
				Assert.assertEquals(excepted.getClass(), IllegalArgumentException.class);
				return;
			}

			Map<String, User> map = executor.getMap(key);

			if (remain < 1) {
				Assert.assertNull(map);
			} else {
				Assert.assertNotNull(map);
				Assert.assertEquals(map.size(), remain);
			}
		} finally {
			if (!Tools.isBlank(key)) {
				executor.del(key);
			}
		}
	}

	private boolean hasEmpty(String[] strs) {
		if (Tools.isBlank(strs)) {
			return true;
		}
		for (String str : strs) {
			if (Tools.isBlank(str)) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Test(groups = "cache-executor", priority = 900, dataProvider = "mget", dataProviderClass = CacheExecutorDataProvider.class, description = "mget")
	public void testMGet(Integer id, String key, Object[] values, int length) throws CacheClientException {

		String[] keys = new String[length];

		for (int i = 0; i < length; i++) {
			keys[i] = key + "." + i;
		}

		try {
			boolean[] maps = new boolean[length];
			for (int i = 0; i < length; i++) {
				if (values[i] instanceof Map) {
					executor.setMap(keys[i], (Map<String, Object>) values[i]);
					maps[i] = true;
				} else {
					executor.set(keys[i], values[i]);
					maps[i] = false;
				}
			}

			List<Object> list = executor.mget(keys);

			Assert.assertNotNull(list);
			Assert.assertEquals(list.size(), keys.length);

			for (int i = 0; i < length; i++) {

				if (maps[i]) {
					Assert.assertNull(list.get(i));
				} else if (values[i] instanceof Character) {
					Assert.assertTrue(list.get(i) instanceof String);
					Assert.assertEquals(((String) list.get(i)).length(), 1);
					Assert.assertEquals(((String) list.get(i)).charAt(0), values[i]);
				} else if (values[i] instanceof Double && list.get(i) instanceof BigDecimal) {
					Assert.assertEquals(((BigDecimal) list.get(i)).doubleValue(), values[i]);
				} else if (values[i] instanceof Float && list.get(i) instanceof BigDecimal) {
					Assert.assertEquals(((BigDecimal) list.get(i)).floatValue(), values[i]);
				} else {
					Assert.assertEquals(list.get(i), values[i]);
				}
			}
		} finally {
			executor.del(keys);
		}
	}

	@Test(groups = "cache-executor", priority = 910, dataProvider = "incrementBy", dataProviderClass = CacheExecutorDataProvider.class, description = "increment")
	public void testIncrement(Integer id, String key, int delta) throws CacheClientException {
		IllegalArgumentException exception = null;
		final int N = 50;

		int start = 0;
		try {
			for (int i = 0; i < 50; i++) {
				Number result = executor.increment(key);
				Assert.assertNotNull(result);
				Assert.assertEquals(result.intValue(), ++start);
			}
		} catch (IllegalArgumentException e) {
			exception = e;
		}
		if (Tools.isBlank(key)) {
			Assert.assertNotNull(exception);
			return;
		}

		try {
			Number value = (Number) executor.get(key);
			Assert.assertNotNull(value);
			Assert.assertEquals(value.intValue(), N);
		} finally {
			executor.del(key);
		}
	}

	@Test(groups = "cache-executor", priority = 920, dataProvider = "incrementBy", dataProviderClass = CacheExecutorDataProvider.class, description = "incrementBy")
	public void testIncrementBy(Integer id, String key, int delta) throws CacheClientException {

		IllegalArgumentException exception = null;

		try {
			Number result = executor.incrementBy(key, delta);
			Assert.assertNotNull(result);
			Assert.assertEquals(result.intValue(), delta);
		} catch (IllegalArgumentException e) {
			exception = e;
		}

		if (Tools.isBlank(key)) {
			Assert.assertNotNull(exception);
			return;
		}

		try {
			Number value = (Number) executor.get(key);
			Assert.assertNotNull(value);
			Assert.assertEquals(value.intValue(), delta);
		} finally {
			executor.del(key);
		}
	}

	@Test(groups = "cache-executor", priority = 930, dataProvider = "incrementMapBy", dataProviderClass = CacheExecutorDataProvider.class, description = "incrementMap")
	public void testIncrementMap(Integer id, String key, String mapKey, int delta) throws CacheClientException {
		IllegalArgumentException exception = null;
		final int N = 50;
		int start = 0;
		try {
			for (int i = 0; i < 50; i++) {
				Number result = executor.increment(key, mapKey);
				Assert.assertNotNull(result);
				Assert.assertEquals(result.intValue(), ++start);
			}
		} catch (IllegalArgumentException e) {
			exception = e;
		}
		if (Tools.isBlank(key) || Tools.isBlank(mapKey)) {
			Assert.assertNotNull(exception);
			return;
		}

		try {
			Number value = (Number) executor.getMapEntry(key, mapKey);
			Assert.assertNotNull(value);
			Assert.assertEquals(value.intValue(), N);
		} finally {
			executor.del(key);
		}
	}

	@Test(groups = "cache-executor", priority = 940, dataProvider = "incrementMapBy", dataProviderClass = CacheExecutorDataProvider.class, description = "incrementMapBy")
	public void testIncrementMapBy(Integer id, String key, String mapKey, int delta) throws CacheClientException {
		IllegalArgumentException exception = null;

		try {
			Number result = executor.incrementBy(key, mapKey, delta);
			Assert.assertNotNull(result);
			Assert.assertEquals(result.intValue(), delta);
		} catch (IllegalArgumentException e) {
			exception = e;
		}

		if (Tools.isBlank(key) || Tools.isBlank(mapKey)) {
			Assert.assertNotNull(exception);
			return;
		}

		try {
			Number value = (Number) executor.getMapEntry(key, mapKey);
			Assert.assertNotNull(value);
			Assert.assertEquals(value.intValue(), delta);
		} finally {
			executor.del(key);
		}
	}

	@Test(groups = "cache-executor", priority = 950, expectedExceptions = CacheTypeCastException.class, dataProvider = "incrementTypeError", dataProviderClass = CacheExecutorDataProvider.class, description = "key is not number")
	public void testIncrementTypeError(Integer id, String key, String field, Integer incrementBy)
			throws CacheClientException {
		try {
			if (field == null) {
				executor.set(key, User.TOM);
				if (incrementBy == null) {
					executor.increment(key);
				} else {
					executor.incrementBy(key, incrementBy);
				}
			} else {
				executor.setMapEntry(key, field, User.TOM);
				if (incrementBy == null) {
					executor.increment(key, field);
				} else {
					executor.incrementBy(key, field, incrementBy);
				}
			}
		} finally {
			executor.del(key);
		}
	}

	@Test(groups = "cache-executor", priority = 2000, expectedExceptions = CacheTypeCastException.class, description = "getMapEntry that cached data is not hash type")
	public void testGetMapEntryException() throws CacheClientException {
		try {
			executor.set("key", "value");
			executor.getMapEntry("key", "field");
		} finally {
			executor.del("key");
		}
	}

	@Test(groups = "cache-executor", priority = 2010, expectedExceptions = CacheTypeCastException.class, description = "mgetMapEntry that cached data has none hash type")
	public void testMGetMapEntryException() throws CacheClientException {
		try {
			executor.set("key", "value");
			executor.mgetMapEntry("key", "field0", "field1");
		} finally {
			executor.del("key");
		}
	}

	@Test(groups = "cache-executor", priority = 2020, description = "set expire seconds is negative")
	public void testSetExpiresSecondsNegative() throws CacheClientException {
		try {
			executor.set("key", "value", Integer.MIN_VALUE);
			Assert.assertFalse(executor.hasKey("key"));
			String value = executor.get("key");
			Assert.assertNull(value);
		} finally {
			executor.del("key");
		}
	}

	@SuppressWarnings("serial")
	@Test(groups = "cache-executor", priority = 2022, description = "set map expire seconds is negative")
	public void testSetMapExpiresSecondsNegative() throws CacheClientException {
		try {
			executor.setMap("key", new HashMap<String, String>() {
				{
					put("field0", "value0");
				}
			}, -1);
			Assert.assertFalse(executor.hasKey("key"));
			Map<String, String> map = executor.get("key");
			Assert.assertNull(map);
		} finally {
			executor.del("key");
		}
	}

	@Test(groups = "cache-executor", priority = 2025, description = "set map entry that cache key is not hash type")
	public void testExpireSecondsNegative() throws CacheClientException {
		try {
			executor.set("key", "value", 1000);
			executor.expire("key", -1);
			Long value = executor.ttl("key");
			Assert.assertNotNull(value);
			Assert.assertEquals(value.longValue() > 500, true);
		} finally {
			executor.del("key");
		}
	}

	@Test(groups = "cache-executor", priority = 2030, description = "set expireAt is lesser that current time")
	public void testSetExpireAtLesserCurrent() throws CacheClientException {
		try {
			executor.set("key", "value", new Date(System.currentTimeMillis() - 100));
			Assert.assertFalse(executor.hasKey("key"));
			String value = executor.get("key");
			Assert.assertNull(value);
		} finally {
			executor.del("key");
		}
	}

	@Test(groups = "cache-executor", priority = 2040, description = "set map entry that is empty")
	public void testSetMapEntryMapEntryIsEmpty() throws CacheClientException {
		try {
			executor.setMapEntry("key", new HashMap<String, String>());
			Assert.assertFalse(executor.hasKey("key"));
		} finally {
			executor.del("key");
		}
	}

	@SuppressWarnings("serial")
	@Test(groups = "cache-executor", priority = 2050, description = "set map entry that cache key not exists")
	public void testSetMapEntryKeyNotExists() throws CacheClientException {
		try {
			executor.setMapEntry("key", new HashMap<String, String>() {
				{
					put("field", "value");
				}
			});
			String value = executor.getMapEntry("key", "field");
			Assert.assertNotNull(value);
			Assert.assertEquals(value, "value");
		} finally {
			executor.del("key");
		}
	}

	@SuppressWarnings("serial")
	@Test(groups = "cache-executor", priority = 2060, expectedExceptions = CacheTypeCastException.class, description = "set map entry that cache key is not hash type")
	public void testSetMapEntryKeyNotHash() throws CacheClientException {
		try {
			executor.set("key", "value");
			executor.setMapEntry("key", new HashMap<String, String>() {
				{
					put("field", "value");
				}
			});
		} finally {
			executor.del("key");
		}
	}

	@Test(groups = "cache-executor", priority = 3000, description = "execution operation in transaction", dataProvider = "executeInTransaction", dataProviderClass = CacheExecutorDataProvider.class)
	public void testExecuteInTransaction(final Integer id, final String key, final String field,
			final Integer expireSeconds, final Integer incrementSeconds, final int exceptionAfterStatementLine,
			final Object value) throws CacheClientException {

		final int executingCount = 10;

		final String[] keys = new String[executingCount];
		for (int i = 0; i < keys.length; i++) {
			keys[i] = key + ":" + (i + 1);
		}

		Exception exceptedException = null;

		try {
			executor.executeInTransaction(new TransactionAction() {

				@Override
				public void doInTxAction(CacheExecutorOperation operation) throws CacheClientException {
					for (int i = 0; i < keys.length; i++) {
						int line = i + 1;
						doTransaction(keys[i], field, value, expireSeconds, incrementSeconds);
						if (exceptionAfterStatementLine > 0 && line == exceptionAfterStatementLine) {
							throw new RuntimeException("Test transaction exception, after statement " + line
									+ ", id = " + id);
						}
					}
				}
			});
		} catch (CacheClientException e) {
			exceptedException = e;
		}

		try {

			if (exceptionAfterStatementLine > 0 && exceptionAfterStatementLine <= executingCount) {

				Assert.assertNotNull(exceptedException);
				Assert.assertEquals(exceptedException.getClass(), CacheSystemException.class);
				for (int i = 0; i < keys.length; i++) {
					Object result = getValue(keys[i], field, value);
					Assert.assertNull(result);
				}
			} else {

				Assert.assertNull(exceptedException);

				for (int i = 0; i < executingCount; i++) {
					Object result = getValue(keys[i], field, value);
					Assert.assertNotNull(result);
					if (value == null) {
						Assert.assertTrue(Number.class.isAssignableFrom(result.getClass()));
						Assert.assertEquals(executingCount, ((Number) result).intValue());
					} else {
						Assert.assertEquals(value, result);
					}
				}
			}
		} finally {
			executor.del(keys);
			executor.del(key);
		}
	}

	@Test(groups = "cache-executor", priority = 3100, description = "execute list operation", dataProvider = "list", dataProviderClass = CacheExecutorDataProvider.class)
	public void testList(Integer id, String key, Object[] elements, Integer expireSeconds, Integer incrementSeconds,
			boolean isArgException) throws CacheClientException {

		boolean isIgnored = isIgnore(expireSeconds, incrementSeconds);

		try {

			IllegalArgumentException exception = null;

			try {
				ApiTestUtil.addListElements(executor, key, elements, expireSeconds, incrementSeconds);
			} catch (IllegalArgumentException e) {
				exception = e;
			}

			if (isArgException) {
				Assert.assertNotNull(exception);
				return;
			}

			boolean expire = false;

			if (EXPIRES_SHORT.equals(incrementSeconds)
					|| (incrementSeconds == null && EXPIRES_SHORT.equals(expireSeconds))) {
				sleep();
				expire = true;
			}

			Long ttl = executor.ttl(key);
			Assert.assertNotNull(ttl);

			if (isIgnored) {
				Assert.assertEquals(ttl.intValue(), -1);
			} else if (EXPIRES_SHORT.equals(incrementSeconds)) {
				Assert.assertEquals(ttl.intValue(), -1);
			} else if (EXPIRES_LONG.equals(incrementSeconds)) {
				Assert.assertTrue(ttl > EXPIRES_LONG - 5);
			} else if (EXPIRES_SHORT.equals(expireSeconds)) {
				Assert.assertEquals(ttl.intValue(), -1);
			} else if (EXPIRES_LONG.equals(expireSeconds)) {
				Assert.assertTrue(ttl > EXPIRES_LONG - 5);
			} else {
				Assert.assertTrue(ttl > 1795);
			}

			Long length = executor.listLength(key);
			Assert.assertNotNull(length);

			isIgnored = expire || isIgnored;

			if (isIgnored) {
				Assert.assertEquals(length.intValue(), -1);
			} else {
				Assert.assertEquals(length.intValue(), elements.length);
			}

			executor.listRemoveFirst(key, elements[0]);

			List<Object> list = executor.listElements(key);

			if (isIgnored) {
				Assert.assertNull(list);
			} else {
				Assert.assertNotNull(list);
				Assert.assertEquals(list.size(), elements.length - 1);

				int index = 1;
				for (Object object : list) {
					Assert.assertEquals(object, elements[index++]);
				}
			}
		} finally {
			if (!Tools.isBlank(key)) {
				executor.del(key);
			}
		}
	}

	@Test(groups = "cache-executor", priority = 3110, description = "execute list pop operation", dataProvider = "listPop", dataProviderClass = CacheExecutorDataProvider.class)
	public void testListPop(Integer id, String key, Object[] elements, String popKey, boolean isArgException)
			throws CacheClientException {

		try {
			if (elements != null) {
				for (Object element : elements) {
					executor.listAppend(key, element);
				}
			}

			Exception ex = null;

			try {

				if (elements != null) {
					for (Object element : elements) {
						Object pop = executor.listPopHead(popKey);
						if (key.equals(popKey)) {
							Assert.assertNotNull(pop);
							Assert.assertEquals(pop, element);
						} else {
							Assert.assertNull(pop);
						}
					}
				}

				Object pop = executor.listPopHead(popKey);
				Assert.assertNull(pop);

				Long length = executor.listLength(popKey);

				if (key.equals(popKey)) {
					Assert.assertNotNull(length);
				}
				Assert.assertEquals(length.intValue(), -1);
			} catch (Exception e) {
				ex = e;
			}

			if (isArgException) {
				Assert.assertNotNull(ex);
				Assert.assertEquals(ex.getClass(), IllegalArgumentException.class);
			}
		} finally {
			if (!Tools.isBlank(key)) {
				executor.del(key);
			}
		}
	}

	@Test(groups = "cache-executor", priority = 3200, description = "execute zaset operation", dataProvider = "zset", dataProviderClass = CacheExecutorDataProvider.class)
	public void testZSet(Integer id, String key, SortedElement<Object>[] elements, Integer expireSeconds,
						 Integer incrementSeconds, boolean isArgException, SortedCond cond, SortedTreeExcept sortedExcept)
			throws CacheClientException, InterruptedException {

		try {

			IllegalArgumentException exception = null;

			try {
				ApiTestUtil.addZSetElements(executor, key, elements, expireSeconds, incrementSeconds);
			} catch (IllegalArgumentException e) {
				exception = e;
			}

			if (isArgException) {
				Assert.assertNotNull(exception);
				return;
			}

			boolean expire = false;

			if (EXPIRES_SHORT.equals(incrementSeconds)
					|| (incrementSeconds == null && EXPIRES_SHORT.equals(expireSeconds))) {
				sleep();
				expire = true;
			}

			Long ttl = executor.ttl(key);
			Assert.assertNotNull(ttl);
			if (EXPIRES_SHORT.equals(incrementSeconds)) {
				Assert.assertTrue(ttl < 0);
			} else if (EXPIRES_LONG.equals(incrementSeconds)) {
				Assert.assertTrue(ttl > EXPIRES_LONG - 5);
			} else if (EXPIRES_SHORT.equals(expireSeconds)) {
				Assert.assertTrue(ttl < 0);
			} else if (EXPIRES_LONG.equals(expireSeconds)) {
				Assert.assertTrue(ttl > EXPIRES_LONG - 5);
			} else {
				Assert.assertTrue(ttl > 1795);
			}

			for (SortedElement<Object> tree : elements) {
				Double score = executor.treeElementScore(key, tree.getElement());
				if (expire) {
					Assert.assertNull(score);
				} else {
					Assert.assertNotNull(score);
					Assert.assertEquals(score.doubleValue(), tree.getScore());
				}
			}

			SortedTree<User> users = executor.treeSort(key, cond);

			if (expire) {
				Assert.assertEquals(users.getElementsSize(), 0);
			} else {

				Assert.assertNotNull(users);
				Assert.assertEquals(users.getTotal(), sortedExcept.getTotal());
				Assert.assertEquals(users.getElementsSize(), sortedExcept.getCount());

				int index = 0;
				for (SortedElement<User> user : users) {
					Assert.assertEquals(user, sortedExcept.getElement(index++));
				}
			}

			executor.treeRemove(key, elements[0].getElement());

			SortedTree<User> all = executor.treeSort(key, SORTED_CONDITION_ALL);

			if (expire) {
				Assert.assertEquals(all.getElementsSize(), 0);
			} else {
				Assert.assertNotNull(all);
				Assert.assertEquals(all.getTotal(), all.getElementsSize());
				Assert.assertEquals(all.getTotal(), elements.length - 1);
			}
		} finally {
			if (!Tools.isBlank(key)) {
				executor.del(key);
			}
		}
	}

	@Test(groups = "cache-executor", priority = 3210, description = "execute zset ignore", dataProvider = "zset-ignore", dataProviderClass = CacheExecutorDataProvider.class)
	public void testZSetIgnore(Integer id, String key, SortedElement<Object>[] elements, Integer expireSeconds,
			Integer incrementSeconds) throws CacheClientException {
		try {
			if (incrementSeconds != null) {
				Date expireAt = createDate(incrementSeconds);
				for (SortedElement<Object> element : elements) {
					executor.treeAdd(key, element.getElement(), element.getScore(), expireAt);
				}
			} else {
				for (SortedElement<Object> element : elements) {
					executor.treeAdd(key, element.getElement(), element.getScore(), expireSeconds);
				}
			}
			SortedTree<Object> sorted = executor.treeSort(key, SORTED_CONDITION_ALL);
			Assert.assertNotNull(sorted);
			Assert.assertEquals(sorted.getTotal(), 0);
			Assert.assertEquals(sorted.getElementsSize(), 0);

			Boolean exists = executor.hasKey(key);
			Assert.assertNotNull(exists);
			Assert.assertFalse(exists);
		} finally {
			executor.del("key");
		}
	}

	@Test(groups = "cache-executor", priority = 3300)
	public void testCacheExecutorOperation() throws CacheClientException {

		executor.executeInTransaction(new TransactionAction() {
			@Override
			@SuppressWarnings("serial")
			public void doInTxAction(CacheExecutorOperation operation) throws CacheClientException {

				operation.set("tom", User.TOM);
				operation.set("jerry", User.JERRY, EXPIRES_LONG);
				operation.set("bob", User.BOB, createDate(EXPIRES_LONG));

				operation.setMap("users", User.ALL_USERS);
				operation.setMap("users", User.ALL_USERS, EXPIRES_LONG);
				operation.setMap("users", User.ALL_USERS, createDate(EXPIRES_LONG));
				operation.setMapEntry("users", "jerry_new", User.JERRY_NEW);
				operation.setMapEntry("users", new HashMap<String, User>() {
					{
						put("bob_new", User.BOB);
						put("tom_new", User.TOM);
					}
				});

				operation.increment("incr");
				operation.increment("incrMap", "field");
				operation.incrementBy("incr", 10);
				operation.incrementBy("incrMap", "field", 10);

				operation.expire("incr", EXPIRES_LONG);
				operation.expireAt("incrMap", createDate(EXPIRES_LONG));

				operation.listAppend("list", User.TOM);
				operation.listAppend("list", User.JERRY, EXPIRES_LONG);
				operation.listAppend("list", User.BOB, createDate(EXPIRES_LONG));
				operation.listRemoveFirst("list", User.JERRY);

				operation.treeAdd("tree", User.TOM, 5);
				operation.treeAdd("tree", User.JERRY, 0, EXPIRES_LONG);
				operation.treeAdd("tree", User.BOB, 1, createDate(EXPIRES_LONG));
				operation.treeRemove("tree", User.JERRY);

				Object tom = operation.get("tom");
				List<Object> users = operation.mget("tom", "jerry", "bob");
				Map<String, Object> map = operation.getMap("users");
				Object tomNew = operation.getMapEntry("users", "tom_new");
				List<Object> entries = operation.mgetMapEntry("users", "tom", "jerry", "bob");
				Boolean exists = operation.hasKey("incr");
				Long ttl = operation.ttl("incr");
				Number incrValue = operation.get("incr");
				Number incrMap = operation.getMapEntry("incrMap", "field");
				Long listLength = operation.listLength("list");
				List<Object> lists = operation.listElements("list");
				Double score = operation.treeElementScore("tree", User.BOB);
				SortedTree<Object> sort = operation.treeSort("tree", new SortedCond(true));

				Assert.assertNull(tom);
				Assert.assertNull(users);
				Assert.assertNull(map);
				Assert.assertNull(tomNew);
				Assert.assertNull(entries);
				Assert.assertNull(exists);
				Assert.assertNull(ttl);
				Assert.assertNull(incrValue);
				Assert.assertNull(incrMap);
				Assert.assertNull(listLength);
				Assert.assertNull(lists);
				Assert.assertNull(score);
				Assert.assertNull(sort);
			}
		});

		{
			Object tom = executor.get("tom");
			List<Object> users = executor.mget("tom", "jerry", "bob");
			Map<String, Object> map = executor.getMap("users");
			Object tomNew = executor.getMapEntry("users", "tom_new");
			List<Object> entries = executor.mgetMapEntry("users", "tom", "jerry", "bob");
			Boolean exists = executor.hasKey("incr");
			Long ttl = executor.ttl("incr");
			Number incrValue = executor.get("incr");
			Number incrMap = executor.getMapEntry("incrMap", "field");
			Long listLength = executor.listLength("list");
			List<Object> lists = executor.listElements("list");
			Double score = executor.treeElementScore("tree", User.BOB);
			SortedTree<Object> sort = executor.treeSort("tree", new SortedCond(true));

			Assert.assertNotNull(tom);
			Assert.assertEquals(tom, User.TOM);

			Assert.assertNotNull(users);
			Assert.assertEquals(users.size(), 3);
			Assert.assertEquals(users.get(0), User.TOM);
			Assert.assertEquals(users.get(1), User.JERRY);
			Assert.assertEquals(users.get(2), User.BOB);

			Assert.assertNotNull(map);
			Assert.assertEquals(map.size(), 6);

			Assert.assertNotNull(tomNew);
			Assert.assertEquals(tomNew, User.TOM);

			Assert.assertNotNull(entries);
			Assert.assertEquals(entries.size(), 3);
			Assert.assertEquals(entries.get(0), User.TOM);
			Assert.assertEquals(entries.get(1), User.JERRY);
			Assert.assertEquals(entries.get(2), User.BOB);

			Assert.assertNotNull(exists);
			Assert.assertTrue(exists);

			Assert.assertNotNull(ttl);
			Assert.assertTrue(ttl > EXPIRES_LONG - 60);
			Assert.assertTrue(ttl <= EXPIRES_LONG);

			Assert.assertNotNull(incrValue);
			Assert.assertEquals(incrValue.intValue(), 11);

			Assert.assertNotNull(incrMap);
			Assert.assertEquals(incrMap.intValue(), 11);

			Assert.assertNotNull(listLength);
			Assert.assertEquals(listLength.intValue(), 2);

			Assert.assertNotNull(lists);
			Assert.assertEquals(lists.size(), 2);
			Assert.assertEquals(lists.get(0), User.TOM);
			Assert.assertEquals(lists.get(1), User.BOB);

			Assert.assertNotNull(score);
			Assert.assertEquals(score.doubleValue(), 1.0);

			Assert.assertNotNull(sort);
			Assert.assertEquals(sort.getTotal(), 2);
			Assert.assertEquals(sort.getElementsSize(), 2);
		}

		executor.executeInTransaction(new TransactionAction() {
			@Override
			public void doInTxAction(CacheExecutorOperation operation) throws CacheClientException {
				operation.del("tom", "jerry", "bob", "users", "incr", "incrMap", "list", "tree");
			}
		});

		{
			Object tom = executor.get("tom");
			List<Object> users = executor.mget("tom", "jerry", "bob");
			Map<String, Object> map = executor.getMap("users");
			Object tomNew = executor.getMapEntry("users", "tom_new");
			List<Object> entries = executor.mgetMapEntry("users", "tom", "jerry", "bob");
			Boolean exists = executor.hasKey("incr");
			Long ttl = executor.ttl("incr");
			Number incrValue = executor.get("incr");
			Number incrMap = executor.getMapEntry("incrMap", "field");
			Long listLength = executor.listLength("list");
			List<Object> lists = executor.listElements("list");
			Double score = executor.treeElementScore("tree", User.BOB);
			SortedTree<Object> sort = executor.treeSort("tree", new SortedCond(true));

			Assert.assertNull(tom);

			Assert.assertNotNull(users);
			Assert.assertEquals(users.size(), 3);
			Assert.assertEquals(users.get(0), null);
			Assert.assertEquals(users.get(1), null);
			Assert.assertEquals(users.get(2), null);

			Assert.assertNull(map);
			Assert.assertNull(tomNew);

			Assert.assertNotNull(entries);
			Assert.assertEquals(entries.size(), 3);
			Assert.assertEquals(entries.get(0), null);
			Assert.assertEquals(entries.get(1), null);
			Assert.assertEquals(entries.get(2), null);

			Assert.assertNotNull(exists);
			Assert.assertFalse(exists);

			Assert.assertNotNull(ttl);
			Assert.assertEquals(ttl.longValue(), -1);

			Assert.assertNull(incrValue);
			Assert.assertNull(incrMap);

			Assert.assertNotNull(listLength);
			Assert.assertEquals(listLength.intValue(), -1);

			Assert.assertNull(lists);
			Assert.assertNull(score);

			Assert.assertNotNull(sort);
			Assert.assertEquals(sort, SortedTree.emptySortedTree());
		}
	}

	private boolean isIgnore(Integer expireSeconds, Integer incrementSeconds) {
		if (incrementSeconds != null && incrementSeconds < 1) {
			return true;
		}
		if (incrementSeconds == null && expireSeconds != null && expireSeconds < 1) {
			return true;
		}
		return false;
	}

	private String cutKey(String key) {
		int lastCommaOffset = key.lastIndexOf(':');
		if (lastCommaOffset > -1) {
			return key.substring(0, lastCommaOffset);
		}
		return key;
	}

	private Object getValue(String key, String field, Object value) throws CacheClientException {
		if (value == null) {
			key = cutKey(key);
		}
		if (Tools.isBlank(field)) {
			return executor.get(key);
		}
		return executor.getMapEntry(key, field);
	}

	private void doTransaction(String key, String field, Object value, Integer expireSeconds, Integer incrementSeconds)
			throws CacheClientException {
		if (Tools.isBlank(field)) {
			if (value == null) {
				increment(key, null);
			} else if (expireSeconds != null) {
				executor.set(key, value, expireSeconds);
			} else if (incrementSeconds != null) {
				executor.set(key, value, createDate(incrementSeconds));
			} else {
				executor.set(key, value);
			}
		} else {
			if (value == null) {
				increment(key, field);
				return;
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put(field, value);
			if (expireSeconds != null) {
				executor.setMap(key, map, expireSeconds);
			} else if (incrementSeconds != null) {
				executor.setMap(key, map, createDate(incrementSeconds));
			} else {
				executor.setMap(key, map);
			}
		}
	}

	private void increment(String key, String field) throws CacheClientException {
		key = cutKey(key);
		if (Tools.isBlank(field)) {
			executor.increment(key);
		} else {
			executor.increment(key, field);
		}
	}

	private boolean isShortExpires(Integer expireSeconds, Integer expireAtIncrement) {
		return EXPIRES_SHORT.equals(expireSeconds) || EXPIRES_SHORT.equals(expireAtIncrement);
	}

	private void sleep() {

		try {

			Thread.sleep(EXPIRES_SHORT * 1000 * 3);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	private boolean isEmptyKey(Map<String, ?> map) {
		for (String key : map.keySet()) {
			if (Tools.isBlank(key)) {
				return true;
			}
		}
		return false;
	}
}
