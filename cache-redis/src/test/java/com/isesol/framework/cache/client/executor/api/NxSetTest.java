package com.isesol.framework.cache.client.executor.api;

import com.isesol.framework.cache.client.data.*;
import com.isesol.framework.cache.client.exception.*;
import com.isesol.framework.cache.client.executor.*;
import com.isesol.framework.cache.client.util.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.*;

public class NxSetTest extends BasicCacheExecutorTest {

	@Test(groups = "cache-executor-nx", priority = 3400, dataProvider = "nx-set", dataProviderClass = CacheExecutorDataProvider.class)
	public void testNxSet(Integer id, boolean needInit, String key, Object value, Integer expireSeconds,
	                      Integer incrementSeconds) throws CacheClientException {

		boolean isIgnored = ApiTestUtil.isIgnore(expireSeconds, incrementSeconds);

		needInit &= !Tools.isBlank(key);
		needInit &= ( value != null );

		try {

			if (needInit) {
				ApiTestUtil.set(executor, key, User.BOB, expireSeconds, incrementSeconds);
				User user = executor.get(key);

				if (isIgnored) {

					Assert.assertNull(user);

				} else {

					Assert.assertNotNull(user);
					Assert.assertEquals(user, User.BOB);
				}
			}

			boolean isExpired = ApiTestUtil.isShortExpires(expireSeconds, incrementSeconds);

			if (needInit && !isIgnored && isExpired) {

				ApiTestUtil.sleep();
			}

			Exception x = null;

			boolean result = false;

			try {

				result = ApiTestUtil.nxSet(executor, key, value, expireSeconds, incrementSeconds);
			} catch ( Exception e ) {

				x = e;
			}

			if (Tools.isBlank(key) || value == null) {

				Assert.assertNotNull(x);
				Assert.assertEquals(x.getClass(), IllegalArgumentException.class);

				return;
			}

			if (isIgnored) {

				Assert.assertFalse(result);

			} else if (needInit) {

				Assert.assertEquals(result, isExpired);

			} else {

				Assert.assertTrue(result);
			}

			User user = executor.get(key);

			if (isIgnored) {

				Assert.assertNull(user);

			} else if (needInit) {

				Assert.assertEquals(user, isExpired ? value : User.BOB);

			} else {

				Assert.assertEquals(user, value);
			}

			result = ApiTestUtil.nxSet(executor, key, value, expireSeconds, incrementSeconds);

			Assert.assertFalse(result);

			User user2 = executor.get(key);

			Assert.assertEquals(user2, user);

		} finally {

			if (!Tools.isBlank(key)) {

				executor.del(key);
			}
		}
	}

	@Test(groups = "cache-executor-nx", priority = 3500, dataProvider = "nx-set-map-entry", dataProviderClass = CacheExecutorDataProvider
			.class)
	public void testNxSetMapEntry(Integer id, boolean needInit, String key, String field, Object value,
	                              Integer expireSeconds, Integer incrementSeconds) throws CacheClientException {

		boolean isIgnored = ApiTestUtil.isIgnore(expireSeconds, incrementSeconds);

		needInit &= !Tools.isBlank(key);
		needInit &= !Tools.isBlank(field);
		needInit &= ( value != null );

		try {

			if (needInit) {

				Map<String, User> users = new HashMap<String, User>();
				users.put(field, User.BOB);
				users.put(field + ":" + 1, User.JERRY_NEW);

				ApiTestUtil.setMap(executor, key, users, expireSeconds, incrementSeconds);
				Map<String, User> map = executor.getMap(key);

				if (isIgnored) {
					Assert.assertNull(map);
				} else {
					Assert.assertNotNull(map);
					Assert.assertEquals(map.size(), users.size());
				}
			}

			boolean isExpired = ApiTestUtil.isShortExpires(expireSeconds, incrementSeconds);

			if (needInit && !isIgnored && isExpired) {
				ApiTestUtil.sleep();
			}

			Exception x = null;

			boolean result = false;

			try {
				result = ApiTestUtil.nxSetMapEntry(executor, key, field, value, expireSeconds, incrementSeconds);
			} catch ( Exception e ) {
				x = e;
			}

			if (Tools.isBlank(key) || Tools.isBlank(field) || value == null) {
				Assert.assertNotNull(x);
				Assert.assertEquals(x.getClass(), IllegalArgumentException.class);
				return;
			}

			if (isIgnored) {
				Assert.assertFalse(result);
			} else if (needInit) {
				Assert.assertEquals(result, isExpired);
			} else {
				Assert.assertTrue(result);
			}

			User user = executor.getMapEntry(key, field);
			if (isIgnored) {
				Assert.assertNull(user);
			} else if (needInit) {
				Assert.assertEquals(user, isExpired ? value : User.BOB);
			} else {
				Assert.assertEquals(user, value);
			}

			result = ApiTestUtil.nxSet(executor, key, value, expireSeconds, incrementSeconds);
			Assert.assertFalse(result);
			User user2 = executor.getMapEntry(key, field);
			Assert.assertEquals(user2, user);
		} finally {
			if (!Tools.isBlank(key)) {
				executor.del(key);
			}
		}
	}

	@Test
	public void testNxSetMapEntryExists() throws CacheClientException {

		final String KEY = "key";

		try {

			Map<String, User> users = new HashMap<String, User>();
			users.put("field0", User.JERRY_NEW);
			ApiTestUtil.setMap(executor, KEY, users, 2000, null);

			ApiTestUtil.nxSetMapEntry(executor, KEY, "field1", User.BOB, 1000, null);

			Long ttl = executor.ttl(KEY);
			Assert.assertNotNull(ttl);
			Assert.assertTrue(ttl.intValue() < 1500);

			Map<String, User> map = executor.getMap(KEY);
			Assert.assertNotNull(map);
			Assert.assertEquals(map.size(), 2);
		} finally {
			executor.del("key");
		}
	}
}
