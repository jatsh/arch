package com.isesol.framework.cache.client.executor.api;

import com.isesol.framework.cache.client.executor.operation.*;
import com.isesol.framework.cache.client.util.*;

import java.util.*;

public class SortedTreeExcept {

	private int total;

	private int count;

	private List<SortedElement<Object>> exceptes;

	public SortedTreeExcept(int total, int count, SortedElement<Object>... elements) {

		this.total = total;
		this.count = count;
		if (!Tools.isBlank(elements)) {
			exceptes = new ArrayList<SortedElement<Object>>(elements.length);
			for(SortedElement<Object> element : elements){
				exceptes.add(element);
			}
		}
	}

	public int getTotal() {

		return total;
	}

	public int getCount() {

		return count;
	}

	public SortedElement<Object> getElement(int index) {

		if (index < 0 || index >= exceptes.size()) {
			return null;
		}
		return exceptes.get(index);
	}

	@Override
	public String toString() {

		EclipseTools.ToString builder = new EclipseTools.ToString(this);
		builder.append("total", total);
		builder.append("count", count);
		return builder.toString();
	}
}
