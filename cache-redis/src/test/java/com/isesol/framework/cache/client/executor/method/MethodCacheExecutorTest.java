package com.isesol.framework.cache.client.executor.method;


import com.isesol.framework.cache.client.*;
import com.isesol.framework.cache.client.data.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.test.context.*;
import org.springframework.test.context.testng.*;
import org.testng.*;
import org.testng.annotations.*;

import java.lang.reflect.*;
import java.text.*;
import java.util.*;

@ContextConfiguration(locations = "classpath:/executor/method/application-context.xml")
public class MethodCacheExecutorTest extends AbstractTestNGSpringContextTests {

	private static final String TEST_NAME = "gao";

	@Autowired
	private UserServiceTest service;

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {
		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {
		TestingUtils.logAfter(result);
	}

	@Test(groups = "cache-executor-method", priority = 4000)
	public void testNotNull() {
		Assert.assertNotNull(service);
	}

	@Test(groups = "cache-executor-method", priority = 4100)
	public void testGetUserFromMethod() {
		User user = service.getUser(User.BOB);
		System.out.println(user);
		Assert.assertNotNull(user);
		Assert.assertEquals(user, User.BOB);
	}

	@Test(groups = "cache-executor-method", priority = 4110)
	public void testGetUserFromCached() {

		User user = service.getUser(User.BOB);
		Assert.assertNotNull(user);
		Assert.assertEquals(user, User.BOB);
	}

	@Test(groups = "cache-executor-method", priority = 4120)
	public void testChangeUserDirthday() throws ParseException {

		Date oldBirthday = User.BOB.getBirthday();
		Date newBirthday = new SimpleDateFormat("yyyy-MM-dd").parse("2003-01-02");

		User cached = service.getUser(User.BOB);

		User.BOB.setBirthday(newBirthday);
		User user = service.getUser(User.BOB);

		Assert.assertNotNull(user);
		Assert.assertEquals(oldBirthday, cached.getBirthday());
		Assert.assertEquals(oldBirthday, user.getBirthday());

	}

	// @Test( groups = "cache-executor-method", priority = 4200, dataProvider =
	// "cache-method-primitive", dataProviderClass =
	// CacheExecutorDataProvider.class )
	// public void testPrimitives(int id, Class<?> primitive, Object value)
	// throws Exception {
	//
	// Notifier notifier = new Notifier();
	// notifier.value = value;
	//
	// Method removeMethod = ReflectionUtils.findMethod( UserServiceTest.class,
	// primitive.getSimpleName() + "Remove", String.class );
	//
	// Method getMethod = ReflectionUtils.findMethod( UserServiceTest.class,
	// primitive.getSimpleName() + "Get", String.class, Notifier.class );
	//
	// removeMethod.invoke( service , TEST_NAME );
	//
	// Object result_nocached = getMethod.invoke( service , TEST_NAME, notifier
	// );
	// Assert.assertEquals( result_nocached , notifier.value );
	//
	// for ( int i = 0; i < 12; i++ ) {
	//
	// Object result_cached = getMethod.invoke( service , TEST_NAME, notifier );
	//
	// Assert.assertEquals( result_cached , notifier.value );
	// Thread.sleep(1000);
	// }
	// }

	// @Test( groups = "cache-executor-method", priority = 4399 )
	// public void cleaning() {
	// service.removeUser( User.BOB );
	// }
}
