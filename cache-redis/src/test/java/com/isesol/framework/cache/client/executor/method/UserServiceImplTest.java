package com.isesol.framework.cache.client.executor.method;

import com.isesol.framework.cache.client.annotation.*;
import com.isesol.framework.cache.client.data.*;
import org.apache.logging.log4j.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

@Service
public class UserServiceImplTest implements UserServiceTest {

	static Logger log = LogManager.getLogger(UserServiceImplTest.class.getName());


	@Transactional
	@EnableCacheable(keyId = "get.user", key = "#user?.name")
	@Override
	public User getUser(User user) {

		System.out.println("##########get user 1");
		User obj = User.ALL_USERS.get(user.getName());
		System.out.println("!!!!!!!!!get user 2");
		log.info("getUser: {}", obj);
		return obj;
	}

	@ClearingCached(keyId = "get.user", key = "#user?.name")
	@Override
	public void removeUser(User user) {
		log.info("name = [{}] was removed", user.getName());
	}

	@EnableCacheable(keyId = "get.byte", key = "#p0", expireSeconds = 10)
	@Override
	public byte byteGet(String name, Notifier notifier) {
		notifier.value = (byte) (((Byte) notifier.value) + 1);
		log.info("byteGet: {} : {}", name, notifier.value);
		return (Byte) notifier.value;
	}

	@ClearingCached(keyId = "get.byte", key = "#p0")
	@Override
	public void byteRemove(String name) {
		log.info("byteRemove, name = {}", name);
	}

	@EnableCacheable(keyId = "get.short", key = "#p0", expireSeconds = 10)
	@Override
	public short shortGet(String name, Notifier notifier) {
		notifier.value = (short) (((Short) notifier.value) + 1);
		log.info("shortGet: {} : {}", name, notifier.value);
		return (Short) notifier.value;
	}

	@ClearingCached(keyId = "get.short", key = "#p0")
	@Override
	public void shortRemove(String name) {
		log.info("shortRemove, name = {}", name);
	}

	@EnableCacheable(keyId = "get.char", key = "#p0", expireSeconds = 10)
	@Override
	public char charGet(String name, Notifier notifier) {
		notifier.value = (char) (((Character) notifier.value) + 1);
		log.info("charGet: {} : {}", name, notifier.value);
		return (Character) notifier.value;
	}

	@ClearingCached(keyId = "get.char", key = "#p0")
	@Override
	public void charRemove(String name) {
		log.info("charRemove, name = {}", name);
	}

	@EnableCacheable(keyId = "get.int", key = "#p0", expireSeconds = 10)
	@Override
	public int intGet(String name, Notifier notifier) {
		notifier.value = (int) (((Integer) notifier.value) + 1);
		log.info("intGet: {} : {}", name, notifier.value);
		return (Integer) notifier.value;
	}

	@ClearingCached(keyId = "get.int", key = "#p0")
	@Override
	public void intRemove(String name) {
		log.info("intRemove, name = {}", name);
	}

	@EnableCacheable(keyId = "get.long", key = "#p0", expireSeconds = 10)
	@Override
	public long longGet(String name, Notifier notifier) {
		notifier.value = (long) (((Long) notifier.value) + 1);
		log.info("longGet: {} : {}", name, notifier.value);
		return (Long) notifier.value;
	}

	@ClearingCached(keyId = "get.long", key = "#p0")
	@Override
	public void longRemove(String name) {
		log.info("longRemove, name = {}", name);
	}

	@EnableCacheable(keyId = "get.float", key = "#p0", expireSeconds = 10)
	@Override
	public float floatGet(String name, Notifier notifier) {
		notifier.value = (float) (((Float) notifier.value) + 1);
		log.info("floatGet: {} : {}", name, notifier.value);
		return (Float) notifier.value;
	}

	@ClearingCached(keyId = "get.float", key = "#p0")
	@Override
	public void floatRemove(String name) {
		log.info("floatRemove, name = {}", name);
	}

	@EnableCacheable(keyId = "get.double", key = "#p0", expireSeconds = 10)
	@Override
	public double doubleGet(String name, Notifier notifier) {
		notifier.value = (double) (((Double) notifier.value) + 1);
		log.info("doubleGet: {} : {}", name, notifier.value);
		return (Double) notifier.value;
	}

	@ClearingCached(keyId = "get.double", key = "#p0")
	@Override
	public void doubleRemove(String name) {
		log.info("doubleRemove, name = {}", name);
	}

	@EnableCacheable(keyId = "get.boolean", key = "#p0", expireSeconds = 10)
	@Override
	public boolean booleanGet(String name, Notifier notifier) {
		notifier.value = !((Boolean) notifier.value);
		log.info("booleanGet: {} : {}", name, notifier.value);
		return (Boolean) notifier.value;
	}

	@ClearingCached(keyId = "get.boolean", key = "#p0")
	@Override
	public void booleanRemove(String name) {
		log.info("booleanRemove, name = {}", name);
	}
}
