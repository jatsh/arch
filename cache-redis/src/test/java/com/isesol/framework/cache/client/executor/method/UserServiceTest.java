package com.isesol.framework.cache.client.executor.method;

import com.isesol.framework.cache.client.data.*;

public interface UserServiceTest {

	User getUser(User user);

	void removeUser(User user);

	byte byteGet(String name, Notifier notifier);

	void byteRemove(String name);

	short shortGet(String name, Notifier notifier);

	void shortRemove(String name);

	char charGet(String name, Notifier notifier);

	void charRemove(String name);

	long longGet(String name, Notifier notifier);

	void longRemove(String name);

	float floatGet(String name, Notifier notifier);

	void floatRemove(String name);

	double doubleGet(String name, Notifier notifier);

	void doubleRemove(String name);

	boolean booleanGet(String name, Notifier notifier);

	void booleanRemove(String name);

	int intGet(String name, Notifier notifier);

	void intRemove(String name);

	static class Notifier {
		Object value;
	}
}
