package com.isesol.framework.cache.client.executor.redis;

import com.isesol.framework.cache.client.*;
import com.isesol.framework.cache.client.executor.*;
import org.testng.*;
import org.testng.annotations.*;

import java.lang.reflect.*;

public class DefaultRedisWrapKeyTest {

	private WrapKey wrap = new DefaultRedisWrapKey();

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {

		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {

		TestingUtils.logAfter(result);
	}

	@Test(priority = 5000,
	      dataProvider = "wrap-key",
	      groups = "cache-keys",
	      dataProviderClass = CacheExecutorDataProvider.class,
	      description = "default redis wrap key")
	public void testWrapKey(Integer id, String key) {

		Assert.assertEquals(wrap.wrapKey(key), key);
	}

	@Test(priority = 5100,
	      dataProvider = "wrap-key",
	      groups = "cache-keys",
	      dataProviderClass = CacheExecutorDataProvider.class,
	      description = "default redis wrap key")
	public void testWrapMapKey(Integer id, String mapKey) {

		Assert.assertEquals(wrap.wrapMapKey(mapKey), mapKey);
	}

	@Test(priority = 5200,
	      dataProvider = "wrap-key-exception",
	      expectedExceptions = IllegalArgumentException.class,
	      groups = "cache-keys",
	      dataProviderClass = CacheExecutorDataProvider.class,
	      description = "default redis wrap key")
	public void testWrapKeyException(Integer id, String key) {

		wrap.wrapKey(key);
	}

	@Test(priority = 5300,
	      dataProvider = "wrap-key-exception",
	      expectedExceptions = IllegalArgumentException.class,
	      groups = "cache-keys",
	      dataProviderClass = CacheExecutorDataProvider.class,
	      description = "default redis wrap key")
	public void testWrapMapKeyException(Integer id, String mapKey) {

		wrap.wrapMapKey(mapKey);
	}

	@Test(priority = 5400,
	      dataProvider = "wrap-keys",
	      groups = "cache-keys",
	      dataProviderClass = CacheExecutorDataProvider.class,
	      description = "default redis wrap key")
	public void testWrapKeys(Integer id, String[] keys) {

		Assert.assertEquals(wrap.wrapKeys(keys), keys);
	}

	@Test(priority = 5500,
	      dataProvider = "wrap-keys",
	      groups = "cache-keys",
	      dataProviderClass = CacheExecutorDataProvider.class,
	      description = "default redis wrap key")
	public void testWrapMapKeys(Integer id, String[] mapKeys) {

		Assert.assertEquals(wrap.wrapKeys(mapKeys), mapKeys);
	}

	@Test(priority = 5600,
	      dataProvider = "wrap-keys-exception",
	      expectedExceptions = IllegalArgumentException.class,
	      groups = "cache-keys",
	      dataProviderClass = CacheExecutorDataProvider.class,
	      description = "default redis wrap key")
	public void testWrapKeysException(Integer id, String[] keys) {

		wrap.wrapKeys(keys);
	}

	@Test(priority = 5700,
	      dataProvider = "wrap-keys-exception",
	      expectedExceptions = IllegalArgumentException.class,
	      groups = "cache-keys",
	      dataProviderClass = CacheExecutorDataProvider.class,
	      description = "default redis wrap key")
	public void testWrapMapKeysException(Integer id, String[] mapKeys) {

		wrap.wrapMapKeys(mapKeys);
	}
}
