package com.isesol.framework.cache.client.expression;

import com.isesol.framework.cache.client.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.test.context.*;
import org.springframework.test.context.testng.*;
import org.testng.*;
import org.testng.annotations.*;

import java.lang.reflect.*;

@ContextConfiguration(locations = "classpath:/expression/application-context.xml")
public class ExpressionEvalTest extends AbstractTestNGSpringContextTests {

	private static final User USER_TOM = new User("tom", 15, true);

	private static final User USER_JERRY = new User("jerry", 18, false);

	private static final String KEY_EXPR = "#user?.username + ':' + #user?.age + ':' + #user?.male + ':' + (#strs == null ? 'null' : " +
			"#strs[0])";

	private static final String VALUE_KEY_TOM = USER_TOM.getUsername() + ':' + USER_TOM.getAge() + ':'
			+ USER_TOM.isMale() + ":a";

	private static final String VALUE_KEY_JERRY = USER_JERRY.getUsername() + ':' + USER_JERRY.getAge() + ':'
			+ USER_JERRY.isMale() + ":a";

	private static final String CONDITION_EXPR_SEX = "#user?.male";

	@Autowired
	private ExpressionEvalFactory factory;

	private ExpressionEval eval;

	private Method method;

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {

		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {

		TestingUtils.logAfter(result);
	}

	@Test(priority = 4500, groups = "cache-executor-method")
	public void testFactoryNotNull() {

		Assert.assertNotNull(factory);
	}

	@Test(priority = 4510, groups = "cache-executor-method")
	public void testMethodNotNull() throws SecurityException, NoSuchMethodException {

		this.method = User.class.getMethod("testMethod", User.class, String[].class);
		Assert.assertNotNull(method);
	}

	@Test(priority = 4520, groups = "cache-executor-method")
	public void testEvalNotNull() throws SecurityException, NoSuchMethodException {

		this.eval = factory.createEval(KEY_EXPR, CONDITION_EXPR_SEX, method);
		Assert.assertNotNull(eval);
	}

	@Test(priority = 4530, groups = "cache-executor-method")
	public void testKeyExpr_0() {

		testKeyExpr(USER_TOM, new String[]{ "a" }, VALUE_KEY_TOM);
	}

	@Test(priority = 4540, groups = "cache-executor-method")
	public void testKeyExpr_1() {

		testKeyExpr(USER_JERRY, new String[]{ "a" }, VALUE_KEY_JERRY);
	}

	@Test(priority = 4550, groups = "cache-executor-method")
	public void testKeyExpr_2() {

		testKeyExpr(null, null, "null:null:null:null");
	}

	@Test(priority = 4560, groups = "cache-executor-method")
	public void testConditionExpr_0() {

		testConditionExpr(USER_TOM, true);
	}

	@Test(priority = 4570, groups = "cache-executor-method")
	public void testConditionExpr_1() {

		testConditionExpr(USER_JERRY, false);
	}

	@Test(priority = 4580, groups = "cache-executor-method")
	public void testConditionExpr_2() {

		testConditionExpr(null, false);
	}

	private void testConditionExpr(User user, boolean excepted) {

		boolean result = eval.computeCondition(new Object[]{ user });

		if (user == null) {

			Assert.assertFalse(result);
			return;
		}

		Assert.assertEquals(excepted, result);
	}

	private void testKeyExpr(User user, String[] strs, String excepted) {

		Object object = eval.computeExpressionKey(new Object[]{ user, strs });

		if (excepted == null) {

			Assert.assertNull(object);
			return;
		}

		Assert.assertNotNull(object);
		Assert.assertEquals(String.class, object.getClass());

		String value = (String) object;
		Assert.assertEquals(excepted, value);
	}

	public final static class User {

		private String username;

		private int age;

		private boolean male;

		public User() {

		}

		public User(String username, int age, boolean male) {

			super();
			this.username = username;
			this.age = age;
			this.male = male;
		}

		public void testMethod(User user, String[] strs) {

		}

		public String getUsername() {

			return username;
		}

		public void setUsername(String username) {

			this.username = username;
		}

		public int getAge() {

			return age;
		}

		public void setAge(int age) {

			this.age = age;
		}

		public boolean isMale() {

			return male;
		}

		public void setMale(boolean male) {

			this.male = male;
		}
	}
}
