package com.isesol.framework.cache.client.http;

import com.isesol.framework.cache.client.*;
import org.springframework.test.context.*;
import org.springframework.test.context.testng.*;
import org.testng.*;
import org.testng.annotations.*;

import javax.annotation.*;
import java.io.*;
import java.lang.reflect.*;
import java.net.*;

@ContextConfiguration(locations = "classpath:/http/application-context.xml")
public class HttpRequestorTest extends AbstractTestNGSpringContextTests {

	@Resource(name = "httpRequestor")
	private HttpRequestor normal;

	@Resource(name = "httpRequestor-noexists")
	private HttpRequestor abnormal;

	public HttpRequestorTest() {

	}

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {

		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {

		TestingUtils.logAfter(result);
	}

	@Test(groups = "http", priority = 8000)
	public void testNormal() throws IOException {

		HttpRequest request = new HttpRequest();
		HttpResponse content = normal.doGet(request);
		Assert.assertNotNull(content);
		Assert.assertEquals(content.getContentTypeResponse().indexOf("<title>百度一下，你就知道</title>") > -1, true);
	}

	@Test(groups = "http", priority = 8100, expectedExceptions = SocketTimeoutException.class)
	public void testAbnormalConnectionTimeout() throws IOException {

		HttpRequest request = new HttpRequest();
		abnormal.doGet(request);
	}
}
