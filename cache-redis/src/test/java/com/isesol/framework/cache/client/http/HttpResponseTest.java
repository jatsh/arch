package com.isesol.framework.cache.client.http;

import com.isesol.framework.cache.client.util.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.*;

public class HttpResponseTest {

	private static final String DATA = "正确的响应";

	private static final byte[] responseData = Tools.toBytes(DATA);

	@Test(groups = "http", priority = 8200, expectedExceptions = UnsupportedOperationException.class)
	public void testAddHeader() {

		HttpResponse response = create(200, "OK");
		response.addHeader("Server", "Server");
	}

	@Test(groups = "http", priority = 8210)
	public void testGetResponseCode() {

		HttpResponse response = create(200, "OK");
		Assert.assertEquals(response.getResponseCode(), 200);
	}

	@Test(groups = "http", priority = 8220)
	public void testGetResponseMessage() {

		HttpResponse response = create(200, "OK");
		Assert.assertEquals(response.getResponseMessage(), "OK");
	}

	@Test(groups = "http", priority = 8230)
	public void testGetResponseData() {

		HttpResponse response = create(200, "OK");
		Assert.assertEquals(response.getResponseData(), responseData);
	}

	@Test(groups = "http", priority = 8240)
	public void testGetDefaultEncodingResponse() {

		HttpResponse response = create(200, "OK");
		Assert.assertEquals(response.getDefaultEncodingResponse(), DATA);
		Assert.assertEquals(response.getContentTypeResponse(), DATA);
		Assert.assertEquals(response.getResponseData(Tools.DEFAULT_CHARSET), DATA);
		Assert.assertEquals(response.getResponseData(Tools.DEFAULT_CHARSET.name()), DATA);
	}

	@Test(groups = "http", priority = 8250)
	public void testIsSuccessResponse() {

		Assert.assertTrue(create(200, "OK").isSuccessResponse());
		Assert.assertTrue(create(201, "Created").isSuccessResponse());
		Assert.assertTrue(create(202, "Accepted").isSuccessResponse());
		Assert.assertTrue(create(298, "UNDEFINIED").isSuccessResponse());
		Assert.assertTrue(create(299, "UNDEFINIED").isSuccessResponse());
		Assert.assertTrue(create(304, "Not Modified").isSuccessResponse());

		Assert.assertFalse(create(100, "Continue").isSuccessResponse());
		Assert.assertFalse(create(198, "UNDEFINIED").isSuccessResponse());
		Assert.assertFalse(create(199, "UNDEFINIED").isSuccessResponse());
		Assert.assertFalse(create(400, "Bad Request").isSuccessResponse());
		Assert.assertFalse(create(404, "Not Found").isSuccessResponse());
		Assert.assertFalse(create(500, "Internal Server Error").isSuccessResponse());
		Assert.assertFalse(create(502, "Bad Gateway").isSuccessResponse());
		Assert.assertFalse(create(503, "Service Unavailable").isSuccessResponse());
		Assert.assertFalse(create(504, "Gateway Timeout").isSuccessResponse());
	}

	@Test(groups = "http", priority = 8260)
	public void testToString() {

		HttpResponse response = create(200, "OK");
		String str = response.toString();
		Assert.assertNotNull(str);

		Assert.assertTrue(str.contains("200"));
		Assert.assertTrue(str.contains("OK"));
		Assert.assertTrue(str.contains(DATA));
	}

	private HttpResponse create(int code, String message) {

		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		addHeader(headers, null, "HTTP/1.1 " + code + " " + message);
		addHeader(headers, "Date", "Fri, 10 May 2013 08:54:12 GMT");
		addHeader(headers, "Set-Cookie",
				"session-id=-; path=/; domain=.www.amazon.cn; expires=Thu, 10-May-2001 08:54:12 GMT");
		addHeader(headers, "Set-Cookie",
				"session-id-time=-; path=/; domain=.www.amazon.cn; expires=Thu, 10-May-2001 08:54:12 GMT");
		addHeader(headers, "Set-Cookie",
				"session-token=-; path=/; domain=.www.amazon.cn; expires=Thu, 10-May-2001 08:54:12 GMT");
		addHeader(headers, "Set-Cookie",
				"ubid-acbcn=-; path=/; domain=.www.amazon.cn; expires=Thu, 10-May-2001 08:54:12 GMT");
		addHeader(headers, "Content-Type", "text/html; charset=UTF-8");

		return new HttpResponse(code, message, responseData, headers);
	}

	private static void addHeader(Map<String, List<String>> headers, String name, String value) {

		List<String> list = headers.get(name);
		if (list == null) {
			list = new LinkedList<String>();
			headers.put(name, list);
		}
		list.add(value);
	}
}
