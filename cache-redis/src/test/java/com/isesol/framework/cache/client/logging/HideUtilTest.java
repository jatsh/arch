package com.isesol.framework.cache.client.logging;

import com.isesol.framework.cache.client.*;
import org.testng.*;
import org.testng.annotations.*;

import java.lang.reflect.*;

public class HideUtilTest {

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {

		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {

		TestingUtils.logAfter(result);
	}

	@Test(groups = "logging", priority = 9000)
	public void testHideJson() {

		String json = "{ \"host\" : \"10.48.193.200\", \"password\"  :   \"123456\" }";
		String expected = "{ \"host\" : \"10.***.***.200\", \"password\":\"******\" }";
		Assert.assertEquals(HideUtils.securityHidden(json), expected);
	}
}
