package com.isesol.framework.cache.client.message;

import com.isesol.framework.cache.client.*;
import org.testng.*;
import org.testng.annotations.*;

import java.lang.reflect.*;
import java.util.*;

public class CacheClientQueueTest {

	private static final ThreadLocal<CacheClientQueue<String>> LOCAL = new ThreadLocal<CacheClientQueue<String>>() {

		@Override
		protected CacheClientQueue<String> initialValue() {

			return new CacheClientQueue<String>();
		}
	};

	@BeforeMethod
	public void before() {

	}

	@AfterMethod
	public void after() {

		LOCAL.get().clear();
		LOCAL.remove();
	}

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {

		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {

		TestingUtils.logAfter(result);
	}

	@Test(groups = "message", priority = 10000, dataProvider = "CacheClientQueue#add&size", dataProviderClass = MessagesDataProvider.class)
	public void testAddAndSize(Integer id, Integer total) {

		CacheClientQueue<String> queue = LOCAL.get();

		int excepted = ( total == null ? 0 : total );
		if (excepted < 0) {
			excepted = 0;
		}

		// add
		if (total == null) {
			queue.add(null);
		} else {
			for(int i = 0; i < total; i++){
				queue.add("data-" + i);
			}
		}

		// size
		Assert.assertEquals(queue.size(), excepted);

		String poll = queue.poll();

		if (excepted == 0) {
			Assert.assertNull(poll);
		} else {
			Assert.assertNotNull(poll);
			excepted--;
		}

		Assert.assertEquals(queue.size(), excepted);
	}

	@Test(groups = "message", priority = 10100, dataProvider = "CacheClientQueue#poll", dataProviderClass = MessagesDataProvider.class)
	public void testPoll(Integer id, Integer total, Integer size) {

		CacheClientQueue<String> queue = LOCAL.get();

		for(int i = 0; i < total; i++){
			queue.add("data-" + i);
		}

		int queueSize = queue.size();
		int pollSize = size;

		List<String> list = null;
		IllegalArgumentException exception = null;
		try {
			list = queue.poll(pollSize);
		} catch ( IllegalArgumentException e ) {
			exception = e;
		}

		if (pollSize < 0) {
			Assert.assertNull(list);
			Assert.assertNotNull(exception);
			return;
		}

		Assert.assertNull(exception);

		if (queueSize == 0) {
			Assert.assertNull(list);
		} else {
			Assert.assertNotNull(list);
		}

		int listSize = ( list == null ? 0 : list.size() );

		int remainQueueSize = 0;

		if (pollSize > queueSize) {
			Assert.assertEquals(listSize, queueSize);
		} else {
			Assert.assertEquals(listSize, pollSize);
			remainQueueSize = queueSize - pollSize;
		}

		Assert.assertEquals(queue.size(), remainQueueSize);
	}
}
