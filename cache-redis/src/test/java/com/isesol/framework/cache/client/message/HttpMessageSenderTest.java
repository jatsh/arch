package com.isesol.framework.cache.client.message;

import com.isesol.framework.cache.client.*;
import com.isesol.framework.cache.client.http.*;
import com.isesol.framework.cache.client.message.sender.*;
import com.isesol.framework.cache.client.util.*;
import org.apache.logging.log4j.*;
import org.easymock.*;
import org.springframework.util.*;
import org.testng.Assert;
import org.testng.*;
import org.testng.annotations.*;

import java.io.*;
import java.lang.reflect.*;
import java.util.*;

public class HttpMessageSenderTest {

	static Logger LOGGER = LogManager.getLogger(HttpMessageSenderTest.class.getName());

	private static final int SUCCESS_CODE = 200;

	private HttpMessageSender sender;

	private HttpRequestor httpRequestor;

	@BeforeClass
	public void before() {

		this.sender = createSender();
		eraseDirectories(sender.getRoot());
	}

	@AfterClass
	public void after() {

	}

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {

		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {

		TestingUtils.logAfter(result);
	}

	@Test(groups = "message", priority = 10200, dataProvider = "HttpMessageSender#send", dataProviderClass = MessagesDataProvider.class)
	public void testSend(Integer id, Integer code, String responseContent, Boolean isResponseNull,
	                     List<CacheClientMessage> messages) throws IOException {

		replay(code, responseContent, isResponseNull);
		sender.send(MessagesDataProvider.getAppCode(), messages);
		EasyMock.verify(httpRequestor);

		if (Boolean.FALSE.equals(isResponseNull) && code == SUCCESS_CODE && messages != null
				&& String.valueOf(messages.size()).equals(responseContent)) {
			Assert.assertNull(sender.getPersistence());
			return;
		}

		if (CollectionUtils.isEmpty(messages)) {
			return;
		}

		File failed = sender.getPersistence();

		Assert.assertTrue(failed.exists());

		reSend(failed, messages);
	}

	private void reSend(File failed, List<CacheClientMessage> messages) throws IOException {

		LOGGER.info("\n----- resend -----");

		replay(SUCCESS_CODE, String.valueOf(messages.size() * 2), false);
		sender.send(MessagesDataProvider.getAppCode(), messages);
		EasyMock.verify(httpRequestor);

		Assert.assertNull(sender.getPersistence());
		Assert.assertFalse(failed.exists());
		Assert.assertTrue(new File(failed.getAbsolutePath() + ".read").exists());
	}

	private HttpMessageSender createSender() {

		sender = new HttpMessageSender();
		httpRequestor = EasyMock.createMock(HttpRequestor.class);
		sender.setHttpRequestor(httpRequestor);
		sender.afterPropertiesSet();
		return sender;
	}

	private void replay(int code, String responseContent, Boolean isResponseNull) throws IOException {

		httpRequestor = EasyMock.createMock(HttpRequestor.class);
		sender.setHttpRequestor(httpRequestor);
		sender.afterPropertiesSet();
		httpRequestor.doPost(EasyMock.isA(HttpRequest.class));

		if (isResponseNull == null) {
			EasyMock.expectLastCall().andThrow(new IOException("Mock IOExcpetion"));
			EasyMock.replay(httpRequestor);
			return;
		}

		HttpResponse response = null;

		if (!isResponseNull) {
			response = new HttpResponse(code, "OK", Tools.toBytes(responseContent), null);
		}

		EasyMock.expectLastCall().andReturn(response).once();
		EasyMock.replay(httpRequestor);
	}

	private void eraseDirectories(File file) {

		if (file.isDirectory()) {
			for(File child : file.listFiles()){
				eraseDirectories(child);
			}
		} else {
			file.delete();
			LOGGER.info("clear file: {}", file);
		}
	}
}
