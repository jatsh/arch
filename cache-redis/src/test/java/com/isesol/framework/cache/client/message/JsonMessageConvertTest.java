package com.isesol.framework.cache.client.message;

import com.isesol.framework.cache.client.*;
import com.isesol.framework.cache.client.message.sender.*;
import org.testng.*;
import org.testng.annotations.*;

import java.lang.reflect.*;
import java.util.*;

public class JsonMessageConvertTest {

	private JsonMessageConvert convert;

	@BeforeClass
	public void beforeClass() {

		this.convert = new JsonMessageConvert();
	}

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {

		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {

		TestingUtils.logAfter(result);
	}

	@Test(groups = "message", priority = 10300, dataProvider = "testMessageConvert", dataProviderClass = MessagesDataProvider.class)
	public void testMessageConvertSerializer(Integer id, List<CacheClientMessage> messages, List<String> container,
	                                         List<String> excepted) {

		if (messages != null && messages.size() == 1) {
			CacheClientMessage message = messages.get(0);
			String json = convert.serialize(message);
			if (message == null) {
				Assert.assertNull(json);
			} else {
				Assert.assertNotNull(json);
				Assert.assertEquals(json, excepted.get(0));
			}
			return;
		}

		List<String> result = convert.serializeLine(messages, container);

		Assert.assertNotNull(result);
		Assert.assertEquals(result, excepted);
	}
}
