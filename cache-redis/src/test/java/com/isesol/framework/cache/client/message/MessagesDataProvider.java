package com.isesol.framework.cache.client.message;

import com.alibaba.fastjson.*;
import com.isesol.framework.cache.client.app.*;
import com.isesol.framework.cache.client.app.impl.*;
import com.isesol.framework.cache.client.executor.exception.*;
import com.isesol.framework.cache.client.util.*;
import org.testng.annotations.*;

import java.util.*;

public class MessagesDataProvider {

	private static final String APP_CODE = "testng";

	private static final CacheClientApplication application = new DefaultCacheClientApplication(APP_CODE);

	private static final List<CacheClientMessage> messages;

	static {
		List<CacheClientMessage> temp = new ArrayList<CacheClientMessage>();
		for(int i = 0; i < 20; i++){
			temp.add(new CacheClientMessage(ServerInfoUtils.generateIdentifier(), application.getAppCode(), application
					.getNodeCode(), "getMap(key)", APP_CODE + "_" + i, CacheSystemException.class.getName()));
		}
		messages = Collections.unmodifiableList(temp);
	}

	public static String getAppCode() {

		return application.getAppCode();
	}

	public static String getNodeCode() {

		return application.getNodeCode();
	}

	@DataProvider(name = "CacheClientQueue#add&size", parallel = true)
	public static Object[][] provideAddAndSize() {

		int n = 35001;

		return new Object[][]{ { n++, Integer.MIN_VALUE }, { n++, -1 }, { n++, 0 }, { n++, 1 }, { n++, 2 },
				{ n++, 5 }, { n++, 10 }, { n++, 50 }, { n++, null }, };
	}

	@DataProvider(name = "CacheClientQueue#poll", parallel = true)
	public static Object[][] providePoll() {

		int n = 35101;

		int[] provideData = { Integer.MIN_VALUE, -1, 0, 1, 2, 5, 10, 50 };

		Object[][] data = new Object[provideData.length * 8][];

		for(int i = 0, k = 0; i < provideData.length; i++){
			data[k++] = new Object[]{ n++, provideData[i], Integer.MIN_VALUE };
			data[k++] = new Object[]{ n++, provideData[i], -1 };
			data[k++] = new Object[]{ n++, provideData[i], 0 };
			data[k++] = new Object[]{ n++, provideData[i], 50 };
			data[k++] = new Object[]{ n++, provideData[i], Integer.MAX_VALUE };
			data[k++] = new Object[]{ n++, provideData[i], provideData[i] / 2 };
			data[k++] = new Object[]{ n++, provideData[i], provideData[i] };
			data[k++] = new Object[]{ n++, provideData[i], provideData[i] + 1 };
		}

		return data;
	}

	@DataProvider(name = "HttpMessageSender#send")
	public static Object[][] provideHttpMessageSenderSend() {

		int n = 35201;

		int[] responseCodes = { 200, 500 };
		String[] responseContents = { String.valueOf(Integer.MIN_VALUE), "-1", "0", "1", "5", "10",
				String.valueOf(messages.size()), String.valueOf(messages.size() * 2), null, "", "   ", "a20" };
		Boolean[] isResponseNulls = { false, true, null };

		Object[][] data = new Object[responseCodes.length * responseContents.length * isResponseNulls.length][];

		int k = 0;
		for(int responseCode : responseCodes){
			for(String responseContent : responseContents){
				for(Boolean isResponseNull : isResponseNulls){
					data[k++] = new Object[]{ n++, responseCode, responseContent, isResponseNull, messages };
				}
			}
		}

		return data;
	}

	@SuppressWarnings("rawtypes")
	@DataProvider(name = "testMessageConvert")
	public static Object[][] privodeMessageConvert() {

		List[] messages = new List[]{ getMessage(-1), getMessage(0), getMessage(1), getMessage(1, 0), getMessage(2),
				getMessage(2, 0), getMessage(2, 1), getMessage(3), getMessage(3, 0), getMessage(3, 1),
				getMessage(3, 2), getMessage(20), getMessage(20, 0), getMessage(20, 10), getMessage(20, 19),
				getMessage(200), getMessage(200, 0), getMessage(200, 100), getMessage(200, 199) };

		int incr = 35501;
		int index = 0;

		Object[][] results = new Object[messages.length * 2][];

		for(List list : messages){
			results[index++] = new Object[]{ incr++, list, null, exceptConvert(list) };
			results[index++] = new Object[]{ incr++, list, new LinkedList<String>(), exceptConvert(list) };
		}

		return results;
	}

	@SuppressWarnings("rawtypes")
	private static List<String> exceptConvert(List list) {

		List<String> json = new LinkedList<String>();
		if (Tools.isBlank(list)) {
			return json;
		}
		for(Object message : list){
			if (message != null) {
				json.add(JSON.toJSONString(message));
			}
		}
		return json;
	}

	private static List<CacheClientMessage> getMessage(int n, int... nullIndex) {

		if (n < 0) {
			return null;
		}
		List<CacheClientMessage> list = new LinkedList<CacheClientMessage>();
		if (n == 0) {
			return list;
		}
		for(int i = 0; i < n; i++){
			list.add(new CacheClientMessage(ServerInfoUtils.generateIdentifier(), "dev", "node", "setMap", "message-"
					+ i, "exception-" + i));
		}
		for(int i = 0; i < nullIndex.length; i++){
			list.set(nullIndex[i], null);
		}
		return list;
	}
}
