package com.isesol.framework.cache.client.thread;

import com.isesol.framework.cache.client.thread.impl.*;
import org.testng.*;
import org.testng.annotations.*;

import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

public class NewThreadCacheClientThreadExecutorTest {

	private CacheClientThreadExecutor executor;

	@BeforeClass
	public void before() {

		executor = new NewThreadCacheClientThreadExecutor();
	}

	@Test(groups = "threads", priority = 21000)
	public void testStart() {

		executor.start();
	}

	@Test(groups = "threads", priority = 21010)
	public void testExecute() throws InterruptedException {

		final AtomicBoolean atomic = new AtomicBoolean(true);
		final CountDownLatch latch = new CountDownLatch(1);
		executor.execute(new Runnable() {

			@Override
			public void run() {

				Assert.assertTrue(atomic.get());
				atomic.compareAndSet(true, false);
				latch.countDown();
			}
		});
		latch.await();
		Assert.assertFalse(atomic.get());
	}

	@Test(groups = "threads", priority = 21020)
	public void testDestroy() {

		executor.destroy();
	}
}
