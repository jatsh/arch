package com.isesol.framework.cache.client.util;

import com.isesol.framework.cache.client.*;
import org.apache.logging.log4j.*;
import org.testng.Assert;
import org.testng.*;
import org.testng.annotations.*;

import java.lang.reflect.*;

public class ConcurrentLRUCacheTest {

	static Logger log = LogManager.getLogger(ConcurrentLRUCacheTest.class.getName());

	private static final int LRU_MAX = 10;

	private ConcurrentLRUCache<Integer, Integer> cache;

	@BeforeTest
	public void init() {

		this.cache = new ConcurrentLRUCache<Integer, Integer>(LRU_MAX);
	}

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {

		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {

		TestingUtils.logAfter(result);
	}

	@Test(groups = "utility", priority = 20000, dataProvider = "LRUCache", dataProviderClass = UtilityDataProvider.class)
	public void testLRUCache(Integer id, Integer count) {

		for(int i = 0; i < count; i++){
			cache.put(i, i);
		}

		log.info("Cache size: {}", cache.length());
		Assert.assertEquals(cache.length(), count.intValue() > LRU_MAX ? LRU_MAX : count.intValue());

		for(int i = count - 1, counter = 1; i >= 0; i--, counter++){
			Integer value = cache.get(i);
			log.info("Get cache, key = {}, value = {}", i, value);
			if (counter > LRU_MAX) {
				Assert.assertNull(value);
			} else {
				Assert.assertNotNull(value);
				Assert.assertEquals(value.intValue(), i);
			}
		}
	}
}
