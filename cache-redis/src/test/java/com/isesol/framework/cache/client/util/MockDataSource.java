package com.isesol.framework.cache.client.util;

import org.aopalliance.intercept.*;
import org.apache.logging.log4j.*;
import org.springframework.aop.framework.*;

import javax.sql.*;
import java.io.*;
import java.sql.*;

public class MockDataSource implements DataSource {

	static Logger LOGGER = LogManager.getLogger(MockDataSource.class.getName());

	private PrintWriter out;

	@Override
	public PrintWriter getLogWriter() throws SQLException {

		return out;
	}

	@Override
	public void setLogWriter(PrintWriter out) throws SQLException {

		this.out = out;
	}

	@Override
	public void setLoginTimeout(int seconds) throws SQLException {

	}

	@Override
	public int getLoginTimeout() throws SQLException {

		return 0;
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {

		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {

		return false;
	}

	@Override
	public Connection getConnection() throws SQLException {

		return createConnection();
	}

	@Override
	public Connection getConnection(String username, String password) throws SQLException {

		return createConnection();
	}

	private static Connection createConnection() {

		ProxyFactory proxy = new ProxyFactory(Connection.class, new MethodInterceptor() {

			@Override
			public Object invoke(MethodInvocation invocation) throws Throwable {

				LOGGER.info("Connection invoke '{}'", invocation.getMethod());

				Class<?> clazz = invocation.getMethod().getReturnType();

				if (clazz == boolean.class) {
					return true;
				}

				return null;
			}
		});

		return (Connection) proxy.getProxy();
	}

	@Override
	public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException {

		return null;
	}

}
