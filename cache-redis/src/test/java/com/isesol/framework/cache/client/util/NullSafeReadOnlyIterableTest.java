package com.isesol.framework.cache.client.util;

import com.isesol.framework.cache.client.*;
import org.testng.Assert;
import org.testng.*;
import org.testng.annotations.*;

import java.lang.reflect.*;
import java.util.*;

public class NullSafeReadOnlyIterableTest {

	@BeforeMethod
	public void beforeMethod(Method method, Object[] params) {

		TestingUtils.logBefore(method, params);
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {

		TestingUtils.logAfter(result);
	}

	@Test(groups = "utility", priority = 20100, dataProvider = "nullSafeReadOnlyIterable", dataProviderClass = UtilityDataProvider.class)
	public void testNullSafeReadOnlyIterable(Integer id, List<String> list, int size) {

		Iterator<String> iterator = NullSafeReadOnlyIterable.wrapIterator(list);

		while ( iterator.hasNext() ) {
			String str = iterator.next();
			Assert.assertNotNull(str);
			iterator.remove();
		}

		int afterSize = list == null ? 0 : list.size();

		Assert.assertEquals(afterSize, size);
	}
}
