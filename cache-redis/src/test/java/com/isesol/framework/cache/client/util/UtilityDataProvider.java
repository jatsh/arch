package com.isesol.framework.cache.client.util;

import org.testng.annotations.*;

import java.util.*;

public class UtilityDataProvider {

	@DataProvider(name = "LRUCache")
	public static Object[][] provideLRUCache() {

		Object[][] data = new Object[20][];
		int idCounter = 40001;
		for(int i = 0; i < data.length; i++){
			data[i] = new Object[]{ idCounter++, i };
		}
		return data;
	}

	@DataProvider(name = "nullSafeReadOnlyIterable")
	public static Object[][] provideNullSafeReadOnlyIterable() {

		int[] sizes = { -1, 0, 1, 5, 10, 15, 20, 25, 100, 200 };
		Object[][] results = new Object[sizes.length][];
		int incr = 40101;
		int index = 0;
		for(int size : sizes){
			List<String> list = gen(size);
			results[index++] = new Object[]{ incr++, list, list == null ? 0 : list.size() };
		}
		return results;
	}

	private static List<String> gen(int size) {

		if (size < 0) {
			return null;
		}
		List<String> list = new LinkedList<String>();
		if (size == 0) {
			return list;
		}
		for(int i = 0; i < size; i++){
			list.add("item-" + i);
		}
		return list;
	}
}
