package com.isesol.arch.common.db.datasource.strategy;

import javax.sql.DataSource;

/**
 * 主从选择负载均衡
 */
public abstract class AbstractStrategy implements Strategy {

	public DataSource select(java.util.List<DataSource> slaves, DataSource master) {
		if (slaves == null || slaves.isEmpty())
			return master;
		if (slaves.size() == 1)
			return slaves.get(0);
		return doSelect(slaves, master);
	}

	protected abstract DataSource doSelect(java.util.List<DataSource> slaves, DataSource master);
}