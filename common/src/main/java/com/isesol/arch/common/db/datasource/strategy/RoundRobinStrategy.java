package com.isesol.arch.common.db.datasource.strategy;

import javax.sql.*;
import java.util.*;
import java.util.concurrent.atomic.*;

/**
 * 轮询策略
 */
public class RoundRobinStrategy extends AbstractStrategy {

	private final AtomicInteger index = new AtomicInteger(0);

	@Override
	protected DataSource doSelect(List<DataSource> slaves, DataSource master) {
		int _index = index.getAndIncrement() % slaves.size();
		index.set(_index);
		return slaves.get(_index);
	}
}