package com.isesol.arch.common.db.datasource.strategy;

import javax.sql.DataSource;

/**
 * 负载均衡
 */
public interface Strategy {

	DataSource select(java.util.List<DataSource> Slaves, DataSource master);
}
