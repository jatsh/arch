package com.isesol.arch.common.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 基础模型
 */
public class BaseModel {

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }
}
