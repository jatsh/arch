package com.isesol.arch.common.mq;


/**
 * 基础Topic
 * @author Peter Zhang
 */
public class BaseTopic implements Topic {

	/**
	 * 主题
	 */
	private String topic;

	/**
	 * 标签
	 */
	private String tags;

	/**
	 * 描述
	 */
	private String desc;

	public BaseTopic() {
	}

	public BaseTopic(String topic, String tags, String desc) {
		this.topic = topic;
		this.tags = tags;
		this.desc = desc;
	}

	@Override
	public String getTopic() {
		return topic;
	}

	@Override
	public String getTags() {
		return tags;
	}

	@Override
	public String getDesc() {
		return desc;
	}
}