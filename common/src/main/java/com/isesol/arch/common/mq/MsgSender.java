package com.isesol.arch.common.mq;

import com.alibaba.rocketmq.client.producer.*;

import java.io.*;

public interface MsgSender {

	/**
	 * 发送消息服务
	 *
	 * @param message 消息对象
	 * @param topicType topic枚举类型
	 * @return 发送结果
	 */
	SendResult sendMessage(Serializable message, Class<? extends Topic> topicType);

	/**
	 * 发送消息服务
	 *
	 * @param message 消息对象
	 * @param topic topic名称
	 * @param tag tag名称
	 * @return 发送结果
	 */
	SendResult sendMessage(Serializable message, String topic, String tag);
}