package com.isesol.arch.common.mq;

import com.alibaba.rocketmq.client.exception.*;
import com.alibaba.rocketmq.client.producer.*;
import com.alibaba.rocketmq.common.message.*;
import com.isesol.arch.common.utils.hessian.*;
import org.apache.commons.lang3.*;
import org.slf4j.*;
import org.springframework.stereotype.*;

import java.io.*;
import java.net.*;
import java.text.*;

@Service
public class MsgSenderService implements MsgSender {

	private static final Logger LOG = LoggerFactory.getLogger(MsgSenderService.class);
	private DefaultMQProducer producer;
	private String nameServer;
	private boolean test = false;

	public void init() {
		//为本地测试关闭metaq
		if ("localTest".equals(nameServer)) {
			test = true;
			return;
		}
		producer = new DefaultMQProducer("xmgc");
		//设置nameserver 地址
		if (StringUtils.isBlank(System.getProperty("rocketmq.namesrv.domain"))) {
			System.setProperty("rocketmq.namesrv.domain", nameServer);
		}
		try {
			producer.setNamesrvAddr(nameServer);
			producer.setInstanceName("DEFAULT_MSG_SENDER-" + InetAddress.getLocalHost().getHostName());

			producer.start();
			LOG.info("metaq 发送端启动成功;rocketmq.namesrv.domain={}",
					System.getProperty("rocketmq.namesrv.domain"));
		} catch (UnknownHostException e) {
			LOG.error("getHostName error", e);
		} catch (MQClientException e) {
			LOG.error(MessageFormat.format("meta 发送端启动失败!nameServer={0},group={1},excepton={2} ",
					System.getProperty("rocketmq.namesrv.domain"), "P_fundselling", e.getMessage()), e);
		}
	}

	@Override
	public SendResult sendMessage(Serializable message, Class<? extends Topic> topicType) {
		Topic topic = RocketMqUtils.getTopic(topicType);
		return sendMessage(message, topic.getTopic(), topic.getTags());
	}


	/**
	 * 发送消息服务
	 *
	 * @param message 消息对象
	 * @param topic   topic名称
	 * @param tag     tag名称
	 * @return 发送结果
	 */

	public SendResult sendMessage(Serializable message, String topic, String tag) {
		if (test) {
			SendResult sendResult = new SendResult();
			sendResult.setSendStatus(SendStatus.SEND_OK);
			return sendResult;
		}
		try {
			LOG.info("发送MQ消息, message={}", message.toString());
			Message msg = new Message(topic, tag, HessianUtils.encode(message));
			SendResult sendResult = producer.send(msg);
			LOG.info("sendResult={},message={}", sendResult, message.toString());
			return sendResult;
		} catch (IOException ioe) {
			LOG.error(MessageFormat.format("meta 发送消息失败 序列化失败   message={} ", message), ioe);
			throw new RuntimeException("meta 发送消息失败 序列化失败   message=" + message);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}


	public void destroy() {
		if (producer != null) {
			producer.shutdown();
			LOG.info("metaq 发送端关闭成功;rocketmq.namesrv.domain={}",
					System.getProperty("rocketmq.namesrv.domain"));
		}
	}

	/**
	 * Setter method for property <tt>nameServer</tt>.
	 *
	 * @param nameServer value to be assigned to property nameServer
	 */
	public void setNameServer(String nameServer) {
		this.nameServer = nameServer;
	}
}