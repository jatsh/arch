package com.isesol.arch.common.mq;

import com.isesol.arch.common.service.*;

import java.util.*;

/**
 * RocketMQ工具类
 * @author Peter Zhang
 */
public class RocketMqUtils {

	private static Map<Class<? extends Topic>, Topic> TOPIC_CACHE = new HashMap<>();

	public static Topic getTopic(Class<? extends Topic> topicType) {
		try {
			Topic topic = TOPIC_CACHE.get(topicType);
			if (topic == null) {
				topic = topicType.newInstance();
			}
			return topic;
		} catch (InstantiationException e) {
			throw new ServiceException("init topic fail: " + topicType, e);
		} catch (IllegalAccessException e) {
			throw new ServiceException("init topic fail: " + topicType, e);
		}
	}

}