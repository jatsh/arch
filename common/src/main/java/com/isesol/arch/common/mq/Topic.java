package com.isesol.arch.common.mq;

/**
 * MQ Topic
 * @author Peter Zhang
 */
public interface Topic {

	String getTopic();

	String getTags();

	String getDesc();

}