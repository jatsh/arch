package com.isesol.arch.common.orm.dynamic;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

/**
 * 字段注解.
 * <p>描述:用于将字段与属性进行绑定</p>
 */
@Target({FIELD})
@Retention(RUNTIME)
@Documented
public @interface Column {
	/** 绑定字段名. */
	String name() default "";
	
	/** 绑定表别名. */
	String table() default "";
}
