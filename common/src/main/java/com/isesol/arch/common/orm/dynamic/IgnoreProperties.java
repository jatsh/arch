package com.isesol.arch.common.orm.dynamic;

import java.lang.annotation.*;

/**
 * 忽略属性注解.
 * <p>描述:用户忽略不需要进行查询的属性</p>
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreProperties {
	/** 属性列表. */
	IgnoreProperty[] value();
}
