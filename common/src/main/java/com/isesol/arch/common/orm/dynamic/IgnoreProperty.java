package com.isesol.arch.common.orm.dynamic;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

/**
 * 忽略属性注解.
 * <p>描述:用户忽略不需要进行查询的属性</p>
 */
@Target({FIELD})
@Retention(RUNTIME)
@Documented
public @interface IgnoreProperty {
	/** 属性名称. */
	String name();
}
