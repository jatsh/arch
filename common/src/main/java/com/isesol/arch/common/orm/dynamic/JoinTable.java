package com.isesol.arch.common.orm.dynamic;

import java.lang.annotation.*;

/**
 * 关联表注解.
 * <p>描述:定义要关联的表</p>
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JoinTable {
	/** 表名. */
	String name();
	
	/** 别名. */
	String alias();
	
	/** 关联字段. */
	JoinColumn[] columns() default {};
	
	/** 关联类型（默认为右连接）. */
	String joinType() default "";
}
