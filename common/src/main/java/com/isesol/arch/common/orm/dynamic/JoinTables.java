package com.isesol.arch.common.orm.dynamic;

import java.lang.annotation.*;

/**
 * 关联表注解.
 * <p>描述:定义要关联的表</p>
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JoinTables {
	/** 关联表列表. */
	JoinTable[] value();
}
