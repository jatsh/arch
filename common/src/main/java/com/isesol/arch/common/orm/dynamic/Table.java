package com.isesol.arch.common.orm.dynamic;

import java.lang.annotation.*;

/**
 * 查询主表注解.
 * <p>描述:定义查询计划中使用的主表</p>
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Table {
	/** 表名. */
	String name();
	
	/** 别名. */
	String alias() default "";
	
	/** 主键字段名. */
	String pkColumn() default "id";
}
