package com.isesol.arch.common.redis;

import org.apache.commons.pool2.impl.*;
import redis.clients.jedis.*;

public class JedisPoolFactory {

    private String host;

    private int port;

    private int timeout;

    private int threadCount;

	public JedisPool createJedisPool() {

        // 合并命令行传入的系统变量与默认值
		String host = System.getProperty("redis.host", this.host);
		String port = System.getProperty("redis.port", String.valueOf(this.port));
		String timeout = System.getProperty("redis.timeout", String.valueOf(this.timeout));

		// 设置Pool大小，设为与线程数等大，并屏蔽掉idle checking
        GenericObjectPoolConfig poolConfig = JedisUtils.createPoolConfig(threadCount, threadCount);

		// create jedis pool
		return new JedisPool(poolConfig, host, Integer.valueOf(port), Integer.valueOf(timeout));
	}

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }
}