package com.isesol.arch.common.redis;

import com.isesol.arch.common.utils.*;
import org.slf4j.*;
import redis.clients.jedis.*;

import java.util.*;
import java.util.concurrent.*;

/**
 * Redis缓存工具类.
 *
 * @author <a href="mailto:admin@ddatsh.com.com">Peter Zhang(章培德)</a>
 */
public class RedisUtil{

	private static Logger LOGGER = LoggerFactory.getLogger(RedisUtil.class);

	private static       ThreadLocal<ShardedJedis> jedisThreadLocalHolder = new ThreadLocal<ShardedJedis>();
	/**
	 * jedis缓存.
	 */
	private static       Map<ShardedJedis, Thread> jedisCache             = new ConcurrentHashMap<ShardedJedis, Thread>();
	/**
	 * jedis超时时间（15秒）.
	 */
	private static final int                       timeout                = 15000;
	private static       Boolean                   timerTaskIsRunning     = false;
	private static final Timer                     timer                  = new Timer();
	/**
	 * 连接超时未还池监测任务.
	 */
	private static       TimerTask                 timeoutCheckTask       = new TimerTask(){

		@Override
		public void run(){

			checkDieThread();
		}
	};

	/**
	 * Redis连接池.
	 */
	private static ShardedJedisPool shardedJedisPool;

	public static Long setnx(String key, String value){

		Long r = 0l;

		try{
			ShardedJedis jedis = getJedis();
			r = jedis.setnx(key, value);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:setnx error, key: {}", key, ex);
		}

		return r;
	}

	/**
	 * 获取集合的元素个数.
	 */
	public static Long scard(String key){

		Long r = 0l;

		try{
			ShardedJedis jedis = getJedis();
			r = jedis.scard(key);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:scard error, key: {}", key, ex);
		}

		return r;
	}

	/**
	 * 向集合中添加元素.
	 */
	public static Long sadd(String key, String... members){

		Long r = 0l;

		try{
			ShardedJedis jedis = getJedis();
			r = jedis.sadd(key, members);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:sadd error, key: {},members: {}", key, members, ex);
		}

		return r;
	}

	/**
	 * 从集合中移除元素.
	 */
	public static Long srem(String key, String... members){

		Long r = 0l;

		try{
			ShardedJedis jedis = getJedis();
			r = jedis.srem(key, members);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:srem error, key: {},members: {}", key, members, ex);
		}

		return r;
	}

	/**
	 * 获取集合中的所有元素.
	 */
	public static Set<String> smembers(String key){

		Set<String> r = null;

		try{
			ShardedJedis jedis = getJedis();
			r = jedis.smembers(key);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:smembers error, key: {}", key, ex);
		}

		return r;
	}

	/**
	 * 判断集合中是否存在指定的元素.
	 */
	public static boolean sismember(String key, String member){

		boolean r = false;

		try{
			ShardedJedis jedis = getJedis();
			r = jedis.sismember(key, member);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:sismember error, key: {},member: {}", key, member, ex);
		}

		return r;
	}

	/**
	 * hset
	 *
	 * @param key
	 * @param field
	 * @param value
	 */
	public static void hset(String key, String field, String value){

		try{
			ShardedJedis jedis = getJedis();
			jedis.hset(key, field, value);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:hset error, key: {},field: {},value: {}", key, field, value, ex);
		}
	}

	/**
	 * hincr
	 *
	 * @param key
	 * @param field
	 */
	public static void hincr(String key, String field){

		try{
			ShardedJedis jedis = getJedis();
			jedis.hincrBy(key, field, 1L);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:hincr error, key: {},field: {}", key, field, ex);
		}
	}

	/**
	 * hincrBy
	 *
	 * @param key
	 * @param field
	 * @param value
	 */
	public static void hincrBy(String key, String field, Long value){

		try{
			ShardedJedis jedis = getJedis();
			jedis.hincrBy(key, field, value);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:hincrBy error, key: {},field: {}, value:{}", key, field, value, ex);
		}
	}

	/**
	 * 自增缓存键中的值.
	 */
	public static Long incr(String key){

		try{
			ShardedJedis jedis = getJedis();
			return jedis.incr(key);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:incr error, key: {}", key, ex);
			throw new RuntimeException("Redis操作失败！", ex);
		}
	}

	/**
	 * 自减缓存中的键值.
	 */
	public static Long decr(String key){

		try{
			ShardedJedis jedis = getJedis();
			return jedis.decr(key);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:decr error, key: {}", key, ex);
			throw new RuntimeException("Redis操作失败！", ex);
		}
	}

	/**
	 * 设置缓存.
	 *
	 * @param key
	 * 		键
	 * @param value
	 * 		值
	 *
	 * @return Status code reply
	 */
	public static String set(String key, String value){

		String r = null;

		try{
			ShardedJedis jedis = getJedis();
			r = jedis.set(key, value);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:set error, key: {}, value: {}", key, value, ex);
		}

		return r;
	}

	/**
	 * 设置缓存.
	 *
	 * @param key
	 * 		键
	 * @param value
	 * 		值
	 * @param expire
	 * 		失效时间(秒)
	 *
	 * @return Status code reply
	 */
	public static String set(String key, String value, int expire){

		String status = null;

		try{
			ShardedJedis jedis = getJedis();
			status = jedis.set(key, value);
			jedis.expire(key, expire);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:set error, key: {}, value: {}, expire: {}", key, value, expire, ex);
		}

		return status;
	}

	/**
	 * 设置过期时间.
	 */
	public static Long expire(String key, int seconds){

		Long r = 0l;

		try{
			ShardedJedis jedis = getJedis();
			r = jedis.expire(key, seconds);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:expire error, key: {}, seconds: {}", key, seconds, ex);
		}

		return r;
	}

	/**
	 * 获取缓存中的值.
	 *
	 * @param key
	 * 		键
	 *
	 * @return 缓存中的值
	 */
	public static String get(String key){

		String r = null;

		try{
			ShardedJedis jedis = getJedis();
			r = jedis.get(key);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:get error, key: {}", key, ex);
		}

		return r;
	}

	/**
	 * 删除缓存中的键.
	 */
	public static Long del(String key){

		Long r = 0l;

		try{
			ShardedJedis jedis = getJedis();
			r = jedis.del(key);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:del error, key: {}", key, ex);
		}

		return r;
	}

	/**
	 * 验证给定的key是否在缓存中出来.
	 */
	public static boolean exists(String key){

		try{
			ShardedJedis jedis = getJedis();
			return jedis.exists(key);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:exists error, key: {}", key, ex);
			throw new RuntimeException("Redis操作失败！", ex);
		}
	}

	/**
	 * 验证指定的缓存Key在Redis中是否存在，如果不存在则进行值的初始化，否则不进行设置.
	 */
	public static String noExistToSet(String key, String value){

		try{
			ShardedJedis jedis = getJedis();
			if (!jedis.exists(key)){
				jedis.set(key, value);
			}

			return jedis.get(key);
		} catch (Exception ex){
			returnBrokenResource();
			LOGGER.error("redis:noExistToSet error, key: {}, value: {}", key, value, ex);
			throw new RuntimeException("Redis操作失败！", ex);
		}
	}

	/**
	 * 资源还池.
	 */
	public static void realese(ShardedJedis jedis){
		//		if (jedis != null) {
		//			jedisThreadLocalHolder.set(null);
		//			shardedJedisPool.returnResource(jedis);
		//		}
	}

	/**
	 * 释放坏死的连接.
	 */
	@SuppressWarnings("deprecation")
	public static void returnBrokenResource(){

		ShardedJedis jedis = null;

		synchronized (Thread.currentThread()){
			jedis = jedisThreadLocalHolder.get();

			if (jedis != null){
				jedisThreadLocalHolder.remove();
			}
		}

		if (jedis != null){
			jedisCache.remove(jedis);

			try{
				shardedJedisPool.returnBrokenResource(jedis);
			} catch (Exception ex){
				LOGGER.warn("redis连接还池失败！", ex);
			}
		}
	}

	/**
	 * 释放本地线程池资源.
	 */
	@SuppressWarnings("deprecation")
	public static void realRealese(){

		ShardedJedis jedis = null;

		synchronized (Thread.currentThread()){
			jedis = jedisThreadLocalHolder.get();

			if (jedis != null){
				jedisThreadLocalHolder.remove();
			}
		}

		if (jedis != null){
			jedisCache.remove(jedis);

			try{
				shardedJedisPool.returnResource(jedis);
			} catch (Exception ex){
				LOGGER.warn("redis连接还池失败！", ex);
			}
		}
	}

	/**
	 * 获取Jedis实例.
	 */
	public static ShardedJedis getJedis(){

		if (shardedJedisPool == null){
			shardedJedisPool = SpringContextHolder.getBean("shardedJedisPoolCache");

			synchronized (timerTaskIsRunning){
				if (!timerTaskIsRunning){
					timer.schedule(timeoutCheckTask, timeout, timeout);
					timerTaskIsRunning = true;
				}
			}
		}

		ShardedJedis jedis = null;

		synchronized (Thread.currentThread()){
			jedis = jedisThreadLocalHolder.get();
		}

		try{
			if (jedis == null){
				jedis = shardedJedisPool.getResource();
				jedisThreadLocalHolder.set(jedis);
				jedisCache.put(jedis, Thread.currentThread());
			}
		} catch (Exception e){
			LOGGER.error("get jedis fail: ", e);
		}

		return jedis;
	}

	/**
	 * 检测已经不活动的进程.
	 */
	@SuppressWarnings("deprecation")
	private static synchronized void checkDieThread(){

		List<ShardedJedis> timeoutJedis = new ArrayList<ShardedJedis>();

		for (ShardedJedis jedis : jedisCache.keySet()){
			Thread cacheThread = jedisCache.get(jedis);

			if (!cacheThread.isAlive()){
				timeoutJedis.add(jedis);
			}
		}

		for (ShardedJedis jedis : timeoutJedis){
			jedisCache.remove(jedis);

			try{
				shardedJedisPool.returnResource(jedis);
			} catch (Exception ex){
				LOGGER.warn("redis连接还池失败！", ex);
			}
		}

		synchronized (Thread.currentThread()){
			jedisThreadLocalHolder.remove();
		}
	}

	/**
	 * 构造函数.
	 */
	private RedisUtil(){

	}
}
