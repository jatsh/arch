package com.isesol.arch.common.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 基础业务对象
 *
 */
public class BaseService {

    protected Logger LOGGER = LoggerFactory.getLogger(getClass());

}
