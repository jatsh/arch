package com.isesol.arch.common.service;

/**
 * Service层公用的Exception.
 *
 */
public class ServiceException extends RuntimeException {

	public ServiceException() {
        super();
	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}