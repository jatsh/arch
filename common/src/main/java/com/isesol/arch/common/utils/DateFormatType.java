package com.isesol.arch.common.utils;

/**
 * 预置日期格式化格式
 *
 */
public class DateFormatType {
    /**
	 * 日期格式：2011-05-12
	 */
	public final static String FORMAT_DATE = "yyyy-MM-dd";
	
	/**
	 * 日期格式：20110512
	 */
	public final static String FORMAT_DATE_NO_SPLIT_CHAR = "yyyyMMdd";
	
	/**
	 * 日期格式：20110512_121359
	 */
	public final static String FORMAT_DATETIME_SPLIT_WITH_UNDERLINE = "yyyyMMdd_HHmmss";
	
	/**
	 * 日期格式：20110512_1213
	 */
	public final static String FORMAT_DATETIME_NO_SECOND_SPLIT_WITH_UNDERLINE = "yyyyMMdd_HHmm";

	/**
	 * 日期格式：2011-05-12 09:10:33
	 */
	public final static String FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 日期格式：2011-05-12 09:10
	 */
	public final static String FORMAT_DATETIME_NO_SECOND = "yyyy-MM-dd HH:mm";

	/**
	 * 日期格式：2011年5月12日
	 */
	public final static String FORMAT_DATE_ZH = "yyyy年M月d日";
	
	/**
	 * 日期格式：2011年5月12日 9时10分33秒
	 */
	public final static String FORMAT_DATETIME_ZH = "yyyy年M月d日 HH时mm分ss秒";
	
	/**
	 * 日期格式：2011年5月12日 9时10分33秒
	 */
	public final static String FORMAT_DATETIME_ZH_NO_SECOND = "yyyy年M月d日 HH时mm分";

}