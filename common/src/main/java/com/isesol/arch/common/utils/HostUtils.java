package com.isesol.arch.common.utils;

import org.slf4j.*;

import java.net.*;

/**
 * HOST工具类
 *
 * @author Peter Zhang
 */
public class HostUtils{

	private static Logger LOGGER = LoggerFactory.getLogger(HostUtils.class);

	public static final String HOST_NAME;

	static{

		HOST_NAME = init();

	}

	/**
	 * 获取主机名
	 *
	 * @return
	 *
	 */
	private static String init(){

		try{

			return InetAddress.getLocalHost().getHostName();

		} catch (Exception e){

			LOGGER.error("host name init error");
		}

		return null;
	}

	/**
	 * 获取本机名
	 *
	 * @return 主机名
	 */
	public static String getHostName(){

		return HOST_NAME;
	}

}
