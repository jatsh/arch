package com.isesol.arch.common.utils;

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

/**
 * HTML代码工具类
 *
 */
public class HtmlUtil {

	/**
	 * 生成HTML的select元素
	 * @param datas	<option的value, option的text>
	 * @return	构建好的select元素HTML代码
	 */
	public static String generateSelect(Map<String, String> datas) {
		return generateSelect(datas, false);
	}
	
	/**
	 * 生成HTML的select元素
	 * @param 	datas	<option的value, option的text>
	 * @param	withSelectTip 是否在第一个选择中添加“请选择”
	 * @return	构建好的select元素HTML代码
	 */
	public static String generateSelect(Map<String, String> datas, boolean withSelectTip) {
		return generateSelect(datas, withSelectTip, null, null, true);
	}
	
	/**
	 * 生成HTML的select元素
	 * @param 	datas	<option的value, option的text>
	 * @param	withSelectTip 是否在第一个选择中添加“请选择”
	 * @return	构建好的select元素HTML代码
	 */
	public static String generateSelectWithTip(Map<String, String> datas, String tipText) {
		return generateSelect(datas, true, tipText, null, true);
	}
	
	/**
	 * 生成HTML的select元素
	 * @param 	datas	<option的value, option的text>
	 * @param	withSelectTip 是否在第一个选择中添加“请选择”
	 * @param	defaultValue	默认选择的值
	 * @return	构建好的select元素HTML代码
	 */
	public static String generateSelect(Map<String, String> datas, boolean withSelectTip, String defaultValue) {
		return generateSelect(datas, withSelectTip, null, defaultValue, true);
	}
	
	/**
	 * 生成HTML的select元素
	 * @param 	datas			<option的value, option的text>
	 * @param	withSelectTip 	是否在第一个选择中添加“请选择”
	 * @param	defaultValue	默认选择的值
	 * @param	setValue		是否设置option的value属性
	 * @return	构建好的select元素HTML代码
	 */
	public static String generateSelect(Map<String, String> datas, boolean withSelectTip, String tipText, String defaultValue, boolean setValue) {
		StringBuilder sb = new StringBuilder();
		sb.append("<select>");
		sb.append(generateOptions(datas, withSelectTip, tipText, defaultValue, setValue));
		sb.append("</select>");
		return sb.toString();
	}
	
	/**
	 * @see #generateOptions(Map, boolean, String, boolean) 
	 */
	public static String generateOptions(Map<String, String> datas, boolean withSelectTip) {
		return generateOptions(datas, withSelectTip, null, null, true);
	}
	
	/**
	 * @see #generateOptions(Map, boolean, String, boolean) 
	 */
	public static String generateOptions(Map<String, String> datas, boolean withSelectTip, String tipText) {
		return generateOptions(datas, withSelectTip, tipText, null, true);
	}

	/**
	 * 生成HTML的option元素
	 * @param 	datas			<option的value, option的text>
	 * @param	withSelectTip 	是否在第一个选择中添加“请选择”
	 * @param	tipText			提示信息，默认为“请选择”
	 * @param	defaultValue	默认选择的值
	 * @param	setValue		是否设置option的value属性
	 * @return
	 */
	public static String generateOptions(Map<String, String> datas, boolean withSelectTip, String tipText, String defaultValue, boolean setValue) {
		StringBuilder sb = new StringBuilder();
		if (withSelectTip) {
			if (StringUtils.isBlank(tipText)) {
				tipText = "请选择";
			}
			sb.append("<option value=''>" + tipText + "</option>");
		}
		
		Set<Entry<String, String>> entrySet = datas.entrySet();
		String defaultHtml = "";
		for (Entry<String, String> entry : entrySet) {
			defaultHtml = "";
			if (StringUtils.isNotBlank(defaultValue) && entry.getKey().equals(defaultValue)) {
				defaultHtml = " selected";
			}
			sb.append("<option");
			if (setValue) {
				sb.append(" value='" + entry.getKey() + "'");
			}
			sb.append(defaultHtml + ">" + entry.getValue() + "</option>");
		}
		return sb.toString();
	}

	/**
	 * 不设置option的value属性
	 * @param datas	<option的value, option的text>
	 * @return	构建好的select元素HTML代码
	 */
	public static String generateSelectNoValue(Map<String, String> datas) {
		return generateSelect(datas, false, null, null, false);
	}
	
	/**
	 * 生成分组下拉框代码，只有options
	 * @param datas	数据源
	 * @return 构建好的OPTGROUP元素HTML代码
	 */
	public static String createGroupSelect(Map<String, Map<String, String>> datas) {
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<String, Map<String, String>> entry : datas.entrySet()) {
			sb.append("<optgroup label='" + entry.getKey() + "'>");
			Map<String, String> subItems = entry.getValue();
			for (Map.Entry<String, String> subEntry : subItems.entrySet()) {
				String subKey = subEntry.getKey();
				String subValue = subEntry.getValue();
				sb.append("<option role='option' value='" + subKey + "'>" + subValue + "</option>");
			}
			sb.append("</optgroup>");
		}
		return sb.toString();
	}
	/**
	 * 生成角色下拉框代码，只有options
	 * @param datas	数据源
	 * @return 构建好的OPTGROUP元素HTML代码
	 */
	public static String createRoleSelect(Map<Long, Map<Long, String>> datas) {
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<Long, Map<Long, String>> entry : datas.entrySet()) {
			Map<Long, String> subItems = entry.getValue();
			for (Map.Entry<Long, String> subEntry : subItems.entrySet()) {
				long subKey = subEntry.getKey();
				String subValue = subEntry.getValue();
				sb.append("<option value='" + subKey + "'>" + subValue + "</option>");
			}
		}
		return sb.toString();
	}
	
}