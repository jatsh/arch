package com.isesol.arch.common.utils;

import org.slf4j.*;

import java.net.*;
import java.util.*;

/**
 * IP工具类
 *
 * @author Peter Zhang
 */
public class IpUtil{

	private static Logger logger = LoggerFactory.getLogger(IpUtil.class);

	public static final String SERVER_IP;

	static{

		SERVER_IP = init();

	}

	/**
	 * 获取本机IP地址
	 *
	 * @return
	 *
	 * @throws java.net.SocketException
	 */
	private static String init(){

		Enumeration<NetworkInterface> e = null;
		try{
			e = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e1){
			logger.error("获取网络信息失败: ", e);
			return "";
		}

		while (e.hasMoreElements()){
			NetworkInterface         x             = e.nextElement();
			Enumeration<InetAddress> inetAddresses = x.getInetAddresses();
			while (inetAddresses.hasMoreElements()){
				String hostAddress = inetAddresses.nextElement().getHostAddress();
				if (hostAddress.equals("127.0.0.1")){
					continue;
				}
				String[] split = hostAddress.split("\\.");
				if (split.length == 4){
					return hostAddress;
				}
			}
		}
		return null;
	}

	/**
	 * 获取本机IP地址
	 *
	 * @return
	 *
	 * @throws java.net.SocketException
	 */
	public static String getServerIp(){

		return SERVER_IP;
	}

}
