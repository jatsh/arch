package com.isesol.arch.common.utils;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 线程上下文对象
 *
 */
public class ThreadContextHolder {

    private static ThreadLocal<Map<String, Object>> attrsThreadLocalHolder = new ThreadLocal<>();

    private static Logger logger = LoggerFactory.getLogger(ThreadContextHolder.class);

    public static void setAttr(String key, Object object) {
        logger.debug("设置线程属性 key:{}, value: {}", key, object);
        getAttrs().put(key, object);
    }

    public static Object getAttr(String key) {
        Object value = getAttrs().get(key);
        logger.debug("设置线程属性 key:{}, value: {}", key, value);
        return value;
    }

    private static Map<String, Object> getAttrs() {
        Map<String, Object> attrs = attrsThreadLocalHolder.get();
        if (attrs == null) {
            attrs = new HashMap<>();
        }
        attrsThreadLocalHolder.set(attrs);
        return attrs;
    }

    public static void cleanAttrs() {
        attrsThreadLocalHolder.set(new HashMap<String, Object>());
    }

    public static void removeAttr(String key) {
        getAttrs().remove(key);
    }
}