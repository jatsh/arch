package com.isesol.arch.common.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import org.springframework.util.StringUtils;

/**
 * Web相关工具类
 *
 */
public class WebUtils {

    /**
     * 把Map集合转换成URL参数
     *
     * @param url
     * @param map
     * @return
     */
    public static String mapToQueryString(String url, Map<String, ? extends Object> map) {
        return mapToQueryString(url, map, true);
    }

    /**
     * 把Map集合转换成URL参数
     *
     * @param url
     * @param map
     * @param encode 是否针对URL参数encode
     * @return
     */
    public static String mapToQueryString(String url, Map<String, ? extends Object> map, boolean encode) {
        if (map == null) {
            return url;
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, ? extends Object> e : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append('&');
            }
            if (encode) {
                try {
                    String key = URLEncoder.encode(e.getKey(), "UTF-8");
                    String value = URLEncoder.encode(e.getValue().toString(), "UTF-8");
                    sb.append(key).append('=').append(value);
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            } else {
                sb.append(e.getKey()).append('=').append(e.getValue());
            }
        }
        if (!StringUtils.isEmpty(sb.toString())) {
            if (url.contains("?")) {
                url += "&";
            } else {
                url += "?";
            }
            url += sb.toString();
        }
        return url;
    }
}