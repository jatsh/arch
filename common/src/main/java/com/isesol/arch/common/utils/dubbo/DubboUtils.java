package com.isesol.arch.common.utils.dubbo;


import com.alibaba.dubbo.config.*;
import com.google.common.collect.*;
import com.isesol.arch.common.utils.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * Dubbo工具类.
 */
public class DubboUtils {
	/**
	 * 获取Dubbo中的Bean.
	 *
	 * @param interfaceClazz 接口类
	 */
	public static <T> T getBean(Class<T> interfaceClazz) {


		return getBean(interfaceClazz, null, null, false);
	}

	/**
	 * 获取Dubbo中的Bean.
	 *
	 * @param interfaceClazz 接口类
	 * @param async          是否异步执行
	 */
	public static <T> T getBean(Class<T> interfaceClazz, boolean async) {

		return getBean(interfaceClazz, null, null, async);
	}

	/**
	 * 获取Dubbo中的Bean.
	 *
	 * @param interfaceClazz 接口类
	 * @param group          所属分组名
	 */
	public static <T> T getBean(Class<T> interfaceClazz, String group) {

		return getBean(interfaceClazz, group, null, false);
	}

	/**
	 * 获取Dubbo中的Bean.
	 *
	 * @param interfaceClazz 接口类
	 * @param group          所属分组名
	 * @param async          是否异步执行
	 */
	public static <T> T getBean(Class<T> interfaceClazz, String group, boolean async) {

		return getBean(interfaceClazz, group, null, async);
	}

	/**
	 * 获取Dubbo中的Bean.
	 *
	 * @param interfaceClazz 接口类
	 * @param version        版本号
	 */
	public static <T> T getBeanByVersion(Class<T> interfaceClazz, String version) {

		return getBean(interfaceClazz, null, version, false);
	}

	/**
	 * 获取Dubbo中的Bean.
	 *
	 * @param interfaceClazz 接口类
	 * @param version        版本号
	 * @param async          是否异步执行
	 */
	public static <T> T getBeanByVersion(Class<T> interfaceClazz, String version, boolean async) {

		return getBean(interfaceClazz, null, version, async);
	}

	/**
	 * 获取Dubbo中的Bean.
	 *
	 * @param interfaceClazz 接口类
	 * @param group          所属分组名
	 * @param version        版本号
	 */
	public static <T> T getBean(Class<T> interfaceClazz, String group, String version) {

		return getBean(interfaceClazz, group, version, false);
	}

	/**
	 * 获取Dubbo中的Bean.
	 *
	 * @param interfaceClazz 接口类
	 * @param group          所属分组名
	 * @param version        版本号
	 * @param async          是否异步执行
	 */
	public static <T> T getBean(Class<T> interfaceClazz, String group, String version, boolean async) {

		try {
			ReferenceConfig<T> reference = getReference(interfaceClazz, group, version, async);

			if (reference != null) {

				return reference.get();

			} else {

				throw new RuntimeException("从Dubbo中获取找不到Bean[" + interfaceClazz.getName() + "]！");
			}
		} catch (Exception ex) {

			throw new RuntimeException("从Zookeeper中获取Bean[" + interfaceClazz.getName() + "]失败！", ex);
		}
	}

	/**
	 * 获取Dubbo中的Bean的引用.
	 *
	 * @param interfaceClazz 接口类
	 * @param group          所属分组名
	 * @param version        版本号
	 * @param async          是否异步执行
	 */
	@SuppressWarnings("unchecked")
	private static <T> ReferenceConfig<T> getReference(Class<T> interfaceClazz, String group, String version, boolean async) {

		String cacheKey = (StringUtils.isNotBlank(group) ? group : "") + "_" + interfaceClazz.getName() + "_" + async + "_" + (StringUtils.isNotBlank(version) ? version : "");

		if (referenceCache.containsKey(cacheKey)) {

			return (ReferenceConfig<T>) referenceCache.get(cacheKey);
		}

		ReferenceConfig<T> reference = new ReferenceConfig<T>();
		reference.setApplication(new ApplicationConfig("dynamicInvocation"));
		reference.setRegistries(getRegistryConfig());
		reference.setInterface(interfaceClazz);
		reference.setCluster("failover");

		if (StringUtils.isNotBlank(version)) {
			reference.setVersion(version);
		}

		if (StringUtils.isNotBlank(group)) {
			reference.setGroup(group);
		}

		reference.setAsync(async);

		if (async) {
			reference.setScope("remote");
		}

		referenceCache.put(cacheKey, reference);

		return reference;
	}

	/**
	 * 获取Dubbo注册中心.
	 */
	private static List<RegistryConfig> getRegistryConfig() {

		if (registrys != null) {
			return registrys;
		}

		String registryAddress = zookeeperAddress;
		String file = null;

		if (StringUtils.isBlank(registryAddress)) {
			registryAddress = PropertyFileUtil.get("dubbo.zookeeperAddress");
			file = PropertyFileUtil.get("dubbo.registry.file");
		}

		if (StringUtils.isNotBlank(registryAddress)) {
			RegistryConfig config = new RegistryConfig(registryAddress);

			if (StringUtils.isNotBlank(file)) {
				config.setFile(file);
			}

			registrys = new ArrayList<>(1);
			registrys.add(config);
			return registrys;
            /*String[] address = registryAddress.split(",");

    		if (address != null && address.length > 0) {

    			for (String addr : address) {
			    	registrys.add(new RegistryConfig(addr));
    			}

    			return registrys;
    		} else {
    			throw new RuntimeException("从配置文件中无法获取dubbo.zookeeperAddress配置信息");
    		}*/
		} else {
			throw new RuntimeException("从配置文件中无法获取dubbo.zookeeperAddress配置信息");
		}
	}

	/**
	 * Dubbo动态远程调用对象缓存.
	 */
	private static final Map<String, ReferenceConfig<?>> referenceCache = Maps.newHashMap();

	private static List<RegistryConfig> registrys = null;
	/**
	 * dubbo zookeeper 地址，用于调试时使用.
	 */
	private static String zookeeperAddress = null;

	/**
	 * 构造函数.
	 */
	private DubboUtils() {

	}

	/**
	 * 设置dubbo zookeeper 地址，用于调试时使用.
	 */
	public static void setZookeeperAddress(String zookeeperAddress) {

		DubboUtils.zookeeperAddress = zookeeperAddress;
	}
}
