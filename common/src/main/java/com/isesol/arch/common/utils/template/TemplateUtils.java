package com.isesol.arch.common.utils.template;


import org.apache.commons.lang3.*;
import org.beetl.core.*;
import org.beetl.core.resource.*;

import java.io.*;
import java.util.*;

/**
 * 模板解析工具
 *
 * @author Peter Zhang
 */
public class TemplateUtils{

	/**
	 * 转换字符串模板中，用args参数替换
	 *
	 * @param template
	 * 		字符串模板，例如: 您好，${name}，您已获得价值${price}的${goodsName}
	 * @param args
	 * 		模板中${}的参数列表
	 *
	 * @return 如果解析异常则返回空字符串
	 */
	public static String parse(String template, Map args){

		if (StringUtils.isBlank(template)){

			return StringUtils.EMPTY;
		}

		StringTemplateResourceLoader resourceLoader = new StringTemplateResourceLoader();

		try{

			Configuration cfg = Configuration.defaultConfiguration();
			GroupTemplate gt  = new GroupTemplate(resourceLoader, cfg);
			Template      t   = gt.getTemplate(template);

			t.binding(args);
			String content = t.render();

			return content;

		} catch (IOException e){

			return "";
		}
	}

}
