package com.isesol.arch.common.utils.uuid;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;
import com.fasterxml.uuid.impl.TimeBasedGenerator;

/**
 * 使用UUID算法生成全球唯一ID
 */
public class UuidGenerator {

    protected static TimeBasedGenerator timeBasedGenerator;

    static {
        ensureGeneratorInitialized();
    }

    protected static void ensureGeneratorInitialized() {
        if (timeBasedGenerator == null) {
            synchronized (UuidGenerator.class) {
                if (timeBasedGenerator == null) {
                    timeBasedGenerator = Generators.timeBasedGenerator(EthernetAddress.fromInterface());
                }
            }
        }
    }

    public static String getNextId() {
        return timeBasedGenerator.generate().toString();
    }

}
