package com.isesol.arch.common;

import com.isesol.arch.common.utils.RegexUtils;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RegexUtilsTest {

    @Test
    public void testEmail() {
        assertFalse(RegexUtils.validEmail("ssss"));
        assertTrue(RegexUtils.validEmail("sss@qq.com"));
        assertTrue(RegexUtils.validEmail("sss@vip.qq.com"));
    }

}
