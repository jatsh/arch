package com.isesol.orm.jpa;

import com.google.common.collect.*;
import org.springframework.data.jpa.domain.support.*;

import javax.persistence.*;
import java.io.*;
import java.util.*;

@MappedSuperclass
@EntityListeners({AuditingEntityListener.class})
public class BaseIdEntity<EIT> implements Serializable {

    protected EIT id;
    protected String jqId;
    protected Map<String, Object> properties = Maps.newHashMap();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public EIT getId() {
        return id;
    }

    public void setId(EIT id) {
        this.id = id;
    }

    public void setJqId(String jqId) {
        this.jqId = jqId;
    }

    /**
     * 获取附加属性
     *
     * @return
     */
    @Transient
    public Map<String, Object> getProperties() {
        return properties;
    }

    @Transient
    public Object getProperty(String key) {
        return properties.get(key);
    }

    public void addProperty(String key, Object value) {
        properties.put(key, value);
    }

}
