package com.isesol.orm.jpa;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface JpaDao<T extends BaseIdEntity<EIT>, EIT extends Serializable> extends
        PagingAndSortingRepository<T, EIT>, JpaSpecificationExecutor<T> {
}