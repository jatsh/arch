package com.isesol.orm.jpa.query;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 查询过滤器
 *
 */
public class SearchFilter {

    /**
     * { oper:'eq', text:'等于'},
     * { oper:'ne', text:'不等'},
     * { oper:'lt', text:'小于'},
     * { oper:'le', text:'小于等于'},
     * { oper:'gt', text:'大于'},
     * { oper:'ge', text:'大于等于'},
     * { oper:'bw', text:'开始于'},
     * { oper:'bn', text:'不开始于'},
     * { oper:'in', text:'属于'},
     * { oper:'ni', text:'不属于'},
     * { oper:'ew', text:'结束于'},
     * { oper:'en', text:'不结束于'},
     * { oper:'cn', text:'包含'},
     * { oper:'nc', text:'不包含'},
     * { oper:'nu', text:'不存在'},
     * { oper:'nn', text:'存在'}
     */
    public enum Operator {
        EQ, NE, LT, LE, GT, GE, BW, BN, IN, NI, EW, EN, CN, NC, NU, NN, NULL
    }

    public String fieldName;
    public Object value;
    public Operator operator;

    public SearchFilter(String fieldName, Operator operator, Object value) {
        this.fieldName = fieldName;
        this.value = value;
        this.operator = operator;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("fieldName", fieldName)
                .append("value", value)
                .append("operator", operator)
                .toString();
    }
}
