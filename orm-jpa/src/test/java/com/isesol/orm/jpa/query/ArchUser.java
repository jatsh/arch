package com.isesol.orm.jpa.query;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.isesol.orm.jpa.IdEntity;

@Entity
@Table(name = "ARCH_USER")
public class ArchUser extends IdEntity<Integer> {

    private String name;
    private Float salary;

    private String homeAddress;

    public String getHomeAddress() {

        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {

        this.homeAddress = homeAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }
}
