package com.isesol.orm.jpa.query;

import com.isesol.orm.jpa.JpaDao;
import org.springframework.stereotype.*;

@Repository
public interface ArchUserDao extends JpaDao<ArchUser, Integer> {
}
