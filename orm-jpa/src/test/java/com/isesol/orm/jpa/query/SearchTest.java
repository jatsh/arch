package com.isesol.orm.jpa.query;

import java.util.List;

import com.google.common.collect.Lists;
import org.junit.Test;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * 查询支持测试
 */
@DirtiesContext
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class SearchTest extends SpringTransactionalTestCase {

    @Resource
    ArchUserDao archUserDao;

    @Test
    public void testDao() {
        assertNotNull(archUserDao);
    }

    @Test
    public void test_EQ() {
        SearchFilter filter = new SearchFilter("name", SearchFilter.Operator.EQ, "张三");
        Specification<ArchUser> specification = DynamicSpecifications.bySearchFilter(Lists.newArrayList(filter), "AND");
        List<ArchUser> users = archUserDao.findAll(specification);
        assertEquals(1, users.size());

        // 测试CN自动转换为EQ
        new SearchFilter("age", SearchFilter.Operator.CN, "27");
        specification = DynamicSpecifications.bySearchFilter(Lists.newArrayList(filter), "AND");
        users = archUserDao.findAll(specification);
        assertEquals(1, users.size());
    }

    @Test
    public void test_CN() {
        SearchFilter filter = new SearchFilter("name", SearchFilter.Operator.CN, "三");
        Specification<ArchUser> specification = DynamicSpecifications.bySearchFilter(Lists.newArrayList(filter), "AND");
        List<ArchUser> users = archUserDao.findAll(specification);
        assertEquals(1, users.size());

        filter = new SearchFilter("name", SearchFilter.Operator.CN, "张");
        specification = DynamicSpecifications.bySearchFilter(Lists.newArrayList(filter), "AND");
        users = archUserDao.findAll(specification);
        assertEquals(1, users.size());
    }

}
