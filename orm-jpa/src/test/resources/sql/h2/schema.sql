create table ARCH_USER
(
  ID                   int(3) not null auto_increment comment '主键ID',
  NAME                 varchar(100) not null comment '姓名',
  AGE                   int(2) comment '年龄',
  SALARY                numeric comment '月薪',
  HOME_ADDRESS    varchar(100) comment '家庭地址',
  primary key (ID)
);