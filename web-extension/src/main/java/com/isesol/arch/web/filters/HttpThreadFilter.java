package com.isesol.arch.web.filters;

import com.isesol.arch.web.utils.web.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * Servlet Filter implementation class HttpThreadFilter
 */
public class HttpThreadFilter implements Filter {

    /**
     * Default constructor. 
     */
    public HttpThreadFilter() {
    }

	/**
     * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpThreadContextHolder.setHttpRequest((HttpServletRequest) request);
		HttpThreadContextHolder.setHttpResponse((HttpServletResponse) response);
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}

}