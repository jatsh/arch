package com.isesol.arch.web.processor;

import com.isesol.arch.common.utils.*;
import com.isesol.arch.web.*;
import org.apache.commons.lang3.*;
import org.apache.logging.log4j.*;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.config.*;
import org.apache.logging.log4j.core.layout.*;
import org.apache.logging.log4j.flume.appender.*;
import org.slf4j.Logger;
import org.slf4j.*;
import org.springframework.beans.factory.config.*;
import org.springframework.stereotype.*;

import javax.servlet.*;
import java.io.*;
import java.net.*;

/**
 * Flume log Processor
 *
 * @author Peter Zhang
 */
@Component
public class FlumeProcessor extends InstantiationAwareBeanPostProcessorAdapter implements ServletContextListener {

	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	private static final String FLUME_LOG_NAME = "flume";

	private FlumeAppender flumeAppender = null;

	public FlumeProcessor() {

		ArchListener.addListener(this);
	}

	public boolean checkFlumeAgent(String node, int port, int timeout) {

		boolean available = false;
		Socket s = null;
		String reason = null;

		try {

			s = new Socket();
			s.setReuseAddress(true);
			SocketAddress sa = new InetSocketAddress(node, port);
			s.connect(sa, timeout * 1000);

		} catch (IOException e) {

			if (e.getMessage().equals("Connection refused")) {

				reason = "port " + port + " on " + node + " is closed.";
			}

			if (e instanceof UnknownHostException) {

				reason = "node " + node + " is unresolved.";
			}

			if (e instanceof SocketTimeoutException) {

				reason = "timeout while attempting to reach node " + node + " on port " + port;
			}

		} finally {

			if (s != null) {

				if (s.isConnected()) {

					available = true;

				} else {

					LOGGER.info("flume Port " + port + " on " + node + " is not reachable; reason: " + reason);
				}

				try {

					s.close();

				} catch (IOException e) {

				}
			}

		}

		return available;
	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {

		String serviceType = PropertyFileUtil.get("system.serviceType");

		String serviceName = PropertyFileUtil.get("system.serviceName");

		String flumeHost = PropertyFileUtil.get("flume.host");

		String flumePort = PropertyFileUtil.get("flume.port");

		try {

			if (StringUtils.isNotBlank(flumeHost)
					&& StringUtils.isNotBlank(flumePort)
					&& StringUtils.isNotBlank(serviceType)
					&& StringUtils.isNotBlank(serviceName)) {

				if (checkFlumeAgent(flumeHost, Integer.parseInt(flumePort), 1)) {

					LOGGER.info("FlumeProcessor init");

					StringBuffer sb = new StringBuffer();
					sb.append("${hostName} ");
					sb.append(serviceType);
					sb.append(" ");
					sb.append(serviceName);
					sb.append(" ");
					//sb.append("[%d{yyyy/MM/dd HH:mm:ss}]");
					//sb.append(" ");
					sb.append("[%t] %p %C.%M(%F:%L) | %m%n");

					LoggerContext ctx = (LoggerContext) LogManager.getContext(false);

					Configuration config = ctx.getConfiguration();

					//  PatternLayout layout = PatternLayout.createLayout(sb.toString(), config, null, Charset.defaultCharset(), true, true, null,null);

					PatternLayout  layout=null;
					Agent flumeAgent = Agent.createAgent(flumeHost, flumePort);

					flumeAppender = FlumeAppender.createAppender(new Agent[]{flumeAgent}, new Property[]{}, "false", null, null,
							null, null, null, null, FLUME_LOG_NAME, null, null, null, null, null, null, "false", null, null, null, layout, null);

					flumeAppender.start();

					config.addAppender(flumeAppender);

					LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);

					loggerConfig.addAppender(flumeAppender, null, null);

					ctx.updateLoggers();

					LOGGER.info("flumeAppender started," +
									"flumeHost={},flumePort={},serviceType={},serviceName={}",
							new Object[]{flumeHost, flumePort, serviceType, serviceName});
				}

			} else {

				LOGGER.info("flumeHost/flumePort/serviceType/serviceName is null,ignore flume appender init");
			}

		} catch (Exception e) {

			LOGGER.info("flume init error", e);

			if (null != flumeAppender && flumeAppender.isStarted()) {

				flumeAppender.stop();
			}
		}

	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {

		if (null != flumeAppender && flumeAppender.isStarted()) {

			LOGGER.info("flumeAppender stopped");

			flumeAppender.stop();

		}

	}

}
