package com.isesol.arch.web.processor;

import com.alibaba.fastjson.*;
import com.alibaba.fastjson.serializer.*;
import com.google.common.collect.*;
import com.isesol.arch.common.utils.*;
import com.isesol.arch.web.*;
import org.apache.commons.lang3.*;
import org.apache.curator.framework.*;
import org.apache.curator.retry.*;
import org.apache.zookeeper.*;
import org.slf4j.*;
import org.springframework.beans.factory.config.*;
import org.springframework.stereotype.*;

import javax.management.*;
import javax.servlet.*;
import java.lang.management.*;
import java.lang.reflect.*;
import java.net.*;
import java.util.*;

/**
 * 系统健康监控Listener
 *
 * @author Peter Zhang
 */
@Component
public class MonitorAnnotationBeanPostProcessor extends InstantiationAwareBeanPostProcessorAdapter implements ServletContextListener{

	private Logger LOGGER = LoggerFactory.getLogger(getClass());

	private CuratorFramework client;

	static String appInfoStr = "";

	static String path = "";

	public MonitorAnnotationBeanPostProcessor(){

		super();

		String ZkConnectString = PropertyFileUtil.get("monitor.zookeeperAddress");

		if (StringUtils.isBlank(ZkConnectString)){

			LOGGER.warn("【XmsjMonitor】ZkConnectString is null");

		} else{

			LOGGER.info("【XmsjMonitor】ZkConnectString={}", ZkConnectString);

			CuratorFrameworkFactory.Builder builder = CuratorFrameworkFactory.builder()
					.connectString(ZkConnectString)
					.retryPolicy(new RetryNTimes(Integer.MAX_VALUE, 1000))
					.connectionTimeoutMs(5000)
					.namespace("xmsj");

			client = builder.build();

			client.start();

		}

		ArchListener.addListener(this);
	}

	@Override
	public void contextInitialized(ServletContextEvent sce){

		try{

			String serviceName = PropertyFileUtil.get("system.serviceName");
			String serviceType = PropertyFileUtil.get("system.serviceType");

			LOGGER.info("【XmsjMonitor】serviceType={},serviceName={}", serviceType, serviceName);

			if (StringUtils.isNoneBlank(serviceType) && StringUtils.isNoneBlank(serviceName) && client != null){

				String parentPath = "/monitor/" + serviceType;
				client.newNamespaceAwareEnsurePath(parentPath).ensure(client.getZookeeperClient());

				MBeanServer     mbs  = ManagementFactory.getPlatformMBeanServer();
				Set<ObjectName> objs = null;

				objs = mbs.queryNames(new ObjectName("*:type=Connector,*"),
				                      Query.match(Query.attr("protocol"), Query.value("HTTP/1.1")));
				String        hostname  = InetAddress.getLocalHost().getHostName();
				InetAddress[] addresses = InetAddress.getAllByName(hostname);
				//				ArrayList<String> endPoints = new ArrayList<>();
				String ep   = "";
				String ip   = "";
				String port = "";
				for (Iterator<ObjectName> i = objs.iterator(); i.hasNext(); ){
					ObjectName obj = i.next();
					//String scheme = mbs.getAttribute(obj, "scheme").toString();
					port = obj.getKeyProperty("port");
					for (InetAddress addr : addresses){

						if (addr.isLoopbackAddress()){
							continue;
						}

						if (addr instanceof Inet6Address){

							continue;
						}

						ip = addr.getHostAddress();
						//	String ep = scheme + "://" + host + ":" + port;
						ep = ip + ":" + port;
						break;
						//	endPoints.add(ep);
					}
				}
				path = parentPath + "/" + serviceName + "/" + ep;
				Map appInfoMap = Maps.newHashMap();
				appInfoMap.put("hostName", hostname);
				appInfoMap.put("ip", ip);
				appInfoMap.put("port", port);
				appInfoMap.put("serviceName", serviceName);
				appInfoMap.put("serviceType", serviceType);
				appInfoMap.put("systemVersion", PropertyFileUtil.get("system.version"));

				appInfoStr = JSON.toJSONString(appInfoMap, SerializerFeature.WriteClassName);

				LOGGER.info("【XmsjMonitor】appInfo={}", appInfoStr);

				if (null == client.checkExists().forPath(path)){

					this.client.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL).forPath(path, appInfoStr.getBytes());
					MonitorConnectionStateListener stateListener = new MonitorConnectionStateListener(path, appInfoStr);
					client.getConnectionStateListenable().addListener(stateListener);
				}

			} else{

				LOGGER.warn("【XmsjMonitor】serviceType/serviceName is null");
			}

		} catch (Exception e){

			LOGGER.warn("【XmsjMonitor】error", e);
		}

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce){

		try{

			Class protocolConfig = Class.forName("com.alibaba.dubbo.config.ProtocolConfig");

			Method method = protocolConfig.getMethod("destroyAll");
			method.invoke(protocolConfig);

			if (StringUtils.isNotBlank(path)){

				LOGGER.warn("【XmsjMonitor】contextDestroyed {}", path);

				if (null != client.checkExists().forPath(path)){

					client.delete().forPath(path);
				}

			}

		} catch (Exception e){

			LOGGER.warn("【XmsjMonitor】contextDestroyed error", e);
		}

	}

}