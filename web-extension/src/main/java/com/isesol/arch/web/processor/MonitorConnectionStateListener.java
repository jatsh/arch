package com.isesol.arch.web.processor;


import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.zookeeper.CreateMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MonitorConnectionStateListener implements ConnectionStateListener {

	private Logger LOGGER = LoggerFactory.getLogger(getClass());

	private String zkRegPathPrefix;

	private String regContent;

	public MonitorConnectionStateListener(String zkRegPathPrefix, String regContent) {

		this.zkRegPathPrefix = zkRegPathPrefix;

		this.regContent = regContent;

	}

	@Override
	public void stateChanged(CuratorFramework curatorFramework, ConnectionState connectionState) {

		try {

			Thread.sleep(5000L);

			if (connectionState == ConnectionState.LOST) {

				while ( true ) {

					if (curatorFramework.getZookeeperClient().blockUntilConnectedOrTimedOut()) {

						if (null == curatorFramework.checkExists().forPath(zkRegPathPrefix)) {

							curatorFramework.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL)
									.forPath(zkRegPathPrefix, regContent.getBytes("UTF-8"));

						}
						break;
					}

				}
			}
		} catch ( Exception e ) {

			LOGGER.error("【MonitorConnectionStateListener】", e);
		}

	}
}
