package com.isesol.arch.web.utils.web;

import javax.servlet.http.*;
import java.util.*;

/**
 * http[request|response]池
 */
public class HttpThreadContextHolder{

	private static ThreadLocal<HttpServletRequest>  httpRequestThreadLocalHolder  = new ThreadLocal<>();
	private static ThreadLocal<HttpServletResponse> httpResponseThreadLocalHolder = new ThreadLocal<>();
	private static ThreadLocal<Map<String, Object>> attrsThreadLocalHolder        = new ThreadLocal<>();

	static{
		attrsThreadLocalHolder.set(new HashMap<String, Object>());
	}

	public static void setHttpRequest(HttpServletRequest request){

		httpRequestThreadLocalHolder.set(request);
	}

	public static HttpServletRequest getHttpRequest(){

		return httpRequestThreadLocalHolder.get();
	}

	public static HttpSession getHttpSession(){

		return getHttpRequest().getSession();
	}

	public static void setHttpResponse(HttpServletResponse response){

		httpResponseThreadLocalHolder.set(response);
	}

	public static HttpServletResponse getHttpResponse(){

		return httpResponseThreadLocalHolder.get();
	}

	public static void addAttr(String key, Object object){

		attrsThreadLocalHolder.get().put(key, object);
	}

	public static Object getAttr(String key){

		return attrsThreadLocalHolder.get().get(key);
	}
}